#ifndef	_UTILS_H
#define	_UTILS_H

inline __u8 hex2val(char ch);
inline __u8 LRC(__u8 *str, __u16 len);
inline __u16 calc_crc_ccitt(const unsigned char *buf, int cnt);
inline int check_crc_ccitt(const unsigned char *buf, int cnt);
inline void append_crc_ccitt(unsigned char *buffer, int len);
inline __u16 CRC16(__u8 *puchMsg, int usDataLen);

#endif		// _UTILS_H
