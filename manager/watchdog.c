#include "watchdog.h"

#include <stdio.h>
#include <dirent.h>

//#define DEBUG

static int isdigits(char *str)
{
	int i;
	for (i = 0; i < strlen(str); i++)
	{
		if (isdigit(str[i])==0)
			return 0;
	}
	return 1;
}

static int proc_parser(char *proc_file, char *cmd_file, struct proc_dat *proc_dat)
{
	FILE *fp,*c_fp;
	char buf[512], c_buf[128], str[32];
	char *name;
	int i, j, st;

	fp = fopen(proc_file, "r");
	if (fp == NULL){
		perror("error : ");
		return (0);
	}
	c_fp = fopen(cmd_file, "r");
	if( c_fp == NULL){
		perror("error : ");
		fclose(fp);
		return (0);
	}

	fgets(buf, 511, fp);
	fgets(c_buf, 127, c_fp);

	name = (char *)strtok(c_buf,"#");
	if( name == NULL) 
		strcpy(proc_dat->name, "unknown");
	else
		strncpy(proc_dat->name, name, 15);

	for(i=0,j=0,st=0; i<strlen(buf); i++){
		if(buf[i] == ' '){ j++;st=0;
		} else if(j==1){		// name
			if(buf[i]=='('){j=j;
			} else if(buf[i]==')'){
				str[st]='\0';
		//		strncpy(proc_dat->name, str, 15);
			}else{
				str[st++] = buf[i];
			}
		} else if(j==0){
			str[st++] = buf[i];
			if(buf[i+1] == ' '){str[st]= '\0';proc_dat->pid = atol(str);}
		} else if(j==2){
			str[st++] = buf[i];
			if(buf[i+1] == ' '){str[st]= '\0';proc_dat->stat = buf[i];}
		} else if(j==13){
			str[st++] = buf[i];
			if(buf[i+1] == ' '){str[st]= '\0';	proc_dat->utime = atol(str);}
		} else if(j==14){
			str[st++] = buf[i];
			if(buf[i+1] == ' '){str[st]= '\0';	proc_dat->stime = atol(str);}
		} else if(j==21){
			str[st++] = buf[i];
			if(buf[i+1] == ' '){str[st]= '\0';	proc_dat->start_time = atol(str);}
		} else{
		}
	}

	fclose(fp);
	fclose(c_fp);

	return 1;
}

int make_proc_info(struct proc_dat proc_dat[], int max_proc_dat)
{
	DIR *directory;
	struct dirent *entry = NULL;
	char proc_file[40], cmd_file[40];
	int proc_no = 0;

	if ((directory = opendir("/proc")) == NULL){
		perror("opendir error :");
		return -1;
	}

	while((entry = readdir(directory)) != NULL){
		if (strcmp(entry->d_name, ".") !=0 && strcmp(entry->d_name, "..") != 0){
			sprintf(proc_file,"/proc/%s/stat", entry->d_name);
			sprintf(cmd_file,"/proc/%s/cmdline", entry->d_name);
			if (access(proc_file, F_OK) != 0)		continue;
			if (access(cmd_file, F_OK) != 0)		continue;
			if (isdigits(entry->d_name))			proc_no += proc_parser(proc_file,cmd_file,&proc_dat[proc_no]);
			if(proc_no == (max_proc_dat-1))		break;
		}
	}

	closedir(directory);

	return proc_no;
}

pid_t search_app(struct proc_dat *proc_dat, int proc_no, char *app_name)
{
	int i;
	pid_t app_pid = 0;
#ifdef DEBUG
	printf("search app : %d\n", proc_no);
#endif
	for(i=0; i<proc_no; i++, proc_dat++){
#ifdef DEBUG
		printf("%d, %s - %s\n", i, proc_dat->name, app_name);
#endif
		if(strcmp(proc_dat->name, app_name)==0){
			app_pid = proc_dat->pid;
			break;
		}
	}
	
#ifdef DEBUG
	printf("App Pid = %d\n\n", app_pid);
#endif
	return app_pid;
}

