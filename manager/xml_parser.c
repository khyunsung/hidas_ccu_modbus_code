/**
 * section: xmlReader
 * synopsis: Parse an XML file with an xmlReader
 * purpose: Demonstrate the use of xmlReaderForFile() to parse an XML file
 *          and dump the informations about the nodes found in the process.
 *          (Note that the XMLReader functions require libxml2 version later
 *          than 2.6.)
 * usage: reader1 <filename>
 * test: reader1 test2.xml > reader1.tmp && diff reader1.tmp $(srcdir)/reader1.res
 * author: Daniel Veillard
 * copy: see Copyright for the status of this software.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <libxml/xmlreader.h>

#ifdef LIBXML_READER_ENABLED

/* XML Info Variables Definition */
typedef struct {

        int address;
        float value;

        int data_table;
        int times;
        int len;
        int max;
        int min;

	xmlChar key_name[300];

} XML_INFO;

int Xml_AllTagNum = 0;
XML_INFO Xml_Tag_Info[3000]; 

/**
 * processNode:
 * @reader: the xmlReader
 *
 * Dump information about the current node
 */
static void
processNode(xmlTextReaderPtr reader) {
//   const xmlChar *name, *value;
    xmlChar *tmp_ptr1, *tmp_ptr2, *tmp_ptr3, *tmp_ptr4, *tmp_ptr5, *tmp_ptr6, *tmp_ptr7;
    xmlChar str[20];

/*
    name = xmlTextReaderConstName(reader);
    if (name == NULL)
	name = BAD_CAST "--";

    value = xmlTextReaderConstValue(reader);
*/

    if(xmlTextReaderDepth(reader) == 3 && xmlTextReaderNodeType(reader) == 1) {
        //Xml_Tag_Info[Xml_AllTagNum].address = atoi(xmlTextReaderGetAttribute(reader, "offset"));
        //strcpy((char*)(Xml_Tag_Info[Xml_AllTagNum].key_name), (char*)(xmlTextReaderGetAttribute(reader, "redisKey")), 5);
        //printf("%d", atoi(xmlTextReaderGetAttribute(reader, "offset")));
        tmp_ptr1 = xmlTextReaderGetAttribute (reader, "offset");
        tmp_ptr2 = xmlTextReaderGetAttribute (reader, "redisKey");

	//ex. data_table="1" times="10" len="1" max="300.0" min="100.0"
	tmp_ptr3 = xmlTextReaderGetAttribute (reader, "data_table");
	tmp_ptr4 = xmlTextReaderGetAttribute (reader, "times");
	tmp_ptr5 = xmlTextReaderGetAttribute (reader, "len");
	tmp_ptr6 = xmlTextReaderGetAttribute (reader, "max");
	tmp_ptr7 = xmlTextReaderGetAttribute (reader, "min"); 

        sprintf(Xml_Tag_Info[Xml_AllTagNum].key_name, "%s", tmp_ptr2);

        sprintf(str, "%s", tmp_ptr1); Xml_Tag_Info[Xml_AllTagNum].address = atoi(str);
        sprintf(str, "%s", tmp_ptr3); Xml_Tag_Info[Xml_AllTagNum].data_table = atoi(str);
        sprintf(str, "%s", tmp_ptr4); Xml_Tag_Info[Xml_AllTagNum].times = atoi(str);
        sprintf(str, "%s", tmp_ptr5); Xml_Tag_Info[Xml_AllTagNum].len = atoi(str);
        sprintf(str, "%s", tmp_ptr6); Xml_Tag_Info[Xml_AllTagNum].max = atoi(str);
        sprintf(str, "%s", tmp_ptr7); Xml_Tag_Info[Xml_AllTagNum].min = atoi(str); 

	Xml_AllTagNum++;
printf("Xml_AllTagNum: %d\n", Xml_AllTagNum);
    }
/*
    if (value == NULL)
	;//printf("\n");
    else {
        if (xmlStrlen(value) > 40)
            printf(" %.40s...\n", value);
        else
	    printf(" %s\n", value);
    }
*/
}

/**
 * streamFile:
 * @filename: the file name to parse
 *
 * Parse and print information about an XML file.
 */
static void
streamFile(const char *filename) {
    xmlTextReaderPtr reader;
    int ret;

    reader = xmlReaderForFile(filename, NULL, 0);
    if (reader != NULL) {
        ret = xmlTextReaderRead(reader);
        while (ret == 1) {
            processNode(reader);
            ret = xmlTextReaderRead(reader);
        }
        xmlFreeTextReader(reader);
        if (ret != 0) {
            fprintf(stderr, "%s : failed to parse\n", filename);
        }
    } else {
        fprintf(stderr, "Unable to open %s\n", filename);
    }
}

//int mmain(int argc, char **argv) {
int xml_address_data_init(void) {
    int i;

    /*
     * this initialize the library and check potential ABI mismatches
     * between the version it was compiled for and the actual shared
     * library used.
     */
    LIBXML_TEST_VERSION

    streamFile("./address_data_conf.xml");

    for(i = 0; i < Xml_AllTagNum; i++)
        printf("%d)  %d, %s\n", i, Xml_Tag_Info[i].address, Xml_Tag_Info[i].key_name);
    /*
     * Cleanup function for the XML library.
     */
    xmlCleanupParser();
    /*
     * this is to debug memory for regression tests
     */
    xmlMemoryDump();
    return(0);
}

#else
int xmain(void) {
    fprintf(stderr, "XInclude support not compiled in\n");
    exit(1);
}
#endif
