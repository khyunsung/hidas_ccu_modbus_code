/*
** project  : ACONIS-DS IOM
** filename : card_info.c
** version  : 1.0.0
** date     : 2009.10.12
** Copyright HHI
***************************
Version history
---------------------------
Version     : 1.0.0
date        : 2009.10.12
revised by  : Jeong Min Sik
description : 
---------------------------
***************************
*/


/*
*******************************************************************************
*                            INCLUDE FILES
*******************************************************************************
*/
#include "card_info.h"
/*
*******************************************************************************
*                            	CONSTANTS
*******************************************************************************
*/

/*
*******************************************************************************
*                            Global VARIABLES
*******************************************************************************
*/
tCardInfo				CardInfo;
tLED				led;
/******************************************************************************
		Function
******************************************************************************/
