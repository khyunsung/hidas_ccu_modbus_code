#ifndef	__ULED_H
#define	__ULED_H

#define UART1_NORMAL	0x01
#define UART1_ERROR	0xfe
#define UART2_NORMAL	0x02
#define UART2_ERROR	0xfd
#define UART3_NORMAL	0x04
#define UART3_ERROR	0xfb
#define UART4_NORMAL	0x08
#define UART4_ERROR	0xf7
#define UART5_NORMAL	0x10
#define UART5_ERROR	0xef
#define UART6_NORMAL	0x20
#define UART6_ERROR	0xdf
#define UART7_NORMAL	0x40
#define UART7_ERROR	0xbf
#define UART8_NORMAL	0x80
#define UART8_ERROR	0x7f
#define UART_NORMAL	0xff
#define UART_ERROR	0x00

int rled_init(void);
int rled_on(__u8 port);
int rled_off(__u8 port);

#endif		// __ULED_H
