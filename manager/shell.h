#ifndef	_SHELL_H
#define	_SHELL_H

#define	MAX_ARGS			32
#define MAX_DEBUG_CONNECT	100
#define MAX_SHELL_STRING	0x10000	//
#define MAX_SHBUFF	2048

#define	CMDF_REFRESH	0x01
#define	CMDF_DEBUG		0x02
#define	CMDF_HIDDEN		0x04
#define	CMDF_FILE		0x08
#define	CMDF_MIME		0x10
#define	CMDF_NOCACHE	0x20

struct command{
	char *cmd;
	int	(*func)(int sfd, int argc, char **argv, char *str, char type);
	char *usage;
	char *help;
	char flag;
	time_t masic;
};

#define shprintf(format, args...) \
	do{\
	ret = snprintf(shbuff, MAX_SHBUFF, format, ## args);	\
	ret = write(sfd, shbuff, ret);	\
	} while(0)

void change_shell_masic(void);

int OpenDebug(void);
void *ShellThread(void *);

#define FALSE	0
#define	TRUE	1

#endif		// _SHELL_H
