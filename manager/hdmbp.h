#ifndef	_HDMBP_H
#define	_HDMBP_H

//#define RTU_HICM860S	37
#define HDCMD_IO_DATA	0x8d		// SYSTEM ACQ & OUT_DATA COMMAND
#define	RECV_BUFF_SIZE		2048

struct hdbus{
	__u8	dst;
	__u8	src;
	__u8	cmd;
	__u8	excp;
};

struct hdlc_status_struct{
	int		recv_size, send_size;
	__u8	recv_buff[RECV_BUFF_SIZE];
	__u8	send_buff[RECV_BUFF_SIZE];
};

#pragma pack(1)
struct hdmb_data{
	__u8	id;
	__u32	data;
};

struct hdmb_data2{
	__u16	id;
	__u32 	data;
};
#pragma pack()

struct hdlc_map_struct{
	__u16	base;
	__u16	size;
	__u8	inf;
};

void hdmbp_thread(void *arg);

#endif		// _HDMBP_H
