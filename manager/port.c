/***********************************************************************************
  Filename:     port.c

  Description:  it is related with communication between hiscm_pm and upper side program.
                In case of COM card, these program communicate by use CAN
				and UDP socket in case of MPM card.

***********************************************************************************/

/***********************************************************************************
* INCLUDES
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <pthread.h>

#include <asm/types.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>

#include "../include/common.h"
#ifdef COM
#include "../include/hicsr.h"
#endif
#include "port.h"
#include "modbus.h"
#include "uled.h"
#include "utils.h"
#include "log.h"
#ifndef COM
#include "xml_parser.h"
#include "hiredis.h"
#endif

/***********************************************************************************
* CONSTANTS AND DEFINES
*/

//#define	MODBUS_ADDRESS	16999

#define	DEBUG
#ifndef	DEBUG
	#define	dprintf(format, args...) log_access(format, ## args)
#else
	#define	dprintf(format, args...) log_access(format, ## args); printf(format, ## args)
#endif	// DEBUG

#define PORT_CHECK
#define TCP_NOTICE
//#define RED_LED

#define BASE	3000

/***********************************************************************************
* GLOBAL VARIABLES
*/
extern int port_tx_cnt[MAX_INTERFACE+1], port_rx_cnt[MAX_INTERFACE+1], port_thr_cnt[MAX_INTERFACE+1];
extern __u8  port_alive[MAX_INTERFACE+1];

extern PORT_INFO PORT_info[MAX_INTERFACE+1];
INFS_STATUS *INFS_st;

#ifdef COM
extern IO_DATA io_data[MAX_INTERFACE+1];
#else
extern INF_MEM INF_mem[MAX_INTERFACE+1];
#endif
extern pthread_mutex_t mutex_mem, mutex_cmd;

#ifdef COM
TX_QUERY_INFO	tx_queue_info[MAX_INTERFACE+1]; 	//

extern HICSR_REG hicsr_reg;
extern HICSR_ATTR hicsr_attr;
extern unsigned char myID;
#endif

#ifndef COM
extern XML_INFO Xml_Tag_Info[3000]; 
extern int xml_address_data_init(void);
extern int Xml_AllTagNum;
extern redisContext *Redis_Ptr;
//extern void redis_connect(redisContext *);
//extern void redis_connect(void);
extern int redis_test(void);
#endif


/***********************************************************************************
* LOCAL VARIABLES
*/
char write_bflag[MAX_INTERFACE];

/***********************************************************************************
* LOCAL FUNCTIONS
*/

/***********************************************************************************
* @fn          open_link
*
* @brief       it is to open UDP socket. That socket is used to communicate between 
			   hiscm_pm and upper side program. 
			   the information of socket is 
			   1. IP : INADDR_ANY
			   2. PORT : BASE(3000 + port_num)
			   3. AF : AF_INET(IPv4)
			   4. PROTOCOL : SOCK_DGRAM(it must be used in UDP socket in IPv4 and IPv6.
*
* @param       port_num - port number variable. 
*             
* @return      sockfd - file description of UDP socket.
*/
static int open_link(int port_num)
{
	int sockfd;
	struct sockaddr_in server;
	int val, result;

	sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);	
	if(sockfd == -1){
		dprintf("[Port %d] socket open error!!!\n",port_num);
		sleep(1);
		return -1;
	}

	memset( (char*)&server, 0, sizeof (server) );
	server.sin_addr.s_addr = htonl(INADDR_ANY);
	server.sin_family = AF_INET;
	server.sin_port = htons(BASE+port_num);

	result = bind(sockfd, (struct sockaddr*)&server, sizeof(server));
	if(result == -1)
	{
		dprintf("[Port %d] bind retry...\n",port_num);
		result = bind(sockfd, (struct sockaddr*)&server, sizeof(server));
		if(result == -1)
		{
			dprintf("[Port %d] bind error\n",port_num);
			sleep(1);
			return -1;
		}
	}
	else
	{
		dprintf("[Port %d Socket %d] bind success...\n",port_num,server.sin_port);	   
	}
	
//	val |= FD_CLOEXEC;
//	fcntl(sockfd,F_SETFD,val);
	fcntl(sockfd,F_SETFD,FD_CLOEXEC);

	return sockfd;
}

static int port_read(__u16 port, __u8 *buff, int len, struct sockaddr_in *interface)
{
	int addrlen;
	if( port <= 0 ) return -1;

	addrlen = sizeof(struct sockaddr_in);
	return recvfrom(port, buff, len, 0, (struct sockaddr *)interface, &addrlen);
}

static int port_write(__u16 port, __u8 *buff, int len, int port_num, struct sockaddr_in *interface)
{
	int i;
	if( port <= 0 ) return -1;

	if(sendto(port, buff, len, 0, (struct sockaddr *)interface, sizeof(struct sockaddr_in)) == len)
	{
#ifdef DEBUG
dprintf("[PORT %d] DOWN [",port_num);
for(i=0;i<len;i++) { dprintf("%02X|",buff[i]); }
dprintf("]\n");
#endif
		return 1;
	}
	return -1;
}

static int cmd_write(__u16 port, __u8 *buff, int len, int port_num)
{
	struct sockaddr_in cmd_interface;
	unsigned short infPort = BASE+BASE+port_num;
	char infIP[32]="127.0.0.1";
	int i;

	if( port <= 0 ) return -1;

	memset(&cmd_interface,0,sizeof(cmd_interface));
	cmd_interface.sin_family=AF_INET;
	cmd_interface.sin_addr.s_addr = inet_addr(infIP);
	cmd_interface.sin_port = htons(infPort);

	if(sendto(port,buff,len,0,(struct sockaddr *)&cmd_interface,sizeof(cmd_interface))
		== len)
	{
#ifdef DEBUG
dprintf("[PORT %d] CMD [",port_num);
for(i=0;i<len;i++) { dprintf("%02X|",buff[i]); }
dprintf("]\n");
#endif
		return 1;
	}
	dprintf("UDP SendTo Error[pm[%d]<->interface] \n",BASE+BASE+port_num);
	return -1;
}

#ifdef COM
static void dequeue_txdata(int port_num, char mode)
{
	char head, tail;

	head = tx_queue_info[port_num].head;
	tail = tx_queue_info[port_num].tail;
	if( head == tail ) return;

	tx_queue_info[port_num].head++;
	tx_queue_info[port_num].head %= MAX_NUM_TX_QUERY;
}
#endif
static void dequeue_cmd(int port_num, char mode)
{
	char head, tail;

	head = PORT_info[port_num-1].write_head;
	tail = PORT_info[port_num-1].write_tail;
	if( head == tail ) return;

	PORT_info[port_num-1].write_head++;
	PORT_info[port_num-1].write_head %= MAX_WRITE_BUFF;

#if 0
	dprintf("[CMD queue %d] dequeue %s (%d-%d)\n",port_num,
		(mode > 0 )?"success":"with failure",
		PORT_info[port_num-1].write_head,tail);
#endif
}

static void clear_cmd(int port_num)
{
pthread_mutex_lock(&mutex_cmd);
	PORT_info[port_num-1].write_head = 0;
	PORT_info[port_num-1].write_tail = 0;
pthread_mutex_unlock(&mutex_cmd);

	dprintf("[CMD queue %d] clear all (%d-%d)\n",port_num,
	   	PORT_info[port_num-1].write_head,PORT_info[port_num-1].write_tail);
}

static void port_handle(int sock, int port_num)
{
	char rzbuff[DEFAULT_BUFF_SIZE];
	char sdbuff[DEFAULT_BUFF_SIZE];
	int i, len;
	__u8 fcode;
	__u16 address, byte_count;
	__u16 crc,r_crc;
	modbus_res_hdr_ext *pres, *wres;

	
	struct sockaddr_in interface;
	char head, tail;

	len = port_read(sock, rzbuff, DEFAULT_BUFF_SIZE-16, &interface);
	if(len < 5) return;
	if(len >= DEFAULT_BUFF_SIZE-16) return;

	port_thr_cnt[port_num]++;
	port_rx_cnt[port_num]++;

	crc = CRC16(rzbuff,len-2)&0xFFFF;

#ifdef DEBUG
if(port_num == 1)
{
printf("22[PORT %d] ==> ",port_num);
for(i=0;i<len;i++) { printf("%02X|",rzbuff[i] & 0xff); }
printf("\n");
printf(": 0x%02X 0x%02X\n",crc>>8,crc&0x00FF);
}
#endif
	
	pres = (modbus_res_hdr_ext *)rzbuff;

/*khs	r_crc = (rzbuff[len-2]<<8)|rzbuff[len-1];
	if(crc!=r_crc){
		dprintf("[Port %d] Comm CRC error!\n",port_num);
		return;
	}*/

	if(pres->unit_id != port_num)
	{
		dprintf("[Port %d]Slave address [%d] mismatch!\n",port_num,pres->unit_id);
		return; 
	}

	fcode = pres->function_code;
	address = pres->address;
	byte_count = pres->byte_count;

	port_tx_cnt[port_num]++;

	if( fcode == 0x06 )
	{
	//	dequeue_cmd(port_num,1);
		return;
	}
	if( fcode == 0xAA )
	{
#ifdef TCP_NOTICE
		clear_cmd(port_num);
#endif
		return;
	}

	if(address >= 2048)
	{
		dprintf("[Port %d]Mapping Address [%d] overflow!\n",port_num,address);
		return;
	}

	if(byte_count > 1024)
	{
		dprintf("[Port %d]Point [%d] overflow!\n",port_num,byte_count);
		return;
	}

	if(address + byte_count/2 > 2048)
	{
		dprintf("[Port %d]Data Space [%d] overflow!\n",port_num,address+byte_count/2);
		return;
	}

	if( fcode == 0x0F || fcode == 0x10 )
	{
		wres = (modbus_res_hdr_ext *)sdbuff;
		wres->unit_id = port_num;
		wres->function_code = 0xFF;
		wres->address = address;
		wres->byte_count = byte_count;

pthread_mutex_lock(&mutex_mem);
		if(fcode == 0x10)
#ifdef COM
			memcpy(wres->data,&io_data[port_num].INF_mem.INF_data.tag[address],byte_count);
#else
			memcpy(wres->data,&INF_mem[port_num].INF_data.tag[address],byte_count);
#endif
		if(fcode == 0x0F)
		{
			for(i = 0; i < byte_count/2 ; i++)
#ifdef COM				
				wres->data[i] = htons(io_data[port_num].INF_mem.INF_data.tag[address+i]);
#else
				wres->data[i] = htons(INF_mem[port_num].INF_data.tag[address+i]);
#endif
		}
pthread_mutex_unlock(&mutex_mem);

		if( port_write(sock, sdbuff, 6+byte_count, port_num, &interface) < 0 )
		{
			dprintf("[Port %d] 0xFF write error!\n",port_num); 
		}
		return;
	}

#ifdef DEBUG
	dprintf("11[PORT %d] ==> slave:0x%02X address:0x%04X points:0x%04X [%x][%x]...\n",port_num,pres->unit_id,address,byte_count, pres->data[0], pres->data[1]);
#endif
#ifdef COM
pthread_mutex_lock(&mutex_mem);
	if( fcode == 0x03 ) memcpy(&io_data[port_num].INF_mem.INF_data.tag[address],pres->data,byte_count);
	tx_queue_info[port_num].mem[tx_queue_info[port_num].tail].block_addr = address;
	tx_queue_info[port_num].mem[tx_queue_info[port_num].tail].byte_count = byte_count;
	tx_queue_info[port_num].tail++;
	tx_queue_info[port_num].tail %= MAX_NUM_TX_QUERY;
printf(">> port_num, address, pres->data, byte_count: %X, %X, %04X, %Xd\n", port_num, address, pres->data[1], byte_count);//khs
pthread_mutex_unlock(&mutex_mem);
#else

pthread_mutex_lock(&mutex_mem);
	if( fcode == 0x03 ) memcpy(&INF_mem[port_num].INF_data.tag[address],pres->data,byte_count);
pthread_mutex_unlock(&mutex_mem);
#endif

//if(address == 60)
//	dprintf("[YBJ] %x %x\n", address, pres->data[0]);
//	port_alive[port_num] = 1;
}

void cmdport_thread(void *arg)
{
	int i, sockfd, port_num;
	char head, tail, bflag;
	int cmdCountNew,cmdCountOld;

	cmdCountNew = 0;
	cmdCountOld = 0;
	pthread_detach(pthread_self());

	sleep(5);

	printf("CMD Manager Start\n");

	while(1)
	{
		for(i = 0; i < MAX_INTERFACE ; i++)
		{
			port_num = i+1;

pthread_mutex_lock(&mutex_cmd);
			head = PORT_info[i].write_head;
			tail = PORT_info[i].write_tail;

			if( head != tail )
			{
				if( cmd_write(PORT_info[i].sockfd,PORT_info[i].write_buff[head],
					PORT_info[i].write_length[head],port_num) > 0 )
                                {
                                    dprintf("PORT [%d]: Sock [%d] head %d tail %d \n",port_num,PORT_info[i].sockfd,head,tail);
			  	    dequeue_cmd(port_num,1);
                                }
				else
                                {
			       	    dequeue_cmd(port_num,0);
                                }
				cmdCountNew++;
			}
pthread_mutex_unlock(&mutex_cmd);

		}
		if(cmdCountNew != cmdCountOld)
		{
                   cmdCountOld = cmdCountNew;
		}
		usleep(20000); // parameter
	}
}
#ifdef COM
/***********************************************************************************
* @fn          txdata_write
*
* @brief       it transmits data received from INF program to MPM by using CAN. 
			   the start number of station index is saved at register named as 'hicsr_reg.uptime'
*				
*
* @param       block_addr - the start address of INF memory that will be transmitted.
*              byte_len - the number of bytes of data that will be transmitted.
*			   port_num - the port number that data was received from.
*			   
*
* @return      -1 - error
				1 - success
*/ 
int txdata_write(unsigned short block_addr, unsigned short byte_len, int port_num)
{
	struct canmsg_t smsg;
	CANID *sid = (CANID *)&smsg.id;	
	unsigned short infAddr;
	unsigned short offset;
	int i, j, tx_num;
	u16 * write_add, write_adda;
	u16 map_address, write_cnt, remain_bytes;
	u8 *buf;
	
	if( (hicsr_reg.uptime  <= 69) || (hicsr_reg.uptime  > 130) ) return -1;		//Because it is still not initialized or is initialized by wrong vaule.
	else		//if the start number of INF is valid, the CAN ID could be initialized
	{
		if(port_num)	//If the port_num is not 0.
		{
			infAddr = hicsr_reg.uptime + port_num - 1;	//Hicsr_reg.uptime is the start number of INF
		}
		else			//If the port num is 0. it is alarm area of INF. it is promised to send the alarm value of COM card to MPM as myID. 
		{
			infAddr = myID;
		}
		
	}
	sid->dir=HICSR_FROM_IO; sid->prio = HICSR_POINT;  sid->proto=HICSR_SEND;
	sid->dev=infAddr; 
	sid->map=0xf000; 
	
	tx_num = (byte_len / 8);	// the number of transmissions is started at 0.
	remain_bytes = byte_len % 8;
	if(remain_bytes == 0) tx_num--;	// in case there are no remain bytes, it decrease by 1.
	write_cnt = 8;
	buf = &smsg.data[0];
	map_address = block_addr * 2;
	for(i = 0; i <=  tx_num; i++)
	{
		offset = block_addr + (i*4); 	//block address is 2 byte per 1address
		sid->map = map_address + (i*8);	//sid->map is 1 byte per 1 address
		write_add = (u16 *)&io_data[port_num].INF_mem.INF_data.tag[offset];	
		
		if(i == tx_num)
			if(remain_bytes != 0) write_cnt = remain_bytes;

		smsg.len = write_cnt;
		
		for(j=0; j<write_cnt/2; j++){			// it have to send the word [LSB][MSB], and info is saved in pm_memory 
			*(buf+j*2) = (*(write_add+j) & 0xff00) >> 8;		
			*(buf+2*j+1) = *(write_add+j) & 0xff;
		}		
		//cpymem2buf(&smsg.data[0], write_add, write_cnt);
		//memcpy((u16 *)&smsg.data[0], write_add, write_cnt);
		
		//2016.12.29-11:50,khs
		/*printf("TX--> id: %x, tx index:%d/%d, map/offset : %d/%d, write_cnt:%x, data : [%x][%x][%x][%x][%x][%x][%x][%x]\n", 
		smsg.id, i, tx_num, sid->map,offset,
		write_cnt,
		smsg.data[0], smsg.data[1], smsg.data[2], smsg.data[3], 
		smsg.data[4], smsg.data[5], smsg.data[6], smsg.data[7]);
		*/
		iowrite(i%2, 0, &smsg, sizeof(struct canmsg_t));
	}
	return 1;	// txdata is finished.
}

void txdata_thread(void *arg)
{
	int i, sockfd;
	char head, tail, bflag;

	pthread_detach(pthread_self());

	sleep(5);
	
	printf("Data CAN TX Thread Start\n");
	
	
	while(1)
	{
		for(i = 0; i <= MAX_INTERFACE ; i++)
		{

pthread_mutex_lock(&mutex_cmd);
			head = tx_queue_info[i].head;
			tail = tx_queue_info[i].tail;

			if( head != tail )
			{
				/*printf("TX data[head : %d, tail : %d, port_num: %d, addr: %d, byte_cnt: %d]\n"
				, head, tail, i
				, tx_queue_info[i].mem[head].block_addr
				, tx_queue_info[i].mem[head].byte_count);
					/*printf("[PORT %d] ==> [%x][%x][%x][%x] address:%04X points:%04X\n"
	            ,i
	            ,io_data[i].INF_mem.INF_data.tag[ tx_queue_info[i].mem[head].block_addr]
	            ,io_data[i].INF_mem.INF_data.tag[ tx_queue_info[i].mem[head].block_addr+1]
	            ,io_data[i].INF_mem.INF_data.tag[ tx_queue_info[i].mem[head].block_addr+2]
	            ,io_data[i].INF_mem.INF_data.tag[ tx_queue_info[i].mem[head].block_addr+3]
	            ,tx_queue_info[i].mem[head].block_addr,tx_queue_info[i].mem[head].byte_count);*/
	
				if( txdata_write(tx_queue_info[i].mem[head].block_addr, tx_queue_info[i].mem[head].byte_count, i)> 0 )
				{
						dequeue_txdata(i,1);
				}
				else
					dequeue_txdata(i,0);
			}
pthread_mutex_unlock(&mutex_cmd);

		}
        usleep(50000);
        //  usleep(100000); // parameter
	    // usleep(500000); // parameter
	}
}
#endif



/***********************************************************************************
* @fn          port_thread
*
* @brief       it managers the communication of port put in by argument.  
*			   the steps of this function can be largely divided as following
*			   1. it opens link of port.
*			   2. it start endless loop wating for MODBUS-like packet from INF program.
*			   3. it call the port_handle function if it receive that packet.
*			   4. it marks "1" signal at position of port th in port 0 INF_data if it does not
*			      receive the packet from port th within 60s.
*
* @param       *arg - argument of port number variable.
*             
* @return      none
*/

#ifndef COM
/*
     1. data_table:  1-bit,   2-int,   3-float,    10-bit_2(0xff00/0x0000)
     2. offset:  Shared memory buffer index number
     3. time:  0-direct(no max/min),    10- x10, 100- x100
     4. len:  1-16 bit,    2-32 bit
     5. max, min 
	 
e.g )
    <Device name="CCU-01">
        <Register>
            <Variable name="Coil I/O" data_table="1" offset="0" times="0" len="1" max="300.0" min="100.0" redisKey="CCT-01_40000"/>
            <Variable name="Coil I/O" data_table="2" offset="0" times="10" len="2" max="200.0" min="100.0" redisKey="CCT-01_40000"/>
            <Variable name="AnalogInputsAD" data_table="2" offset="0" times="100" len="2" max="1000.0" min="100.0" redisKey="CCT-01_40000"/>
            <Variable name="AnalogInputsAD" data_table="3" offset="0" times="1000" len="2" max="3000.0" min="100.0" redisKey="CCS-01_40001"/>
            <Variable name="AnalogInputs" data_table="3" offset="10" times="1" len="2" max="30000.0" min="10000.0" redisKey="CCU-01_40010"/>
            <Variable name="AnalogInput1Mode" data_table="4" offset="12" times="1" len="1" max="100.0" min="50.0" redisKey="CCU-01_40012"/>
            <Variable name="AnalogInput1Slope" data_table="4" offset="13" times="10" len="2" max="300.0" min="30.0" redisKey="CCU-01_40013"/>
            <Variable name="AnalogInputConfig" data_table="5" offset="15" times="1" len="1" max="300.0" min="10.0" redisKey="CCU-01_40015"/>
        </Register>
    </Device>
*/
enum Data_Type {
	TYPE_BIT = 1, 
	TYPE_INT, 
	TYPE_FLOAT,
	TYPE_BIT_2 = 10, 
};

unsigned int digital_bit_input(int raw_value, int len)
{
	int i;
	
	len -= 1;  //len > 0
	
	if(raw_value & (1 << len))
		i = 1;
	else
		i = 0;
	
	if(len < 0) i = 0; //ignore len-fault
	
	return (unsigned int)i;
}

unsigned int digital_bit_input_2(int raw_value)
{
	int i;
	
	if(raw_value & 0xffff)
		i = 1;
	else
		i = 0;
	
	return (unsigned int)i;
}

float convert_value(int data_table, int offset, int times, int len, int max, int min)
{
	float val;
	unsigned int ui_tmp;

	if(data_table == TYPE_BIT) {
		ui_tmp = digital_bit_input(INF_mem[1].INF_data.tag[offset], len);
		val = (float) ui_tmp;
		return val;
	} else if(data_table == TYPE_BIT_2) {
		ui_tmp = digital_bit_input_2(INF_mem[1].INF_data.tag[offset]);
		val = (float) ui_tmp;
		return val;
	}

	if(len >= 2) {	//32 bit
		val = (float)((INF_mem[1].INF_data.tag[offset + 1] << 16) | INF_mem[1].INF_data.tag[offset]);
	else if(len == 3) {//float
		ui_tmp = (unsigned int)((INF_mem[1].INF_data.tag[offset + 1] << 16) | INF_mem[1].INF_data.tag[offset]);
		memcpy(&val, &ui_tmp, sizeof(unsigned int)); 
		return val;
	} else {	//16 bit
		val = (float)INF_mem[1].INF_data.tag[offset];
	}
	
	if(times == 0) {
		val = (float)INF_mem[1].INF_data.tag[offset];
		return val;
	}
//printf("val_0 = %f\n", val);
	if(len > 1) {  //32 bit
		val = ((((float)max - (float)min) / (float)0xffffffff) * val) + (float)min;
	} else {	//16 bit
		val = ((((float)max - (float)min) / (float)0xffff) * val) + (float)min;
	}
//printf("val_1 = %f\n", val);

	val = val / (float)times;
//printf("val_2 = %f\n", val);	
	
    return val;
}
#endif

void port_thread(void *arg)
{
	struct sockaddr_in server;
	int len, result, optval=1;
	fd_set readfds,	testfds;
	int sockfd;
	int	nread;
	int port_num, index;                // from port number 1 // index = port_num-1
	struct timeval timeout;

	index = (int)arg;					
	port_num = index+1;					//because the interface is started at 1.
	pthread_detach(pthread_self());
int i, j;
char str[20];
#ifndef COM 
        //redisContext *c;
        redisReply *reply;
#endif

/*	
#ifdef COM
	INFS_st = (INF_STATUS *) io_data[0].INF_mem.INF_data.tag;
#else
	INFS_st = (INF_STATUS *) INF_mem[0].INF_data.tag;
#endif
*/
	
restart:	
	if(port_num != 0) { //khss
		sockfd = open_link(port_num);
		if( sockfd < 0 ) goto restart;
	} else
		goto restart;  //Dangerous code but ...

	dprintf("Port %d socket %d Start \n",port_num,sockfd);
	if( index >= 0 ) PORT_info[index].sockfd = sockfd;
	else
	{
#ifdef RED_LED
		result = rled_init();
		if( result < 0 )
		{
			dprintf("[PORT %d] LED INIT ERROR[1]\n",port_num);
		}
		else if( result == 0 )
		{
			dprintf("[PORT %d] LED INIT ERROR[2]\n",port_num);
		}
		else
		{
			dprintf("[PORT %d] LED INIT\n",port_num);
		}
#endif
	}

	FD_ZERO(&readfds);
	FD_SET(sockfd, &readfds);
	
	while(1){
		testfds	= readfds;

		timeout.tv_sec = 60;
		timeout.tv_usec = port_num*100000;

		if(select(sockfd+1, &testfds, (fd_set*)0, (fd_set*)0, (struct timeval*)&timeout) != 0)
		{
			if(FD_ISSET(sockfd, &testfds)) {
				ioctl(sockfd, FIONREAD,	&nread);
				if(nread > 0) {
					port_alive[port_num] = 1;
					port_handle(sockfd,port_num);
#ifndef COM
//printf( ")) %d, %s, [ %d, %d ]\n", __LINE__, __FILE__, index, io_data[0].INF_mem.INF_data.tag[10] );//khs
//redis_test();
//printf("aaaaaaaa slkfjsklafjlksfja\n\n");
//    for(i=0; i < 500; i++)
//        printf("%04X, ", INF_mem[1].INF_data.tag[i]);
//printf("\t Index: %d\n", index);

    for(i = 0; i < Xml_AllTagNum; i++) {
        //khs 20.12.23 Xml_Tag_Info[i].value = INF_mem[1].INF_data.tag[Xml_Tag_Info[i].address];
	Xml_Tag_Info[i].value = convert_value(Xml_Tag_Info[i].data_table, Xml_Tag_Info[i].address, Xml_Tag_Info[i].times, Xml_Tag_Info[i].len, Xml_Tag_Info[i].max, Xml_Tag_Info[i].min);
        //printf("\t Index: %d, Address: %d, Value: %x  == %d,%d,%d,%d,%d\n", i, Xml_Tag_Info[i].address, Xml_Tag_Info[i].value,
	//	Xml_Tag_Info[i].data_table, Xml_Tag_Info[i].times, Xml_Tag_Info[i].len, Xml_Tag_Info[i].max, Xml_Tag_Info[i].min);
		if(Xml_Tag_Info[i].data_table == TYPE_BIT || Xml_Tag_Info[i].data_table == TYPE_BIT_2)
			sprintf(str, "%d", (int)Xml_Tag_Info[i].value);
		else
			sprintf(str, "%f", (float)Xml_Tag_Info[i].value);
		
        reply = redisCommand(Redis_Ptr,"SET %s %s", Xml_Tag_Info[i].key_name, str);
    } 
    //reply = redisCommand(Redis_Ptr,"SET %s %s", "foo", redisKey, Xml_Tag_Info[i].value);
    printf("SET: %s\n", reply->str);
    freeReplyObject(reply);
    /*redisFree(c);*/
//printf( ")) %d, %s, [ %d, %d ]\n", __LINE__, __FILE__, index, INF_mem[0].INF_data.tag );//khs
#endif

#ifdef PORT_CHECK
#ifdef COM
					if((port_num > 0)&&((io_data[0].INF_mem.INF_data.tag[MAX_INF_CH+index]&htons(0x0001)) != 0))	//if it is 1
#else
					if((port_num > 0)&&((INF_mem[0].INF_data.tag[MAX_INTERFACE+index]&htons(0x0001)) != 0))
#endif
					{
						dprintf("[PORT %d] Check - Port Abnormal Cancelled\n",port_num);
pthread_mutex_lock(&mutex_mem);
#ifdef COM
						io_data[0].INF_mem.INF_data.tag[MAX_INTERFACE+index] &= ~htons(0x0001);
#else
						INF_mem[0].INF_data.tag[MAX_INTERFACE+index] &= ~htons(0x0001);
#endif						
pthread_mutex_unlock(&mutex_mem);
#ifdef RED_LED
						rled_off(port_num);
#endif
					}
#endif
				}
			}
		}
		else
		{
		
#ifdef PORT_CHECK
			dprintf("[PORT %d] Port Check\n",port_num);
			port_alive[port_num] = 0;

			if( port_num > 0 && port_num <= MAX_INTERFACE )
			{
				//printf("[PORT %d] Check - Port Abnormal\n",port_num);	
pthread_mutex_lock(&mutex_mem);
#ifdef COM
				//io_data[0].INF_mem.INF_data.tag[10+index] |= htons(0x0001);
#else
				INF_mem[0].INF_data.tag[10+index] |= htons(0x0001);
#endif
pthread_mutex_unlock(&mutex_mem);
#ifdef RED_LED
				rled_on(port_num);
#endif
			}

			close(sockfd);			
			goto restart;
#endif
		}
		usleep(1000);
	}

	close(sockfd);
}

