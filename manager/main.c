#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>

#include <asm/types.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

#include "common.h"
#include "icss_types.h"
#include "protocol_frame.h"
#include "modbus_down.h"
//#include "mydefs.h"
#include "hdmbp.h"
#include "scmbp.h"
#include "dbgmbp.h"
#include "port.h"
#include "db_port.h"
#include "watchdog.h"
#include "shell.h"
#include "log.h"
#include "version.h"
#ifdef COM
#include "taskcan.h"
#include "hicsr.h"
#endif
#include "hiredis.h"

#ifdef COM
#include "xml_parser.h"
#endif

const char *app_name = APP_NAME;
const char *app_version = VERSION;
const char *app_seq = SEQ_NO;
const char *app_time = COMPILE_TIME;
const char *app_by = COMPILE_BY;
const char *app_host = COMPILE_HOST;
const char *app_compiler = COMPILER;

//#define	DEBUG
#ifndef	DEBUG
	#define	dprintf(format, args...) log_access(format, ## args)
#else
	#define	dprintf(format, args...) log_access(format, ## args); printf(format, ## args)
#endif	// DEBUG

int  DebugGuardStart[1024];
PTC_HEADER DB_main;
PM_INFO PM_info[MAX_INTERFACE + 1];
PORT_INFO PORT_info[MAX_INTERFACE + 1];
int  DebugGuardEnd[1024];

#ifdef COM
IO_DATA io_data[MAX_INTERFACE + 1];
#else
INF_MEM INF_mem[MAX_INTERFACE+1];
#endif

pthread_t scm_thr, hdbus_thr[2], debug_thr;
pthread_t ifmgr_thr, cmd_thr, db_thr;
pthread_t Shellthr=0;

pthread_mutex_t mutex_mem = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex_cmd = PTHREAD_MUTEX_INITIALIZER;

int scm_tx_cnt, scm_rx_cnt, scm_thr_cnt;
int hdbus_tx_cnt, hdbus_rx_cnt, hdbus_thr_cnt;
int port_tx_cnt[MAX_INTERFACE+1], port_rx_cnt[MAX_INTERFACE+1], port_thr_cnt[MAX_INTERFACE+1];
int thr_cnt[2];
#ifdef COM
extern unsigned char myID;
pthread_t txdata_thr, canio_thr, cantask_thr;
extern u16 canrx_header[2], canrx_tail[2];
#endif

__u8 port_alive[MAX_INTERFACE+1], hdlc_alive, scm_alive;
__u8 db_update, nw_update;

#ifndef COM
extern int xml_address_data_init(void);
#endif

static void print_backtrace(void)
{
	int j, nptrs;
#define SIZE 100
	void *buffer[100];
	char **strings;

	nptrs = backtrace(buffer,SIZE);
	dprintf("SIGV backtrace() returned %d addresses\n",nptrs);

	strings = backtrace_symbols(buffer,nptrs);
	if(strings == NULL) {
		perror("backtrace_symbols");
		exit(EXIT_FAILURE);
	}

	for( j = 0; j < nptrs; j++)
	{
		dprintf("\t%s\n",strings[j]);
	}

	free(strings);
}

static char *strsigno(int sig)
{
	switch(sig){
	case SIGHUP:	return "Hangup";
	case SIGINT:	return "Interrupt";
	case SIGQUIT:	return "Quit";
	case SIGILL:	return "Illegal instruction";
	case SIGTRAP:	return "Trace trap";
	case SIGABRT:	return "Abort";
	case SIGBUS:	return "BUS error";
	case SIGFPE:	return "Floating-point exception";
	case SIGKILL:	return "Kill, unblockable";
	case SIGUSR1:	return "User-defined signal 1";
	case SIGSEGV:	return "Segmentation violation";
	case SIGUSR2:	return "User-defined signal 2";
	case SIGPIPE:	return "Broken pipe";
	case SIGALRM:	return "Alarm clock";
	case SIGTERM:	return "Termination";
	case SIGSTKFLT:	return "Stack fault";
	case SIGCHLD:	return "Child status has changed";
	case SIGCONT:	return "Continue";
	case SIGSTOP:	return "Stop, unblockable";
	case SIGTSTP:	return "Keyboard stop";
	case SIGTTIN:	return "Background read from tty";
	case SIGTTOU:	return "Background write to tty";
	case SIGURG:	return "Urgent condition on socket";
	case SIGXCPU:	return "CPU limit exceeded";
	case SIGXFSZ:	return "File size limit exceeded";
	case SIGVTALRM:	return "Virtual alarm clock";
	case SIGPROF:	return "Profiling alarm clock";
	case SIGWINCH:	return "Window size change";
	case SIGIO:		return "I/O now possible";
	case SIGPWR:	return "Power failure restart";
	case SIGSYS:	return "Bad system call";
	default:		return "Bad SIG No.";
	}
}

static void sig_ign(int sig)
{
	if( sig < NSIG ){
		dprintf("sig ign : %d(%s)\n", sig,strsigno(sig));
		signal(sig, sig_ign);
		if( sig == SIGTERM )
		{
//			unlink("/tmp/"APP_NAME);
			exit(EXIT_SUCCESS);
		}
		if( sig == SIGCHLD )
		{
			
			int status, n;
           
			do {
				if( (n = waitpid(-1,&status,WNOHANG)) < 0)
					return;
				if(WIFEXITED(status))
				{
				//	printf("Child's exited normally!!\n");
				}
			} while(n > 0);
			
		}
		if( sig == SIGSEGV )
		{
			print_backtrace();
			sleep(1); // 2013-02-05
		}
	}
}

static void make_daemon()
{
	// make daemon
	int i;

#if 1
	pid_t f;

	f = fork();
	if ( f > 0 ){	// if parent
		exit(0);
	} else if ( f < 0 ) {	// fork error
		exit(-1);
	}

	setsid();

#if 0
	f = fork();
	if ( f > 0 ){	// if parent
		exit(0);
	} else if ( f < 0 ) {	// fork error
		exit(-1);
	}

	chdir("/");
#endif
#endif

	//daemon(0,0);

	for(i=1; i < NSIG; ++i)	signal(i, sig_ign);
	signal(SIGINT, SIG_DFL);
}
#ifdef COM
#include <fcntl.h>
static unsigned char read_myID()
{	
	unsigned char read_id;
	
	static int deviceID_fd;
	char buffer[1];
	int recv_byte;
		
	deviceID_fd = open(ID_DIR"baddr",O_RDONLY);
  
	if(deviceID_fd < 0)
	{
		dprintf("ID info FILE [%s] is not found!\n","baddr");
		return 0;
	}
	else
	{
		recv_byte = read(deviceID_fd, buffer, 1);
		read_id = (unsigned char)buffer[0];
	}
	
	close(deviceID_fd);
	
	//read_id = 0x01;
	return read_id;
}
#endif
static int initial(int version)
{
	int i;
	for( i = 0 ; i < MAX_INTERFACE+1 ; i++)
	{
		port_tx_cnt[i] = 0;
		port_rx_cnt[i] = 0;
		port_thr_cnt[i] = 0;
	}

	hdbus_tx_cnt = 0;
	hdbus_rx_cnt = 0;
	hdbus_thr_cnt = 0;

	scm_tx_cnt = 0;
	scm_rx_cnt = 0;
	scm_thr_cnt = 0;

	debug_thr = 0;

	nw_update = 0;
	
#ifdef COM	
	myID = read_myID();		//it must be initialized by read id function.
	printf("\nMY CAN ID %d",myID);
pthread_mutex_lock(&mutex_mem);
	memset(&io_data, 0, sizeof(IO_DATA)*(MAX_INTERFACE+1));
pthread_mutex_unlock(&mutex_mem);
#else
pthread_mutex_lock(&mutex_mem);
	memset(&INF_mem, 0, sizeof(INF_MEM)*(MAX_INTERFACE+1));
//	if( version == ACONIS_DS || version == ACONIS_RIO )
//		memset(&INF_mem[0].INF_data.tag[0], 0xFF, 20);
pthread_mutex_unlock(&mutex_mem);
#endif
	memset(&DB_main, 0, sizeof(PTC_HEADER));

	return 1;
}

static int open_link(int version)
{
	int i;

	pthread_create(&debug_thr, NULL, (void*)&dbgmbp_thread, (void *)0);
#ifdef COM
#else
	if( version == ACONIS_2000 )
	{
		pthread_create(&hdbus_thr[0], NULL, (void *)&hdmbp_thread, (void *)0);
		pthread_create(&hdbus_thr[1], NULL, (void *)&hdmbp_thread, (void *)1);
	}
	else
		pthread_create(&scm_thr, NULL, (void *)&scmbp_thread, (void *)0);
#endif
	pthread_create(&ifmgr_thr, NULL, (void *)&port_thread, (void *)-1);
	for(i = 0; i < MAX_INTERFACE ; i++)
		pthread_create(&PORT_info[i].port_thr, NULL, (void *)&port_thread, (void *)i);
#ifdef COM
	pthread_create(&canio_thr, NULL, &IOCommThread, (void *)0);
	pthread_create(&txdata_thr, NULL, (void *)&txdata_thread, (void *)0);
	pthread_create(&cantask_thr, NULL, &TaskFlowThread, (void *)0);
#else
	pthread_create(&db_thr, NULL, (void *)&dbport_thread, (void *)0);
#endif
	pthread_create(&cmd_thr, NULL, (void *)&cmdport_thread, (void *)0);
#ifdef  WEB
	pthread_create(&Shellthr, NULL, (void *)&ShellThread, NULL);
#endif
	return 1;
}

static char *DBtoString(int type, int value)
{
	if( type == DB_TYPE )
	{
		switch(value)
		{
		case ACONIS_2000: return "ACONIS-2000"; break;
		case ACONIS_DS: return "ACONIS-DS"; break;
		default: break;
		}
	}

	if( type == PROTOCOL_TYPE )
	{
		switch(value)
		{
		case PTC_PROTOCOL_NO: return "Not Assigned"; break;
		case PTC_MODBUS_MASTER:	return "MODBUS MASTER"; break;
		case PTC_MODBUS_SLAVE: return "MODBUS SLAVE"; break;
		case PTC_SECONDARY_PORT: return "SECONDARY PORT"; break;
		case PTC_NMEA0183: return "NMEA 0183"; break;
		case PTC_LOADCOM: return "LOADCOM"; break;
		case PTC_REEFERCON: return "REFCON"; break;
		case PTC_VDR_WRITE: return "VDR"; break;
		case PTC_DNP_MASTER: break;
		case PTC_DNP_SLAVE: break;
		case PTC_IEC101_MASTER: break;
		case PTC_IEC101_SLAVE: break;
		default: break;
		}
	}

	if( type == BAUDRATE )
	{
		switch(value)
		{
		case BAUDR_4800: return "4800"; break;
		case BAUDR_9600: return "9600"; break;
		case BAUDR_19200: return "19200"; break;
		case BAUDR_38400: return "38400"; break;
		case BAUDR_57600: return "57600"; break;
		case BAUDR_115200: return "115200"; break;
		}
	}

	if( type == DATABIT )
	{
		switch(value)
		{
		case 5: return "5"; break;
		case 6: return "6"; break;
		case 7: return "7"; break;
		case 8: return "8"; break;
		}
	}

	if( type == PARITYBIT )
	{
		switch(value)
		{
		case 0: return "NONE"; break;
		case 1: return "ODD"; break;
		case 2: return "EVEN"; break;
		}
	}

	if( type == STOPBIT )
	{
		switch(value)
		{
		case 0: return "1"; break;
		case 1: return "2"; break;
		}
	}

	if( type == FLOWCTRL )
	{
		switch(value)
		{
		case 0: return "N"; break;
		case 1: return "RTS"; break;
		}
	}

	return "Unknown";
}

static char *find_exe(int protocol, char *phy_type, FILE *fp)
{
	char buffer[256], *p;

	fseek(fp,0,SEEK_SET);
	while(fgets(buffer,256,fp)!=NULL)
	{
		p = &buffer[0];

		char *p_ptc = strtok(p,"\t ");
		char *p_phy = strtok(NULL,"\t ");
		char *p_exe = strtok(NULL,"\r\n");
		
		if( protocol == atoi(p_ptc) && strcmp(p_phy,phy_type) == 0 )
		{
			return p_exe;
		}
	}

	return "";
}

static int db_change(int version)
{
	int i;
	char file_name[256];

	if( version == ACONIS_2000 )
		return 0;
	else if( version == ACONIS_DS )
	{
		if( db_update )
		{
			db_update = 0;

			if(access(CONF_DIR"conf.fix",F_OK) == 0)
			{
				dprintf("Configuration Fix found!!! => DB_UPDATE ignored\n");
				return 0;
			}

			remove(CONF_DIR"pm.conf");

			for(i = 0; i < 10 ; i++)
			{
				sprintf(file_name,CONF_DIR"port%02d.conf",i+1);
				remove(file_name);
				sprintf(file_name,CONF_DIR"ptc%02d.conf",i+1);
				remove(file_name);
			}

			remove(CONF_DIR"ifcfg-eth2");
			remove(CONF_DIR"ifcfg-eth3");
			
			return 1;
		}
		else
			return 0;
	}
	else
		return 0;
}

static int open_db(int version)
{
	int i;
	FILE *main_fp = NULL;
	FILE *fp = NULL, *w_fp = NULL, *d_fp = NULL;
	char fname[256], *p;
	int nread;
	struct in_addr ip, subnet, broad, network, gw;

	if( version == ACONIS_2000 ) return 1;
	if( version == ACONIS_RIO ) return 1;
	return 1;

	main_fp = fopen(DB_DIR"PTC.dwn","r");
	if(main_fp == NULL)
	{
		dprintf("Main DB %s not found!\n","PTC.dwn");
		return 0;
	}
	
	if( (nread = fread(&DB_main,1,sizeof(PTC_HEADER),main_fp)) != sizeof(PTC_HEADER) )
	{
		dprintf("Main DB Size Error!(%d)-(%d)\n",nread,sizeof(PTC_HEADER));
		return 0;
	}
	fclose(main_fp);

	dprintf("Main DB Open and Read (%d) bytes\n",nread);
	dprintf("\tfile crc: %d\n",DB_main.file.filecrc);
	dprintf("\trsc: %d\n",DB_main.file.rsc);
	dprintf("\tsys_type: %s\n",DB_main.file.systype?"ACONIS-DS":"ACONIS-2K");
	dprintf("\tftime: %d\n",DB_main.file.ftime);
	dprintf("\tcnt: %d\n",DB_main.file.cnt);
	dprintf("\tsize: %d\n",DB_main.file.size);

	dprintf("IP config:\n");
	nw_update = 0;
	for(i = 0; i < MAX_LANCNT ; i++)
	{
		if( DB_main.lan[i].ip.s_addr != 0 )
		{
			sprintf(fname,CONF_DIR"ifcfg-eth%d",i+2);
			fp = fopen(fname,"r");
			if( fp == NULL )
			{
				w_fp = fopen(fname,"w");
				if( w_fp == NULL )
				{
					dprintf("Cannot create configuration file %s!\n",fname);
					return 0;
				}

				ip.s_addr = DB_main.lan[i].ip.s_addr;
				subnet.s_addr = DB_main.lan[i].subnet.s_addr;
				gw.s_addr = DB_main.lan[i].defgw.s_addr;
				broad.s_addr = ip.s_addr | (~subnet.s_addr);
				network.s_addr = ip.s_addr & (subnet.s_addr);

				if(gw.s_addr != 0L)
				   dprintf("\teth%d - addr:%s(%s) gw:%s\n",i+2,inet_ntoa(ip),inet_ntoa(subnet),inet_ntoa(gw));
				else
				   dprintf("\teth%d - addr:%s(%s) gw:%s\n",i+2,inet_ntoa(ip),inet_ntoa(subnet),"none");
					
				fprintf(w_fp,"DEVICE=eth%d\n",i+2);
				fprintf(w_fp,"BOOTPROTO=static\n");
				fprintf(w_fp,"IPADDR=%s\n",inet_ntoa(ip));
				fprintf(w_fp,"NETMASK=%s\n",inet_ntoa(subnet));
				fprintf(w_fp,"BROADCAST=%s\n",inet_ntoa(broad)); 
				fprintf(w_fp,"NETWORK=%s\n",inet_ntoa(network));
				fprintf(w_fp,"ONBOOT=yes\n");
				if(gw.s_addr != 0L)
				fprintf(w_fp,"GATEWAY=%s\n",inet_ntoa(gw));
				fprintf(w_fp,"GATEWAYDEV=\n");
				fprintf(w_fp,"MACADDR=00:09:3b:%02x:%02x:%02x\n",rand()%256,rand()%256,rand()%256);

				nw_update = 1;
				
				fclose(w_fp);
			}
			else
				fclose(fp);		
		}
		else
		{
			dprintf("\teth%d - not used\n",i+2);
		}
	}
	
	sprintf(fname,CONF_DIR"pm.conf");
	fp = fopen(fname,"r");
	if( fp == NULL )
	{
		w_fp = fopen(fname,"w");
		if( w_fp == NULL )
		{
			dprintf("Cannot create configuration file %s!\n",fname);
			return 0;
		}
	
		d_fp = fopen(CONF_DIR"pm.def","r");
		if( d_fp == NULL )
		{
			dprintf("Cannot create definition file %s!\n",fname);
			return 0;
		}

		fprintf(w_fp,"$P%d\n",MAX_PORT);
		for(i = 0; i < MAX_UARTCNT ; i++)
		{
			dprintf("UART %d - %s\n",i+1,DBtoString(PROTOCOL_TYPE,DB_main.uart[i].ptype));
			fprintf(w_fp,":%d %s\n",i+1,find_exe(DB_main.uart[i].ptype,"UART",d_fp));
		}
		for(i = 0; i < MAX_LANCNT ; i++)
		{
			dprintf("LAN %d - %s\n",i+1,DBtoString(PROTOCOL_TYPE,DB_main.lan[i].ptype[0]));
			fprintf(w_fp,":%d %s\n",MAX_UARTCNT+i+1,find_exe(DB_main.lan[i].ptype[0],"LAN",d_fp));
		}

		dprintf("Process configuration %s created\n",fname);

		fclose(w_fp);
		fclose(d_fp);
	}
	else
		fclose(fp);



	return 1;
}

static int read_db(int version)
{
	int i, num, len;
   	int type=0, index=0;
	FILE *fp;
	char fname[256], buffer[256], *p;

	sprintf(fname,CONF_DIR"pm.conf");
	fp = fopen(fname,"r");
	if( fp != NULL ) 
	{
		while(fgets(buffer,256,fp)!=NULL)
		{
			p = &buffer[0];

			if( *p == '$' || *p == '@' )
			{
				p++;
				if(*p == 'P')
				{
					p++;
					num = atoi(p);	//pm.conf
					type = 1;
				}
			}
			else if( *p == ':' || *p == '*' || *p == ';' )
			{
				p++;
				if( type == 1 )
				{
					char *p_num = strtok(p," ");
					char *p_exe = strtok(NULL,"\r\n,");
					char *p_param1 = strtok(NULL,"\r\n,");		//-bypass or -slave 
					char *p_param2 = strtok(NULL,"\r\n,");		// param 2
					char *p_adaptor = strtok(NULL,"\r\n,");		// 
					char *p_parama = strtok(NULL,"\r\n,");		// 

					if(p_exe != NULL)
					{
						strncpy(PM_info[index].exe,p_exe,16);		//PM_info

						if(p_param1 == NULL) PM_info[index].param1[0]=0;
						else if(p_param1[0] == ' '||p_param1[0] == '#'||p_param1[0] == '*')
							PM_info[index].param1[0]=0;
						else strncpy(PM_info[index].param1,p_param1,8);
						
						if(p_param2 == NULL) PM_info[index].param2[0]=0;
						else if(p_param2[0] == ' '||p_param2[0] == '#'||p_param2[0] == '*')
							PM_info[index].param2[0]=0;
						else strncpy(PM_info[index].param2,p_param2,8);

						if(p_adaptor != NULL) strncpy(PM_info[index].adapt,p_adaptor,16);	
						else PM_info[index].adapt[0]=0;
						if(p_parama != NULL) strncpy(PM_info[index].adapt_param,p_parama,8);
						else PM_info[index].adapt_param[0]=0;
						
						PM_info[index].index = atoi(p_num);	//interface
					}
					else
					{
						PM_info[index].exe[0] = 0;	
						PM_info[index].index =  0;
					}

					index++;
				}
			}
		}

		fclose(fp);
	}
	else
	{
		dprintf("Process configuration file %s not found!!!\n",fname);	//pm.conf 
		if( version == ACONIS_2000 || version == ACONIS_RIO )
		{
			dprintf("EXIT - ACONIS2000 Only\n");	
			exit(EXIT_FAILURE);
		}
		return 0;
	}

	dprintf("Process List: %d\n",num);
	for(i = 0; i< num; i++)
	{
		dprintf("\t%-2d - %s %s %s %s %s\n",i+1,
			PM_info[i].exe,PM_info[i].param1,PM_info[i].param2,
			PM_info[i].adapt,PM_info[i].adapt_param);
	}

	return 1;
}

static int close_db()
{
	return 1;
}

static int script_run()
{
	pid_t f;
	int ret;
	char fname[64], param1[16], param2[16];

	if( nw_update == 0 ) return 1;
	nw_update = 0;

	dprintf("Script Run:\n");

	memset(fname,0,64);
	sprintf(fname,SCR_DIR"%s","ext_network");

	if( access(fname,X_OK) != 0 )
	{
		dprintf("\treboot is needed to config networks.\n");
		return 0;		
	}

	f = fork();
	if( f == 0 )
	{
//		sprintf(fname,SCR_DIR"%s","ext_network");
		sprintf(param1,"%s%02d","script",1);
		sprintf(param2,"%s","restart");

		dprintf("\t%s %s %s\n",fname,param1,param2);
			
		ret = execl(fname,param1,param2,NULL);

		if( ret < 0 ) exit(EXIT_FAILURE);
	}
	else if( f < 0 )
	{
		exit(EXIT_FAILURE);
	}

	return 1;
}

static int port_open()
{
	int i;
	FILE *fp, *w_fp;
	char fname[256];

	for(i = 0; i < MAX_UARTCNT ; i++)
	{
		if( DB_main.uart[i].ptype != 0 )
		{
			if( DB_main.uart[i].ptype != PTC_SECONDARY_PORT )
			{
				sprintf(fname,CONF_DIR"port%02d.conf",i+1);
				fp = fopen(fname,"r");
				if( fp == NULL ) 
				{
					w_fp = fopen(fname,"w");
					if( w_fp == NULL )
					{
						dprintf("Cannot create configuration file %s!\n",fname);
						return 0;
					}

					fprintf(w_fp,"# INF Port Configuration\n");
					fprintf(w_fp,"#\n");
					fprintf(w_fp,"# Prefix Definition\n");
					fprintf(w_fp,"#   $ @ => Control Header\n");
					fprintf(w_fp,"#   : * => Sub Header\n");
					fprintf(w_fp,"#\n");
					fprintf(w_fp,"# Usage Description\n");
					fprintf(w_fp,"# $U<1,2>\n");
					fprintf(w_fp,"# :<port1> <baud> <data> <parity> <stop> <flow>\n");
					fprintf(w_fp,"# [:<port2> <baud> <data> <parity> <stop> <flow>]\n");
					fprintf(w_fp,"# $L<1,2>\n");
					fprintf(w_fp,"# :1 <ip address1> <port number1>\n");
					fprintf(w_fp,"# [:2 <ip address2> <port number2>]\n\n");
					fprintf(w_fp,"# INF %d\n\n",i+1);
					if( DB_main.file.systype == ACONIS_DS ) fprintf(w_fp,"$DS\n");
					fprintf(w_fp,"$U%d\n",DB_main.uart[i].secport?2:1);
					fprintf(w_fp,":%d %s %s %s %s %s\n",i+1,
						DBtoString(BAUDRATE,DB_main.uart[i].baud),
						DBtoString(DATABIT,DB_main.uart[i].databits),
						DBtoString(PARITYBIT,DB_main.uart[i].parity),
						DBtoString(STOPBIT,DB_main.uart[i].stopbits),
						DBtoString(FLOWCTRL,DB_main.uart[i].flowctrl));

					if( DB_main.uart[i].secport )
					{
						fprintf(w_fp,":%d %s %s %s %s %s\n",
							DB_main.uart[i].secport,
							DBtoString(BAUDRATE,DB_main.uart[i].baud),
							DBtoString(DATABIT,DB_main.uart[i].databits),
							DBtoString(PARITYBIT,DB_main.uart[i].parity),
							DBtoString(STOPBIT,DB_main.uart[i].stopbits),
							DBtoString(FLOWCTRL,DB_main.uart[i].flowctrl));
					}

					fclose(w_fp);
				}
				else
					fclose(fp);
			}
		}
	}

	for(i = 0; i < MAX_LANCNT ; i++)
	{
		if( DB_main.lan[i].ptype[0] != 0 )
		{
			if( DB_main.lan[i].ptype[0] != PTC_SECONDARY_PORT )
			{
				sprintf(fname,CONF_DIR"port%02d.conf",MAX_UARTCNT+i+1);
				fp = fopen(fname,"r");
				if( fp == NULL ) 
				{
					w_fp = fopen(fname,"w");
					if( w_fp == NULL )
					{
						dprintf("Cannot create configuration file %s!\n",fname);
						return 0;
					}

					fprintf(w_fp,"# INF Port Configuration\n");
					fprintf(w_fp,"#\n");
					fprintf(w_fp,"# Prefix Definition\n");
					fprintf(w_fp,"#   $ @ => Control Header\n");
					fprintf(w_fp,"#   : * => Sub Header\n");
					fprintf(w_fp,"#\n");
					fprintf(w_fp,"# Usage Description\n");
					fprintf(w_fp,"# $U<1,2>\n");
					fprintf(w_fp,"# :<port1> <baud> <data> <parity> <stop> <flow>\n");
					fprintf(w_fp,"# [:<port2> <baud> <data> <parity> <stop> <flow>]\n");
					fprintf(w_fp,"# $L<1,2>\n");
					fprintf(w_fp,"# :1 <ip address1> <port number1>\n");
					fprintf(w_fp,"# [:2 <ip address2> <port number2>]\n\n");
					fprintf(w_fp,"# INF %d\n\n",i+1);
					if( DB_main.file.systype == ACONIS_DS ) fprintf(w_fp,"$DS\n");
					fprintf(w_fp,"$L%d\n",(DB_main.lan[i].secport&&i==0)?2:1);
					fprintf(w_fp,":%d %s %d\n",i+1,
						inet_ntoa(DB_main.lan[i].ip_addr[0]),
						DB_main.lan[i].portnumber[0]);

					if( DB_main.lan[i].secport && i == 0 )
					{
						fprintf(w_fp,":%d %s %d\n",i+2,
							inet_ntoa(DB_main.lan[i+1].ip_addr[0]),
							DB_main.lan[i+1].portnumber[0]);
					}

					fclose(w_fp);
				}
				else
					fclose(fp);
			}
		}
	}

	return 1;
}

static int port_close()
{
	return 1;
}

static int create_adaptor(int i)
{
	pid_t f;
	int ret;
	char fname[64], param1[16], param2[16];

	if( PM_info[i].index <= 0 ) return 0;

	if( PM_info[i].adapt[0] != 0 )
	{
		memset(fname,0,64);

		f = fork();
		if( f == 0 )
		{
			sprintf(fname,APP_DIR"%s",PM_info[i].adapt);
			sprintf(param1,"adaptor%s#",PM_info[i].adapt_param);
			sprintf(param2,"%s",PM_info[i].adapt_param);

			dprintf("\t-- %s %s %s\n",fname,param1,param2);
			
			ret = execl(fname,param1,param2,NULL);

			if( ret < 0 ) exit(EXIT_FAILURE);
		}
		else if( f < 0 )
		{
			exit(EXIT_FAILURE);
		}
	}

	return 1;
}

static int create_thread(int i)
{
	pid_t f;
	int ret;
	int num =0;
	char fname[64], param1[16], param2[16];
	
	memset(fname,0,64);

	if( PM_info[i].index > 0 )
	{
		f = fork();
		if( f == 0 ) // if child
		{
			sprintf(fname,APP_DIR"%s",PM_info[i].exe);
			sprintf(param1,"interface%d#",PM_info[i].index);
			sprintf(param2,"%d",PM_info[i].index);

			if( PM_info[i].param1[0] != 0 )
				num = 1;
			if( PM_info[i].param2[0] != 0 )
				num = 2;

			dprintf("\t%s %s %s %s %s\n",fname,param1,param2,
				PM_info[i].param1,PM_info[i].param2);

			if( num == 0 )
				ret = execl(fname,param1,param2,NULL);
			else if( num == 1 )
				ret = execl(fname,param1,param2,PM_info[i].param1,NULL);
			else if( num == 2 )
				ret = execl(fname,param1,param2,PM_info[i].param1,PM_info[i].param2,NULL);
			else
				ret = -1;

			exit(EXIT_SUCCESS);
		}
		else if( f < 0 )
		{
			dprintf("Error: Create Thread %d!!\n",i+1);
			exit(EXIT_FAILURE);
		}
		
		dprintf("Create Thread [Interface %d]\n",i+1);
	}

	return 1;
}

static int kill_adaptor(int i)
{
	pid_t f;
	int ret;
	char fname[64];

	memset(fname,0,64);

	f = fork();
	if( f == 0 )
	{
		sprintf(fname,"adaptor%d#",i);
		dprintf("KILL ==> %s\n",fname);
		execl("/bin/killall","killall",fname,NULL);
		exit(EXIT_SUCCESS);
	}

}

static int kill_thread(int i)
{
	pid_t f;
	int ret;
	char fname[64];

	memset(fname,0,64);

	f = fork();
	if( f == 0 )
	{
		sprintf(fname,"interface%d#",i);
		dprintf("KILL ==> %s\n",fname);
		execl("/bin/killall","killall",fname,NULL);
		exit(EXIT_SUCCESS);
	}

}

static int create_threads(int thread_no)
{
	int i;

	if( thread_no < 0 ) return 0;
	if( thread_no > MAX_INTERFACE ) return 0;
	

#ifndef RESPAWN
	if( thread_no == 0 )
	{
		for( i = 0; i < MAX_INTERFACE; i++)
		{
			create_thread(i);
			wait(NULL);
			create_adaptor(i);
			wait(NULL);
		}
	}
	else
	{
		create_thread(0);
		wait(NULL);
	}
#endif
	
	return 1;
}

static int kill_threads(int version)
{
	int i;

	if( version == ACONIS_2000 ) return 1;
	if( version == ACONIS_RIO ) return 1;

	for ( i = 0; i < MAX_INTERFACE; i++)
	{
		kill_thread(i+1);
		wait(NULL);
		kill_adaptor(i+1);
		wait(NULL);
	}

	return 1;
}

static int manage_threads()
{
#ifdef RESPAWN 
	int i, proc_no;
	pid_t app_pid;
	struct proc_dat proc_dat[100];
	char fname[128];

	proc_no = make_proc_info(proc_dat, 100);

	for( i = 0; i < MAX_INTERFACE; i++)
	{
		if( PM_info[i].index > 0 )
		{
			sprintf(fname,"interface%d",PM_info[i].index);
			app_pid = search_app(proc_dat, proc_no, fname);
			if(app_pid == 0)
			{
				dprintf("Restart Thread [Interface %d]\n",PM_info[i].index);
				create_thread(i);
				wait(NULL);
			}

			if( PM_info[i].adapt[0] != 0 )
			{
				sprintf(fname,"adaptor%s",PM_info[i].adapt_param);
				app_pid = search_app(proc_dat, proc_no, fname);
				if(app_pid == 0)
				{
					create_adaptor(i);
					wait(NULL);
				}
			}
		}
	}
#endif

	return 1;
}

#ifndef COM
redisContext *Redis_Ptr;
void redis_connect(void)
{
    unsigned int j, isunix = 0;
    redisReply *reply;
    redisContext *c;
    const char *hostname = "127.0.0.1";

    int port = 6379;

    struct timeval timeout = { 1, 500000 }; // 1.5 seconds
isunix = 0;
    if (isunix) {
        c = redisConnectUnixWithTimeout(hostname, timeout);
    } else {
        c = redisConnectWithTimeout(hostname, port, timeout);
    }   

    Redis_Ptr = c;
    if (c == NULL || c->err) {
        if (c) {
            printf("Connection error: %s\n", c->errstr);
            redisFree(c);
        } else {
            printf("Connection error: can't allocate redis context\n");
        }
        exit(1);
    } 
}

int redis_test(void)
{
    unsigned int j, isunix = 0;
    redisContext *c;
    redisReply *reply;
    const char *hostname = "127.0.0.1";

    int port = 6379;

    struct timeval timeout = { 1, 500000 }; // 1.5 seconds
isunix = 0;
    if (isunix) {
        c = redisConnectUnixWithTimeout(hostname, timeout);
    } else {
        c = redisConnectWithTimeout(hostname, port, timeout);
    }   

    if (c == NULL || c->err) {
        if (c) {
            printf("Connection error: %s\n", c->errstr);
            redisFree(c);
        } else {
            printf("Connection error: can't allocate redis context\n");
        }
        exit(1);
    }

    /* PING server */
    reply = redisCommand(c,"PING");
    printf("PING: %s\n", reply->str);
    freeReplyObject(reply);

    /* Set a key */
    reply = redisCommand(c,"SET %s %s", "foo", "hello world");
    printf("SET: %s\n", reply->str);
    freeReplyObject(reply);

    /* Set a key using binary safe API */
    reply = redisCommand(c,"SET %b %b", "bar", (size_t) 3, "hello", (size_t) 5);
    printf("SET (binary API): %s\n", reply->str);
    freeReplyObject(reply);

    /* Try a GET and two INCR */
    reply = redisCommand(c,"GET foo");
    printf("GET foo: %s\n", reply->str);
    freeReplyObject(reply);

    reply = redisCommand(c,"INCR counter");
    printf("INCR counter: %lld\n", reply->integer);
    freeReplyObject(reply);
    /* again ... */
    reply = redisCommand(c,"INCR counter");
    printf("INCR counter: %lld\n", reply->integer);
    freeReplyObject(reply);

    /* Create a list of numbers, from 0 to 9 */
    reply = redisCommand(c,"DEL mylist");
    freeReplyObject(reply);
    for (j = 0; j < 10; j++) {
        char buf[64];

        snprintf(buf,64,"%u",j);
        reply = redisCommand(c,"LPUSH mylist element-%s", buf);
        freeReplyObject(reply);
    }

    /* Let's check what we have inside the list */
    reply = redisCommand(c,"LRANGE mylist 0 -1");
    if (reply->type == REDIS_REPLY_ARRAY) {
        for (j = 0; j < reply->elements; j++) {
            printf("%u) %s\n", j, reply->element[j]->str);
        }
    }
    freeReplyObject(reply);

    /* Disconnects and frees the context */
    redisFree(c);

    return 0;
}
#endif

int main(int argc, char **argv)
{
	int i, state, retries = 0;
	int mode, last_mode=IDLE;
	int version;

#ifndef COM
	xml_address_data_init();
	printf("xml_address_data_init();\n");
#endif
#ifdef COM
	version = MLS2;
#else
	version = ACONIS_DS;

	if( argc == 2 )
	{
		if( strcmp(argv[1],"-2000") == 0 ||
			strcmp(argv[1],"-2K") == 0 )
			version = ACONIS_2000;
		else if( strcmp(argv[1],"-RIO") == 0 ||
			strcmp(argv[1],"-RTU") == 0 )
			version = ACONIS_RIO;
	}
#endif

#if 0
	state = mkfifo("/tmp/"APP_NAME,S_IRUSR);
	if( state < 0 )
	{
		dprintf("Already executed!!!!\n");
		unlink("/tmp/"APP_NAME);
		return -1;
	}
#endif
#ifndef COM
//redis_test();
redis_connect();
#endif

	make_daemon();

	printf("\n-------------- HiSCM IF MANAGER ---------------\n");
	open_logs(1);
	dprintf("--------------- HiSCM IF Manager Start ---------------\n");

	if( version == ACONIS_2000 ) { dprintf("System Version: ACONIS-2000\n"); }
	else if( version == ACONIS_DS ) { dprintf("System Version: ACONIS-DS\n"); }
	else if( version == ACONIS_RIO ) { dprintf("System Version: Remote IO\n"); }
    else if( version == MLS2 ) { dprintf("System Version: MLS2\n"); }
	else { dprintf("System Version: Not Defined Version\n"); }
	
	db_update = 0;

	if( open_link(version) > 0 )
		mode = INITIALIZE;
	else
		return -1;

	while(1)
	{
		switch(mode)
		{
		case INITIALIZE:
			 initial(version);
#ifdef  COM			
			 InitTaskCanVariables();
#endif
#ifndef COM
		case OPEN_DB:
			mode = (open_db(version)?READ_DB:IDLE);
			break;
#endif
		case READ_DB:
			dprintf("READ_DB\n");
			mode = (read_db(version)?CLOSE_DB:IDLE);
			break;
		case CLOSE_DB:
			dprintf("CLOSEDB\n");
			close_db();
			script_run();
			mode = KILL_THREAD;
			break;	
		case PORT_OPEN:
			dprintf("PORT OPEN\n");
			mode = (port_open()?CREATE_THREAD:PORT_OPEN);
			break;
		case PORT_CLOSE:
			dprintf("PORT CLOSE\n");
			mode = (port_close()?PORT_OPEN:PORT_CLOSE);
			break;				
		case CREATE_THREAD:
			dprintf("CREATE_THREAD\n");
			//printf("\n-------------- Create THREAD ---------------\n");
			mode = (create_threads(0)?MANAGE_THREAD:CREATE_THREAD);
			break;
		case KILL_THREAD:
			dprintf("KILL_THREAD\n");
			//printf("\n-------------- Kill THREAD ---------------\n");
			mode = (kill_threads(version)?PORT_CLOSE:KILL_THREAD);
			break;
		case MANAGE_THREAD:
			dprintf("MANAGE_THREAD\n");
			//printf("\n-------------- Manage THREAD ---------------\n");
			manage_threads();
			usleep(100000);
		case IDLE:
			if( db_change(version) )
			{
				dprintf("DB CHANGE!!\n");
				mode = INITIALIZE;
			}
			else
				sleep(5);
			break;
		}

		if( mode < MANAGE_THREAD )
		{
			if( mode == last_mode )
				retries++;

			if( retries > 5 )
				mode = IDLE;
			usleep(100000);
		}
        usleep(100000);
		last_mode = mode;
	}

	return -1;
}

