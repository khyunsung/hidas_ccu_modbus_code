/*
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>

#include <sys/timeb.h>
#include <sys/utsname.h>
#include <sys/resource.h>
#include <sys/param.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <pthread.h>

#include <asm/types.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>

//#include "../../include/common.h"
//#include "hindhs.h"
#include "shell.h"
#include "log.h"
#include "utils.h"

#define POST_FILE	"/tmp/post"

//#define	DEBUG
#ifndef	DEBUG
	#define	dprintf(format, args...)
	#define	tprintf(format, args...) printf( "%s:%d:" format "\n", __FUNCTION__, __LINE__ , ## args)
#else
	#define	dprintf(format, args...) printf("%s:%d:" format "\n", __FUNCTION__, __LINE__ , ##	args)
	#define	tprintf(format, args...) printf( "%s:%d:" format "\n", __FUNCTION__, __LINE__ , ## args)
#endif	// DEBUG

#define FALSE	0
#define TRUE	1
int debug = FALSE;

int DebugSockFd;


#define SOCK_SNBUF	8192
struct client_info{
	int fd;
	int method;
	char cmd[SOCK_SNBUF];
	char filename[256];
	int fsize;
	int cpos;		// cmd position
	int ppos;		// parser position
	int status;
	char *header_line;
	char *header_end;
	char *modified_since;
	char *modified;
	char *content_type;
	char *content_length;
	char *header_user_agent;
	char *cookie;
};

enum{
	READ_HEADER=0,
	ONE_CR,
	ONE_LF,
	TWO_CR,
	BODY_READ,
	BODY_WRITE,
	WRITE,
	PIPE_READ,
	PIPE_WRITE,
	DONE,
	DEAD
};

enum{
	M_GET=1,
	M_HEAD,
	M_POST
};

#define	CMDF_REFRESH	0x01
#define	CMDF_DEBUG		0x02
#define	CMDF_HIDDEN		0x04
#define	CMDF_FILE		0x08
#define	CMDF_MIME		0x10


struct mime{
	char *name;
	unsigned char type;
};

//enum mime_type{
//	MIMET_TEXT = 0,
//	MIMET_JPEG = 1,
//	MIMET_GIF = 2,
//	MIMET_BIN = 3,
//	MIMET_PNG = 4,
//	MIMET_UK
//};

//Keep-Alive
int refresh_time = 0;
static char HTTP_HEADER[] = "HTTP/1.1 200 OK\r\nDate: Mon, 09 Feb 2004 10:12:00 GMT\r\n" \
				"Server: HICM\r\nConnection: close\r\nTransfer-Encoding: chunked\r\n" \
				"Content-Type: text/html; charset=EUC-KR\r\n\r\n%x\r\n";

static char HTTP_MIME_HEADER[] = "HTTP/1.1 200 OK\r\nDate: Mon, 09 Feb 2008 10:12:00 GMT\r\n" \
				"Server: HICM\r\nConnection: close\r\n" \
				"Content-Length: %d\r\nContent-Type: %s\r\n";

static char HTTP_MIME_NOCACHE_HEADER[] = "HTTP/1.1 200 OK\r\nDate: Mon, 09 Feb 2008 10:12:00 GMT\r\n" \
				"Server: HICM\r\nConnection: close\r\n" \
				"Cache-Control: no-cache\r\n" \
				"Content-Length: %d\r\nContent-Type: %s\r\n";

static char HTTP_MIME_NM_HEADER[] = "HTTP/1.1 304 Not Modified\r\nDate: Mon, 09 Feb 2004 10:12:00 GMT\r\n" \
				"Server: HICM\r\nConnection: close\r\n" \
				"ETag: \"000000-00-00000001\"\r\n\r\n";

static char HTTP_NOTFOUND_HEADER[] = "HTTP/1.0 404 Not Found\r\n" \
				"Content-Length: 0\r\n" \
				"Connection: close\r\n\r\n";

static char HTML_HEADER[] = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\">\n" \
				"<html>\r\n\t<head>\r\n\t<title>*** HIPCS ***</title>\r\n\t</head>\n" \
				"<style>\nh3{color: fuchsia;}a:link {color:#blue;font-style:bold;text-decoration:none}\n " \
				"a:visited {color:#1111aa;text-decoration:none}\n " \
				"a:active {color:#1111aa;text-decoration:none}\n " \
				"a:hover {color:red;text-decoration: none; }\n " \
				"table{border-collapse:collapse;} </style>\n" \
				"<script type='text/javascript' src='pcu.js'></script>\n" \
				"<body bgcolor=silver text=#000000 font=\"courier new\" background=\"bg.png\">\r\n<pre>";

static char HTML_TAILER[] = "</pre>\r\n</body>\r\n</html>\r\n\r\n0\r\n\r\n";

static char DOTIMG[77] = {
	0x47,0x49,0x46,0x38,0x37,0x61,0x01,0x00,0x01,0x00,0xb3,0x00,0x00,0x00,0x00,0x00,
	0x80,0x00,0x00,0x00,0x80,0x00,0x80,0x80,0x00,0x00,0x00,0x80,0x80,0x00,0x80,0x00,
	0x80,0x80,0xc0,0xc0,0xc0,0x80,0x80,0x80,0xff,0x00,0x00,0x00,0xff,0x00,0xff,0xff,
	0x00,0x00,0x00,0xff,0xff,0x00,0xff,0x00,0xff,0xff,0xff,0xff,0xff,0x2c,0x00,0x00,
	0x00,0x00,0x01,0x00,0x01,0x00,0x00,0x04,0x02,0x10,0x44,0x00,0x3b};


//static int ShHexDataDisp(int count, char *buffer, char *str);

char shbuff[MAX_SHBUFF];
extern struct command cmd_list[];
static time_t ShellMasic;
extern int DebugSockFd;
////////////////////////////
/// function command format
////////////////////////////
#ifdef func_
static int func_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;

	return ret;
}
#endif

void change_shell_masic()
{
	time(&ShellMasic);
}

int help_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0, i=1;
	struct command *pcmd;

	shprintf("Command Lists...\r\n");

	for(pcmd=cmd_list;pcmd->cmd;pcmd++){
		if(pcmd->flag & CMDF_HIDDEN)	continue;
		if(pcmd->func == NULL)	continue;

		shprintf("\r\n%d. ", i++);
		if(type)	shprintf("<a href=\"%s\">", pcmd->cmd);
		shprintf(pcmd->cmd);
		if(type)	shprintf("</a>");
		shprintf(" : ");
		shprintf(pcmd->help);
		shprintf("\r\n   -> Usage :%s\r\n", pcmd->usage);
	}
	shprintf("\r\n");

	return ret;
}
static char *get_mime_type(char *mime)
{
	char *ptr;
	unsigned int exti;
	ptr = strrchr(mime, '.');
    if (!ptr || *ptr++ == '\0')
        return "text/html";

	exti = 0;
	while(1){
		exti <<= 8;
		exti |= *ptr++;
		if(*ptr == '\0')	break;
	}

	switch(exti){
	case 0x006a7067:	// jpg
	case 0x006a7065:	// jpe
	case 0x6a706567:	// jpeg
		return "image/jpeg";
	break;
	case 0x0068746d:	// htm
	case 0x68746d6c:	// html
		return "text/html";
	break;
	case 0x00676966:	// gif
		return "image/gif";
	break;
	case 0x00706e67:	// png
		return "image/png";
	break;
	case 0x0062696e:	// bin
	case 0x00646d73:	// dms
	case 0x006c6861:	// lha
	case 0x00657865:	// exe
	case 0x0000736f:	// so
	case 0x00646c6c:	// dll
		return "application/octet-stream";
	break;
	case 0x00786d6c:	// xml
		return "text/xml";	// "text/xml"
	break;
	case 0x00637376:	// csv
		return "text/csv";	// "text/xml"
	break;
	default:
		return "text/plain";
	break;
	}
}

#define MAXHOSTNAMELEN	64
int http_header(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
	char myhostname[MAXHOSTNAMELEN+1];

	gethostname(myhostname, sizeof(myhostname));
	shprintf("</pre><H2><a href=\\ target=\"_blank\">*** %s ***</a></H2>\n", myhostname);

	shprintf(".</H3>");
	shprintf("<img src=\"logo.png\" style=\"position: absolute;right:30px; top:30px\">\n");

	return ret;
}

int http_command(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
	int group = 0;
	int cmd = 0;
	struct command *pcmd;

	shprintf("<script language=\"JavaScript\">\n<!--\n");
	shprintf("ie4=((navigator.appName == \"Microsoft Internet Explorer\") && (parseInt(navigator.appVersion) >= 4))\n\n");
	shprintf("function hidestatus(){window.status=\"\";return true;}");
	shprintf("if(document.layers)document.captureEvents(Event.MOUSEOVER | Event.MOUSEOUT)\n");
	shprintf("document.onmouseover=hidestatus\n");
	shprintf("document.onmouseout=hidestatus\n\n");
	shprintf("function openClose(X){\n");
	shprintf("if(ie4){\n");
	shprintf("var ray = new Array(1,3,5,7,9,11,13,15,17,19);\n");
	shprintf("if(ctable.rows[ray[X-1]].style.display == \"\"){\n");
	shprintf("ctable.rows[ray[X-1]].style.display = \"none\";\n");
	shprintf("document.cookie = "" + ctable.rows[ray[X-1]].id + \"=hidden;\"\n");
	shprintf("} else{\n");
	shprintf("ctable.rows[ray[X-1]].style.display = \"\";\n");
	shprintf("document.cookie = "" + ctable.rows[ray[X-1]].id + \"=hidden;\"}\n");
	shprintf("window.event.cancelbubble = true;\n");
	shprintf("}}\n\n");
	shprintf("function mi(it,txt){\nit.style.background='#8fbc8f'\ndefaultStatus=txt\n}\n");
	shprintf("function mo(it){\nit.style.background='silver'\ndefaultStatus=\"\"\n}\n");
	shprintf("//-->\n</script>\n");

	shprintf("<b>Command Lists...\r\n");
	shprintf("<table border=\"0\" width=\"95%%\" id=ctable>");

	for(pcmd=cmd_list; pcmd->cmd; pcmd++){
		if(pcmd->flag & CMDF_HIDDEN)	continue;
		if((pcmd->flag & CMDF_DEBUG) && (debug == FALSE))	continue;
		if(pcmd->func == NULL){
			if(group == 0){
				group = 1;
			} else{
				shprintf("</td></tr>");
			}
			shprintf("<tr><td onclick=\"openClose(%d);\" onmouseover=\"this.style.cursor='hand'\" >", group);
			shprintf(" %s", pcmd->usage);
			shprintf("</td></tr>");
			shprintf("<tr><td><ul>");
			group++;
			continue;
		}
		if(type)
			shprintf("<li><a href=\"%s\" target=\"detail\" id=\"c%d\" onMouseOver=\"mi(c%d,'%s')\" onMouseOut=\"mo(c%d)\">",
				pcmd->cmd, cmd, cmd, pcmd->help, cmd);
		shprintf(pcmd->cmd);
		if(type)	shprintf("</a>\r\n");
		cmd++;
	}
	shprintf("</td></tr></table></b>");

	return ret;
}

int http_frame(int sfd, char *str, char type)
{
	int	ret	= 0;
	struct command *pcmd;

	shprintf("<html>");
	shprintf("<head>");
	shprintf("<title>*** HiSCM PM ***</title>");
	shprintf("</head>");
	shprintf("<frameset rows=\"110,*\" frameborder=0>");
	shprintf("<frame name=\"header\" scrolling=\"no\" target_frame=\"_blank\" noresize src=\"header\">");
	shprintf("<frameset cols=\"190, *\">");
	shprintf("<frame name=\"command\" scrolling=\"auto\" target_frame=\"detail\" noresize src=\"command\">");
	shprintf("<frame name=\"detail\" scrolling=\"yes\" src=\"help\">");
	shprintf("</frameset>");

	shprintf("<noframes>");
	shprintf("<body bgcolor=\"#FFFFFF\" text=\"#000000\" link=\"#0000FF\" vlink=\"#800080\" alink=\"#FF0000\">");
	shprintf("Command Lists...\r\n");
	for(pcmd=cmd_list;pcmd->cmd;pcmd++){
		if(pcmd->flag & CMDF_HIDDEN)	continue;
		if((pcmd->flag & CMDF_DEBUG) && (debug == FALSE))	continue;

		if(type)	shprintf("<li><a href=\"%s\" target=\"detail\">", pcmd->cmd);
		shprintf(pcmd->cmd);
		if(type)	shprintf("</a>\r\n");
	}
	shprintf("</body>");
	shprintf("</noframes>");
	shprintf("</frameset>");
	shprintf("</html>\r\n\r\n0\r\n\r\n");

	return ret;
}

static int get_args(char *s, char **argv)
{
	int	args = 0;

	if (!s || *s=='\0')	return 0;
	while (args	< MAX_ARGS){
		while((*s=='%')){		// for URL escape char
			s+=3;
		}
		while((*s==' ') ||	(*s=='\r') || (*s=='\n') || (*s=='\t') || (*s=='?') || (*s=='&') || (*s=='='))
			s++;

		if (*s=='\0'){
			argv[args] = 0;
			return args;
		}
		argv[args++] = s;

//		while (*s && (*s!=' ') && (*s!='\r') && (*s!='\n') && (*s!='_') && (*s!='\t') && (*s!='?') && (*s!='&') && (*s!='='))
		while (*s && (*s!=' ') && (*s!='\r') && (*s!='\n') && (*s!='\t') && (*s!='?') && (*s!='&') && (*s!='='))
			s++;
		if (*s=='\0'){
			argv[args] = 0;
			return args;
		}
		*s++ = '\0';
	}
	return args;
}

static char *simple_itoa(unsigned int i)
{
    /* 21 digits plus null terminator, good for 64-bit or smaller ints
     * for bigger ints, use a bigger buffer!
     *
     * 4294967295 is, incidentally, MAX_UINT (on 32bit systems at this time)
     * and is 10 bytes long
     */
    static char local[22];
    char *p = &local[21];
    *p-- = '\0';
    do {
        *p-- = '0' + i % 10;
        i /= 10;
    } while (i > 0);
    return p + 1;
}

/* rfc822 (1123) time is exactly 29 characters long
 * "Sun, 06 Nov 1994 08:49:37 GMT"
 */
static const char month_tab[49] =
    "Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec ";
static const char day_tab[] = "Sun,Mon,Tue,Wed,Thu,Fri,Sat,";
static void rfc822_time_buf(char *buf, time_t s)
{
    struct tm *t;
    char *p;
    unsigned int a;

    t = gmtime(&s);

    p = buf + 28;
    /* p points to the last char in the buf */

    p -= 3;
    /* p points to where the ' ' will go */
    memcpy(p--, " GMT", 4);

    a = t->tm_sec;
    *p-- = '0' + a % 10;
    *p-- = '0' + a / 10;
    *p-- = ':';
    a = t->tm_min;
    *p-- = '0' + a % 10;
    *p-- = '0' + a / 10;
    *p-- = ':';
    a = t->tm_hour;
    *p-- = '0' + a % 10;
    *p-- = '0' + a / 10;
    *p-- = ' ';
    a = 1900 + t->tm_year;
    while (a) {
        *p-- = '0' + a % 10;
        a /= 10;
    }
    /* p points to an unused spot to where the space will go */
    p -= 3;
    /* p points to where the first char of the month will go */
    memcpy(p--, month_tab + 4 * (t->tm_mon), 4);
    *p-- = ' ';
    a = t->tm_mday;
    *p-- = '0' + a % 10;
    *p-- = '0' + a / 10;
    *p-- = ' ';
    p -= 3;
    memcpy(p, day_tab + t->tm_wday * 4, 4);
}


/* I don't "do" negative conversions
 * Therefore, -1 indicates error
 */

static int shell_atoi(char *s)
{
    int retval;
    char *reconv;

    if(s==NULL)	return -1;
    if (!isdigit(*s))
        return -1;

    retval = atoi(s);
    if (retval < 0)
        return -1;

    reconv = simple_itoa(retval);
    if (memcmp(s,reconv,strlen(s)) != 0) {
        return -1;
    }
    return retval;
}

/*
 * Name: month2int
 *
 * Description: Turns a three letter month into a 0-11 int
 *
 * Note: This function is from wn-v1.07 -- it's clever and fast
 */

static int month2int(char *monthname)
{
    switch (*monthname) {
    case 'A':
        return (*++monthname == 'p' ? 3 : 7);
    case 'D':
        return (11);
    case 'F':
        return (1);
    case 'J':
        if (*++monthname == 'a')
            return (0);
        return (*++monthname == 'n' ? 5 : 6);
    case 'M':
        return (*(monthname + 2) == 'r' ? 2 : 4);
    case 'N':
        return (10);
    case 'O':
        return (9);
    case 'S':
        return (8);
    default:
        return (-1);
    }
}

/*
 * Name: modified_since
 * Description: Decides whether a file's mtime is newer than the
 * If-Modified-Since header of a request.
 *

 Sun, 06 Nov 1994 08:49:37 GMT    ; RFC 822, updated by RFC 1123
 Sunday, 06-Nov-94 08:49:37 GMT   ; RFC 850, obsoleted by RFC 1036
 Sun Nov  6 08:49:37 1994         ; ANSI C's asctime() format
 31 September 2000 23:59:59 GMT   ; non-standard

 * RETURN VALUES:
 *  0: File has not been modified since specified time.
 *  1: File has been.
 * -1: Error!
 */
static int modified_since(time_t * mtime, char *if_modified_since)
{
    struct tm *file_gmt;
    char *ims_info;
    char monthname[10 + 1];
    int day, month, year, hour, minute, second;
    int comp;

    ims_info = if_modified_since;
    while (*ims_info != ' ' && *ims_info != '\0')
        ++ims_info;
    if (*ims_info != ' ')
        return -1;

    /* the pre-space in the third scanf skips whitespace for the string */
    if (sscanf(ims_info, "%d %3s %d %d:%d:%d GMT", /* RFC 1123 */
               &day, monthname, &year, &hour, &minute, &second) == 6);
    else if (sscanf(ims_info, "%d-%3s-%d %d:%d:%d GMT", /* RFC 1036 */
                    &day, monthname, &year, &hour, &minute, &second) == 6)
        year += 1900;
    else if (sscanf(ims_info, " %3s %d %d:%d:%d %d", /* asctime() format */
                    monthname, &day, &hour, &minute, &second, &year) == 6);
    /*  allow this non-standard date format: 31 September 2000 23:59:59 GMT */
    /* NOTE: Use if_modified_since here, because the date *starts*
     *       with the day, versus a throwaway item
     */
    else if (sscanf(if_modified_since, "%d %10s %d %d:%d:%d GMT",
                    &day, monthname, &year, &hour, &minute, &second) == 6);
    else {
        fprintf(stderr, "Error in %s, line %d: Unable to sscanf \"%s\"\n",
                __FILE__, __LINE__, ims_info);
        return -1;              /* error */
    }

    file_gmt = gmtime(mtime);
    month = month2int(monthname);

    /* Go through from years to seconds -- if they are ever unequal,
     we know which one is newer and can return */

    if ((comp = 1900 + file_gmt->tm_year - year))
        return (comp > 0);
    if ((comp = file_gmt->tm_mon - month))
        return (comp > 0);
    if ((comp = file_gmt->tm_mday - day))
        return (comp > 0);
    if ((comp = file_gmt->tm_hour - hour))
        return (comp > 0);
    if ((comp = file_gmt->tm_min - minute))
        return (comp > 0);
    if ((comp = file_gmt->tm_sec - second))
        return (comp > 0);

    return 0;                   /* this person must really be into the latest/greatest */
}

static int cookie_parser(struct client_info *req)
{
	char *ptr;
	char buf[256];
	int level=0;

dprintf("COOKIE:%s", req->cookie);

	if(req->cookie == NULL)	return 0;

	strncpy(buf, req->cookie, 256);
	buf[255] = 0;
	ptr = strstr(buf, "ulevel");
dprintf("ulevel:%s", ptr);
	if(ptr == NULL)	return 0;

	ptr = strchr(ptr, '=');
dprintf("ulevel:%s", ptr);
	sscanf(ptr, "=%d", &level);
dprintf("ulevel num:%d", level);
	return level;
}

static int http_cmd(struct client_info *req, int argc, char **argv, char *str, char type)
{
	int	ret	= 0, ret2=0;
	int send, res;
	int	i;
	char *argv2[MAX_ARGS];
	char cmd[128], https[5];
	char fname[256];
	struct command *pcmd2;
	char mime_modify=0;
	struct stat sb;
	int sfd=0, pfd=0;
	char buff[2048];
	time_t cur_time;
	int ulevel=0;
	int no_cache=0;

//int mime_type;

	memset(cmd, 0, sizeof(cmd));
	if(argc>1)
		sscanf(argv[1], "/%s", cmd);

	time(&cur_time);

	if(strcmp(argv[0], "POST")==0){
		dprintf("cmd:%s", cmd);
	}

	for(i=0; i<argc-1; i++){
		strncpy(https, argv[i+1], 4);
		if(strncmp(https, "HTTP", 4)==0)	break;
		argv2[i] = argv[i+1];
dprintf("arg[%d]:%s", i, argv2[i]);
	}

	res = argc;	// just temp
	argc = i;
dprintf("cmd:%s, argc:%d", cmd, argc);
	if(cmd[0]==0 && argc==1){		// default page...
//		strncpy(cmd, "frame", 16);
	}

	if(req->cookie){
		ulevel = cookie_parser(req);
	}

	if(req->modified)	mime_modify = 1;

	// internal commands...
	for(pcmd2=cmd_list; pcmd2->cmd; pcmd2++){	// search command
		if(pcmd2->func == NULL)	continue;
		if(strcmp(cmd, pcmd2->cmd) == 0){
dprintf("command:%s", cmd);
			snprintf(fname, sizeof(fname), "/tmp/_%s", cmd);
			pfd = open(fname, O_WRONLY|O_CREAT, S_IRWXU|S_IRWXG|S_IRWXO);
			if(pfd == -1){
dprintf("pfd:%d", pfd);
				return -1;
			}
			sfd = pfd;
			shprintf("%s", HTML_HEADER);
			ret2 = pcmd2->func(pfd, argc, argv2, str+ret, 1);
			if(ret2<0){
				if(pcmd2->flag & CMDF_NOCACHE)	no_cache = 1;
				close(pfd);
				remove(fname);
				for(;pcmd2->cmd; pcmd2++);
				break;
			}

			if(refresh_time){
				if(pcmd2->flag & CMDF_REFRESH){
					shprintf("\n\n<b><div id='rmsg'>This page will be refreshed in ? sec automatically!</div></b>\n");
					shprintf("<script language=\"JavaScript\">\n<!--\n");
					shprintf("rtime = %d;\n", refresh_time);
					shprintf("window.onload=Refresh\n");
					shprintf("//-->\n</script>\n");
				}
			}
			shprintf("%s", HTML_TAILER);

			close(pfd);

			pfd = open(fname, O_RDONLY);
dprintf("cmd:%s::pfd:%d", cmd, pfd);
			if(pfd < 0)	return -1;

			fstat(pfd, &sb);

			ret = snprintf(shbuff, MAX_SHBUFF, HTTP_HEADER, sb.st_size-7);
			rfc822_time_buf(shbuff+23, cur_time);	// date position : 17+6
			sfd = req->fd;
			send = write(sfd, shbuff, ret);
			res	= 0;
			ret = sb.st_size;
			while(res<ret){
				ret2=read(pfd, buff, 2048);
				send = write(sfd, buff, ret2);
				if(send<=0){
					close(pfd);
					remove(fname);
					return -1;
				}
				res	+= send;
//
			}
			usleep(1);
			close(pfd);
			remove(fname);

			return 0;
		}
	}

	if (!(pcmd2->cmd) && cmd[0]!='\0'){	// command not found -> search mime
		if(mime_modify){
dprintf("MIME Not Modify:");
			ret = 0;
			ret = snprintf(shbuff, MAX_SHBUFF, HTTP_MIME_NM_HEADER);
			rfc822_time_buf(shbuff+33, cur_time);	// date position : 27+6

			sfd = req->fd;
			send = write(sfd, shbuff, ret);
			return 0;
		}

		// dotimg
		if(strcmp(cmd, "dotimg")==0){
//			mime_type = MIMET_GIF;

			ret = 0;
			ret = snprintf(shbuff, MAX_SHBUFF, HTTP_MIME_HEADER, 77, "image/gif");
			rfc822_time_buf(shbuff+23, cur_time);	// date position : 17+6
			ret += snprintf(shbuff+ret, MAX_SHBUFF-ret, "\r\n");
			res = argv[2][0];
			if(res>='a')		res = res - 'a' + 10;
			else if(res>='A')	res = res - 'A' + 10;
			else if(res>='0')	res = res - '0';
			DOTIMG[73] = 0x10 | ((res&0x0e)<<4);
			DOTIMG[74] = 0x44 | (res&0x01);
			memcpy(shbuff+ret, DOTIMG, 77);
			ret += 77;
			sfd = req->fd;
			send = write(sfd, shbuff, ret);
			return 0;
		}

		snprintf(fname, sizeof(fname), "/tmp/%s", cmd);		// xml
		pfd = open(fname, O_RDONLY);

		if(pfd < 0){
#ifdef 	PC_TEST
			snprintf(fname, sizeof(fname), "./www/%s", cmd);
#else
			snprintf(fname, sizeof(fname), "/home/www/%s", cmd);
#endif
			pfd = open(fname, O_RDONLY);
		}
		if(pfd > 0){
			fstat(pfd, &sb);

dprintf("modi_since:%s", req->modified_since);
			if (req->modified_since && !modified_since(&(sb.st_mtime), req->modified_since)) {
dprintf("MIME Not Modify:");
				ret = 0;
				ret = snprintf(shbuff, MAX_SHBUFF, HTTP_MIME_NM_HEADER);
				rfc822_time_buf(shbuff+33, cur_time);	// date position : 27+6

				sfd = req->fd;
				send = write(sfd, shbuff, ret);
				close(pfd);
				return 0;
			}

dprintf("open mime %s:%d", fname, (int)sb.st_size);
			ret = 0;
			if(no_cache)	ret = snprintf(shbuff, MAX_SHBUFF, HTTP_MIME_NOCACHE_HEADER, sb.st_size, get_mime_type(cmd));
			else			ret = snprintf(shbuff, MAX_SHBUFF, HTTP_MIME_HEADER, sb.st_size, get_mime_type(cmd));
			rfc822_time_buf(shbuff+23, cur_time);	// date position : 17+6
			ret2=ret+15;
			ret += snprintf(shbuff+ret, MAX_SHBUFF-ret, "Last-Modified:                              \r\n");
			rfc822_time_buf(shbuff+ret2, sb.st_mtime);

//			ret2=ret+9;
//			ret += snprintf(shbuff+ret, MAX_SHBUFF-ret, "Expires: Mon, 16 Feb 2008 10:12:00 GMT\r\n\r\n");
//			rfc822_time_buf(shbuff+ret2, sb.st_mtime+86400);
			ret += snprintf(shbuff+ret, MAX_SHBUFF-ret, "\r\n");

			sfd = req->fd;
			send = write(sfd, shbuff, ret);

			res	= 0;
			ret = sb.st_size;
			while(res<ret){
				ret2=read(pfd, buff, 2048);
				send = write(sfd, buff, ret2);
				if(send<=0){
					close(pfd);
//					remove(fname);
					return -1;
				}
				res	+= send;
	//				usleep(1);
			}
			close(pfd);

			return 0;
		}
	}

	if(argc<=1 && cmd[0]=='\0'){	// default page...
#define DEFAULT_PAGE_NAME	"/tmp/frame"
		snprintf(fname, sizeof(fname), DEFAULT_PAGE_NAME);
		pfd = open(fname, O_WRONLY|O_CREAT, S_IRWXU|S_IRWXG|S_IRWXO);
		if(pfd == -1){
dprintf("pfd:%d", pfd);
			return -1;
		}
		sfd = pfd;
//				shprintf("%s", HTML_HEADER);
		ret2 = http_frame(sfd, str, 1);
//				shprintf("%s", HTML_TAILER);
		close(pfd);

		pfd = open(fname, O_RDONLY);
dprintf("pfd:%d", pfd);
		if(pfd < 0)	return -1;

		fstat(pfd, &sb);
dprintf("http_frame:%d", (int)sb.st_size-7);
		// -7 : data chunk boundary(2) + end of chunked encoding(3) + end chunk boundary(2)
		ret = snprintf(shbuff, MAX_SHBUFF, HTTP_HEADER, sb.st_size-7);
		sfd = req->fd;
		send = write(sfd, shbuff, ret);
dprintf("send:%d", send);
		res	= 0;
		ret = sb.st_size;
		while(res<ret){
			ret2=read(pfd, buff, 2048);
			send = write(sfd, buff, ret2);
			if(send<=0){
				close(pfd);
				return -1;
			}
			res	+= send;
//				usleep(1);
		}
		usleep(50000);
		close(pfd);
		return 0;
	} else{
dprintf("%s not found...", cmd);
tprintf("%s not found...", cmd);

		ret = 0;
		sfd = req->fd;
		ret = snprintf(shbuff, MAX_SHBUFF, HTTP_NOTFOUND_HEADER);
		send = write(sfd, shbuff, ret);

		return -1;
//
//		shprintf("%d", argc);
//		ret2 = help_cmd(pfd, argc, argv2, str+ret, 1);
//		ret += ret2;
	}

	return ret;
}

static char *to_upper(char *str)
{
    char *start = str;

    while (*str) {
        if (*str == '-')
            *str = '_';
        else
            *str = toupper(*str);

        str++;
    }

    return start;
}

static int process_option_line(struct client_info *req)
{
	char c, *value, *line = req->header_line;

	value = strchr(line, ':');
    if (value == NULL)
        return 0;
    *value++ = '\0';            /* overwrite the : */
	to_upper(line);             /* header types are case-insensitive */
//	printf("OPT:%s\n", line);
	while ((c = *value) && (c == ' ' || c == '\t'))
        value++;

	to_upper(line);

	if (!memcmp(line, "IF_MODIFIED_SINCE", 18) && !req->modified_since)
		req->modified_since = value;
    else if (!memcmp(line, "IF_NONE_MATCH", 14) && !req->modified_since)
    	req->modified = value;
    else if (!memcmp(line, "CONTENT_TYPE", 13) && !req->content_type)
    	req->content_type = value;
    else if (!memcmp(line, "CONTENT_LENGTH", 15) && !req->content_length)
    	req->content_length = value;
    else if (!memcmp(line, "USER_AGENT", 11)) {
        	req->header_user_agent = value;
    }
    else if (!memcmp(line, "COOKIE", 6)) {
        	req->cookie = value;
    }
	return 0;
}

static char hex2bin(char *c)
{
	char dat;
	char ch;

	ch = *c++;
	if(ch>='0' && ch<='9')	dat = ch-'0';
	else if(ch>='a' && ch<='f')	dat = ch-'a'+10;
	else if(ch>='A' && ch<='F')	dat = ch-'A'+10;
	else dat = 0;
	dat<<=4;
	ch = *c++;
	if(ch>='0' && ch<='9')	dat += ch-'0';
	else if(ch>='a' && ch<='f')	dat += ch-'a'+10;
	else if(ch>='A' && ch<='F')	dat += ch-'A'+10;
	else dat = 0;

	return dat;
}

int relocate_file(char *dir, char *name)
{
	int ret;
	char scmd[256];
dprintf("%s", dir);
	if(strncmp(dir, "/tmp", 4) == 0)	// same dir
		return 0;
dprintf("%s", name);

	// remove file
	sprintf(scmd, "rm /%s/%s", dir, name);
dprintf("remove:%s", scmd);
	system(scmd);

	// chmod
	sprintf(scmd, "chmod 755 /tmp/%s", name);
dprintf("chmod:%s", scmd);
	system(scmd);

	// move
//	sprintf(scmd, "mv %s/%s /%s/%s", TMP_DIR, name, dir, name);
	sprintf(scmd, "cp /tmp/%s /%s/%s", name, dir, name);
dprintf("cp:%s", scmd);
	ret = system(scmd);

	sprintf(scmd, "rm /tmp/%s", name);
dprintf("rm:%s", scmd);
	ret = system(scmd);

	if(ret)	return -1;

	return 0;
}

static int process_body(struct client_info *req)
{
	char ch;
	char *check;
	char *check_end;
	char *c, *f;
	char bound[128], blen;
	char dirname[256], filename[256];
	char tmpname[256];
	int clen, plen, fsize;
	int rlen;
	int ret;
	int fd;
	int i;
	char buf[SOCK_SNBUF];
	char fbuf[4096];
	int fcnt;

	clen = shell_atoi(req->content_length);
	memcpy(buf, req->cmd, req->cpos);	// req->cmd 버퍼를 내부 버퍼로 전환하자.

	rlen = req->cpos - req->ppos;		// 남은 크기
	check = buf + req->ppos;		// 현재 포인터
	check_end = buf + req->cpos;	// 데이터의 마지막 포인터

	if(!rlen){
		usleep(500000);
		ret = read(req->fd, buf+req->cpos, SOCK_SNBUF-req->cpos);
		if(ret<0)	return -1;
		req->cpos += ret;
		check_end = buf + req->cpos;
		rlen = req->cpos - req->ppos;
	}

dprintf("content_length:%s", req->content_length);
dprintf("content_type:%s", req->content_type);
	check = strstr(req->content_type, "boundary=");
	if(check){
		check += 9;
		strncpy(bound, check, 128); blen = strlen(bound)-1;
	} else{
		return -1;
	}

	check = buf + req->ppos;
	req->header_line = check;
	req->status = READ_HEADER;
	plen = 0;
	filename[0] = 0;
	sprintf(dirname, "/tmp/");
dprintf("check_length:%d", check_end-check);

	while (plen < clen) {
		switch (req->status) {
		case READ_HEADER:
			if (*check == '\r') {
				req->status = ONE_CR;
				req->header_end = check;
			} else if (*check == '\n') {
				req->status = ONE_LF;
				req->header_end = check;
			}
		break;
		case ONE_CR:
			if (*check == '\n'){
				req->status = ONE_LF;
			} else if (*check != '\r'){
				req->status = READ_HEADER;
			}
		break;
		case ONE_LF:
			/* if here, we've found the end (for sure) of a header */
			if (*check == '\r'){ /* could be end o headers */
				req->status = TWO_CR;
			} else if (*check == '\n'){
				req->status = BODY_READ;
			} else{
				req->status = READ_HEADER;
			}
		break;
		case TWO_CR:
			if (*check == '\n'){
				if(filename[0]){
					req->status = BODY_WRITE;
				} else{
					req->status = BODY_READ;
				}
			} else if (*check != '\r'){
				req->status = READ_HEADER;
			}
		break;

		default:
			break;
		}
		req->ppos++;       /* update parse position */
		check++;
		plen++;

		if (req->status == ONE_LF) {
			*req->header_end = '\0';
			c = strstr(req->header_line, "filename=");
			if(c){
				c += 10;

				f = strrchr(c, '/');
				if(f) c=f+1;
				f = strrchr(c, '\\');
				if(f) c=f+1;

				strncpy(req->filename, c, 256);
				req->filename[strlen(req->filename)-1] = '\0';

				sprintf(filename, "%s", req->filename);
				sprintf(tmpname, "/tmp/%s", filename);
dprintf(">>filename::%s(%d)", filename, strlen(filename));
			}
			req->header_line = check;
		} else if (req->status == BODY_READ) {		// only dir...
			i=0;
			do{
				dirname[i] = *check;
				req->ppos++;       /* update parse position */
				check++;
				plen++;
				if(i < (sizeof(dirname)-2))	i++;
			}while(*check != '\n');

			dirname[i-1] = 0;	// '\r' = NULL
dprintf(">>dirname::%s(%d)", dirname, strlen(dirname));

			req->status = READ_HEADER;
		} else if (req->status == BODY_WRITE){		// file save...
dprintf("POST process...:%s", filename);
			remove(tmpname);
			fd = open(tmpname, O_WRONLY|O_CREAT, S_IRWXU|S_IRWXG|S_IRWXO);
			if(fd < 0)	return -1;

log_access("filesave start");
			fsize = 0;
			fcnt=0;
			while(1){
				ch = *check;
				if(ch == '\r'){
					if(*(check+1)=='\n' && *(check+2)=='-' && *(check+3)=='-'){
						if(!memcmp(check+4, bound, blen)){
							req->status = READ_HEADER;
							break;
						}
					}
				}
				fbuf[fcnt++] = ch;
				if(fcnt >= sizeof(fbuf)){
					write(fd, fbuf, fcnt);
					fsize+=fcnt;
					fcnt = 0;
				}
				plen++;
				check++;
				if(clen == plen){
					break;
				}
				if(check == check_end){	// read more
					if(SOCK_SNBUF-req->cpos <= 0){
						req->cpos = 0;
						check = buf;
					}
					ret = read(req->fd, buf+req->cpos, SOCK_SNBUF-req->cpos);

					if(ret < 0){
						break;
					}
					req->cpos += ret;
					check_end = buf + req->cpos;
				}
			}
			write(fd, fbuf, fcnt);
			fsize+=fcnt;
			close(fd);
			req->fsize = fsize;
log_access("::END...fsize:%d\n", fsize);

			relocate_file(dirname, filename);

			return fsize;
		}
	}

	return 0;
}

static int read_header(struct client_info *req)
{
	char *check;
	char *check_end;
	int bytes;

	check = req->cmd + req->ppos;
	bytes = req->cpos;
	check_end = req->cmd + req->cpos;


    if (!memcmp(req->cmd, "GET ", 4))
        req->method = M_GET;
    else if (!memcmp(req->cmd, "HEAD ", 5))
        /* head is just get w/no body */
        req->method = M_HEAD;
    else if (!memcmp(req->cmd, "POST ", 5))
        req->method = M_POST;
    else {
        return 0;
    }

    req->header_line = check;
    while (check < check_end) {
		switch (req->status) {
		case READ_HEADER:
			if (*check == '\r') {
				req->status = ONE_CR;
				req->header_end = check;
			} else if (*check == '\n') {
				req->status = ONE_LF;
				req->header_end = check;
			}
		break;
		case ONE_CR:
			if (*check == '\n'){
				req->status = ONE_LF;
			} else if (*check != '\r'){
				req->status = READ_HEADER;
			}
		break;
		case ONE_LF:
			/* if here, we've found the end (for sure) of a header */
			if (*check == '\r'){ /* could be end o headers */
				req->status = TWO_CR;
			} else if (*check == '\n'){
				req->status = BODY_READ;
			} else{
				req->status = READ_HEADER;
			}
		break;
		case TWO_CR:
			if (*check == '\n'){
				req->status = BODY_READ;
			} else if (*check != '\r'){
				req->status = READ_HEADER;
			}
		break;

		default:
			break;
		}
		req->ppos++;       /* update parse position */
		check++;

		if (req->status == ONE_LF) {
			*req->header_end = '\0';
            process_option_line(req);
            req->header_line = check;
		} else if (req->status == BODY_READ) {
			if(req->method == M_POST){
				 return process_body(req);
			}
		}
	}

//	if(req->modified) printf("M:%s\n", req->modified);
//	if(req->content_type) printf("T:%s\n", req->content_type);
//	if(req->content_length) printf("L:%s\n", req->content_length);
//	if(req->cookie) printf("COOKIE:%s\n", req->cookie);

	return 0;
}

int OpenDebug(void)
{
	int port = 80;
//	struct linger ling;
	int tmp;
	struct sockaddr_in DebugServerAddr;

	printf(" - Open Debug...");

	DebugSockFd = socket(AF_INET, SOCK_STREAM, 0);		// tcp socket
	if (DebugSockFd < 0){
//		log_error ("debug socket Error\n");
		return (-1);
	}
//	ling.l_onoff  = 0;
//	ling.l_linger = 0;
//	setsockopt(DebugSockFd, SOL_SOCKET, SO_LINGER, (const char *) &ling, sizeof(struct linger));

	tmp = 1;
    setsockopt(DebugSockFd, SOL_SOCKET, SO_REUSEADDR, &tmp, sizeof(tmp));
	fcntl(DebugSockFd, F_SETFL, O_NONBLOCK);
	fcntl(DebugSockFd, F_SETFD, 1);	//	close-on-exec

	memset(&DebugServerAddr, 0, sizeof(struct sockaddr));

	DebugServerAddr.sin_family = AF_INET;
	DebugServerAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	DebugServerAddr.sin_port = htons(port);

	if (bind (DebugSockFd, (struct sockaddr *)&DebugServerAddr, sizeof(struct sockaddr)) < 0){
		port = 8000;
		while(1){
		DebugServerAddr.sin_port = htons(port);
			if (bind (DebugSockFd, (struct sockaddr *)&DebugServerAddr, sizeof(struct sockaddr)) < 0){
				port++;
				continue;
			}
			break;
		}
	}

	printf(" port:%d", port);
	printf(" bind passed\n");
	fflush(stdout);

	listen(DebugSockFd,	16);

	return 0;
}
#include <sys/resource.h>

void *ShellThread(void *arg)
{
	int	i, res;
	int	fd,	clientfd;
	fd_set readfds,	testfds, exceptfds;
	struct sockaddr_in client;
	int	client_len;
	int	nread;
//	char cmd[MAX_DEBUG_CONNECT][8192], str[MAX_SHELL_STRING];
	char str[MAX_SHELL_STRING];

//	int cmdset[MAX_DEBUG_CONNECT], cmdcnt[MAX_DEBUG_CONNECT], cmdidx;
	int cmdidx;
	struct client_info cinfo[MAX_DEBUG_CONNECT];
	struct client_info *pcinfo;
	int	ret, send;
	int	argc=0;
	char *argv[MAX_ARGS];
	struct command *pcmd;
#ifndef	PC_LINUX
	pid_t f;

	OpenDebug();

	f = getpid();
	setpriority(PRIO_PROCESS, f, 0);
#else
	struct timeval t;
#endif

//	pid_info(PID_SHELL);

	FD_ZERO(&readfds);
	FD_SET(DebugSockFd,	&readfds);
	for(i=0;i<MAX_DEBUG_CONNECT;i++){
		cinfo[i].fd = 0;
		cinfo[i].cpos = 0;
		cinfo[i].ppos = 0;
		cinfo[i].status = DONE;
	}
	while(1){
//		pid_loop(PID_SHELL);
		testfds	= readfds;
		exceptfds = readfds;
#ifndef	PC_LINUX
		res	= select(FD_SETSIZE, &testfds, (fd_set*)0, (fd_set*)&exceptfds, (struct timeval*)0);
#else
		t.tv_sec = 10;
		t.tv_usec = 0;
		res	= select(32, &testfds, (fd_set*)0, (fd_set*)0, &t);
#endif
		if(res < 0){
//			log_access("Shell error : bad select!!!");
//			log_error("Shell error : bad select!!!");
//			log_access("Shell shutdown : clear & retry");
//			log_error("Shell shutdown : clear & retry");
			FD_ZERO(&readfds);
			FD_SET(DebugSockFd,	&readfds);
			sleep(1);
			continue;	//return;
		}


		for(fd=0; fd<FD_SETSIZE; fd++){
			if(FD_ISSET(fd,	&exceptfds)){
//log_access("excep %d", fd);
			}
			if(FD_ISSET(fd,	&testfds)){
				if(fd == DebugSockFd){
					client_len = sizeof(client);
					clientfd = accept(DebugSockFd, (struct sockaddr*)&client, (socklen_t*)&client_len);

					for(i=0; i<MAX_DEBUG_CONNECT; i++){
						if(cinfo[i].fd == 0){
							memset(&cinfo[i], 0, sizeof(struct client_info));
							cinfo[i].fd = clientfd;
							break;
						}
					}

					if(i == MAX_DEBUG_CONNECT){
dprintf("MAX Connection!!!");
						ret = sprintf(str, "Sorry Max Debug connection!!!");
						res	= 0;
						while(res<ret){
							send = write(clientfd, str+res, ret-res);
							if(send<0)	break;
							res	+= send;
						}
						close(clientfd);
						continue;
					}
					FD_SET(clientfd, &readfds);
dprintf("adding	client %d", clientfd);
//log_access("add %d",clientfd);
				} else{
					ioctl(fd, FIONREAD, &nread);
					if(nread ==	0){
//log_access("close %d",fd);
						close(fd);
						FD_CLR(fd, &readfds);
						for(i=0; i<MAX_DEBUG_CONNECT; i++){
							if(cinfo[i].fd == fd){
								cinfo[i].fd = 0;
								break;
							}
						}
dprintf("removing client %d", fd);
					} else{
						for(i=0; i<MAX_DEBUG_CONNECT; i++){
							if(cinfo[i].fd == fd){
								break;
							}
						}

						if(i==MAX_DEBUG_CONNECT)	continue;
						cmdidx = i;
//						ret	= read(fd, &cmd[cmdidx][cmdcnt[cmdidx]], sizeof(cmd[cmdidx])-cmdcnt[cmdidx] );
						pcinfo = &cinfo[cmdidx];
						ret	= read(fd, &pcinfo->cmd[pcinfo->cpos], SOCK_SNBUF-pcinfo->cpos );
						pcinfo->cpos += ret;
//						cmd[cmdidx][cmdcnt[cmdidx]]='\0';
dprintf("recv client %d(%d) - idx:%d", fd, ret, cmdidx);
//dprintf("MSG: %s", pcinfo->cmd);

						read_header(pcinfo);
dprintf("read_header");
/*
						for(i=0;i<pcinfo->cpos;i++){
							if(pcinfo->cmd[i] == '\n')	break;
						}
						if(i>=pcinfo->cpos)	continue;
						pcinfo->cpos = 0;
*/
						argc = get_args(pcinfo->cmd, argv);
						if(argc<1) continue;

						if(strcmp(argv[0], "GET")==0 || strcmp(argv[0], "POST")==0){
							http_cmd(pcinfo, argc, argv, str, 0);
							continue;
						}

						for(pcmd=cmd_list;pcmd->cmd;pcmd++){
dprintf("\"%s\"-\"%s\"(%d)::%p::%p", argv[0], pcmd->cmd, strcmp(argv[0], pcmd->cmd), pcmd,cmd_list);
							if(pcmd->cmd==NULL)	continue;
							if(strcmp(argv[0], pcmd->cmd)==0){
dprintf("Run cmd..%s", argv[0]);
								ret	= pcmd->func(fd, argc, argv, str, 0);
								break;
							}
						}
dprintf("cmd..%s", argv[0]);
						if (strcmp(argv[0],	"help")==0 || strcmp(argv[0], "?")==0){
							ret	= help_cmd(fd, argc, argv, str, 0);

						} else if (!(pcmd->cmd)){
							ret	= sprintf(str, "Unknown	command	: %s\r\n", argv[0]);
						}
					}
				}
			}
		}
	}
}


