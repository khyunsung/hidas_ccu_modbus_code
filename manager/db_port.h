#ifndef	__DB_PORT_H
#define	__DB_PORT_H

#define CMD_SCM_REQ_DNFILE	0x13
#define CMD_SCM_PTC_DNLOAD	0x14
#define CMD_SCM_REQ_SUCCESS 	0x15
#define CMD_SCM_DNLOAD_END	0x16

enum {
	DB_IDLE,
	SCM_REQ_DNFILE,
	HI_ACK_DNLOAD,
	HI_NACK_DNLOAD,
	SCM_PTC_DNLOAD,
	HI_ACK,
	HI_NACK,
	SCM_REQ_SUCCESS,
	HI_ACK_END,
	HI_NACK_END,
	HI_ACK_APPLY,
	HI_NACK_APPLY
};

typedef struct {
	__u8 src;
	__u8 dst;
	__u8 cmd;
	__u16 ack;
} CMD_HEADER;

void dbport_thread(void *arg);

#endif		// __DB_PORT_H
