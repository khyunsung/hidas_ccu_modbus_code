

#define CANA 0
#define CANB 1
#define 	CAN_BUFF_SIZE		40

//+++ Version
#define	VERSION_MAJOR	2
#define	VERSION_MINOR	0
#define VERSION_MINE	0
#define VERSION_RELS	8
#define HICSR_HWVER 0x00000002
#define HICSR_SWVER (unsigned long)(((unsigned long)VERSION_RELS<< 24 | VERSION_MINE << 16) | (VERSION_MINOR << 8) | VERSION_MAJOR)
//0x00000102

void InitTaskCanVariables();
void *IOCommThread(void* arg);
void *TaskFlowThread(void* arg);

extern HICSR_REG hicsr_reg;
extern HICSR_ATTR hicsr_attr;
#ifdef COM
unsigned char myID;
#endif