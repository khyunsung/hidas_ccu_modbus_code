/***********************************************************************************
  Filename:     taskcan.c

  Description:  these functions help to communicate with himpm program in MPM Card.
				

***********************************************************************************/

/***********************************************************************************
* INCLUDES
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <pthread.h>

#include <asm/types.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>

#include <time.h>
#include <netdb.h>

#include <semaphore.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <net/if.h>

#include "../include/common.h"
#include "../include/hicsr.h"
#include "taskcan.h"
#include "log.h"
#include "card_info.h"

#include <linux/can.h>
#include <linux/can/raw.h>

/***********************************************************************************
* CONSTANTS AND DEFINES
*/

#define CAN_SET_BAUD	0x11001

#ifndef	DEBUG
	#define	dprintf(format, args...) log_access(format, ## args)
#else
	#define	dprintf(format, args...) log_access(format, ## args); printf(format, ## args)
#endif	// DEBUG

const __u32 BitMask[32]={
	0x1, 0x2, 0x4, 0x8, 0x10, 0x20, 0x40, 0x80,
	0x100, 0x200, 0x400, 0x800, 0x1000, 0x2000, 0x4000, 0x8000,
	0x10000, 0x20000, 0x40000, 0x80000, 0x100000, 0x200000, 0x400000, 0x800000,
	0x1000000, 0x2000000, 0x4000000, 0x8000000, 0x10000000, 0x20000000, 0x40000000, 0x80000000
};

enum{
	CANLINE_PRI	= 0,
	CANLINE_SEC	= 1,
	CANLINE_SEL	= 2,
	CANLINE_ALL	= 3
};

HICSR_REG hicsr_reg;
HICSR_ATTR hicsr_attr;

u8 id_wack_flag=0, run_wack_flag=0, crc_wack_flag=0, id_rack_flag=0;
u8 crcrx_flag=0;

/***********************************************************************************
* GLOBAL VARIABLES
*/
unsigned char myID;
extern u8 RunProg;
extern PORT_INFO PORT_info[MAX_INTERFACE];
extern IO_DATA io_data[MAX_INTERFACE + 1];
extern pthread_mutex_t mutex_mem, mutex_cmd;

/***********************************************************************************
* LOCAL VARIABLES
*/
int IoFd[2];
u16 canrx_header[2], canrx_tail[2];
REG_ERR_FRM reg_err_frm[2];
tCardInfo CardInfo;
u16 DEBUG_FLOW;
unsigned char inf_station[17];

/***********************************************************************************
* LOCAL FUNCTIONS
*/

void InitTaskCanVariables()
{
	u16 * cfg;

//	flow_step = 0;
	//canrx_header[0] = 0;	canrx_header[1] = 0;
	//canrx_tail[1] = 0;		canrx_tail[1] = 0;
	//led.sys = 0xff;
	//led.point = 0xffff;
	CardInfo.Addr     = myID;
	inf_station[0]    = myID; //inf status info is sent to device id th station.
	hicsr_reg.runstat = HICSR_RUN_SAMEADDR;
	hicsr_reg.runctrl = HICSR_RUN_SAMEADDR;
	hicsr_reg.cardtype = HICSR_CARDTYPE_COM;
	hicsr_reg.resetctrl = 0x0102;
	hicsr_reg.linestat = HICSR_LINESTAT_PRI | HICSR_LINESTAT_SEC;
	hicsr_reg.linectrl = HICSR_LINECTRL_PRI;
	hicsr_reg.redunstat = HICSR_RED_OK;
	hicsr_reg.redunctrl = HICSR_RED_READY;
	hicsr_reg.diagstat = 0;

//cfg = (u16 *)&usr_flash.io_con;
	//cfg = (u16 *)&usr_flash.io_con.ch[0].u.s.digital.type;
	//crc = crc16(cfg, sizeof(IO_CONFIG));
	//old_crc = crc;
	//if(DEBUG_FLOW) printf("Initial CRC = [%x]\r\n>>", crc);
	//hicsr_reg.cfgcrc = crc;

	hicsr_reg.uptime = 5;

	hicsr_reg.hwversion = (u32) HICSR_HWVER;
	hicsr_reg.swversion = (u32) HICSR_SWVER;
	hicsr_reg.imgpagestat = 0;
	hicsr_reg.imgpagectrl = 0;

/*++++++++++ HICSR Register Attribute +++++*/
	hicsr_attr.bit.runstat = 0; 	//ro
	hicsr_attr.bit.runctrl = 1; 	//rw
	hicsr_attr.bit.cardtype = 0; 	//ro
	hicsr_attr.bit.resetctrl = 1; 	//rw
	hicsr_attr.bit.syncsec = 3;  	// rw 2bit
	hicsr_attr.bit.syncmicro = 3;	//rw 2bit
	hicsr_attr.bit.linestat = 0;
	hicsr_attr.bit.linectrl = 1;
	hicsr_attr.bit.redunstat = 0;
	hicsr_attr.bit.redunctrl = 1;
	hicsr_attr.bit.diagstat = 0;	
	hicsr_attr.bit.cfgcrc = 1;
	hicsr_attr.bit.uptime = 1;
	hicsr_attr.bit.reserved0 = 0;
	hicsr_attr.bit.hwversion = 0;
	hicsr_attr.bit.swversion = 0;
	hicsr_attr.bit.runtime = 0;
	hicsr_attr.bit.reserved1 = 0;
	hicsr_attr.bit.imgpagestat = 0;
	hicsr_attr.bit.imgpagectrl = 1; //rw
	hicsr_attr.bit.reserved2 = 0;

	//io_data.dig.digital = 0;
}

void cpybuf2mem(u16 *mem, u8 *buf, u16 len){
	u16 i;
	for(i=0; i<len/2; i++){
		*mem++ = (*(buf+2*i) | (*(buf+2*i+1) << 8));		//[mem]<---[1][0]
	}	
}

void cpybuf2mem_big(u16 *mem, u8 *buf, u16 len){
	u16 i;
	for(i=0; i<len/2; i++){
		*mem++ = ((*(buf+2*i) << 8) | (*(buf+2*i+1)));		//[mem]<---[0][1]
	}	
}

void cpymem2buf(u8 *buf, u16 *mem, u16 len){
	u16 i;
	for(i=0; i<len/2; i++){
		*(buf+i*2) = *(mem+i) & 0xff;
		*(buf+2*i+1) = (*(mem+i) & 0xff00) >> 8;
	}
}

int iowrite(int line, int dev, struct canmsg_t *msg, int len)
{
	int ret = 0;
	int apos, bpos;
	PCU_AVAIL *pcu;
	struct can_frame frame;
	int loop=100;
	int fd;

	frame.can_id = msg->id;
	frame.can_id &= CAN_EFF_MASK;
	frame.can_id |= CAN_EFF_FLAG;
	
	frame.can_dlc = msg->len;
	memcpy(frame.data, msg->data, 8);

	if(dev!=0){
		dev--;
		//pcu = &PcuCsr.pcu;
		apos = (dev>>5)&3;
		bpos = BitMask[dev&0x1f];
	}
	
	if(line == CANLINE_PRI){			// primary
		fd = IoFd[0];
	} else if(line == CANLINE_SEC){	// secondary
		fd = IoFd[1];
	}
	else if(line==CANLINE_SEL && dev!=0){	// select
		if((pcu->linectrl[apos] & bpos) == 0){		// line 0
			if((pcu->line[0].stat[apos]&bpos) == 0){
				fd = IoFd[0];
				line = 0;
			} else{
				fd = IoFd[1];
				line = 1;
			}
			pcu->linectrl[apos] |= bpos;
		} else{									// line 1
			if((pcu->line[1].stat[apos]&bpos) == 0){
				fd = IoFd[1];
				line = 1;
			} else{
				fd = IoFd[0];
				line = 0;
			}
			pcu->linectrl[apos] &= ~bpos;
		}
	}
	else{					// all
		do{
			ret = write(IoFd[0], &frame, sizeof(frame));
			fd = write(IoFd[1], &frame, sizeof(frame));

			if((ret==sizeof(frame)) || (fd==sizeof(frame))){
				ret=sizeof(frame);
			} else{
				usleep(1000);
				loop--;
				if(loop==0)	break;
				ret = 0;
			}
		}while(ret!=sizeof(frame));

		return ret;
	}
	//printf("Send(line:%d, fd_Sel:%d[1:%d,2:%d]):%d::%04x\n", line,fd,IoFd[0],IoFd[1], loop, msg->id);
	ret = write(fd, &frame, sizeof(frame));
	
	while(ret!=sizeof(frame)){
		usleep(1000);
		ret = write(fd, &frame, sizeof(frame));
		loop--;
		if(loop==0){
			if(line<=1 && dev!=0)
				pcu->line[line].stat[apos]|=bpos;
			break;
		}
	};

fflush(stdout);

	return ret;
}

void EventLog(char *evnt)
{
	u32 prio;
	u32 dir;
	u32 proto;
	u32 dev;
	u16 map;
	u8 dlc;
	u16 * write_add;
	u16 temp_h, temp_l;
	u8 ch, i;
	u16 aa[8];
	struct canmsg_t msg;
	
		dir=HICSR_FROM_IO; prio = HICSR_POINT;  proto=HICSR_SEND; dev=CardInfo.Addr; map=0xe000; dlc=8;

	write_add = (u16 *)aa;

	for(i=0; i<8; i++){
		temp_l = (*(evnt+2*i) & 0xff);
		temp_h = (*(evnt+2*i+1) & 0xff);

		aa[i] = ((temp_h << 8) | temp_l)  ;
	}
/*
		for(ch=0; ch<=1; ch++)
		{
			cpymem2buf((u16 *)&can_tx_buf[ch].dat_buf[0], write_add, dlc);
			CAN_Obj_TXD (ch, &can_tx_buf[ch].dat_buf[0], dlc, prio, dir, proto, dev, map);
		}*/
}
u16 read_cnt;
//u8 flow_step=0;
u16 crc;
u16 down_pack=0;
u16 down_crc, old_crc;
extern u8 TaskFlow_start;
static u8 fFlowStop;

int ChkRegAttr(u16 offset, u16 cnt)
{
	u16 temp_offset, temp_cnt;
	u32 attr_mask;
	u8 i;

	temp_offset = offset>>1; temp_cnt=cnt>>1;

	attr_mask = 0;
	for(i=0; i<temp_cnt; i++)	attr_mask += (1<<i);

	attr_mask <<= temp_offset;

			//printf("attr:%x, attr_mask:%x\n",hicsr_attr.all,attr_mask);
			//printf("offset:%x, cnt:%x\n",offset,cnt);
			//printf("tempoffset:%x, temp_cnt:%x\n",temp_offset,temp_cnt);
			
	if((hicsr_attr.all & attr_mask) == attr_mask) return 0;
	else return -1;
}

void dump_proto(CANID *id)
{
	if(id->dir == 1){
		printf("  IO ");
	} else{
		printf("PCU ");
	}

	switch(id->proto){
	case HICSR_READ:	printf("READ ");	break;
	case HICSR_WRITE:	printf("WRITE ");	break;
	case HICSR_SEND:	printf("SEND ");	break;
	case HICSR_RACK:	printf("RACK ");	break;
	case HICSR_WACK:	printf("WACK ");	break;
	case HICSR_TSYNC:	printf("TSYNC ");	break;
	default:			printf("proto(%d)", id->proto);break;
	}

	switch(id->prio){
	case HICSR_SOE:		printf("SOE");		break;
	case HICSR_POINT:	printf("POINT");	break;
	case HICSR_STATUS:	printf("STATUS");	break;
	case HICSR_CONFIG:	printf("CONFIG");	break;
	}
	printf(" dev:%d(0x%x) map:", id->dev, id->dev);
	switch(id->map){
	case 0xf000:printf("runstat");break;
	case 0xf002:printf("runctrl");break;
	case 0xf004:printf("cardtype");break;
	case 0xf006:printf("resetctrl");break;
	case 0xf008:printf("syncsec");break;
	case 0xf00c:printf("syncmicro");break;
	case 0xf010:printf("linestat");break;
	case 0xf012:printf("linectrl");break;
	case 0xf014:printf("redunstat");break;
	case 0xf016:printf("redunctrl");break;
	case 0xf018:printf("diagstat");break;
	case 0xf01a:printf("cfgcrc");break;
	case 0xf01c:printf("uptime");break;
	case 0xf020:printf("hwversion");break;
	case 0xf024:printf("swversion");break;
	case 0xf028:printf("runtime");break;
	case 0xf030:printf("imgpagestat");break;
	case 0xf034:printf("imgpagectrl");break;
	}
	fflush(stdout);
}

#include <sys/timeb.h>
void dump_msg(int line, struct canmsg_t *msg)
{
	int i;
	struct tm gmt;
	time_t cur_time;

	time(&cur_time);
	localtime_r(&cur_time, &gmt);

	printf("%02d:%02d:%02d L%d:",gmt.tm_hour, gmt.tm_min, gmt.tm_sec, line);
	dump_proto((CANID *)&msg->id);
	printf("::%x(%d)", msg->id, msg->len);

	for(i=0;i<msg->len;i++)
		printf(" %02x", msg->data[i]);
	printf("\n");
	fflush(stdout);
}

/***********************************************************************************
* @fn          TaskFlowThread
*
* @brief       
*
* @param       
*             
* @return      
*/
void *TaskFlowThread(void* arg)
{
	pid_t f;
	u32 prio;
	u32 dir;
	u32 proto;
	u32 dev;
	u16 map;
	u8 dlc;
	u16 * write_add;
	u16 * read_add;	
	u16 * cfg;
	int ch;
	u16 temp;
	static u16 cSameAdd, cRunInit, cRunConfig, cRunConfig_Done ;
	
	struct canmsg_t smsg;
	CANID *sid = (CANID *)&smsg.id;	

	
//while(!TaskFlow_start) 	TSK_sleep(200); // 1sec
//fFlowStop = 0;
cSameAdd = 0; cRunInit = 0; cRunConfig = 0; cRunConfig_Done = 0;

	printf("------------- Begin TaskFlow -------------\n");
	while(1){
		//if(fFlowStop) 	{TSK_sleep(200); continue;} // Flow Stop from ADDR Conflict!!

		switch(hicsr_reg.runctrl){
			case HICSR_RUN_SAMEADDR:
					//printf("State : HICSR_RUN_SAMEADDR\n");
				    /******** Step 0 : Check Address Conflict ******/
					if(!id_rack_flag){ // type write ack //rack를 했으면 초기화된것임으로 추정하는 듯...
					//rack가 안되었기 때문에 ..
					sid->dir=HICSR_FROM_IO; sid->prio = HICSR_POINT;  sid->proto=HICSR_READ;
					sid->dev=CardInfo.Addr; sid->map=0xf000; smsg.len=2;
					temp = 0x0002; // 같은 어드레스에게 runstat read 요청
if(DEBUG_FLOW) printf("Check Address Conflict [%d]...\r\n>>", CardInfo.Addr);
					read_add = &temp;
					dlc = 2;
					cpymem2buf((u16 *)&smsg.data[0], read_add, dlc);
					smsg.len = 1;
					for(ch=0; ch<=1; ch++)
					{				
						iowrite(ch, 0, &smsg, sizeof(struct canmsg_t));
					}
					if(cSameAdd++ > 2){
						 hicsr_reg.runctrl = hicsr_reg.runstat = HICSR_RUN_INIT;
						 cSameAdd = 0;
					}
				}

			break;

			case HICSR_RUN_INIT:
			//printf("State : HICSR_RUN_INIT(run_wack_flag:%d)\n",run_wack_flag);
				/******** Step 0 : Runstat Write ******/
				if(!run_wack_flag){ // runstat write ack	//wack 0이여야 진행 한다.
					if(!(cRunInit++ % 100)){	//100번에 1번 수행
						sid->dir=HICSR_FROM_IO; sid->prio = HICSR_POINT;  sid->proto=HICSR_WRITE;
						sid->dev=CardInfo.Addr; sid->map=0xf000; smsg.len=2;
						write_add = (u16 *)&hicsr_reg.runstat;
	if(DEBUG_FLOW) printf("Runstat TX [%d]...\r\n>>", hicsr_reg.runstat);
						write_add = &temp;
						dlc = 2;
						cpymem2buf((u16 *)&smsg.data[0], write_add, dlc);
						smsg.len = 1;
						for(ch=0; ch<=1; ch++)
						{
							iowrite(ch, 0, &smsg, sizeof(struct canmsg_t));
						}
					}
				}
//				TSK_sleep(99); //500msec
			break;

			case HICSR_RUN_CONFIG:
				//printf("State : HICSR_RUN_CONFIG - crcrx_flag:%d\n",crcrx_flag);
if(DEBUG_FLOW) printf("1\r\n");
if(hicsr_reg.runstat == HICSR_CFG_DONE) goto tx_cfg_done;
				hicsr_reg.runstat = HICSR_RUN_CONFIG;
if(DEBUG_FLOW) printf("2\r\n");
				//CRC Error 체크해서 다운로드 요청 함
				if(!crcrx_flag) break; // wait until config download
					/*
				cfg = (u16 *)&usr_flash.io_con.ch[0].u.s.digital.type;
				crc = crc16(cfg, sizeof(IO_CONFIG));
				down_crc = hicsr_reg.cfgcrc;
if(DEBUG_FLOW) printf("3\r\n");

				if(down_crc != crc){
					if(!(cRunConfig++ % 100)){
						EventLog("CRCErr");
						if(DEBUG_FLOW) printf("Config Mismatch!! ORG[%x] NEW[%x]\r\n", crc, down_crc); 
						break;
					}
				}

if(DEBUG_FLOW) printf("CRC CAL[%x] DOWN[%x]\r\n", crc, down_crc);
				if(old_crc == crc) {EventLog("CRCSame"); goto tx_cfg_done;} // jms
				load_flash(FLASH_CALIB_AR);	
				save_flash(); // Flash Write......
if(DEBUG_FLOW) printf("New Database Applied !!\r\n>>");
EventLog("New DB");
				old_crc = crc;
				cRunConfig_Done = 0;*/
tx_cfg_done:

			if(!(cRunConfig_Done++ % 100))
			{
				hicsr_reg.runstat = HICSR_CFG_DONE;
				//rack가 안되었기 때문에 ..
				sid->dir=HICSR_FROM_IO; sid->prio = HICSR_POINT;  sid->proto=HICSR_WRITE;
				sid->dev=CardInfo.Addr; sid->map=0xf000; smsg.len=2;

				write_add = (u16 *)&hicsr_reg.runstat;
				dlc = 2;
				cpymem2buf((u16 *)&smsg.data[0], write_add, dlc);

				for(ch=0; ch<=1; ch++)
				{				
					iowrite(ch, 0, &smsg, sizeof(struct canmsg_t));
				}
				printf("the start num of INF is %d\n", hicsr_reg.uptime);
									
			}
//TSK_sleep(99); // 500msec
			break;
			case HICSR_RUN_RUN:
				//printf("State : HICSR_RUN_RUN\n");
				hicsr_reg.runstat = HICSR_RUN_RUN;
				crcrx_flag = 0;// jms
			break;
			
			case HICSR_RUN_DIAG:
				//printf("State : HICSR_RUN_DIAG\n");
			break;
			default: 
			break;
			
		}
		usleep(5000); // 5msec	
	}
}

#include <linux/can.h>
#include <linux/can/raw.h>

int openCanSocket(char *interface)
{
	int s;
	struct ifreq ifr;
	int family = PF_CAN, type = SOCK_RAW, proto = CAN_RAW;
	struct sockaddr_can addr;
	
	s = socket(family, type, proto);
	
	addr.can_family = family;
	strncpy(ifr.ifr_name, interface, sizeof(ifr.ifr_name));
	if (ioctl(s, SIOCGIFINDEX, &ifr)) {
		perror("ioctl");
		close(s);
		return -1;
	}
	addr.can_ifindex = ifr.ifr_ifindex;

	if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("bind");
		return -1;
	}	
	
	return s;
}

int OpenIOComm(void)
{
	int family = PF_CAN, type = SOCK_RAW, proto = CAN_RAW;
	struct sockaddr_can addr;
	int cmd;
	int ret;

	IoFd[0] = openCanSocket("can0");
	IoFd[1] = openCanSocket("can1");

	if(IoFd[0] < 0)	return -1;
	if(IoFd[1] < 0)	return -2;
	
	dprintf("socket 1 : %d, socket 2 : %d\n", IoFd[0],IoFd[1]);
	
	return 0;
}

u16 chk_reg_err=0;
u8 runtime_flag=0;

int addressing_canid(struct canmsg_t *msg)
{
	CANID *id;
	id = (CANID *)&msg->id;
	if((id->dev == inf_station[0]) || id->dev == 127)
	{
		return 0;	//it is interface 0.
	}
	else if((id->dev >= inf_station[1]) && (id->dev <= inf_station[MAX_INTERFACE]))
	{
		return id->dev - inf_station[1] + 1;		//because interfaces are started at number 1.
	}
	else
		return -1;
}

static char delay_ovrf = 0;	
void ioc_protocol(int ch, struct canmsg_t *msg, unsigned int inf_num )
{
	CANID *id;
	int slotno;
	SOE_DATA *soe;
	SOEDATA *usoe;
	COMM_INFO *com;
	HICSR_CARD *card;
	PCU_AVAIL *pcu;
	char *dst;
	unsigned int dat=0, crc;
	IO_CONFIG *cfg;
	struct canmsg_t smsg;
	CANID *sid = (CANID *)&smsg.id;	
	char iomsg[10];
	struct tm gmt;
	id = (CANID *)&msg->id;
	
	
	
	u32 prio;
	u32 dir;
	u32 proto;
	u32 dev;
	u16 map;
	u16 * write_add;
	u16 * read_add;
	u16 dlc;
	u16 area;
	u16 offset_add;
	int chk_reg_attr;
	u8 fromdir;
	//u16 temp;
	u8 i;
	char sMap[8];

	int index;
	char head, tail;
	char str[32], temp[8];
	int sendSize, send_len;
	
	fromdir = id->dir;
//+++++ General Data. +++++
	//dir=HICSR_FROM_IO; dev=CardInfo.Addr; map=id->map;
	dir=HICSR_FROM_IO; dev=CardInfo.Addr; map=id->map;
	// 어느 영역에 쓰는건지 확인 함.
	if((map & REG_SPACE_BASE) == REG_SPACE_BASE) area = REG_SPACE_BASE;
	else if((map & PRIV_SPACE_BASE) == PRIV_SPACE_BASE) area = PRIV_SPACE_BASE;
	else area = AVAIL_SPACE_BASE;

	offset_add = map - area;	//영역의 어느 주소에 관련한것인지 offset 추출
	
	switch(id->proto){
		case HICSR_WRITE:
			//printf("\t\n--CAN PROTOCOL : HISCR_WRITE[id:%x, dir:%x, map:%x, %x/%x/%x/%x]\t\n",id->dev,id->dir,id->map,msg->data[0],msg->data[1],msg->data[2],msg->data[3]);
		case HICSR_TSYNC:	//주로 reg영역에 시간정보를 쓰는 것임
			//printf("\t\n--CAN PROTOCOL : HICSR_TSYNC[%x, %x]\t\n",map,area);
			if(fromdir == HICSR_FROM_IO) break;
			prio = HICSR_POINT;  proto=HICSR_WACK;
			switch(area){
				case REG_SPACE_BASE:
					if(offset_add >= (sizeof(HICSR_REG) * 2)){printf("RegAddr Over1 [%x]\r\n", map); sprintf(sMap, "%x", map); EventLog("AdOvWR:");  EventLog(sMap);} //REG영역안에 들어오는 것인가 확인
					
					if(area == REG_SPACE_BASE){
						chk_reg_attr=ChkRegAttr(offset_add, msg->len);
						if(chk_reg_attr < 0){
							//can_stat[ch].all &= (0<<obj); 
							reg_err_frm[ch].err_cnt++;
printf("RegAttr Violation : ");
EventLog("REG Vio");

//Disp_CurrTime();
printf("\r\n\t  AREA ERR : ID=>0x");
printf("%08X", (u32)id);
printf("\t DATA=>0x");

							if(reg_err_frm[ch].err_cnt % 2){
								reg_err_frm[ch].err_frm[0].id = (u32)id;
								write_add = (u16 *)&reg_err_frm[ch].err_frm[0].high;
								cpybuf2mem(write_add, &msg->data[0], msg->len);  // save to register
printf("%04x",(u16)reg_err_frm[ch].err_frm[0].high);
printf("%04x",(u16)reg_err_frm[ch].err_frm[0].low);
							} 
							else{
								reg_err_frm[ch].err_frm[1].id = (u32)id;
								write_add = (u16 *)&reg_err_frm[ch].err_frm[1].high;
								cpybuf2mem(write_add, &msg->data[0], msg->len);  // save to register
//Register Attribute violation..
printf("%04x",(u16)reg_err_frm[ch].err_frm[1].high);
printf("%04x",(u16)reg_err_frm[ch].err_frm[1].low);
							}
							return;
						}
					}
					write_add = ((u16 *)&hicsr_reg.runstat + offset_add/2);
					cpybuf2mem(write_add, &msg->data[0], msg->len);  // save to register
					if((offset_add <= 0x001c) && (offset_add + msg->len >= 0x001d))	//if the start number of inf is updated, renew the array of station number of inf.
					{
                        dprintf("hicsr_reg.uptime is updated to %d\n", hicsr_reg.uptime);					
						for(i = 0; i < MAX_INTERFACE ; i++)
							inf_station[1+i] = hicsr_reg.uptime + i;	//inf_station[0] has status info of infs.
					}					
					if(id->proto == HICSR_TSYNC)
					{
						#ifdef TIME_CHK
						timestmp = ECanaMOTSRegs.MOTS16;
						time_pcu = (hicsr_reg.syncsec-old_sec)*1000000 + (hicsr_reg.syncmicro - old_usec);
						time_err = abs(time_pcu-timestmp);
						old_sec = hicsr_reg.syncsec;
						old_usec = hicsr_reg.syncmicro;
						#endif
						settimeofday((const struct timeval *)&hicsr_reg.syncsec, NULL);
						if(runtime_flag==0) {
							hicsr_reg.runtime = hicsr_reg.syncsec;
							//Disp_BootTime();
							runtime_flag=1; 
						}
					}

					dlc = msg->len; read_add = (u16 *)&dlc;
					
					sid->prio = prio;
					sid->dir = dir;
					sid->proto = proto;
					sid->dev = dev;
					sid->map = map;
					cpymem2buf((u16 *)&smsg.data[0], read_add, 2);
					smsg.len = 1;
					//CAN_Obj_TXD(ch, &can_tx_buf[ch].dat_buf[0], 1, prio, dir, proto, dev, map);					
					iowrite(ch, 0, &smsg, sizeof(struct canmsg_t));
				break;
				case PRIV_SPACE_BASE:
				break;
				case AVAIL_SPACE_BASE:
				
					if(offset_add < 0x0fff){
                       if(offset_add >= (sizeof(IO_DATA) * 2)){printf("RegAddr Over2 [%x]\r\n", map); sprintf(sMap, "%x", map); EventLog("AdOvWA:");  EventLog(sMap);} //jms
					// Pulse Input Write
						write_add = ((u16 *)&io_data[inf_num].INF_mem.INF_data.tag[0] + offset_add/2);	
						//cpybuf2mem(write_add, &msg_buf[ch][obj].dat_buf[0], msg_buf[ch][obj].dlc);  // save to register
						cpybuf2mem(write_add, (u16 *)&msg->data[0], msg->len);  // save to register
//						printf("[%d][%d]\r\n",offset_add, msg_buf[ch][obj].dat_buf[0]);					
						 
					}/*
					else{ // Configuration
						offset_add -= 0x0fff;
						if(offset_add > (sizeof(IO_CONFIG)*2)) {
//							printf("Wrong config address [%x]\r\n", map);
//							EventLog("CnfSzErr");
							printf("RegAddr Over3 [%x]\r\n", map); sprintf(sMap, "%x", map); EventLog("AdOvWC:");  EventLog(sMap); //jms
							break;
						}

						write_add = ((u16 *)&usr_flash + offset_add/2);
						cpybuf2mem(write_add, &msg_buf[ch][obj].dat_buf[0], msg_buf[ch][obj].dlc);  // save to register
						down_pack++;
//						printf("cfg_rx [%d]\r\n",down_pack); 
						if(down_pack == (sizeof(IO_CONFIG)/4)){  // sizeof(IO_CONFIG)=512, 8 byte packet => 256/4
							crcrx_flag = 1;
							printf("CFG RX Completed [%d] Bytes\r\n>>", down_pack*8);
							InitReg(CardInfo.Type);
							down_pack = 0;
						}			
					}*/
					crcrx_flag = 1;
				break;
				default: break;
			}
			
		break;
//---------------- READ Protocol ----------------//		
		case HICSR_READ:
			//printf("\t\n--CAN PROTOCOL : HISCR_READ[id:%x, dir:%x, map:%x, %x/%x/%x/%x]\t\n",id->dev,id->dir,id->map,msg->data[0],msg->data[1],msg->data[2],msg->data[3]);
			prio = HICSR_POINT;  proto=HICSR_RACK;
			//dev=CardInfo.Addr; map=0xf000; dlc=2;

			read_cnt = msg->data[0];
			switch(area){
				case REG_SPACE_BASE:
					read_add = ((u16 *)&hicsr_reg.runstat + offset_add/2);
				break;

				case PRIV_SPACE_BASE:
				break;

				case AVAIL_SPACE_BASE:
				// 이부분이 INF에 특화되어 바뀌어야 함
					//if(offset_add < 0x200){
						read_add = ((u16 *)&io_data[inf_num].INF_mem.INF_data.tag[0] + (offset_add/2));	
					//}
					//else {offset_add-= 0x200; read_add = ((u16 *)&usr_flash.io_con.ch[0].u.s.digital.type + offset_add/2);}

				break;
				default: break;
			}
			sid->prio = prio;
			sid->dir = dir;
			sid->proto = proto;	
			sid->dev = dev;
			sid->map = map;
			cpymem2buf((u8 *)(&smsg.data[0]), read_add, read_cnt);
			smsg.len = read_cnt;
			//printf("--HICSR_READ RESPONSE[type:%x, offset:%x, id:%x, proto:%x, map:%x, %x/%x/%x/%x]\n",*read_add, read_add, sid->id,sid->proto,sid->map,smsg.data[0],smsg.data[1],smsg.data[2],smsg.data[3]);
			iowrite(ch, 0, &smsg, sizeof(struct canmsg_t));		
		break;
//---------------- SEND Protocol ----------------//		
		case HICSR_SEND:
			/*
			printf("\t\n--CAN PROTOCOL : HICSR_SEND[id:%x, inf_num:%d, len:%d, dir:%x, proto:%x, map:%x, %x/%x/%x/%x]\t\n"
			,id->dev,inf_num,msg->len,id->dir,id->proto,id->map,msg->data[0],msg->data[1],msg->data[2],msg->data[3]);
			*/
			if(fromdir == HICSR_FROM_IO) break;

			switch(area){
				case REG_SPACE_BASE:
if(offset_add >= (sizeof(HICSR_REG) * 2)){printf("RegAddr Over4 [%x]\r\n", map); sprintf(sMap, "%x", map); EventLog("AdOvSR:");  EventLog(sMap);} //jms
					
					write_add = ((u16 *)&hicsr_reg.runstat + offset_add/2);
					cpybuf2mem(write_add, &msg->data[0], msg->len);  // save to register
					if((offset_add <= 0x001c) && (offset_add + msg->len >= 0x001d))	//if the start number of inf is updated, renew the array of station number of inf.
					{
dprintf("hicsr_reg.uptime is updated to %d\n", hicsr_reg.uptime);					
						for(i = 0; i < MAX_INTERFACE ; i++)
							inf_station[1+i] = hicsr_reg.uptime + i;	//inf_station[0] has status info of infs.
					}
				break;
				case PRIV_SPACE_BASE:
				break;
				case AVAIL_SPACE_BASE:
					if(map < 0xfff){  // General output
if(offset_add >= (sizeof(IO_DATA) * 2)){printf("RegAddr Over5 [%x]\r\n", map); sprintf(sMap, "%x", map); EventLog("AdOvSA:");  EventLog(sMap);} //jms	
					
					//	write_add = ((u16 *)&io_data.analog[0] + offset_add/2);	//INF에 맞게 바뀌여야 함
						write_add = ((u16 *)&io_data[inf_num].INF_mem.INF_data.tag[0] + offset_add/2);
						cpybuf2mem_big(write_add, &msg->data[0], msg->len);  // save to register
					
						if( inf_num > 0 && inf_num <= MAX_INTERFACE )
						{	
							dprintf("it is in normal inf range\n");
							if( msg->len == 4 || msg->len == 2 )
							{
								index = inf_num;

				pthread_mutex_lock(&mutex_cmd);
								head = PORT_info[index-1].write_head;
								tail = PORT_info[index-1].write_tail;
				pthread_mutex_unlock(&mutex_cmd);

								if( (tail+1)%MAX_WRITE_BUFF == head )
								{
									dprintf("[CMD queue %d] Write buffer is full!!\n",index);
				pthread_mutex_lock(&mutex_mem);
									io_data[0].INF_mem.INF_data.tag[MAX_INTERFACE+index-1] |= htons(0x0002);
				pthread_mutex_unlock(&mutex_mem);

									return;
								}
								else
								{
				pthread_mutex_lock(&mutex_cmd);
									if( msg->len == 2 )	
									{
										PORT_info[index-1].write_buff[tail][0] = inf_num;
										PORT_info[index-1].write_buff[tail][1] = 0x10;
										PORT_info[index-1].write_buff[tail][2] = (offset_add/2)&0x00FF;
										PORT_info[index-1].write_buff[tail][3] = (offset_add/2)>>8;
										PORT_info[index-1].write_buff[tail][4] = 2; // byte order
										PORT_info[index-1].write_buff[tail][5] = 0;
										PORT_info[index-1].write_buff[tail][6] = 4;
										PORT_info[index-1].write_buff[tail][7] = msg->data[1];
										PORT_info[index-1].write_buff[tail][8] = msg->data[0];
										PORT_info[index-1].write_buff[tail][9] = msg->data[3];
										PORT_info[index-1].write_buff[tail][10] = msg->data[2];
										send_len = 11;
									}
									else
									{
										PORT_info[index-1].write_buff[tail][0] = inf_num;
										PORT_info[index-1].write_buff[tail][1] = 0x06;
										PORT_info[index-1].write_buff[tail][2] = (offset_add/2)&0x00FF;
										PORT_info[index-1].write_buff[tail][3] = (offset_add/2)>>8;
										PORT_info[index-1].write_buff[tail][4] = msg->data[1];
										PORT_info[index-1].write_buff[tail][5] = msg->data[0];
										send_len = 6;
									}
					
									PORT_info[index-1].write_length[tail] = send_len;

									PORT_info[index-1].write_tail++;
									PORT_info[index-1].write_tail %= MAX_WRITE_BUFF;
				pthread_mutex_unlock(&mutex_cmd);
				//#if 0
									dprintf("[CMD queue %d] head:%d tail:%d\n",index,head,PORT_info[index-1].write_tail);
									sprintf(str,"\t");
									for(i=0; i<send_len; i++)
									{
										sprintf(temp,"%02X ",PORT_info[index-1].write_buff[tail][i]);
										strcat(str,temp);
									}
									dprintf("%s\n",str);
				//#endif
				pthread_mutex_lock(&mutex_mem);
									if((io_data[0].INF_mem.INF_data.tag[MAX_INF_CH+index-1]& htons(0x0002)) != 0)				
									{
										delay_ovrf++;
										if(delay_ovrf >= 10)
										{				
											io_data[0].INF_mem.INF_data.tag[MAX_INF_CH+index-1] &= ~htons(0x0002);						
											delay_ovrf = 0;
										}
									}
				pthread_mutex_unlock(&mutex_mem);
								}
							}	
						}
					}	
				break;
			}
		break;			
		break;
		case HICSR_RACK:
			//printf("--CAN PROTOCOL : HICSR_RACK\t\n");
			/*
			if(map == 0xf000) {
				EventLog("RedIO Ex"); 	EventLog("FlowStop");
				printf("==> Same Addr racked\r\n>>"); id_rack_flag++;
				cpybuf2mem(&temp, &msg_buf[ch][obj].dat_buf[0], msg_buf[ch][obj].dlc);  // save to register
				if(temp == HICSR_RUN_RUN){
					if(id_rack_flag == 1) CardInfo.Addr += 64;
					printf("==> Other Stat is RUN Mode\r\n>>");
					hicsr_reg.runctrl = hicsr_reg.runstat = HICSR_RUN_INIT;
				}else fFlowStop = 1;
			}		*/	
		break;
		case HICSR_WACK:
			//printf("\t\n--CAN PROTOCOL : HICSR_WACK\t\n");
			if(map == 0xf000) {printf("==> Runstat Type wacked\r\n>>"); run_wack_flag=1;}
			if(map == 0xf004) {printf("==> Card Type wacked\r\n>>"); id_wack_flag=1;}
			if(map == 0xf01a) {printf("==> CFG CRC wacked\r\n>>"); crc_wack_flag = 1;}			
		break;
		default: break;
	}		
	
}

void *IOCommThread(void* arg)
{
	pid_t f;
	int res, ret;
	int service;
	fd_set readfds,	testfds;
	struct canmsg_t msg[32];
	struct can_frame frame;
	int inf_num;
	//arg = arg;

	if(OpenIOComm() < 0){
		log_error("IO Comm Open error!\n");
	} else{
		log_error("IO Comm Open ok!\n");
	}

	//pid_info(PID_RTUR);

	FD_ZERO(&readfds);
	FD_SET(IoFd[0],	&readfds);
	FD_SET(IoFd[1],	&readfds);

	//pthread_create(&IOCAlive_thr, NULL, &IOCommAliveThead, (void *)0);
	//pthread_create(&IOCOutput_thr, NULL, &IOCommOutputThead, (void *)0);

	//ReadPgm();
	
	//printf("-------------  Begin CAN RX  -------------\n");
	while(1){
		testfds = readfds;
		res	= select(FD_SETSIZE, &testfds, (fd_set*)NULL, (fd_set*)NULL, (struct timeval*)NULL);
		/* if(res < 0){
			log_error("IO Comm error : bad select!!!");
			FD_ZERO(&readfds);
			FD_SET(IoFd[0],	&readfds);
			FD_SET(IoFd[1],	&readfds);
			sleep(1);
			continue;
		} khs */
		service = 0;
		if(FD_ISSET(IoFd[0], &testfds)){
			ioctl(IoFd[0], FIONREAD, &res);
			if(res > 0){
//				ret = read(IoFd[0], &msg, sizeof(msg));
				ret = read(IoFd[0], &frame, sizeof(struct can_frame));
				if(ret <= 0){
					log_error("IO Comm error : Read error : %d(%s)!", res, strerror(errno));
				}	
				inf_num = addressing_canid(&frame);
				if(inf_num >= 0)
				{
                    //printf("read 1(inf#%d):%d - %08x(%d)[%02X][%02X][%02X][%02X]\n",inf_num, ret, frame.can_id, frame.can_dlc,frame.data[0],frame.data[1],frame.data[2],frame.data[3]);
					msg[0].id = frame.can_id & CAN_EFF_MASK;
					msg[0].len = frame.can_dlc;
					memcpy(msg[0].data, frame.data, 8);

					ioc_protocol(CANLINE_PRI, &msg[0], inf_num);
				}
//				}
			}
			service = 1;
		}
		if(FD_ISSET(IoFd[1], &testfds)){
			ioctl(IoFd[1], FIONREAD, &res);
			if(res > 0){
//				ret = read(IoFd[1], &msg, sizeof(msg));
				ret = read(IoFd[1], &frame, sizeof(struct can_frame));
				if(ret <= 0){
					log_error("IO Comm error : Read error : %d(%s)!", res, strerror(errno));
				}
				inf_num = addressing_canid(&frame);
				if(inf_num >= 0)
				{				
                    //printf("read 2(inf#%d):%d - %08x(%d)[%02X][%02X][%02X][%02X]\n",inf_num, ret, frame.can_id, frame.can_dlc,frame.data[0],frame.data[1],frame.data[2],frame.data[3]);
					msg[0].id = frame.can_id & CAN_EFF_MASK;
					msg[0].len = frame.can_dlc;
					memcpy(msg[0].data, frame.data, 8);
					ioc_protocol(CANLINE_SEC, &msg[0], inf_num);
				}
//				}
			}
			service = 1;
	    }

		if(service==0){
			log_error("IO Comm error : bad fd set!!");
			usleep(1);
			continue;
		}
	}

	return NULL;
}


