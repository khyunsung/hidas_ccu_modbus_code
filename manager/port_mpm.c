#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <pthread.h>

#include <asm/types.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>

#include "../../include/common.h"
#include "port.h"
#include "modbus.h"
#include "uled.h"
#include "utils.h"
#include "log.h"

//#define	MODBUS_ADDRESS	16999

//#define	DEBUG
#ifndef	DEBUG
	#define	dprintf(format, args...) log_access(format, ## args)
#else
	#define	dprintf(format, args...) log_access(format, ## args); printf(format, ## args)
#endif	// DEBUG

#define PORT_CHECK
#define TCP_NOTICE
//#define RED_LED

#define BASE	3000

extern int port_tx_cnt[MAX_INTERFACE+1], port_rx_cnt[MAX_INTERFACE+1], port_thr_cnt[MAX_INTERFACE+1];
extern __u8  port_alive[MAX_INTERFACE+1];

extern PORT_INFO PORT_info[MAX_INTERFACE];
extern INF_MEM INF_mem[MAX_INTERFACE+1];

extern pthread_mutex_t mutex_mem, mutex_cmd;

char write_bflag[MAX_INTERFACE];

static int open_link(int port_num)
{
	int sockfd;
	struct sockaddr_in server;
	int val, result;

	sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);	
	if(sockfd == -1){
		dprintf("[Port %d] socket open error!!!\n",port_num);
		sleep(1);
		return -1;
	}

	memset( (char*)&server, 0, sizeof (server) );
	server.sin_addr.s_addr = htonl(INADDR_ANY);
	server.sin_family = AF_INET;
	server.sin_port = htons(BASE+port_num);

	result = bind(sockfd, (struct sockaddr*)&server, sizeof(server));
	if(result == -1){
		dprintf("[Port %d] bind retry...\n",port_num);
		result = bind(sockfd, (struct sockaddr*)&server, sizeof(server));
		if(result == -1){
			dprintf("[Port %d] bind error\n",port_num);
			sleep(1);
			return -1;
		}
	}

//	val |= FD_CLOEXEC;
//	fcntl(sockfd,F_SETFD,val);
	fcntl(sockfd,F_SETFD,FD_CLOEXEC);

	return sockfd;
}

static int port_read(__u16 port, __u8 *buff, int len, struct sockaddr_in *interface)
{
	int addrlen;
	if( port <= 0 ) return -1;

	addrlen = sizeof(struct sockaddr_in);
	return recvfrom(port, buff, len, 0, (struct sockaddr *)interface, &addrlen);
}

static int port_write(__u16 port, __u8 *buff, int len, int port_num, struct sockaddr_in *interface)
{
	int i;
	if( port <= 0 ) return -1;

	if(sendto(port, buff, len, 0, (struct sockaddr *)interface, sizeof(struct sockaddr_in)) == len)
	{
#ifdef DEBUG
dprintf("[PORT %d] DOWN [",port_num);
for(i=0;i<len;i++) { dprintf("%02X|",buff[i]); }
dprintf("]\n");
#endif
		return 1;
	}
	return -1;
}

static int cmd_write(__u16 port, __u8 *buff, int len, int port_num)
{
	struct sockaddr_in cmd_interface;
	unsigned short infPort = BASE+BASE+port_num;
	char infIP[32]="127.0.0.1";
	int i;

	if( port <= 0 ) return -1;

	memset(&cmd_interface,0,sizeof(cmd_interface));
	cmd_interface.sin_family=AF_INET;
	cmd_interface.sin_addr.s_addr = inet_addr(infIP);
	cmd_interface.sin_port = htons(infPort);

	if(sendto(port,buff,len,0,(struct sockaddr *)&cmd_interface,sizeof(cmd_interface))
		== len)
	{
#ifdef DEBUG
dprintf("[PORT %d] CMD [",port_num);
for(i=0;i<len;i++) { dprintf("%02X|",buff[i]); }
dprintf("]\n");
#endif
		return 1;
	}
	return -1;
}

static void dequeue_cmd(int port_num, char mode)
{
	char head, tail;

	head = PORT_info[port_num-1].write_head;
	tail = PORT_info[port_num-1].write_tail;
	if( head == tail ) return;

	PORT_info[port_num-1].write_head++;
	PORT_info[port_num-1].write_head %= MAX_WRITE_BUFF;

#if 0
	dprintf("[CMD queue %d] dequeue %s (%d-%d)\n",port_num,
		(mode > 0 )?"success":"with failure",
		PORT_info[port_num-1].write_head,tail);
#endif
}

static void clear_cmd(int port_num)
{
pthread_mutex_lock(&mutex_cmd);
	PORT_info[port_num-1].write_head = 0;
	PORT_info[port_num-1].write_tail = 0;
pthread_mutex_unlock(&mutex_cmd);

	dprintf("[CMD queue %d] clear all (%d-%d)\n",port_num,
	   	PORT_info[port_num-1].write_head,PORT_info[port_num-1].write_tail);
}

static void port_handle(int sock, int port_num)
{
	char rzbuff[DEFAULT_BUFF_SIZE];
	char sdbuff[DEFAULT_BUFF_SIZE];
	int i, len;
	__u8 fcode;
	__u16 address, byte_count;
	__u16 crc,r_crc;
	modbus_res_hdr_ext *pres, *wres;
	struct sockaddr_in interface;
	char head, tail;

	len = port_read(sock, rzbuff, DEFAULT_BUFF_SIZE-16, &interface);
	if(len < 5) return;
	if(len >= DEFAULT_BUFF_SIZE-16) return;

	port_thr_cnt[port_num]++;
	port_rx_cnt[port_num]++;

	crc = CRC16(rzbuff,len-2)&0xFFFF;

#ifdef DEBUG
dprintf("[PORT %d] ==> ",port_num);
for(i=0;i<len;i++) { dprintf("%02X|",rzbuff[i]); }
dprintf("\n");
dprintf(": %02X %02X\n",crc>>8,crc&0x00FF);
#endif
	
	pres = (modbus_res_hdr_ext *)rzbuff;

	r_crc = (rzbuff[len-2]<<8)|rzbuff[len-1];
	if(crc!=r_crc){
		dprintf("[Port %d] Comm CRC error!\n",port_num);
		return;
	}

	if(pres->unit_id != port_num)
	{
		dprintf("[Port %d]Slave address [%d] mismatch!\n",port_num,pres->unit_id);
		return; 
	}

	fcode = pres->function_code;
	address = pres->address;
	byte_count = pres->byte_count;

	port_tx_cnt[port_num]++;

	if( fcode == 0x06 )
	{
	//	dequeue_cmd(port_num,1);
		return;
	}
	if( fcode == 0xAA )
	{
#ifdef TCP_NOTICE
		clear_cmd(port_num);
#endif
		return;
	}

	if(address >= 2048)
	{
		dprintf("[Port %d]Mapping Address [%d] overflow!\n",port_num,address);
		return;
	}

	if(byte_count > 1024)
	{
		dprintf("[Port %d]Point [%d] overflow!\n",port_num,byte_count);
		return;
	}

	if(address + byte_count/2 > 2048)
	{
		dprintf("[Port %d]Data Space [%d] overflow!\n",port_num,address+byte_count/2);
		return;
	}

	if( fcode == 0x0F || fcode == 0x10 )
	{
		wres = (modbus_res_hdr_ext *)sdbuff;
		wres->unit_id = port_num;
		wres->function_code = 0xFF;
		wres->address = address;
		wres->byte_count = byte_count;

pthread_mutex_lock(&mutex_mem);
		if(fcode == 0x10)
			memcpy(wres->data,&INF_mem[port_num].INF_data.tag[address],byte_count);
		if(fcode == 0x0F)
		{
			for(i = 0; i < byte_count/2 ; i++)
				wres->data[i] = htons(INF_mem[port_num].INF_data.tag[address+i]);
		}
pthread_mutex_unlock(&mutex_mem);

		if( port_write(sock, sdbuff, 6+byte_count, port_num, &interface) < 0 )
		{
			dprintf("[Port %d] 0xFF write error!\n",port_num); 
		}
		return;
	}

#ifdef DEBUG
	printf("[PORT %d] ==> slave:%02X address:%04X points:%04X\n",port_num,pres->unit_id,address,byte_count);
#endif

pthread_mutex_lock(&mutex_mem);
	if( fcode == 0x03 ) memcpy(&INF_mem[port_num].INF_data.tag[address],pres->data,byte_count);
pthread_mutex_unlock(&mutex_mem);

//	port_alive[port_num] = 1;
}

void cmdport_thread(void *arg)
{
	int i, sockfd, port_num;
	char head, tail, bflag;

	pthread_detach(pthread_self());

	sleep(5);

	printf("CMD Manager Start\n");

	while(1)
	{
		for(i = 0; i < MAX_INTERFACE ; i++)
		{
			port_num = i+1;

pthread_mutex_lock(&mutex_cmd);
			head = PORT_info[i].write_head;
			tail = PORT_info[i].write_tail;

			if( head != tail )
			{
				if( cmd_write(PORT_info[i].sockfd,PORT_info[i].write_buff[head],
					PORT_info[i].write_length[head],port_num) > 0 )
					dequeue_cmd(port_num,1);
				else
					dequeue_cmd(port_num,0);
			}
pthread_mutex_unlock(&mutex_cmd);

		}

//		usleep(100000); // parameter
		usleep(500000); // parameter
	}
}

void port_thread(void *arg)
{
	struct sockaddr_in server;
	int len, result, optval=1;
	fd_set readfds,	testfds;
	int sockfd;
	int	nread;
	int port_num, index; // from port number 1 // index = port_num-1
	struct timeval timeout;

	index = (int)arg;
	port_num = index+1;
	pthread_detach(pthread_self());

restart:	
	sockfd = open_link(port_num);
	if( sockfd < 0 ) goto restart;

	printf("Port %d Start\n",port_num);
	if( index >= 0 ) PORT_info[index].sockfd = sockfd;
	else
	{
#ifdef RED_LED
		result = rled_init();
		if( result < 0 )
		{
			dprintf("[PORT %d] LED INIT ERROR[1]\n",port_num);
		}
		else if( result == 0 )
		{
			dprintf("[PORT %d] LED INIT ERROR[2]\n",port_num);
		}
		else
		{
			dprintf("[PORT %d] LED INIT\n",port_num);
		}
#endif
	}

	FD_ZERO(&readfds);
	FD_SET(sockfd, &readfds);
	
	while(1){
		testfds	= readfds;

		timeout.tv_sec = 60;
		timeout.tv_usec = port_num*100000;

		if(select(sockfd+1, &testfds, (fd_set*)0, (fd_set*)0, (struct timeval*)&timeout) != 0)
		{
			if(FD_ISSET(sockfd, &testfds)) {
				ioctl(sockfd, FIONREAD,	&nread);
				if(nread > 0) {
					port_alive[port_num] = 1;
					port_handle(sockfd,port_num);
#ifdef PORT_CHECK
					if((port_num > 0)&&((INF_mem[0].INF_data.tag[10+index]&htons(0x0001)) != 0))
					{
						dprintf("[PORT %d] Check - Port Abnormal Cancelled\n",port_num);
pthread_mutex_lock(&mutex_mem);
						INF_mem[0].INF_data.tag[10+index] &= ~htons(0x0001);
pthread_mutex_unlock(&mutex_mem);
#ifdef RED_LED
						rled_off(port_num);
#endif
					}
#endif
				}
			}
		}
		else
		{
#ifdef PORT_CHECK
//			dprintf("[PORT %d] Port Check\n",port_num);
			port_alive[port_num] = 0;

			if( port_num > 0 && port_num <= MAX_INTERFACE )
			{
				printf("[PORT %d] Check - Port Abnormal\n",port_num);
pthread_mutex_lock(&mutex_mem);
				INF_mem[0].INF_data.tag[10+index] |= htons(0x0001);
pthread_mutex_unlock(&mutex_mem);
#ifdef RED_LED
				rled_on(port_num);
#endif
			}

			close(sockfd);
			goto restart;
#endif
		}
	}

	close(sockfd);
}

