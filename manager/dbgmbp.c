#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>

#include <asm/types.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/resource.h>
#include <netinet/in.h>

#include "common.h"
#ifdef COM
#include "hicsr.h"
#endif
#include "scmbp.h"
#include "modbus.h"
#include "utils.h"
#include "log.h"

#define BASE   		 2001

//#define	DEBUG
#ifndef	DEBUG
	#define	dprintf(format, args...) log_access(format, ## args)
#else
	#define	dprintf(format, args...) log_access(format, ## args); printf(format, ## args)
#endif	// DEBUG


extern PORT_INFO PORT_info[MAX_INTERFACE];
#ifdef COM
extern IO_DATA io_data[MAX_INTERFACE + 1];
#else
extern INF_MEM INF_mem[MAX_INTERFACE+1];
#endif

extern pthread_mutex_t mutex_mem, mutex_cmd;

static int open_link(void)
{
	int sockfd;
	struct sockaddr_in server;
	int val, optval, result;

	sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(sockfd < 0){
		dprintf("[DBG] socket open error!!!");
		sleep(1);
		return -1;
	}

   	memset( (char*)&server, 0, sizeof (server) );
	server.sin_addr.s_addr = htonl(INADDR_ANY);
	server.sin_family = AF_INET;
	server.sin_port = htons(BASE);
	
	result = bind(sockfd, (struct sockaddr *)&server, sizeof(server));
	if(result < 0){
		dprintf("Exit due to duplicated execution\n");
		return -1;
	}

//	val |= FD_CLOEXEC;
//	fcntl(sockfd,F_SETFD,val);
	fcntl(sockfd,F_SETFD,FD_CLOEXEC);

	return sockfd;
}

struct sockaddr_in dbg;

static int dbg_read(__u16 port, __u8 *buff, int len)
{
	unsigned int fromSize = sizeof(dbg);
	return recvfrom(port, buff, len-16, 0, (struct sockaddr *)&dbg, &fromSize);
}

static int dbg_write(__u16 port, __u8 *buff, int len)
{
	int i;
	int result;

	if((result = sendto(port,buff,len,0,(struct sockaddr *)&dbg,sizeof(dbg))) == len)
	{
#ifdef DEBUG
printf("[DBG] DBG_UP [%d]\n",len);
#endif
		return 1;
	}
	else
	{
		printf("[DBG] Error: DBG UP (%d)-(%d)!!!\n",result,len);
	}

	return -1;
}

static void dbgmbp(__u16 port, __u8 *recv_buff, int len)
{
	__u8  send_buff[2048];
	modbus_req_hdr *mbhdr;
	modbus_wreq_hdr *wmbhdr;
	modbus_res_hdr *res_mbhdr;

	__u16 address, points, byte_count;
	__u8 interface, fcode, count, *pbyte;
	__u16 crc;

	int sendfd, sendSize, send_len;
	int i, j, index;
	char head, tail;

	mbhdr = (modbus_req_hdr *)recv_buff;
	res_mbhdr = (modbus_res_hdr *)send_buff;

	interface = mbhdr->unit_id;
	fcode = mbhdr->function_code;
	address = htons(mbhdr->address);
	points = htons(mbhdr->points);

#if 0
	printf("[DBG] Modbus Query: %02X %02X %04X %04X\n",interface,fcode,address,points);
	if( fcode == 0x10 )
	{
		wmbhdr = (modbus_wreq_hdr *)recv_buff;
		printf("\tWrite Data: %04X (%d)\n",wmbhdr->data[0],wmbhdr->byte_count);
	}
#endif

	switch(fcode){
	case read_mult_regs:
		byte_count = points*2;

		if(address>=2048){
			printf("[DBG] Modbus address error [%d]\n", address);
			return;
		}

		if(points>512){	
			printf("[DBG] Modbus send size:[%d] Overflow\n", points);
			return;
		}

		if((address+points)>2048){
			printf("[DBG] TotalData[%d] Overflow:Start_addr[%d], Send_size[%d]\n",
				address+points,address,points);
			return;
		}

		res_mbhdr->unit_id = interface;
		res_mbhdr->function_code = fcode;
pthread_mutex_lock(&mutex_mem);
#ifdef COM
		memcpy(res_mbhdr->data,&io_data[interface].INF_mem.INF_data.tag[address],byte_count);
#else
		memcpy(res_mbhdr->data,&INF_mem[interface].INF_data.tag[address],byte_count);
#endif
pthread_mutex_unlock(&mutex_mem);
		res_mbhdr->byte_count = htons(byte_count);
		pbyte = (__u8 *)(send_buff+4+byte_count);

		crc = CRC16((__u8*)res_mbhdr,4+byte_count)&0xFFFF;
		*pbyte++ = crc>>8;
		*pbyte++ = crc&0x00FF;

		sendSize = 6+byte_count;
	break;
	default:
		printf("[DBG] function code error : %d\n", fcode);
		return;
	break;
	}

	sendfd = port;
	
	dbg_write(sendfd, send_buff, sendSize);
}

static void dbg_recv(int port)
{
	__u8  rzbuffer[DEFAULT_BUFF_SIZE];
	__u16 crc, r_crc;
	int len;
	
	int i;
	struct timeval time;
	struct tm *gmt;

	len = 0;
	len = dbg_read(port, rzbuffer, DEFAULT_BUFF_SIZE-16);
	if( len < 5 ) return;
	if( len >= DEFAULT_BUFF_SIZE-16 ) return;

	crc = CRC16(rzbuffer,len-2)&0xFFFF;

#if 0
	gettimeofday(&time,NULL);
	gmt = (struct tm *)localtime(&time.tv_sec);

	printf("%02d:%02d:%02d.%06d ",gmt->tm_hour,
		gmt->tm_min,gmt->tm_sec,time.tv_usec);
		 
	for( i = 0 ; i < len ; i++)
		printf("%02x|",rzbuffer[i]);
	printf("\n");
	printf(": %02X %02X\n",crc>>8,crc&0x00FF);
#endif

	r_crc = (rzbuffer[len-2]<<8|rzbuffer[len-1]);
	if( crc != r_crc )
	{
		printf("[DBG] CRC error!\n");
		return;
	}
		
	dbgmbp(port, rzbuffer, len);
}

void dbgmbp_thread(void *arg)
{
	fd_set readfds,	testfds;
	int sockfd;
	int	nread;
	pid_t my_pid;

	pthread_detach(pthread_self());

restart:
	sockfd = open_link();
	if( sockfd < 0 ) exit(EXIT_FAILURE);

	printf("DBG Port Start\n");
	FD_ZERO(&readfds);
	FD_SET(sockfd, &readfds);

	while(1){
		testfds	= readfds;
		select(sockfd+1, &testfds, (fd_set*)0, (fd_set*)0, (struct timeval*)0);

		if(FD_ISSET(sockfd,&testfds)) {
			ioctl(sockfd, FIONREAD, &nread);
			if(nread >  0) {
				dbg_recv(sockfd);
			}
		}
	}

	close(sockfd);
}
