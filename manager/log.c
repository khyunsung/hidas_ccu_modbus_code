#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <stdarg.h>
#include <asm/types.h>
#include <sys/stat.h>

char *log_dir_name = "/mnt/";
char *log_path_name = "/mnt/pm/";
char *log_name = "pm";
char *temp_log_name = "tmp";
char *version_name = "version";
char *info_name = "info";

static FILE *access_log = NULL;

#define S1K_BYTES			1024		// for test
#define S2K_BYTES			2048
#define S128K_BYTES			131072		// 256 * 1024
#define S250K_BYTES			256000		// 256 * 1024
#define S512K_BYTES			524288		// 512 * 1024
#define S1M_BYTES			1048576		// 1024 * 1024
#define S2M_BYTES			2097152		// 2 * 1024 * 1024
#define MAX_LOG_SIZE		S128K_BYTES
#define RE_LOG_SIZE			S128K_BYTES

extern const char *app_name;
extern const char *app_version;
extern const char *app_seq;
extern const char *app_time;
extern const char *app_by;
extern const char *app_host;
extern const char *app_compiler;

static char *log_time(void)
{
	static char buf[30];
	time_t cur_time;
	struct tm *gmt;

	time(&cur_time);
	gmt = (struct tm *)localtime(&cur_time);
	strftime(buf, 30, "%Y.%m.%d %H:%M:%S ", gmt);

	return buf;
}

static void log_rotate(int mode)
{
	struct stat sb;
	char name[256], tmpname[256], buf[2048];
	int size, rsize, cnt;
	int fdr, fdt, log;

	strcpy(name, log_path_name);
	strcat(name, log_name);

	strcpy(tmpname, log_path_name);
	strcat(tmpname, temp_log_name);

	stat(name, &sb);
	size = sb.st_size;
	if(size < MAX_LOG_SIZE)	return;

	size -= RE_LOG_SIZE;
	fdt = open(tmpname, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR | S_IROTH | S_IRGRP);
	fdr = open(name, O_RDONLY, S_IRUSR | S_IWUSR | S_IROTH | S_IRGRP);
	if(fdt<0 || fdr<0)	{
		close(fdt);
		close(fdr);
		return;
	}

	lseek(fdr, RE_LOG_SIZE, SEEK_SET);
	while(size){
		rsize = (size > sizeof(buf)) ? sizeof(buf) : size ;
		cnt = read(fdr, buf, rsize);
		if(cnt<0) break;
		write(fdt, buf, cnt);
		size -= cnt;
	}
	close(fdr);
	close(fdt);
	rename(tmpname, name);

	if(mode==0){
		log = open(name, O_WRONLY | O_CREAT | O_APPEND, S_IRUSR | S_IWUSR | S_IROTH | S_IRGRP);
		if(log<0)	return;
		dup2(log, STDERR_FILENO);
		close(log);
		fcntl(STDERR_FILENO, F_SETFD, 1);
	} else if(mode==1){
		if(access_log)	fclose(access_log);
		access_log = NULL;
		log = open(name, O_WRONLY | O_CREAT | O_APPEND, S_IRUSR | S_IWUSR | S_IROTH | S_IRGRP);
		if(log<0)	return;
		access_log = fdopen(log, "w");
	} else	return;

	return;
}

void log_access(char *format, ...)
{
	va_list args;
	int n;
	char buf[2048];

	log_rotate(1);

	va_start(args, format);
	n = vsnprintf(buf, sizeof(buf), format, args);
	va_end(args);

	if(n < 1)	return;

	if(!access_log) return;
	fprintf(access_log, "%s%s", log_time(), buf);
	fflush(access_log);
}


void log_error(char *format, ...)
{
	va_list args;
	int n;
	char buf[2048];

	log_rotate(0);

	va_start(args, format);
	n = vsnprintf(buf, sizeof(buf), format, args);
	va_end(args);

	if(n < 1)	return;

	fprintf(stderr, "%s%s", log_time(), buf);
	fflush(stderr);
}

void open_logs(int mode)
{
	int log_fd;
	int fd;
	char name[256];
	char str[256];
  char syscmd[256];
  

	strcpy(name, log_path_name);
	strcat(name, log_name);
	
	log_fd = open(name,
		O_WRONLY | O_CREAT | O_APPEND, S_IRUSR | S_IWUSR | S_IROTH | S_IRGRP);
		
	if(log_fd<0)
	{
		mkdir("/mnt/log/", S_IRUSR | S_IWUSR | S_IROTH | S_IRGRP);
		mkdir(log_path_name, S_IRUSR | S_IWUSR | S_IROTH | S_IRGRP);		
		log_fd = open(name, O_WRONLY | O_CREAT | O_APPEND, S_IRUSR | S_IWUSR | S_IROTH | S_IRGRP);
		if(log_fd<0)
			printf("ERR: failed to open(%s), %s\n", name, strerror(errno));
	}
	
	if(log_fd<0) {
		close(log_fd);
		return;
	}

	if( mode == 0 )
	{
		if (dup2(log_fd, STDERR_FILENO) == -1) {
			printf("unable to dup2 the error log\n");
		}

		if (fcntl(STDERR_FILENO, F_SETFD, FD_CLOEXEC) == -1) {
			printf("unable to fcntl the error log\n");
		}
	}
	else if ( mode == 1 )
	{
		if(access_log) fclose(access_log);
		access_log = NULL;
		access_log = fdopen(log_fd, "w");
		if (fcntl(log_fd, F_SETFD, FD_CLOEXEC) == -1) {
			printf("unable to fcntl the error log\n");
		}
	}
	else {
		close(log_fd);
		return;
	}

	//close(log_fd);
	strcpy(name, log_path_name);
	strcat(name, version_name);
	fd = open(name,	O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR | S_IROTH | S_IRGRP);
	sprintf(str,"%s\n%s\n%s\n%s\n%s\n%s",app_name,app_version,app_time,app_by,app_host,app_compiler);
	write(fd, str, strlen(str));
	close(fd);
}
