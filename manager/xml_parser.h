#ifndef __XML_PARSE__
#define __XML_PARSE__

/* XML Info Variables Definition */
typedef struct {

        int address;
        float value;

	int data_table;
	int times;
	int len;
	int max;
	int min;

        unsigned char key_name[300];

} XML_INFO;

#endif
