#ifndef	__PORT_H
#define	__PORT_H

void cmdport_thread(void *arg);
#ifdef COM
void txdata_thread(void *arg);
#endif
void port_thread(void *arg);

#endif		// __PORT_H
