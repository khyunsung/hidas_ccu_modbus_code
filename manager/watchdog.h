#ifndef	_WATCHDOG_H
#define	_WATCHDOG_H

#include <sys/types.h>
#include <unistd.h>

struct proc_dat{
	char name[20];
	int pid;
	char stat;
	unsigned long utime;
	unsigned long stime;
	unsigned long start_time;
};

int make_proc_info(struct proc_dat proc_dat[], int max_proc_dat);
pid_t search_app(struct proc_dat *proc_dat, int proc_no, char *app_name);

#endif		// _WATCHDOG_H
