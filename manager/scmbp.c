#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>

#include <asm/types.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/resource.h>
#include <netinet/in.h>

#include "../include/common.h"
#ifdef COM
#include "../include/hicsr.h"
#endif
#include "scmbp.h"
#include "modbus.h"
#include "utils.h"
#include "log.h"

#define SCM_PORT	 2999
#define BASE   		 2000

//#define	DEBUG
#ifndef	DEBUG
	#define	dprintf(format, args...) log_access(format, ## args)
#else
	#define	dprintf(format, args...) log_access(format, ## args); printf(format, ## args)
#endif	// DEBUG

extern int scm_tx_cnt, scm_rx_cnt, scm_thr_cnt;
extern __u8 scm_alive;

extern PORT_INFO PORT_info[MAX_INTERFACE];
#ifdef COM
extern IO_DATA io_data[MAX_INTERFACE + 1];
#else
extern INF_MEM INF_mem[MAX_INTERFACE+1];
#endif

extern pthread_mutex_t mutex_mem, mutex_cmd;

extern __u8 db_update;

static int open_link(void)
{
	int sockfd;
	struct sockaddr_in server;
	int val, optval, result;

	sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(sockfd < 0){
		dprintf("[SCM INTERNAL] socket open error!!!\n");
		sleep(1);
		return -1;
	}

 	memset( (char*)&server, 0, sizeof (server) );
	server.sin_addr.s_addr = htonl(INADDR_ANY);
	server.sin_family = AF_INET;
	server.sin_port = htons(BASE);
	
	result = bind(sockfd, (struct sockaddr *)&server, sizeof(server));
	if(result < 0){
		dprintf("[SCM INTERNAL] bind retry...\n");
		result = bind(sockfd, (struct sockaddr *)&server, sizeof(server));
		if(result < 0){
			dprintf("[SCM INTERNAL] bind error\n");
			sleep(1);
			return -1;
		}
	}

//	val |= FD_CLOEXEC;
//	fcntl(sockfd,F_SETFD,val);
	fcntl(sockfd,F_SETFD,FD_CLOEXEC);

	return sockfd;
}

static int init_queue()
{
	int i;
	
pthread_mutex_lock(&mutex_cmd);
	for( i = 0 ; i < MAX_INTERFACE ; i++)
	{
		PORT_info[i].write_head = 0;
		PORT_info[i].write_tail = 0;
	}
pthread_mutex_unlock(&mutex_cmd);
}

struct sockaddr_in scm;

static int scm_read(int port, __u8 *buff, int len)
{
	unsigned int fromSize = sizeof(scm);
	return recvfrom(port, buff, len, 0, (struct sockaddr *)&scm, &fromSize);
}

static int scm_write(int port, __u8 *buff, int len)
{
	int i;
	int result;
    char strTemp[512];
	if((result = sendto(port,buff,len,0,(struct sockaddr *)&scm,sizeof(scm))) == len)
	{
#ifdef DEBUG
printf("[SCM INTERNAL] SCM_UP [%d]\n",len);
#endif
#if 0
sprintf( strTemp,"SCM_UP [%02X][%02X][%02X][%02X][%02X][%02X][%02X][%02X][%02X][%02X] to %s(%d)",
		          buff[0],buff[1],buff[2],buff[3],buff[4],buff[5],buff[6],buff[7],buff[8],buff[9],
				 inet_ntoa(scm.sin_addr),htons(scm.sin_port) );
			
dprintf("%s\n",strTemp);
#endif
		return 1;
	}
	else
	{
		dprintf("[SCM INTERNAL] Error(%d): SCM[%d] UP (%d)-(%d)!!!\n",errno,port,result,len);
		return -1;
	}
}

static char delay_ovrf = 0;

#define DOUBLEBASE 6000
static int dump_write(__u16 port, __u8 *buff, int len, int port_num)
{
	int sockfd;
	struct sockaddr_in cmd_interface;
	unsigned short infPort = DOUBLEBASE+port_num;
	char infIP[32]="127.0.0.1";
	int i;

	sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);	
	if(sockfd == -1){
		dprintf("[PM DUMP %d] socket open error!!!\n",port_num);
		sleep(1);
		return -1;
	}

	memset(&cmd_interface,0,sizeof(cmd_interface));
	cmd_interface.sin_family=AF_INET;
	cmd_interface.sin_addr.s_addr = inet_addr(infIP);
	cmd_interface.sin_port = htons(infPort);

	if(sendto(sockfd,buff,len,0,(struct sockaddr *)&cmd_interface,sizeof(cmd_interface))
		== len)
	{
#ifdef DEBUG
dprintf("[PORT %d] CMD [",port_num);
for(i=0;i<len;i++) { dprintf("%02X|",buff[i]); }
dprintf("]\n");
#endif
    close(sockfd);
		return 1;
	}
  close(sockfd);
	return -1;
}

static void scmbp(int port, __u8 *recv_buff, int len)
{
	__u8 send_buff[2048];
	modbus_req_hdr *mbhdr;
	modbus_wreq_hdr_org *wmbhdr;
	modbus_res_hdr_org *res_mbhdr;

	__u16 address, points, byte_count;
	__u8 interface, fcode, count, *pbyte;
	__u16 crc;

	__u16 m_address, b_address, m_points;

	int sendfd, sendSize, send_len;
	int i, j, index;
	char head, tail;
	char str[32], temp[8];

	mbhdr     = (modbus_req_hdr *)recv_buff;
	res_mbhdr = (modbus_res_hdr_org *)send_buff;

	interface = mbhdr->unit_id - MPM_INF_PORT_0;
	fcode     = mbhdr->function_code;
	address   = mbhdr->address;
	points    = mbhdr->points;
	
	sendSize = 0;
//	dprintf("[SCM INTERNAL] Modbus Query: %02X %02X %04X %04X\n",interface,fcode,address,points);
	if( interface < 0 || interface > MPM_MAX_INTERFACE)
	{
		dprintf("[SCM INTERNAL] Modbus interface ID error [%d], (it must be in range from 0 to 4)\n", interface);
		return;
	}


#if 0

#ifndef DEBUG
	if( fcode == 0x10 || fcode == 0x05 )
	{
		dprintf("[SCM INTERNAL] Modbus Query: %02X %02X %04X %04X\n",interface,fcode,address,points); 
	}
#else
	dprintf("[SCM INTERNAL] Modbus Query: %02X %02X %04X %04X\n",interface,fcode,address,points); 
#endif
	

	if( fcode == 0x10 )
	{
		wmbhdr = (modbus_wreq_hdr_org *)recv_buff;
		if( wmbhdr->byte_count == 1 )
		{ dprintf("\tWrite Data: %04X (%d)\n",wmbhdr->data[0],wmbhdr->byte_count); }
		else if ( wmbhdr->byte_count == 2 )
		{ dprintf("\tWrite Data: %04X %4X (%d)\n",wmbhdr->data[0],wmbhdr->data[1],wmbhdr->byte_count); }
		else
		{ dprintf("\tWrite Data: (%d)\n",wmbhdr->byte_count); }
	}
	if( fcode == 0x05 )
	{
		m_address = address>>4;
		b_address = address&0x000F;
		m_points = ((points)?(1<<b_address):0);
		dprintf("\tCMD Data: %04X(%d) %04X\n",m_address,b_address,m_points);
	}

#endif


	if( fcode == 0x10 || fcode == 0xFF)
	{
#ifdef DEBUG	
		dprintf("[SCM INTERNAL] Modbus Query: %02X %02X %04X %04X\n",interface,fcode,address,points); 
#endif	
		wmbhdr = (modbus_wreq_hdr_org *)recv_buff;
	}

	switch(fcode){
	case read_mult_regs:
		byte_count = points*2;

		if(address>=2048){
			dprintf("[SCM INTERNAL] Modbus address error [%d]\n", address);
			return;
		}

		if(points>125){	
			dprintf("[SCM INTERNAL] Modbus send size:[%d] Overflow\n", points);
			return;
		}

		if((address+points)>2048){
			dprintf("[SCM INTERNAL] TotalData[%d] Overflow:Start_addr[%d], Send_size[%d]\n",
				address+points,address,points);
			return;
		}
		res_mbhdr->unit_id = interface + MPM_INF_PORT_0;
		res_mbhdr->function_code = fcode;
pthread_mutex_lock(&mutex_mem);
#ifdef COM
		for( i = 0 ; i < points ; i++)
			res_mbhdr->data[i] = htons(io_data[interface].INF_mem.INF_data.tag[address+i]);
#else			
		for( i = 0 ; i < points ; i++)
			res_mbhdr->data[i] = htons(INF_mem[interface].INF_data.tag[address+i]);
#endif
pthread_mutex_unlock(&mutex_mem);
		res_mbhdr->byte_count = byte_count;
		pbyte = (__u8 *)(send_buff+3+byte_count);

		crc = CRC16((__u8*)res_mbhdr,3+byte_count)&0xFFFF;
		*pbyte++ = crc>>8;
		*pbyte++ = crc&0x00FF;

		sendSize = 5+byte_count;
	break;
	case write_mult_regs:

		byte_count = points*2;
		wmbhdr = (modbus_wreq_hdr_org *)recv_buff;

		if(address >= 2048){
			dprintf("[SCM INTERNAL] Modbus address error [%d]\n", address);
			return;
		}
		if((address+points)>2048){
			dprintf("[SCM INTERNAL] TotalData[%d] Overflow:Start_addr[%d], Send_size[%d]\n",
				address+points,address,points);
			return;
		}
		if( byte_count > 1024 )
		{
			dprintf("[SCM INTERNAL] Byte_count Too Large [%d]\n",byte_count);
			return;
		}
		if( byte_count != wmbhdr->byte_count )
		{
			dprintf("[SCM INTERNAL] Byte_count Mismatch Error [%d]\n",byte_count);
			return;
		}

pthread_mutex_lock(&mutex_mem);
		for( i = 0 ; i < points ; i++ )
#ifdef COM
			io_data[interface].INF_mem.INF_data.tag[address+i] = htons(wmbhdr->data[i]);
#else
			INF_mem[interface].INF_data.tag[address+i] = htons(wmbhdr->data[i]);
#endif
pthread_mutex_unlock(&mutex_mem);

		if( interface > 0 && interface < MAX_INTERFACE )
		{
			if( points == 2 || points == 1 )
			{
				index = interface;

pthread_mutex_lock(&mutex_cmd);
				head = PORT_info[index-1].write_head;
				tail = PORT_info[index-1].write_tail;
pthread_mutex_unlock(&mutex_cmd);

				if( (tail+1)%MAX_WRITE_BUFF == head )
				{
					dprintf("[CMD queue %d] Write buffer is full!!\n",index);
pthread_mutex_lock(&mutex_mem);
#ifdef COM
					io_data[0].INF_mem.INF_data.tag[MAX_INTERFACE+index-1] |= htons(0x0002);
#else
					INF_mem[0].INF_data.tag[10+index-1] |= htons(0x0002);
#endif
pthread_mutex_unlock(&mutex_mem);

					return;
				}
				else
				{
pthread_mutex_lock(&mutex_cmd);
					if( points == 2 )
					{
						PORT_info[index-1].write_buff[tail][0] = interface;
						PORT_info[index-1].write_buff[tail][1] = 0x10;
						PORT_info[index-1].write_buff[tail][2] = (wmbhdr->address)&0x00FF;
						PORT_info[index-1].write_buff[tail][3] = (wmbhdr->address)>>8;
						PORT_info[index-1].write_buff[tail][4] = 2; // byte order
						PORT_info[index-1].write_buff[tail][5] = 0;
						PORT_info[index-1].write_buff[tail][6] = 4;
						PORT_info[index-1].write_buff[tail][7] = (wmbhdr->data[0])>>8;
						PORT_info[index-1].write_buff[tail][8] = (wmbhdr->data[0])&0x00FF;
						PORT_info[index-1].write_buff[tail][9] = (wmbhdr->data[1])>>8;
						PORT_info[index-1].write_buff[tail][10] = (wmbhdr->data[1])&0x00FF;
						send_len = 11;
					}
					else
					{
						PORT_info[index-1].write_buff[tail][0] = interface;
						PORT_info[index-1].write_buff[tail][1] = 0x06;
						PORT_info[index-1].write_buff[tail][2] = (wmbhdr->address)&0x00FF;
						PORT_info[index-1].write_buff[tail][3] = (wmbhdr->address)>>8;
						PORT_info[index-1].write_buff[tail][4] = (wmbhdr->data[0])&0x00FF;
						PORT_info[index-1].write_buff[tail][5] = (wmbhdr->data[0])>>8;
						send_len = 6;
					}
	
					PORT_info[index-1].write_length[tail] = send_len;

					PORT_info[index-1].write_tail++;
					PORT_info[index-1].write_tail %= MAX_WRITE_BUFF;
pthread_mutex_unlock(&mutex_cmd);
//#if 0
					dprintf("[CMD queue %d] head:%d tail:%d\n",index,head,PORT_info[index-1].write_tail);
					sprintf(str,"\t");
					for(i=0; i<send_len; i++)
					{
						sprintf(temp,"%02X ",PORT_info[index-1].write_buff[tail][i]);
						strcat(str,temp);
					}
					dprintf("%s\n",str);
//#endif
pthread_mutex_lock(&mutex_mem);
#ifdef COM
					if((io_data[0].INF_mem.INF_data.tag[MAX_INTERFACE+index-1]& htons(0x0002)) != 0)
#else
					if((INF_mem[0].INF_data.tag[MAX_INTERFACE+index-1]& htons(0x0002)) != 0)
#endif					
					{
						delay_ovrf++;
						if(delay_ovrf >= 10)
						{
#ifdef COM						
							io_data[0].INF_mem.INF_data.tag[MAX_INTERFACE+index-1] &= ~htons(0x0002);
#else
							INF_mem[0].INF_data.tag[MAX_INTERFACE+index-1] &= ~htons(0x0002);
#endif							
							delay_ovrf = 0;
						}
					}
pthread_mutex_unlock(&mutex_mem);
				}
			}
		}

		memcpy(res_mbhdr,mbhdr,5);
		pbyte = (__u8 *)(send_buff+5);
			
		crc = CRC16((__u8 *)res_mbhdr,5)&0xFFFF;
		*pbyte++ = crc>>8;
		*pbyte++ = crc&0x00FF;

		sendSize = 8;
	break;
	case write_coil:
		m_address = address>>4;
		b_address = address&0x000F;
//		m_points = ((points)?(1<<b_address):0);
		m_points = 1<<b_address;

		if(m_address >= 2048){
			dprintf("[SCM INTERNAL] Modbus address error [%d]\n", address);
			return;
		}

pthread_mutex_lock(&mutex_mem);
#ifdef COM
		if( points ) io_data[interface].INF_mem.INF_data.tag[m_address] |= htons(m_points);
		else io_data[interface].INF_mem.INF_data.tag[m_address] &= ~htons(m_points);
		m_points = htons(io_data[interface].INF_mem.INF_data.tag[m_address]);
#else
		if( points ) INF_mem[interface].INF_data.tag[m_address] |= htons(m_points);
		else INF_mem[interface].INF_data.tag[m_address] &= ~htons(m_points);
		m_points = htons(INF_mem[interface].INF_data.tag[m_address]);
#endif		
pthread_mutex_unlock(&mutex_mem);

		if( interface > 0 && interface < MAX_INTERFACE )
		{
			index = interface;

pthread_mutex_lock(&mutex_cmd);
			head = PORT_info[index-1].write_head;	
			tail = PORT_info[index-1].write_tail;
pthread_mutex_unlock(&mutex_cmd);
			
			if( (tail+1)%MAX_WRITE_BUFF == head )
			{
				dprintf("[CMD queue %d] Write buffer is full!!\n",index);
pthread_mutex_lock(&mutex_mem);
#ifdef COM
				io_data[0].INF_mem.INF_data.tag[MAX_INTERFACE+index-1] |= htons(0x0002);
#else
				INF_mem[0].INF_data.tag[MAX_INTERFACE+index-1] |= htons(0x0002);
#endif
pthread_mutex_unlock(&mutex_mem);

				return;
			}
			else
			{
pthread_mutex_lock(&mutex_cmd);
				PORT_info[index-1].write_buff[tail][0] = interface;
				PORT_info[index-1].write_buff[tail][1] = 0x06;
				PORT_info[index-1].write_buff[tail][2] = m_address&0x00FF;
				PORT_info[index-1].write_buff[tail][3] = m_address>>8;
				PORT_info[index-1].write_buff[tail][4] = m_points&0x00FF;
				PORT_info[index-1].write_buff[tail][5] = m_points>>8;
				PORT_info[index-1].write_length[tail] = 6;

				PORT_info[index-1].write_tail++;
				PORT_info[index-1].write_tail %= MAX_WRITE_BUFF;
pthread_mutex_unlock(&mutex_cmd);
#if 0
				dprintf("[CMD queue %d] head:%d tail:%d\n",index,
					head,PORT_info[index-1].write_tail);
				sprintf(str,"\t");
				for(i=0; i<6; i++) 
				{
					sprintf(temp,"%02X ",PORT_info[index-1].write_buff[tail][i]);
					strcat(str,temp);
				} 
				dprintf("%s\n",str);
#endif
pthread_mutex_lock(&mutex_mem);
#ifdef COM
				if((io_data[0].INF_mem.INF_data.tag[MAX_INTERFACE+index-1]& htons(0x0002)) != 0)
				{
					delay_ovrf++;
					if(delay_ovrf >= 10)
					{
						io_data[0].INF_mem.INF_data.tag[MAX_INTERFACE+index-1] &= ~htons(0x0002);
						delay_ovrf = 0;
					}
				}
#else
				if((INF_mem[0].INF_data.tag[MAX_INTERFACE+index-1]& htons(0x0002)) != 0)
				{
					delay_ovrf++;
					if(delay_ovrf >= 10)
					{
						INF_mem[0].INF_data.tag[MAX_INTERFACE+index-1] &= ~htons(0x0002);
						delay_ovrf = 0;
					}
				}
#endif				
pthread_mutex_unlock(&mutex_mem);
			}

		}

		memcpy(res_mbhdr,mbhdr,8);
		sendSize = 8;
	break;
	default:
		dprintf("[SCM INTERNAL] function code error : %d\n", fcode);
		return;
	break;
	}

	sendfd = port;
	if( scm_write(sendfd, send_buff, sendSize) > 0 )
		scm_tx_cnt++;

	scm_alive = 1; // true;
}

static void scm_recv(int port)
{
	__u8  rzbuffer[DEFAULT_BUFF_SIZE];
	__u16 crc, r_crc;
	int len;
	
	int i;
	struct timeval time;
	struct tm *gmt;
	char  strTemp[512];


	len = 0;
	len = scm_read(port, rzbuffer, DEFAULT_BUFF_SIZE-16);
	if( len < 0 ) 
	{
		dprintf("[SCM INTERNAL] receive error! (port:%d errno:%d)\n",port,errno);
		return;
	}
	if( len < 5 ) return;
	if( len >= DEFAULT_BUFF_SIZE-16 ) return;

	scm_thr_cnt++;
	
	crc = CRC16(rzbuffer,len-2)&0xFFFF;

#if 0
	gettimeofday(&time,NULL);
	gmt = (struct tm *)localtime(&time.tv_sec);

	printf("%02d:%02d:%02d.%06d ",gmt->tm_hour,
		gmt->tm_min,gmt->tm_sec,time.tv_usec);
		 
	for( i = 0 ; i < len ; i++)
		sprintf("%02x|",rzbuffer[i]);
	printf("\n");
	printf(": %02X %02X\n",crc>>8,crc&0x00FF);
#endif

	r_crc = (rzbuffer[len-2]<<8|rzbuffer[len-1]);
	if( crc != r_crc )
	{
//		scm_err_cnt++;
		sprintf(strTemp,"[E: %02X %02X %02X %02X %02X %02X %02X %02X",rzbuffer[0],rzbuffer[1],rzbuffer[2],rzbuffer[3],rzbuffer[4],rzbuffer[5],rzbuffer[6],rzbuffer[7]);
//		dprintf("[SCM INTERNAL] CRC error! (%d)-(%d) %s\n",crc,r_crc,strTemp);
		//return;
	}
		
	scm_rx_cnt++;
	scmbp(port, rzbuffer, len);
}

void scmbp_thread(void *arg)
{
	fd_set readfds,	testfds;
	int sockfd, nread, ret;
	int maxfd;
	pid_t my_pid;
	struct timeval timeout;

	pthread_detach(pthread_self());

restart0:
	sleep(10);

restart:
	sockfd = open_link();
	if( sockfd < 0 ) exit(EXIT_FAILURE);

	dprintf("SCM Internal Modbus Start [%d]\n",sockfd);

	maxfd = sockfd+1;
	
	FD_ZERO(&readfds);
	FD_SET(sockfd, &readfds);

	my_pid = getpid();
	setpriority(PRIO_PROCESS, my_pid, -10);

	init_queue();

	while(1){

		testfds	= readfds;

		timeout.tv_sec = 20;
		timeout.tv_usec = 0;

		ret = select(maxfd, &testfds, (fd_set*)0, (fd_set*)0, (struct timeval*)&timeout);
		if( ret > 0 )
		{
			if(FD_ISSET(sockfd,&testfds)) {
				ioctl(sockfd, FIONREAD, &nread);
				if(nread > 0) {
					scm_recv(sockfd);
				}
			}
		}
		else
		{
			dprintf("[SCM INTERNAL] Port Reopen\n");
			close(sockfd);
			goto restart;
		}

		if( db_update )
		{
			dprintf("[SCM INTERNAL] Process Restart\n");
			close(sockfd);
			goto restart0;
		}
	}

	close(sockfd);
}
