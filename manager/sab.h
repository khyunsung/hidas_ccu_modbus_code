#ifndef _SAB_H
#define _SAB_H

// ioctls
#define	SAB_GET_ADDR	0x10000
#define	SAB_GET_BAUD	0x10001
#define	SAB_GET_SPC		0x10002

#define	SAB_SET_ADDR	0x11000
#define	SAB_SET_BAUD	0x11001
#define	SAB_SET_SPC		0x11002

// Serial Port Configuration
#define SAB_SPC_NRZ		0x0
#define SAB_SPC_BT1		0x1
#define SAB_SPC_NRZI	0x2
#define SAB_SPC_BT2		0x3
#define SAB_SPC_FM0		0x4
#define SAB_SPC_FM1		0x5
#define SAB_SPC_MAN		0x6
#define SAB_SPC_NA		0x7

// Receive Status Register (RSTA)
#define SAB82532_RSTA_VFR		0x80	// Vaild frame
#define SAB82532_RSTA_RDO		0x40	// Receive Data overflow
#define SAB82532_RSTA_CRC		0x20	// CRC check
#define SAB82532_RSTA_RAB		0x10	// Receive message aborted
#define SAB82532_RSTA_HA1		0x08	// high byte address compare
#define SAB82532_RSTA_HA0		0x04	//   <- 2bytes address mode
#define SAB82532_RSTA_CR		0x02	// command/response  <- 2bytes address mode
#define SAB82532_RSTA_LA		0x01	// low byte address

#endif /* !(_SAB_H) */
