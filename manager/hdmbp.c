#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>

#include <asm/types.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/resource.h>
#include <netinet/in.h>

#include "common.h"
#ifdef COM
#include "hicsr.h"
#endif
#include "hdmbp.h"
#include "modbus.h"
#include "utils.h"
#include "log.h"
#include "sab.h"

#define NR_HDLC_PORT	2

#define COCOS
#define LOADING

#define DDEBUG

//#define	DEBUG
#ifndef	DEBUG
	#define	dprintf(format, args...) log_access(format, ## args)
#else
	#define	dprintf(format, args...) log_access(format, ## args); printf(format, ## args)
#endif	// DEBUG

int cocos_index, vdr_index, load_index;
int hdlc_err_cnt, hdlc_err_size;
__u8  hdlc_err_frame[RECV_BUFF_SIZE];
int hdlc_rx_cnt[NR_HDLC_PORT];
int hdlc_tx_cnt[NR_HDLC_PORT];
struct hdlc_status_struct hdlc_status[NR_HDLC_PORT];
struct hdlc_map_struct hdlc_map[MAX_INTERFACE];
pthread_mutex_t mutex_hdlc = PTHREAD_MUTEX_INITIALIZER;

extern int hdbus_tx_cnt, hdbus_rx_cnt, hdbus_thr_cnt;
extern __u8 cocos_alive, hdlc_alive;

extern PORT_INFO PORT_info[MAX_INTERFACE];
#ifdef COM
extern IO_DATA io_data[MAX_INTERFACE + 1];
#else
extern INF_MEM INF_mem[MAX_INTERFACE+1];
#endif
extern pthread_mutex_t mutex_mem, mutex_cmd;

static int open_link(__u8 index)
{
	int fd, addr;
	int val;
	FILE *fp;

	dprintf("[HDLC %d] Link open...\n",index);
	fp = fopen(CONF_DIR"hdlc.addr", "r");
	if(fp == NULL){
pthread_mutex_lock(&mutex_hdlc);
		fp = fopen(CONF_DIR"hdlc.addr","w");
		addr = 0x01;
		fprintf(fp,"%d",addr);
		fclose(fp);
pthread_mutex_unlock(&mutex_hdlc);
	}
	else{
		fscanf(fp, "%d", &addr);
		fclose(fp);
	}
	dprintf("[HDLC %d] Address: %d\n",index,addr);
	
	if( index == 0 )
		fd = open("/dev/hdlc0", O_RDWR);
	else if( index == 1 )
		fd = open("/dev/hdlc1", O_RDWR);
	else
	{
		sleep(1);
		return (-1);
	}

	if(fd < 0){
		dprintf("[HDLC %d] Open /dev/hdlc failed (error:%s)\n",index,strerror(errno));
		sleep(1);
		return (-1);
	}
	ioctl(fd, SAB_SET_ADDR, &addr);

	dprintf("[HDLC %d] Open Devfd : %d\n",index,fd);

//	val |= FD_CLOEXEC;
//	fcntl(fd,F_SETFD,val);
	fcntl(fd,F_SETFD,FD_CLOEXEC);

	return fd;
}

static int open_auxlink(void)
{
	FILE *fp;

#ifdef COCOS
	fp = fopen(CONF_DIR"cocos.def","r");
	if(fp == NULL)
		cocos_index = -1;
	else
	{
		fscanf(fp,"%d",&cocos_index);
		dprintf("\tCOCOS index: %d\n",cocos_index);
		fclose(fp);
	}
#endif
#ifdef LOADING
	fp = fopen(CONF_DIR"loading.def","r");
	if(fp == NULL)
		load_index = -1;
	else
	{
		fscanf(fp,"%d",&load_index);
		dprintf("\tLOADING index: %d\n",load_index);
		fclose(fp);
	}
#endif

	return 1;
}

#ifdef DDEBUG
int bypassfd;
char bypass_ip[32];

static void open_dlink(void)
{
	FILE *fp;
//	int bypassfd;
	struct sockaddr_in bypass_addr;
	int val, optval, result;

	fp = fopen(CONF_DIR"bypass.conf","r");
	if( fp == NULL )
	{
		dprintf("[HDLC] \tBypass configuration not found!!!\n");
		return;
	}

	fgets(bypass_ip,32,fp);

	bypassfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if( bypassfd < 0 )
	{
		dprintf("[HDLC] \tBypass socket open error!!!\n");
		fclose(fp);
		return;
	}

	val = FD_CLOEXEC;
	fcntl(bypassfd,F_SETFD,val);

	dprintf("[HDLC] Bypass: socket open\n");
	fclose(fp);
}

#endif

static void default_map(void)
{
	int i;

	for(i = 0; i < MAX_INTERFACE; i++)
	{
		hdlc_map[i].base = MAX_INF_TAG*i;
		hdlc_map[i].size = MAX_INF_TAG;
		hdlc_map[i].inf = i+1;	
	}
}

static int open_map(void)
{
	FILE *fp;
	int i,line;
	char type = 0, index = 0;
	char buffer[64], *p;

	default_map();

	fp = fopen("/etc/sysconfig/hdlc.map","r");
	if(fp == NULL) {
		fp = fopen("/etc/sysconfig/hdlc.map","w");
		fprintf(fp,"$M%d\n",MAX_INTERFACE);
		for(i = 0; i < MAX_INTERFACE; i++)
			fprintf(fp, ":%d %d %d\n",hdlc_map[i].base,
				hdlc_map[i].size,hdlc_map[i].inf);
	}	
	else {
		while(fgets(buffer,64,fp)!=NULL)
		{
			p = &buffer[0];

			if( type == 1 )
			{
				p++; // : *
				char *base = strtok(p," ");
				char *size = strtok(NULL," ");
				char *inf = strtok(NULL," ");

				hdlc_map[index].base = atoi(base);
				hdlc_map[index].size = atoi(size);
				hdlc_map[index].inf = atoi(inf);
				index++;
			}

			if( *p == '$' || *p == '@' )
			{
				p++; //$ @
				p++; // M
				line = atoi(p);
				type = 1;
			}
		}
	}

	dprintf("[HLDC] Map: \n");
	for(i = 0; i < MAX_INTERFACE; i++)
	{
		dprintf("\t%d - %d %d %d\n",i+1,
			hdlc_map[i].base,hdlc_map[i].size,hdlc_map[i].inf);
	}

	fclose(fp);
	return 1;
}

static int port_kill(int i)
{
	pid_t f;
	int ret;
	char fname[64];

	memset(fname,0,64);

	f = fork();
	if( f == 0 )
	{
		sprintf(fname,"interface%d#",i);
		dprintf("[HDLC] PORT KILL ==> %s\n",fname);
		execl("/bin/killall","killall",fname,NULL);
	}

	return 1;
}

static int init_queue()
{
	int i;

pthread_mutex_lock(&mutex_cmd);
	for( i = 0 ; i < MAX_INTERFACE ; i++)
	{
		PORT_info[i].write_head = 0;
		PORT_info[i].write_tail = 0;
	}
pthread_mutex_unlock(&mutex_cmd);
}

static int hdlc_read(__u16 port, __u8 *buff, int len)
{
	return read(port, buff, len);
}

static int hdlc_write(__u16 port, __u8 *buff, int len)
{
	if( write(port, buff, len) == len )
	{
#ifdef DEBUG
dprintf("[HDLC] UP [%d]\n",len);
#endif
		return 1;
	}
	else
	{
		dprintf("[HDLC] Error: HDLC UP!!!\n");
	}

	return -1;
}

static char delay_ovrf = 0;

static void hdmbp(__u8 port, __u16 fd, __u8 *buff, int len)
{
	__u8  send_buff[RECV_BUFF_SIZE];
	struct hdbus *req, *res;
	modbus_req_hdr *mbhdr, *wmhdr;
	modbus_res_hdr *res_mbhdr;
	struct hdmb_data *mbdata;
	struct hdmb_data2 *mbdata2;
	__u32 points2, temp;
	__u16 address, points, byte_count;
	__u8 interface,fcode, *count, *pbyte;
	__u16 crc;
	int sendfd, sendSize;
	int i, j, index;
	char head, tail;
	char str[40], tempstr[8];
	__u8 load_id,load_tag,load_type;
#ifdef DDEBUG
	struct sockaddr_in bypass_addr;
#endif

	struct timeval time;
	struct tm *gmt;

	req = (struct hdbus *)buff;
	res = (struct hdbus *)send_buff;

	if(req->cmd != HDCMD_IO_DATA){		// bad hdbus command
		dprintf("[HDLC] Bad HDBUS command : 0x%02x\n", req->cmd);
		return;	
	}
	if(req->excp != 0x00){				// exception code
		dprintf("[HDLC] Exception code error : 0x%02x\n", req->excp);
		return;
	}

	res->dst = req->src;
	res->src = req->dst;
	res->cmd = req->cmd;
	res->excp = req->excp;
	
	mbhdr = (modbus_req_hdr *)(buff+sizeof(struct hdbus));
	res_mbhdr = (modbus_res_hdr *)(send_buff+sizeof(struct hdbus));

	if(mbhdr->unit_id != req->dst){		// bad id
		dprintf("[HDLC] Modbus id error 0x%02x-0x%02x\n", mbhdr->unit_id, req->dst);
		return;
	}

	interface = mbhdr->unit_id;
	fcode = mbhdr->function_code;
	address = htons(mbhdr->address);
	points = htons(mbhdr->points);

	for(i = 0; i < MAX_INTERFACE; i++)  // Mapping for ACONIS-2000
	{
		if( address >= hdlc_map[i].base &&
			address < hdlc_map[i].base+hdlc_map[i].size)
		{
			index = hdlc_map[i].inf;
#if 1
			if( index >= 0 && index <= MAX_INTERFACE )
			{
				address -= hdlc_map[i].base;
				break;
			}
#endif
		}
	}
	if( i >= MAX_INTERFACE )
	{
		dprintf("[HDLC] mapping error\n");
		return;
	}

	if(address>=2048){
		dprintf("[HDLC] Modbus address error [%d]\n", address);
		return;
	}

#ifdef DDEBUG
	if(bypassfd > 0)
	{
		memset(&bypass_addr,0,sizeof(bypass_addr));
		bypass_addr.sin_family = AF_INET;
		bypass_addr.sin_addr.s_addr = inet_addr(bypass_ip);
		bypass_addr.sin_port = htons(8000+index);

		if( sendto(bypassfd,(char *)mbhdr,6,0,(struct sockaddr *)&bypass_addr,sizeof(bypass_addr)) != 6 )
		{
			dprintf("[HDLC] Error: HDLC Bypass!!!\n");
		}
	}
#endif

	switch(fcode){
	case read_mult_regs:
		byte_count = points*2;

		if(address>=1024){
			dprintf("[HDLC] Modbus read address error [%d]\n", address);
			return;
		}

		if(points>512){	
			dprintf("[HDLC] Modbus send size:[%d] Overflow\n", points);
			return;
		}

		if((address+points)>2048){
			dprintf("[HDLC] TotalData[%d] Overflow:Start_addr[%d],Send Size[%d]\n",
				address+points,address,points);
			return;
		}

		res_mbhdr->unit_id = interface;
		res_mbhdr->function_code = fcode;
pthread_mutex_lock(&mutex_mem);
#ifdef COM
		memcpy(res_mbhdr->data,&io_data[index].INF_mem.INF_data.tag[address],byte_count);
#else
		memcpy(res_mbhdr->data,&INF_mem[index].INF_data.tag[address],byte_count);
#endif
pthread_mutex_unlock(&mutex_mem);
		res_mbhdr->byte_count = htons(byte_count);
		pbyte = (__u8 *)(send_buff+sizeof(struct hdbus)+4+byte_count);

		crc = CRC16((__u8*)res_mbhdr,4+byte_count)&0xFFFF;
		*pbyte++ = crc>>8;
		*pbyte++ = crc&0x00FF;

		if( len > ((sizeof(struct hdbus)+8)+5) )
		{
			count = (__u8 *)(buff+sizeof(struct hdbus)+8);
			if( *count <= 0 || *count > 255 ) 
			{
				dprintf("[AUX] Count Error!\n");
				goto aux_out;
			}
			dprintf("[AUX] Count: %d\n",*count);

#ifdef COCOS
			if( cocos_index > 0 && cocos_index < 10 )
			{
				mbdata = (struct hdmb_data *)(buff+sizeof(struct hdbus)+9);
				for(i=0; i<*count; i++,mbdata++)
				{
					dprintf("[COCOS] inf:%d addr:%d data:%d\n",cocos_index,mbdata->id,mbdata->data);
					if( mbdata->id < 0 || mbdata->id > 128 ) continue;
pthread_mutex_lock(&mutex_mem);
#ifdef COM
					io_data[cocos_index].INF_mem.INF_data.tag[mbdata->id]=htons((mbdata->data)&0xFFFF);
#else
					INF_mem[cocos_index].INF_data.tag[mbdata->id]=htons((mbdata->data)&0xFFFF);
#endif
pthread_mutex_unlock(&mutex_mem);
				}
			}
#endif
#ifdef LOADING
			if( load_index > 0 && load_index < 10 )
			{
				mbdata2 = (struct hdmb_data2 *)(buff+sizeof(struct hdbus)+9);
				for(i=0; i<*count; i++,mbdata2++)
				{
					load_id   = (htons(mbdata2->id)&0x1FC0)>>6;
					load_tag  = (htons(mbdata2->id)&0xE000)>>13;
					load_type = (htons(mbdata2->id)&0x003C)>>2;
					dprintf("[LOADING] inf:%d addr:%d(%d|%d|%d) data:%d\n",load_index,
						mbdata2->id,load_id,load_tag,load_type,mbdata2->data);
					if( load_id < 0 || load_id > 128 ) continue;
					if( load_tag == 0 )
					{
						if( load_type != 0 ) mbdata2->data /= 100;
					}
pthread_mutex_lock(&mutex_mem);
#ifdef COM
					io_data[load_index].INF_mem.INF_data.tag[load_id]=htons((mbdata2->data)&0xFFFF);
#else
					INF_mem[load_index].INF_data.tag[load_id]=htons((mbdata2->data)&0xFFFF);
#endif					
pthread_mutex_unlock(&mutex_mem);
				}
			}
#endif
		}

aux_out:
		sendSize = sizeof(struct hdbus)+6+byte_count;
	break;
	case write_single_reg:
pthread_mutex_lock(&mutex_mem);
#ifdef COM
		io_data[index].INF_mem.INF_data.tag[address] = htons(points);
#else
		INF_mem[index].INF_data.tag[address] = htons(points);
#endif		
pthread_mutex_unlock(&mutex_mem);

		if( index <= 0 )
		{
#ifdef COM
			if((io_data[0].INF_mem.INF_data.tag[100]&htons(0x0001)) != 0 ) // Select before
			{
				dprintf("[HDLC] Selected before Opertion from HMI\n");

				if((io_data[0].INF_mem.INF_data.tag[101]&htons(0x0001)) != 0 ) // Module Reset
				{
					dprintf("[HDLC] Reset Requested from HMI\n");
					execl("/bin/busybox","reboot","-f",NULL);
				}
				else if((io_data[0].INF_mem.INF_data.tag[110]&htons(0x0001)) != 0 ) // Port Reset
				{
					dprintf("[HDLC] Port1 Reset Requested from HMI\n");
					port_kill(1);
				}
				else if((io_data[0].INF_mem.INF_data.tag[111]&htons(0x0001)) != 0 )
				{
					dprintf("[HDLC] Port2 Reset Requested from HMI\n");
					port_kill(2);
				}
				else if((io_data[0].INF_mem.INF_data.tag[112]&htons(0x0001)) != 0 )
				{
					dprintf("[HDLC] Port3 Reset Requested from HMI\n");
					port_kill(3);
				}
			}
#else
			if((INF_mem[0].INF_data.tag[100]&htons(0x0001)) != 0 ) // Select before
			{
				dprintf("[HDLC] Selected before Opertion from HMI\n");

				if((INF_mem[0].INF_data.tag[101]&htons(0x0001)) != 0 ) // Module Reset
				{
					dprintf("[HDLC] Reset Requested from HMI\n");
					execl("/bin/busybox","reboot","-f",NULL);
				}
				else if((INF_mem[0].INF_data.tag[110]&htons(0x0001)) != 0 ) // Port Reset
				{
					dprintf("[HDLC] Port1 Reset Requested from HMI\n");
					port_kill(1);
				}
				else if((INF_mem[0].INF_data.tag[111]&htons(0x0001)) != 0 )
				{
					dprintf("[HDLC] Port2 Reset Requested from HMI\n");
					port_kill(2);
				}
				else if((INF_mem[0].INF_data.tag[112]&htons(0x0001)) != 0 )
				{
					dprintf("[HDLC] Port3 Reset Requested from HMI\n");
					port_kill(3);
				}
			}
#endif			

			return;
		}

pthread_mutex_lock(&mutex_cmd);
		head = PORT_info[index-1].write_head;	
		tail = PORT_info[index-1].write_tail;
pthread_mutex_unlock(&mutex_cmd);

		if( (tail+1)%MAX_WRITE_BUFF == head )
		{
			dprintf("[CMD queue %d] Write buffer is full!!\n",index);
pthread_mutex_lock(&mutex_mem);
#ifdef COM
			io_data[0].INF_mem.INF_data.tag[10+index-1] |= htons(0x0002);
#else
			INF_mem[0].INF_data.tag[10+index-1] |= htons(0x0002);
#endif			
pthread_mutex_unlock(&mutex_mem);

			return;
		}
		else
		{
pthread_mutex_lock(&mutex_cmd);
			PORT_info[index-1].write_buff[tail][0] = index;
			PORT_info[index-1].write_buff[tail][1] = fcode;
			PORT_info[index-1].write_buff[tail][2] = (mbhdr->address)>>8;
			PORT_info[index-1].write_buff[tail][3] = (mbhdr->address)&0x00FF;
			PORT_info[index-1].write_buff[tail][4] = (mbhdr->points)>>8;
			PORT_info[index-1].write_buff[tail][5] = (mbhdr->points)&0x00FF;
			PORT_info[index-1].write_length[tail] = 6;

			PORT_info[index-1].write_tail++;
			PORT_info[index-1].write_tail %= MAX_WRITE_BUFF;
pthread_mutex_unlock(&mutex_cmd);
#if 1
			dprintf("[CMD queue %d] head:%d tail:%d\n",index,
				head,PORT_info[index-1].write_tail);
			sprintf(str,"\t");
			for(i=0; i<6; i++) 
			{
				sprintf(tempstr,"%02X ",PORT_info[index-1].write_buff[tail][i]);
				strcat(str,tempstr);				
			}
			dprintf("%s\n",str);
#endif
pthread_mutex_lock(&mutex_mem);
#ifdef COM
			if((io_data[0].INF_mem.INF_data.tag[10+index-1]& htons(0x0002)) != 0)
			{
				delay_ovrf++;
				if(delay_ovrf >= 10)
				{
					io_data[0].INF_mem.INF_data.tag[10+index-1] &= ~htons(0x0002);
					delay_ovrf = 0;
				}
			}
#else
			if((INF_mem[0].INF_data.tag[10+index-1]& htons(0x0002)) != 0)
			{
				delay_ovrf++;
				if(delay_ovrf >= 10)
				{
					INF_mem[0].INF_data.tag[10+index-1] &= ~htons(0x0002);
					delay_ovrf = 0;
				}
			}
#endif			
pthread_mutex_unlock(&mutex_mem);

			memcpy(res_mbhdr,mbhdr,8);
			sendSize = sizeof(struct hdbus)+8;
		}
	break;
	case write_mult_regs:
		if((address+2)>2048){
			dprintf("[HDLC] Modbus write space error [%d]-[%d]\n",
				address,address+1);
			return;
		}

		pbyte = (__u8 *)(buff+sizeof(struct hdbus)+4);
		memcpy(&points2,pbyte,4);
pthread_mutex_lock(&mutex_mem);
#ifdef COM
		memcpy(&io_data[index].INF_mem.INF_data.tag[address],&points2,4);
#else
		memcpy(&INF_mem[index].INF_data.tag[address],&points2,4);
#endif		
pthread_mutex_unlock(&mutex_mem);

		if( index <= 0 ) return;

pthread_mutex_lock(&mutex_cmd);
		head = PORT_info[index-1].write_head;	
		tail = PORT_info[index-1].write_tail;
pthread_mutex_unlock(&mutex_cmd);

		if( (tail+1)%MAX_WRITE_BUFF == head )
		{
			dprintf("[CMD queue %d] Write buffer is full!!\n",index);
pthread_mutex_lock(&mutex_mem);
#ifdef COM
			io_data[0].INF_mem.INF_data.tag[10+index-1] |= htons(0x0002);
#else
			INF_mem[0].INF_data.tag[10+index-1] |= htons(0x0002);
#endif			
pthread_mutex_unlock(&mutex_mem);

			return;
		}
		else
		{
pthread_mutex_lock(&mutex_cmd);
			PORT_info[index-1].write_buff[tail][0] = index;
			PORT_info[index-1].write_buff[tail][1] = fcode;
			PORT_info[index-1].write_buff[tail][2] = (mbhdr->address)>>8;
			PORT_info[index-1].write_buff[tail][3] = (mbhdr->address)&0x00FF;
			PORT_info[index-1].write_buff[tail][4] = 2;	// byte order
			PORT_info[index-1].write_buff[tail][5] = 0;
			PORT_info[index-1].write_buff[tail][6] = 4;
			memcpy(&PORT_info[index-1].write_buff[tail][7],&points2,4);
			PORT_info[index-1].write_length[tail] = 11;
	
			PORT_info[index-1].write_tail++;
			PORT_info[index-1].write_tail %= MAX_WRITE_BUFF;
pthread_mutex_unlock(&mutex_cmd);
#if 1
			dprintf("[CMD queue %d] head:%d tail:%d\n",index,
				head,PORT_info[index-1].write_tail);
			sprintf(str,"\t");
			for(i=0; i<11; i++)
			{
				sprintf(tempstr,"%02X ",PORT_info[index-1].write_buff[tail][i]);
				strcat(str,tempstr);
			}
			dprintf("%s\n",str);
#endif
pthread_mutex_lock(&mutex_mem);
#ifdef COM
			if((io_data[0].INF_mem.INF_data.tag[10+index-1]& htons(0x0002)) != 0)
			{
				delay_ovrf++;
				if(delay_ovrf >= 10)
				{
					io_data[0].INF_mem.INF_data.tag[10+index-1] &= ~htons(0x0002);
					delay_ovrf = 0;
				}
			}
#else
			if((INF_mem[0].INF_data.tag[10+index-1]& htons(0x0002)) != 0)
			{
				delay_ovrf++;
				if(delay_ovrf >= 10)
				{
					INF_mem[0].INF_data.tag[10+index-1] &= ~htons(0x0002);
					delay_ovrf = 0;
				}
			}
#endif			
pthread_mutex_unlock(&mutex_mem);

			memcpy(res_mbhdr,mbhdr,6);
			pbyte = (__u8 *)(send_buff+6);

			crc = CRC16((__u8 *)res_mbhdr,6)&0xFFFF;
			*pbyte++ = crc>>8;
			*pbyte++ = crc&0x00FF;

			sendSize = sizeof(struct hdbus)+8;
		}
	break;
	default:
		dprintf("[HDLC] HDMB function code error : %d\n", fcode);
		return;
	break;
	}
	
	hdlc_status[port].send_size = sendSize;
	memcpy(hdlc_status[port].send_buff, send_buff,sendSize);

	if( hdlc_write(fd, send_buff,sendSize) > 0 )
	{
		hdlc_tx_cnt[port]++;
		hdbus_tx_cnt++;

#ifdef DEBUG
		dprintf("hdlc tx[%d] rx[%d]\n",hdbus_tx_cnt,hdbus_rx_cnt);
#endif

		hdlc_alive = 1; // true;
	}
}

static void hdlc_recv(__u8 index, int fd)
{
	__u8  rzbuffer[RECV_BUFF_SIZE];
	int len;
	
	int i;
	struct timeval time;
	struct tm *gmt;

	len = hdlc_read(fd, rzbuffer, RECV_BUFF_SIZE-3);
	if(len < 6){
		dprintf("[HDLC %d] recv error\n",index);
		return;
	}

	hdbus_thr_cnt++;
	hdbus_rx_cnt++;
	hdlc_status[index].recv_size = len;
	memcpy(hdlc_status[index].recv_buff, rzbuffer, len);
		
#ifdef DEBUG
	gettimeofday(&time,NULL);
	gmt = (struct tm *)localtime(&time.tv_sec);

	dprintf("%02d:%02d:%02d.%06d ",gmt->tm_hour,
		gmt->tm_min,gmt->tm_sec,time.tv_usec);
		 
	for( i = 0 ; i < len ; i++)
		dprintf("%02x|",rzbuffer[i]);
	dprintf("\n");
#endif

	if( !(rzbuffer[len-1] & SAB82532_RSTA_CRC) ) {
		hdlc_err_size = (len<RECV_BUFF_SIZE)?len:RECV_BUFF_SIZE;
		memcpy(hdlc_err_frame, rzbuffer, hdlc_err_size);
		hdlc_err_cnt++;
		dprintf("[HDLC %d] CRC error!\n",index);
		return;
	}
		
	hdmbp(index, fd, rzbuffer, len);

	hdlc_rx_cnt[index]++;
}

void hdmbp_thread(void *arg)
{
	fd_set readfds,	testfds;
	int fd, result;
	int nread;
	pid_t my_pid;
	int index;
	struct timeval timeout;

	index = (int)arg;
	pthread_detach(pthread_self());
	
	if( index == 0 )
	{
		init_queue();
		open_map();

#ifdef DDEBUG
		open_dlink();
#endif
		open_auxlink();
	}

	sleep(20);

restart:
	fd = open_link(index);
	if( fd < 0 ) goto restart;

	printf("[HDLC %d] HDLC Comm Start\n",index);
	
	FD_ZERO(&readfds);
	FD_SET(fd, &readfds);

	my_pid = getpid();
	setpriority(PRIO_PROCESS, my_pid, -10);

	while(1){
		testfds	= readfds;

		timeout.tv_sec = 20;
		timeout.tv_usec = 0;

		if(select(fd+1, &testfds, (fd_set*)0, (fd_set*)0, (struct timeval*)&timeout) != 0)
		{
			if(FD_ISSET(fd,	&testfds)) {
				ioctl(fd, FIONREAD, &nread);
				if(nread > 0) {
					hdlc_recv(index, fd);
				}
			}
		}
		else
		{
			dprintf("[HDLC %d] Port Reopen\n",index);
			close(fd);
			goto restart;
		}
	}
}

