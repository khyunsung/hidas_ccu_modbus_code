#ifndef _CARD_INFO_H_
#define _CARD_INFO_H_

//#include "../../include/protocol_frame.h"
typedef struct{
	u8 sys;
	u16 point;
} tLED;

typedef struct{
	u8 cnt[2];
	u8 bo[2];
} tCAN;

typedef struct {
	u8 Addr;
	u8 Type;
	u8 scantime;
} tCardInfo;


extern tCardInfo CardInfo;
extern tLED led;
#endif

