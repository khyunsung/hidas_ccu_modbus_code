#ifndef	_SCMBP_H
#define	_SCMBP_H

#if 0
struct scm_status_struct{
	int		recv_size, send_size;
	__u8	recv_buff[RECV_BUFF_SIZE];
	__u8	send_buff[RECV_BUFF_SIZE];
};

#pragma pack(1)
struct scmb_data{
	__u8	id;
	__u32	data;
};
#pragma pack()

#endif

void scmbp_thread(void *arg);

#endif		// _SCMBP_H
