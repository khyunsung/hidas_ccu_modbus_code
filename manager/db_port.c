#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <pthread.h>

#include <asm/types.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>

#include "common.h"
#include "icss_types.h"
//#include "mydefs.h"
//#include "cocos.h"
#include "db_port.h"
#include "port.h"
#include "modbus.h"
#include "utils.h"
#include "log.h"

//#define	MODBUS_ADDRESS	16999

//#define	DEBUG
#ifndef	DEBUG
	#define	dprintf(format, args...) log_access(format, ## args)
#else
	#define	dprintf(format, args...) log_access(format, ## args); printf(format, ## args)
#endif	// DEBUG

#define DB_PORT	5601
#define BASE	5602

extern int port_tx_cnt[MAX_INTERFACE+1], port_rx_cnt[MAX_INTERFACE+1], port_thr_cnt[MAX_INTERFACE+1];
extern __u8  port_alive[MAX_INTERFACE+1];

extern PORT_INFO PORT_info[MAX_INTERFACE];
extern INF_MEM INF_mem[MAX_INTERFACE+1];

extern __u8 db_update;

static int open_link()
{
	int sockfd;
	struct sockaddr_in server;
	int val, result;

	sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);	
	if(sockfd == -1){
		dprintf("[DB PORT] socket open error!!!\n");
		sleep(1);
		return -1;
	}

	memset( (char*)&server, 0, sizeof (server) );
	server.sin_addr.s_addr = htonl(INADDR_ANY);
	server.sin_family = AF_INET;
	server.sin_port = htons(BASE);

	result = bind(sockfd, (struct sockaddr*)&server, sizeof(server));
	if(result == -1){
		dprintf("[DB PORT] bind retry...\n");
		result = bind(sockfd, (struct sockaddr*)&server, sizeof(server));
		if(result == -1){
			dprintf("[DB PORT] bind error\n");
			sleep(1);
			return -1;
		}
	}

//	val |= FD_CLOEXEC;
//	fcntl(sockfd,F_SETFD,val);
	fcntl(sockfd,F_SETFD,FD_CLOEXEC);

	return sockfd;
}

struct sockaddr_in db_tuner;

static int port_read(__u16 port, __u8 *buff, int len)
{
	unsigned int addr_len;

#if 0
int i;
dprintf("TUNER->SCM [");
for(i=0;i<len;i++) { dprintf("%02X|",buff[i]); }
dprintf("]\n");
#endif

	addr_len = sizeof(db_tuner);
	return recvfrom(port,buff,len,0,(struct sockaddr *)&db_tuner,&addr_len);
}

static int port_write(__u16 port, __u8 *buff, int len)
{
	if(sendto(port,buff,len,0,(struct sockaddr *)&db_tuner,sizeof(db_tuner)) == len)
	{
#ifdef DEBUG
int i;
dprintf("SCM->TUNER [");
for(i=0;i<len;i++) { dprintf("%02X|",buff[i]); }
dprintf("]\n");
#endif
		return 1;
	}
	return -1;
}

#define UDP_MAX   2600
#define BLOCK_SIZE 140

unsigned char dn_mode;
unsigned char trans_id;
unsigned int total_size, recv_size;
FILE *dn_fp, *temp_fp;
unsigned short crc_table[20];
COMDBFILE com_db;
int timeset;

static void initial()
{
	memset(&com_db,0,sizeof(com_db));

	dn_mode = DB_IDLE;
	trans_id = 0;
	total_size = 0;
	recv_size = 0;
	dn_fp = NULL;
	temp_fp = NULL;

	timeset = 30;
}

static void dbport_handle(int sock)
{
	char rzBuff[UDP_MAX];
	char sdBuff[10];
	char path_name[256], temp_name[256];
	unsigned char ret_mode;
	
	int i, len, ret = -1;
	CMD_HEADER *cmd_hdr;
	__u16 block_id = 0;
	__u8 record_count = 0;

	len = port_read(sock, rzBuff, UDP_MAX);
	if(len < 3) return;
	if(len >= UDP_MAX ) return;

	cmd_hdr = (CMD_HEADER *)rzBuff;
	
	if( cmd_hdr->dst != 0x01 ) return;
	if( cmd_hdr->src != 0x0F ) return;
	
	switch(cmd_hdr->cmd)
	{
	case CMD_SCM_REQ_DNFILE:
		if( dn_mode != DB_IDLE && dn_mode != SCM_REQ_SUCCESS)
		{
			dprintf("[DB PORT] ID:%03d \tError: State Incorrect [1]\n",trans_id);
			return;
		}

		dprintf("[DB PORT] ID:%03d <=COMBD (len: %d)\n",trans_id,len);

		if( len != sizeof(COMDBFILE)+3 )
		{
			dprintf("[DB PORT] ID:%03d \tError: Wrong COMBD Size\n",trans_id);
			dn_mode = DB_IDLE;
			ret = -1;
		}
		else
		{
			memcpy(&com_db,&rzBuff[3],sizeof(COMDBFILE));
			dprintf("[DB PORT] ID:%03d \tfile crc: %d\n",trans_id,com_db.filecrc);
			dprintf("[DB PORT] ID:%03d \trsc: %d\n",trans_id,com_db.rsc);
			dprintf("[DB PORT] ID:%03d \tsys_type: %s\n",trans_id,com_db.systype?"aconis-ds":"aconis-2000");
			dprintf("[DB PORT] ID:%03d \tftime: %d\n",trans_id,com_db.ftime);
			dprintf("[DB PORT] ID:%03d \tcnt: %d\n",trans_id,com_db.cnt);
			dprintf("[DB PORT] ID:%03d \tsize: %d\n",trans_id,com_db.size);
			dprintf("[DB PORT] ID:%03d \tversion: %d.%d.%d.%d\n",trans_id,com_db.ver.aco_major,com_db.ver.aco_minor,com_db.ver.app_major,com_db.ver.app_minor);
			dprintf("[DB PORT] ID:%03d \tfile_name: %s\n",trans_id,com_db.name);

			dn_mode = SCM_REQ_DNFILE;
			ret = 1;
		}

		total_size = 0;
		ret_mode = HI_ACK_DNLOAD;
		break;
	case CMD_SCM_PTC_DNLOAD: // 임시 파일을 생성하여 순차적 저장
		if( dn_mode != SCM_REQ_DNFILE && dn_mode != SCM_PTC_DNLOAD )
		{
			dprintf("[DB PORT] ID:%03d \tError: State Incorrect [2]\n",trans_id);
			return;
		}

//		dprintf("[DB PORT] ID:%03d <=%s Downloading (len: %d)\n",trans_id,com_db.name,len);
	
		if( len < 6+BLOCK_SIZE )
		{
			dprintf("[DB PORT] ID:%03d \tError: block size too small\n",trans_id);
			ret = -1;
			ret_mode = HI_ACK;
			break;
		}

		block_id = (rzBuff[4]<<8)|rzBuff[3];
		record_count = rzBuff[5];

//		dprintf("[DB PORT] ID:%03d \tblock_id:%d record_count:%d\n",trans_id,block_id,record_count);
	
		if( record_count <= 0 || record_count > 18 )
		{
			dprintf("[DB PORT] ID:%03d \tError: record count out of boundary\n",trans_id);
			ret = -1;
			ret_mode = HI_ACK;
			break;
		}

		if( record_count*BLOCK_SIZE != len-6 )
		{
			dprintf("[DB PORT] ID:%03d \tError: block size incorrect (%d)-(%d)\n",trans_id,
				record_count*BLOCK_SIZE,len-6);
			ret = -1;
			ret_mode = HI_ACK;
			break;
		}

		sprintf(temp_name,DB_DIR"/%s.tmp",com_db.name);

		temp_fp = fopen(temp_name, "r+");
		if(temp_fp == NULL)
		{
			dn_fp = fopen(temp_name,"w");
			if(dn_fp == NULL)
			{
				dprintf("[DB PORT] ID:%03d \tError: %s file creation\n",trans_id,temp_name);
				ret = -1;
				ret_mode = HI_ACK;
				break;
			}
		}
		else
		{
			dn_fp = temp_fp;
//			if( block_id > 0 ) fseek(dn_fp,0,SEEK_END);
			if( block_id >= 0 ) fseek(dn_fp,BLOCK_SIZE*record_count*block_id,SEEK_SET);
		}

		recv_size = fwrite(&rzBuff[6],BLOCK_SIZE,record_count,dn_fp);
		total_size += recv_size;
		fclose(dn_fp);

		temp_fp = NULL;
		dn_fp = NULL;

		ret = 1;
		ret_mode = HI_ACK;
		dn_mode = SCM_PTC_DNLOAD;
		break;
	case CMD_SCM_REQ_SUCCESS: // 용량이 맞는지 체크하여 임시파일 이름 변경
		if( dn_mode != SCM_PTC_DNLOAD ) 
		{
			dprintf("[DB PORT] ID:%03d \tError: State Incorrect [3]\n",trans_id);
			return;
		}

		dprintf("[DB PORT] ID:%03d <=%s Download Success\n",trans_id,com_db.name);

		sprintf(temp_name,DB_DIR"/%s.tmp",com_db.name);

#if 1
		if( com_db.cnt != total_size )
		{
			dprintf("[DB PORT] ID:%03d \tError: recv block size (%d)-(%d)\n",trans_id,
				com_db.cnt,total_size);

			total_size = 0;
//			remove(temp_name);

			ret = -1;
			ret_mode = HI_ACK_END;
			break;
		}
#endif

		sprintf(path_name,DB_DIR"/%s",com_db.name);

		total_size = 0;
		rename(temp_name,path_name);
		
		ret = 1;
		ret_mode = HI_ACK_END;
		dn_mode = SCM_REQ_SUCCESS;

		timeset = 60*5; // 5min
		break;
	case CMD_SCM_DNLOAD_END: // DB 변경 적용
		if( dn_mode != SCM_REQ_SUCCESS )
		{
			dprintf("[DB PORT] ID:%03d \tError: State Incorrect [4]\n",trans_id);
			return;
		}

		dprintf("[DB PORT] ID:%03d DB Download Done!!!\n",trans_id);
		dprintf("[DB PORT] ID:%03d \tApply...\n",trans_id);

		// DB 적용 루틴
		db_update = 1;

		ret = 1;
		ret_mode = HI_ACK_APPLY;
		dn_mode = DB_IDLE;

		timeset = 30;
		break;
	default:
		dprintf("[DB PORT] ID:%03d \tError: undefined CMD\n",trans_id);
		//initial();
		return;
	}

	cmd_hdr = (CMD_HEADER *)sdBuff;

	if( ret_mode == HI_ACK_DNLOAD || ret_mode == HI_NACK_DNLOAD )
		cmd_hdr->cmd = CMD_SCM_REQ_DNFILE;
	else if( ret_mode == HI_ACK || ret_mode == HI_NACK )
		cmd_hdr->cmd = CMD_SCM_PTC_DNLOAD;
	else if( ret_mode == HI_ACK_END || ret_mode == HI_NACK_END )
		cmd_hdr->cmd = CMD_SCM_REQ_SUCCESS;
	else if( ret_mode == HI_ACK_APPLY || ret_mode == HI_NACK_APPLY )
		cmd_hdr->cmd = CMD_SCM_DNLOAD_END;
	else
	{
		dprintf("[DB PORT] ID:%03d \tError: undefined logic\n",trans_id);
		//initial();
		return;
	}

	cmd_hdr->src = 0x01;
	cmd_hdr->dst = 0x0F;

	if( ret > 0 )
	{
		sdBuff[3] = 0x01;
		sdBuff[4] = 0x01;
//		dprintf("[DB PORT] ID:%03d =>ACK\n",trans_id);
	}
	else
	{
		sdBuff[3] = 0x0F;
		sdBuff[4] = 0x0F;
		dprintf("[DB PORT] ID:%03d =>NACK!!!\n",trans_id);
		dn_mode = DB_IDLE;
	}

	port_write(sock,sdBuff,5);

	trans_id++;
}

void dbport_thread(void *arg)
{
	int len, result, optval=1;
	fd_set readfds,	testfds;
	int i, j;
	int sockfd, nread;
	struct timeval timeout;

	pthread_detach(pthread_self());

restart:	
	sockfd = open_link();
	if( sockfd < 0 ) goto restart;

	dprintf("DB Port Start\n");
	FD_ZERO(&readfds);
	FD_SET(sockfd, &readfds);

	initial();
			
	while(1){
		testfds	= readfds;

		timeout.tv_sec = timeset;
		timeout.tv_usec = 0;

		if(select(sockfd+1, &testfds, (fd_set*)0, (fd_set*)0, (struct timeval*)&timeout) != 0)
		{
			if(FD_ISSET(sockfd,&testfds)) {
				ioctl(sockfd, FIONREAD, &nread);
				if(nread > 0) {
					dbport_handle(sockfd);
				}
			}
		}
		else
		{
//			dprintf("[DB PORT] Port Reopen\n");
//			close(sockfd);
			usleep(10000);
//			goto restart;
			continue;
		}

		usleep(10000);
	}
	
	close(sockfd);
}
