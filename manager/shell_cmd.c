#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/utsname.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <semaphore.h>
#include <sys/ioctl.h>
#include "../include/common.h"

#ifdef COM
#include "../include/hicsr.h"
#endif
#include "log.h"


#include "shell.h"

//#define	DEBUG
#ifndef	DEBUG
	#define	dprintf(format, args...)
	#define	tprintf(format, args...) printf( "%s:%d:" format "\n", __FUNCTION__, __LINE__ , ## args)
#else
	#define	dprintf(format, args...) printf(format"\n", ## args);fflush(stdout)
#endif	// DEBUG


#ifdef COM
extern IO_DATA io_data[MAX_INTERFACE+1];
#else
extern INF_MEM INF_mem[MAX_INTERFACE+1];
#endif

extern unsigned char myID;

enum color_code{
	BLACK,
	GRAY,
	MAROON,
	RED,
	GREEN,
	LIME,
	OLIVE,
	YELLOW,
	NAVY,
	BLUE,
	PURPLE,
	FUCHSIA,
	TEAL,
	AQUA,
	SILVER,
	WHITE
};
const char *strcolor[16]={
	"black",
	"gray",
	"maroon",
	"red",
	"green",
	"lime",
	"olive",
	"yellow",
	"navy",
	"blue",
	"purple",
	"fuchsia",
	"teal",
	"aqua",
	"silver",
	"white"
};


extern char shbuff[MAX_SHBUFF];
extern int refresh_time;

#define HTML_TABLE_HEADER_	"<table bgcolor='snow' border=2 bordercolor=black cellpadding=3>\r\n"
#define HTML_TABLE_HEADER_200	"<table bgcolor='snow' border=2 bordercolor=black cellpadding=3 width=200>\r\n"
#define HTML_TABLE_HEADER_400	"<table bgcolor='snow' border=2 bordercolor=black cellpadding=3 width=400>\r\n"
#define HTML_TABLE_HEADER_500	"<table bgcolor='snow' border=2 bordercolor=black cellpadding=3 width=500>\r\n"
#define HTML_TABLE_HEADER_600	"<table bgcolor='snow' border=2 bordercolor=black cellpadding=3 width=600>\r\n"
#define HTML_TABLE_HEADER_700	"<table bgcolor='snow' border=2 bordercolor=black cellpadding=3 width=700>\r\n"
#define HTML_TABLE_ROW_		"<tr align=center>"


////////////////////////////
/// function command format
////////////////////////////
#ifdef func_
static int func_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;

	return ret;
}
#endif

/*
static int datamngr_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;

	shprintf("<UL>");
	shprintf("<li>attitude...\n");
	shprintf("src_attitude:%s(%04x)\n", strdev(data_manager.src_attitude),data_manager.src_attitude);
	shprintf("manual_status:%04x\n", data_manager.manual_status);
	shprintf("heading:%.5f\n", data_manager.heading);
	shprintf("roll:%.5f\n", data_manager.roll);
	shprintf("pitch:%.5f\n", data_manager.pitch);
	shprintf("heading_rate:%.5f\n", data_manager.heading_rate);
	shprintf("roll_rate:%.5f\n", data_manager.roll_rate);
	shprintf("pitch_rate:%.5f\n", data_manager.pitch_rate);
	shprintf("heave:%.5f\n", data_manager.heave);
	shprintf("heave_rate:%.5f\n", data_manager.heave_rate);

	shprintf("<li>position...\n");
	shprintf("src_position:%s(%04x)\n", strdev(data_manager.src_position),data_manager.src_position);
	shprintf("latitude:%.5lf\n", data_manager.latitude);
	shprintf("longitude:%.5lf\n", data_manager.longitude);
	shprintf("stddev_north:%d\n", data_manager.stddev_north);
	shprintf("stddev_east:%d\n", data_manager.stddev_east);

	shprintf("<li>water speed...\n");
	shprintf("src_waterspd:%s(%04x)\n", strdev(data_manager.src_stw),data_manager.src_stw);
	shprintf("waterspd_longitudinal:%.5f\n", data_manager.stw_longitudinal);
	shprintf("waterspd_transverse:%.5f\n", data_manager.stw_transverse);

	shprintf("<li>ground speed...\n");
	shprintf("src_groundspd:%s(%04x)\n", strdev(data_manager.src_sog),data_manager.src_sog);
	shprintf("groundspd_longitudinal:%.5f\n", data_manager.sog_longitudinal);
	shprintf("groundspd_transverse:%.5f\n", data_manager.sog_transverse);
	shprintf("groundspd_vertical:%.5f\n", data_manager.sog_vertical);

	shprintf("<li>propeller speed...\n");
	shprintf("status_propellerspd:%04x\n", data_manager.status_rpm);
	shprintf("propellerspd_nom:%d\n", data_manager.rpm_nom);
	shprintf("propellerspd_act:%d\n", data_manager.rpm_act);

	shprintf("<li>diving depth...\n");
	shprintf("src_divingdep:%s(%04x)\n", strdev(data_manager.src_divingdep),data_manager.src_divingdep);
	shprintf("divingdep:%.5f\n", data_manager.divingdep);

	shprintf("<li>water depth...\n");
	shprintf("src_waterdep:%s(%04x)\n", strdev(data_manager.src_waterdep),data_manager.src_waterdep);
	shprintf("waterdep:%.5f\n", data_manager.waterdep/10.f);

	shprintf("<li>set and drift...\n");
	shprintf("src_setndrift:%s(%04x)\n", strdev(data_manager.src_setndrift),data_manager.src_setndrift);
	shprintf("direction:%.5f\n", data_manager.direction);
	shprintf("currentspd:%.5f\n", data_manager.currentspd);

	shprintf("<li>ctd/s2vtd...\n");
	shprintf("status_probes:%04x\n", data_manager.status_probes);
	shprintf("salinity1:%.5f\n", data_manager.salinity);
	shprintf("temperature1:%.5f\n", data_manager.temperature);
	shprintf("density1:%.5f\n", data_manager.density);
	shprintf("soundvel1:%.5f\n", data_manager.soundvel);
	shprintf("depth1:%.5f\n", data_manager.depth);

	shprintf("<li>HMEU status...\n");
	shprintf("control_mcu:%04x\n", data_manager.control_mcu);
	shprintf("config_mcu:%04x\n", data_manager.config_mcu);
	shprintf("mode_mcu:%04x\n", data_manager.mode_mcu);
	shprintf("operation_mcu:%04x\n", data_manager.operation_mcu);
	shprintf("status_mcu:%04x\n", data_manager.status_mcu);
	shprintf("snorkel_position:%.2f\n", data_manager.snorkel_position);
	shprintf("com_position:%.2f\n", data_manager.com_position);
	shprintf("hf_position:%.2f\n", data_manager.hf_position);
	shprintf("combi_position:%.2f\n", data_manager.combi_position);
	shprintf("oss1_position:%.2f\n", data_manager.oss1_position);
	shprintf("oss2_position:%.2f\n", data_manager.oss2_position);
	shprintf("radar_position:%.2f\n", data_manager.radar_position);
	shprintf("command_echo:%04x\n", data_manager.command_echo);
	shprintf("oss1cmd_echo:%.2f\n", data_manager.oss1cmd_echo);
	shprintf("oss2cmd_echo:%.2f\n", data_manager.oss2cmd_echo);
	shprintf("error_mcu:%04d\n", data_manager.error_mcu);

	shprintf("<li>ssc...\n");
	shprintf("hpf_nom:%.5f\n", data_manager.hpf_nom);
	shprintf("hpf_act:%.5f\n", data_manager.hpf_act);
	shprintf("hpa_nom:%.5f\n", data_manager.hpa_nom);
	shprintf("hpa_act:%.5f\n", data_manager.hpa_act);
	shprintf("sr_nom:%.5f\n", data_manager.sr_nom);
	shprintf("sr_act:%.5f\n", data_manager.sr_act);
	shprintf("pitch:%.5f\n", data_manager.sscpitch);
	shprintf("roll:%.5f\n", data_manager.sscroll);
	shprintf("status:%d\n", data_manager.ssc_status);

	shprintf("<li>time, test...\n");
	shprintf("timestamp:%d\n", data_manager.timestamp);
	shprintf("timeoffset:%d\n", data_manager.timeoffset);
	shprintf("test:%c\n", data_manager.test);
	shprintf("</UL>");

	shprintf("<li>network...\n");
	shprintf("serial_status1:%08x\n", data_manager.serial_status1);
	shprintf("serial_status2:%08x\n", data_manager.serial_status2);
	shprintf("network_status1:%08x\n", data_manager.network_status1);
	shprintf("network_status2:%08x\n", data_manager.network_status2);

	return ret;
}

static int commsg_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
	int i;
	int res;
	char *color;

	COM_MSG *com;

	shprintf("<li>COM_MSG...\n");
	shprintf(HTML_TABLE_HEADER_700);
	shprintf(HTML_TABLE_ROW_"<td>name</td><td>port</td><td>fd</td><td>sem</td><td>dev</td><td>alive</td>");
	shprintf("<td>rzlen</td><td>rzseq</td><td>rzbuf</td><td>szlen</td><td>szseq</td><td>szbuf</td>");
	shprintf("<td>dataptr</td><td>next</td><td>timeout</td><td>mtime</td></tr>\n");

	for(i=0; i<MAX_DEV; i++){
		com = &devcom[i];
		if(com->dev==0)	continue;
		sem_getvalue(&com->sem, &res);

		if(com->alive){
			color = "green";
		} else{
			color = "red";
		}

		shprintf("<tr></tr>");
		shprintf("<tr align=center bgcolor=%s><td>%s</td><td>%d</td><td>%d</td><td>%d</td><td>%d</td><td>%d</td>", color,
				strdev(com->dev), com->port, com->fd, res, com->dev, com->alive);

		shprintf("<td>%d</td><td>%d</td><td>%p</td><td>%d</td><td>%d</td><td>%p</td>",
				com->rzlen, com->rzseq, com->rzbuf, com->szlen, com->szseq, com->szbuf);

		shprintf("<td>%p</td><td>%p</td><td>%d</td><td>%d</td></tr>\n",
				com->dataptr, com->next, com->timeout, com->mtime);
	}
	shprintf("</table>");
	return ret;
}


static int ipms_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
	COM_MSG *com;
	struct ipms *ipms;

	com = &devcom[IPMS_DEV];
	if(com->dataptr==NULL){
		shprintf("No Assign dev %d...!!!", IPMS_DEV);
		return ret;
	}
	ipms = com->dataptr;

	shprintf("<b>IPMS...</b>\n");

	shprintf("\n<ul>");
	shprintf("<li>IPMS data...\n");
	shprintf("batt_state:%d\n", ipms->batt_state);
	shprintf("rpm_state:%d\n", ipms->rpm_state);
	shprintf("rpm_act:%.5f\n", ipms->rpm_act);
	shprintf("rpm_sp:%.5f\n", ipms->rpm_sp);

	shprintf("submarine_state:%d\n", ipms->submarine_state);
	shprintf("c_alarm:%d\n", ipms->c_alarm);

	ret += comm_dump(sfd, com);

	return ret;
}

static int ssc_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
	COM_MSG *com;
	struct ssc *ssc;

	com = &devcom[SSC_DEV];
	if(com->dataptr==NULL){
		shprintf("No Assign dev...!!!");
		return ret;
	}
	ssc = com->dataptr;

	shprintf("<b>SSC...</b>\n");

	shprintf("\n<ul>");
	shprintf(" <li>SSC data...\n");
	shprintf("hpf_nom:%.5f\n", ssc->hpf_nom);
	shprintf("hpf_act:%.5f\n", ssc->hpf_act);
	shprintf("hpa_nom:%.5f\n", ssc->hpa_nom);
	shprintf("hpa_act:%.5f\n", ssc->hpa_act);
	shprintf("sr_nom:%.5f\n", ssc->sr_nom);
	shprintf("sr_act:%.5f\n", ssc->sr_act);
	shprintf("pitch:%.5f\n", ssc->pitch);
	shprintf("roll:%.5f\n", ssc->roll);
	shprintf("status:%d\n", ssc->status);

	ret += comm_dump(sfd, com);

	return ret;
}

static int echosnd_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
	COM_MSG *com;
	struct echosnd *echosnd;

	com = &devcom[ECHOSND_DEV];
	if(com->dataptr==NULL){
		shprintf("No Assign dev...!!!");
		return ret;
	}

	echosnd = com->dataptr;

	shprintf("<b>Echo Sounder...</b>\n");

	shprintf("\n<ul>");
	shprintf("<li>Echo Sounder data...\n");
	shprintf("WD1:%.2f(%c)\n", echosnd->waterdep1, echosnd->waterdep1_st);
	shprintf("WD2:%.2f(%c)\n", echosnd->waterdep2, echosnd->waterdep2_st);
	shprintf("DD1:%.2f(%c)\n", echosnd->divingdep1, echosnd->divingdep1_st);
	shprintf("DD2:%.2f(%c)\n", echosnd->divingdep2, echosnd->divingdep2_st);

	ret += comm_dump(sfd, com);

	return ret;
}

static int mcu_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
	int res=0;
//	struct timespec tv;
	COM_MSG *com;
	struct mcu *mcu;

	com = &devcom[MCU_DEV];
	if(com->dataptr==NULL){
		shprintf("No Assign dev...!!!");
		return ret;
	}

	mcu = com->dataptr;

	shprintf("<li>MCU...\n");

//	tv.tv_sec = 1;
//	tv.tv_nsec = 0;
//	res = sem_timedwait(&com->sem, &tv);
	if(res==0){
		shprintf("<UL>");
		shprintf("control_mcu:%04x\n", mcu->control_mcu);
		shprintf("config_mcu:%04x\n", mcu->config_mcu);
		shprintf("mode_mcu:%04x\n", mcu->mode_mcu);
		shprintf("operation_mcu:%04x\n", mcu->operation_mcu);
		shprintf("status_mcu:%04x\n", mcu->status_mcu);

		shprintf("snorkel_position:%.2f\n", mcu->snorkel_position);
		shprintf("com_position:%.2f\n", mcu->com_position);
		shprintf("hf_position:%.2f\n", mcu->hf_position);
		shprintf("combi_position:%.2f\n", mcu->combi_position);
		shprintf("oss1_position:%.2f\n", mcu->oss1_position);
		shprintf("oss2_position:%.2f\n", mcu->oss2_position);
		shprintf("radar_position:%.2f\n", mcu->radar_position);
		shprintf("command_echo:%04x\n", mcu->command_echo);
		shprintf("oss1cmd_echo:%.2f\n", mcu->oss1cmd_echo);
		shprintf("oss2cmd_echo:%.2f\n", mcu->oss2cmd_echo);

		shprintf("error_mcu:%04d\n", mcu->error_mcu);

		shprintf("</UL>");

		shprintf("\n");
		shprintf("<li>Communication...\n");
		shprintf(" <li>To MCU...\n");
		ret += comm_dump(sfd, com);
//		sem_post(&com->sem);
	} else{
//		sem_getvalue(&com->sem, &res);
		shprintf("Busy MCU com:%d\n", res);
	}

	return ret;
}


static int emlog_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
	int res=0;
//	struct timespec tv;
	COM_MSG *com;
	struct emlog *emlog;

	com = &devcom[EM_LOG_DEV];
	if(com->dataptr==NULL){
		shprintf("No Assign dev...!!!");
		return ret;
	}

	emlog = com->dataptr;

	shprintf("<li>EMLOG...\n");

//	tv.tv_sec = 1;
//	tv.tv_nsec = 0;
//	res = sem_timedwait(&com->sem, &tv);
	if(res==0){
		shprintf("<UL>");
		shprintf("status_stw:%c\n", emlog->status_stw);
		shprintf("stw_longitudinal:%f\n", emlog->stw_longitudinal);
		shprintf("stw_transverse:%f\n", emlog->stw_transverse);
		shprintf("status_sog:%c\n", emlog->status_sog);
		shprintf("sog_longitudinal:%f\n", emlog->sog_longitudinal);
		shprintf("sog_transverse:%f\n", emlog->sog_transverse);
		shprintf("status_stw_stern:%c\n", emlog->status_stw_stern);
		shprintf("stw_stern_transverse:%f\n", emlog->stw_stern_transverse);
		shprintf("status_sog_stern:%c\n", emlog->status_sog_stern);
		shprintf("sog_stern_transverse:%f\n", emlog->sog_stern_transverse);
		shprintf("total_distance:%f\n", emlog->total_distance);
		shprintf("distance:%f\n", emlog->distance);
		shprintf("status_equip:%04X\n", (unsigned short)emlog->status_equip);
		shprintf("</UL>");

		shprintf("\n");
		shprintf("<li>Communication...\n");
		shprintf(" <li>To EMLOG...\n");
		ret += comm_dump(sfd, com);
//		sem_post(&com->sem);
	} else{
//		sem_getvalue(&com->sem, &res);
		shprintf("Busy SSC com:%d\n", res);
	}

	return ret;
}

static int ins1_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
	int res=0;
//	struct timespec tv;
	COM_MSG *com;
	struct ins_navs *navs;
	struct ins_navg *navg;

	com = &devcom[INS1_NAVS];	// navs
	if(com->dataptr==NULL){
		shprintf("No Assign dev...!!!");
		return ret;
	}

	navs = com->dataptr;

	shprintf("<li>INS1 NAVS...\n");

//	tv.tv_sec = 1;
//	tv.tv_nsec = 0;
//	res = sem_timedwait(&com->sem, &tv);
	if(res==0){
		shprintf("<UL>");
		shprintf("id:%04x\n", navs->id);
		shprintf("size:%d\n", navs->size);
		shprintf("status1:%02x\n", navs->status1);
		shprintf("status2:%02x\n", navs->status2);
		shprintf("status3:%02x\n", navs->status3);
		shprintf("time:%f\n", navs->time);
		shprintf("heading:%f\n", navs->heading);
		shprintf("roll:%f\n", navs->roll);
		shprintf("pitch:%f\n", navs->pitch);
		shprintf("heading_rate:%f\n", navs->heading_rate);
		shprintf("roll_rate:%f\n", navs->roll_rate);
		shprintf("pitch_rate:%f\n", navs->pitch_rate);
		shprintf("sog_longitudinal:%f\n", navs->sog_longitudinal);
		shprintf("sog_transverse:%f\n", navs->sog_transverse);
		shprintf("heave:%f\n", navs->heave);
		shprintf("heave_rate:%f\n", navs->heave_rate);

		shprintf("</UL>");

		shprintf("\n");
		shprintf("<li>Communication...\n");
		shprintf(" <li>To INS1...\n");
		ret += comm_dump(sfd, com);
//		sem_post(&com->sem);
	} else{
//		sem_getvalue(&com->sem, &res);
		shprintf("Busy INS1 com:%d\n", res);
	}

	com = &devcom[INS1_NAVG];	// navg
	if(com->dataptr==NULL){
		shprintf("No Assign dev...!!!");
		return ret;
	}
	navg = com->dataptr;
	shprintf("<li>INS1 NAVG...\n");
	if(res==0){
		shprintf("<UL>");
		shprintf("id:%04x\n", navg->id);
		shprintf("size:%d\n", navg->size);
		shprintf("freq:%02x\n", navg->freq);

		shprintf("year:%d\n", navg->year);
		shprintf("month:%d\n", navg->month);
		shprintf("day:%d\n", navg->day);
		shprintf("time:%f\n", navg->time);
		shprintf("tm_latency:%d\n", navg->tm_latency);
		shprintf("status5:%02x\n", navg->status5);
		shprintf("status1:%02x\n", navg->status1);
		shprintf("heading:%f\n", navg->heading);
		shprintf("roll:%f\n", navg->roll);
		shprintf("pitch:%f\n", navg->pitch);
		shprintf("heading_rate:%f\n", navg->heading_rate);
		shprintf("roll_rate:%f\n", navg->roll_rate);
		shprintf("pitch_rate:%f\n", navg->pitch_rate);
		shprintf("att_latency:%d\n", navg->att_latency);
		shprintf("heading_acc:%f\n", navg->heading_acc);
		shprintf("status2:%02x\n", navg->status2);
		shprintf("sog_longitudinal:%f\n", navg->sog_longitudinal);
		shprintf("sog_transverse:%f\n", navg->sog_transverse);
		shprintf("vel_latency:%d\n", navg->vel_latency);
		shprintf("sog_longitudinal_acc:%f\n", navg->sog_longitudinal_acc);
		shprintf("sog_transverse_acc:%f\n", navg->sog_transverse_acc);
		shprintf("status3:%02x\n", navg->status3);
		shprintf("status4:%02x\n", navg->status4);
		shprintf("down_vel:%f\n", navg->down_vel);
		shprintf("depth:%f\n", navg->depth);
		shprintf("ver_latency:%d\n", navg->ver_latency);
		shprintf("status11:%02x\n", navg->status11);
		shprintf("status12:%02x\n", navg->status12);
		shprintf("sog_longitudinal_accel:%f\n", navg->sog_longitudinal_accel);
		shprintf("sog_transverse_accel:%f\n", navg->sog_transverse_accel);
		shprintf("heave_accel:%f\n", navg->heave_accel);
		shprintf("acc_latency:%d\n", navg->acc_latency);
		shprintf("status10:%02x\n", navg->status10);
		shprintf("course:%f\n", navg->course);
		shprintf("sog:%f\n", navg->sog);
		shprintf("set:%f\n", navg->set);
		shprintf("drift:%f\n", navg->drift);
		shprintf("nav_latency:%d\n", navg->nav_latency);
		shprintf("status7:%02x\n", navg->status7);
		shprintf("latitude_spd:%f\n", navg->latitude_spd);
		shprintf("longitude_spd:%f\n", navg->longitude_spd);
		shprintf("status6:%02x\n", navg->status6);
		shprintf("latitude:%lf\n", navg->latitude);
		shprintf("longitude:%lf\n", navg->longitude);
		shprintf("pos_latency:%d\n", navg->pos_latency);
		shprintf("latitude_acc:%lf\n", navg->latitude_acc);
		shprintf("longitude_acc:%lf\n", navg->longitude_acc);
		shprintf("pce:%f\n", navg->pce);
		shprintf("pos_time:%d\n", navg->pos_time);
		shprintf("latitude:%lf\n", navg->latitude_last);
		shprintf("longitude:%lf\n", navg->longitude_last);
		shprintf("status8:%02x\n", navg->status8);
		shprintf("status9:%02x\n", navg->status9);

		shprintf("</UL>");

		shprintf("\n");
		shprintf("<li>Communication...\n");
		shprintf(" <li>To INS1...\n");
		ret += comm_dump(sfd, com);
//		sem_post(&com->sem);
	} else{
//		sem_getvalue(&com->sem, &res);
		shprintf("Busy INS1 com:%d\n", res);
	}

	return ret;
}


static int ins2_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
	int res=0;
//	struct timespec tv;
	COM_MSG *com;
	struct ins_navs *navs;
	struct ins_navg *navg;

	com = &devcom[INS2_NAVS];	// navs
	if(com->dataptr==NULL){
		shprintf("No Assign dev...!!!");
		return ret;
	}

	navs = com->dataptr;

	shprintf("<li>INS2 NAVS...\n");

//	tv.tv_sec = 1;
//	tv.tv_nsec = 0;
//	res = sem_timedwait(&com->sem, &tv);
	if(res==0){
		shprintf("<UL>");
		shprintf("id:%04x\n", navs->id);
		shprintf("size:%d\n", navs->size);
		shprintf("status1:%02x\n", navs->status1);
		shprintf("status2:%02x\n", navs->status2);
		shprintf("status3:%02x\n", navs->status3);
		shprintf("time:%f\n", navs->time);
		shprintf("heading:%f\n", navs->heading);
		shprintf("roll:%f\n", navs->roll);
		shprintf("pitch:%f\n", navs->pitch);
		shprintf("heading_rate:%f\n", navs->heading_rate);
		shprintf("roll_rate:%f\n", navs->roll_rate);
		shprintf("pitch_rate:%f\n", navs->pitch_rate);
		shprintf("sog_longitudinal:%f\n", navs->sog_longitudinal);
		shprintf("sog_transverse:%f\n", navs->sog_transverse);
		shprintf("heave:%f\n", navs->heave);
		shprintf("heave_rate:%f\n", navs->heave_rate);

		shprintf("</UL>");

		shprintf("\n");
		shprintf("<li>Communication...\n");
		shprintf(" <li>To INS2...\n");
		ret += comm_dump(sfd, com);
//		sem_post(&com->sem);
	} else{
//		sem_getvalue(&com->sem, &res);
		shprintf("Busy INS2 com:%d\n", res);
	}

	com = &devcom[INS2_NAVG];	// navg
	if(com->dataptr==NULL){
		shprintf("No Assign dev...!!!");
		return ret;
	}
	navg = com->dataptr;
	shprintf("<li>INS2 NAVG...\n");
	if(res==0){
		shprintf("<UL>");
		shprintf("id:%04x\n", navg->id);
		shprintf("size:%d\n", navg->size);
		shprintf("freq:%02x\n", navg->freq);

		shprintf("year:%d\n", navg->year);
		shprintf("month:%d\n", navg->month);
		shprintf("day:%d\n", navg->day);
		shprintf("time:%f\n", navg->time);
		shprintf("tm_latency:%d\n", navg->tm_latency);
		shprintf("status5:%02x\n", navg->status5);
		shprintf("status1:%02x\n", navg->status1);
		shprintf("heading:%f\n", navg->heading);
		shprintf("roll:%f\n", navg->roll);
		shprintf("pitch:%f\n", navg->pitch);
		shprintf("heading_rate:%f\n", navg->heading_rate);
		shprintf("roll_rate:%f\n", navg->roll_rate);
		shprintf("pitch_rate:%f\n", navg->pitch_rate);
		shprintf("att_latency:%d\n", navg->att_latency);
		shprintf("heading_acc:%f\n", navg->heading_acc);
		shprintf("status2:%02x\n", navg->status2);
		shprintf("sog_longitudinal:%f\n", navg->sog_longitudinal);
		shprintf("sog_transverse:%f\n", navg->sog_transverse);
		shprintf("vel_latency:%d\n", navg->vel_latency);
		shprintf("sog_longitudinal_acc:%f\n", navg->sog_longitudinal_acc);
		shprintf("sog_transverse_acc:%f\n", navg->sog_transverse_acc);
		shprintf("status3:%02x\n", navg->status3);
		shprintf("status4:%02x\n", navg->status4);
		shprintf("down_vel:%f\n", navg->down_vel);
		shprintf("depth:%f\n", navg->depth);
		shprintf("ver_latency:%d\n", navg->ver_latency);
		shprintf("status11:%02x\n", navg->status11);
		shprintf("status12:%02x\n", navg->status12);
		shprintf("sog_longitudinal_accel:%f\n", navg->sog_longitudinal_accel);
		shprintf("sog_transverse_accel:%f\n", navg->sog_transverse_accel);
		shprintf("heave_accel:%f\n", navg->heave_accel);
		shprintf("acc_latency:%d\n", navg->acc_latency);
		shprintf("status10:%02x\n", navg->status10);
		shprintf("course:%f\n", navg->course);
		shprintf("sog:%f\n", navg->sog);
		shprintf("set:%f\n", navg->set);
		shprintf("drift:%f\n", navg->drift);
		shprintf("nav_latency:%d\n", navg->nav_latency);
		shprintf("status7:%02x\n", navg->status7);
		shprintf("latitude_spd:%f\n", navg->latitude_spd);
		shprintf("longitude_spd:%f\n", navg->longitude_spd);
		shprintf("status6:%02x\n", navg->status6);
		shprintf("latitude:%lf\n", navg->latitude);
		shprintf("longitude:%lf\n", navg->longitude);
		shprintf("pos_latency:%d\n", navg->pos_latency);
		shprintf("latitude_acc:%lf\n", navg->latitude_acc);
		shprintf("longitude_acc:%lf\n", navg->longitude_acc);
		shprintf("pce:%f\n", navg->pce);
		shprintf("pos_time:%d\n", navg->pos_time);
		shprintf("latitude:%lf\n", navg->latitude_last);
		shprintf("longitude:%lf\n", navg->longitude_last);
		shprintf("status8:%02x\n", navg->status8);
		shprintf("status9:%02x\n", navg->status9);

		shprintf("</UL>");

		shprintf("\n");
		shprintf("<li>Communication...\n");
		shprintf(" <li>To INS2...\n");
		ret += comm_dump(sfd, com);
//		sem_post(&com->sem);
	} else{
//		sem_getvalue(&com->sem, &res);
		shprintf("Busy INS2 com:%d\n", res);
	}

	return ret;
}


static int combuoy_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
	int res=0;
//	struct timespec tv;
	COM_MSG *com;
	struct combuoy *combuoy;

	com = &devcom[COMBUOY_DEV];
	if(com->dataptr==NULL){
		shprintf("No Assign dev...!!!");
		return ret;
	}

	combuoy = com->dataptr;

	shprintf("<li>ComBuoy...\n");

//	tv.tv_sec = 1;
//	tv.tv_nsec = 0;
//	res = sem_timedwait(&com->sem, &tv);
	if(res==0){
		shprintf("<UL>");
		shprintf("timestamp:%d\n", combuoy->timestamp);
		shprintf("valid:%d\n", combuoy->valid);
		shprintf("latitude:%lf\n", combuoy->latitude);
		shprintf("longitude:%lf\n", combuoy->longitude);
		shprintf("speed:%f\n", combuoy->speed);
		shprintf("course:%f\n", combuoy->course);
		shprintf("datestamp:%d\n", combuoy->datestamp);
		shprintf("variation:%f\n", combuoy->variation);
		shprintf("utc:%d\n", combuoy->utc);
		shprintf("fixquality:%d\n", combuoy->fixquality);
		shprintf("satellites:%d\n", combuoy->satellites);
		shprintf("horizontal:%f\n", combuoy->horizontal);
		shprintf("altitude:%f\n", combuoy->altitude);
		shprintf("height:%f\n", combuoy->height);
		shprintf("number:%d\n", combuoy->number);
		shprintf("mode:%d\n", combuoy->mode);
		shprintf("</UL>");

		shprintf("\n");
		shprintf("<li>Communication...\n");
		shprintf(" <li>To ComBuoy...\n");
		ret += comm_dump(sfd, com);
//		sem_post(&com->sem);
	} else{
//		sem_getvalue(&com->sem, &res);
		shprintf("Busy ComBuoy com:%d\n", res);
	}

	return ret;
}
static int gps_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
	int res=0;
//	struct timespec tv;
	COM_MSG *com;
	struct gps *gps;

	com = &devcom[GPS1_DEV];
	if(com->dataptr==NULL){
		shprintf("No Assign dev...!!!");
		return ret;
	}

	gps = com->dataptr;

	shprintf("<li>GPS...\n");

//	tv.tv_sec = 1;
//	tv.tv_nsec = 0;
//	res = sem_timedwait(&com->sem, &tv);
	if(res==0){
		shprintf("<UL>");
		shprintf("second:%02d\n", gps->second);
		shprintf("minute:%02d\n", gps->minute);
		shprintf("hour:%02d\n", gps->hour);
		shprintf("millisecond:%02d\n", gps->millisecond);
		shprintf("latitude:%f\n", gps->latitude);
		shprintf("longitude:%f\n", gps->longitude);
		shprintf("quality:%d\n", gps->quality);
		shprintf("nr_satellites:%d\n", gps->nr_satellites);
		shprintf("hdop:%d\n", gps->hdop);
		shprintf("altitude:%d\n", gps->altitude);
		shprintf("geoidal:%d\n", gps->geoidal);
		shprintf("data_age:%d\n", gps->data_age);
		shprintf("ref_station:%d\n", gps->ref_station);
		shprintf("speed_over_ground:%d\n", gps->speed_over_ground);
		shprintf("course_over_ground:%d\n", gps->course_over_ground);
		shprintf("flag:%d\n", gps->flag);
		shprintf("</UL>");

		shprintf("\n");
		shprintf("<li>Communication...\n");
		shprintf(" <li>To SSC...\n");
		ret += comm_dump(sfd, com);
//		sem_post(&com->sem);
	} else{
//		sem_getvalue(&com->sem, &res);
		shprintf("Busy SSC com:%d\n", res);
	}

	return ret;
}


static int dlu_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
	int res=0;
//	struct timespec tv;
	COM_MSG *com;
	struct dlu *dlu;

	com = &devcom[DLU_NMEA_DEV];
	if(com->dataptr==NULL){
		shprintf("No Assign dev...!!!");
		return ret;
	}

	dlu = com->dataptr;

	shprintf("<li>DLU...\n");

//	tv.tv_sec = 1;
//	tv.tv_nsec = 0;
//	res = sem_timedwait(&com->sem, &tv);
	if(res==0){
		shprintf("<UL>");
		shprintf("depth:%.2f\n", dlu->depth);
		shprintf("offset:%.2f\n", dlu->offset);
		shprintf("maxrange:%.2f\n", dlu->maxrange);

		shprintf("stw_longitudinal:%.2f\n", dlu->stw_longitudinal);
		shprintf("stw_transverse:%.2f\n", dlu->stw_transverse);
		shprintf("status_stw:%c\n", dlu->status_stw);
		shprintf("sog_longitudinal:%.2f\n", dlu->sog_longitudinal);
		shprintf("sog_transverse:%.2f\n", dlu->sog_transverse);
		shprintf("status_sog:%c\n", dlu->status_sog);
		shprintf("stern_waterspd:%.2f\n", dlu->stern_waterspd);
		shprintf("status_sternwspd:%c\n", dlu->status_sternwspd);
		shprintf("stern_groundspd:%.2f\n", dlu->stern_groundspd);
		shprintf("status_sterngspd:%c\n", dlu->status_sterngspd);

		shprintf("fstw_longitudinal:%.2f\n", dlu->fstw_longitudinal);
		shprintf("fstw_transverse:%.2f\n", dlu->fstw_transverse);
		shprintf("status_fstw:%c\n", dlu->status_fstw);
		shprintf("fsog_longitudinal:%.2f\n", dlu->fsog_longitudinal);
		shprintf("fsog_transverse:%.2f\n", dlu->fsog_transverse);
		shprintf("status_fsog:%c\n", dlu->status_fsog);
		shprintf("fstw:%.2f\n", dlu->fstw);
		shprintf("status_fcstw:%c\n", dlu->status_fcstw);
		shprintf("fsog:%.2f\n", dlu->fsog);
		shprintf("status_fcsog:%c\n", dlu->status_fcsog);

		shprintf("llc:%.2f\n", dlu->llc);
		shprintf("status_llc:%c\n", dlu->status_llc);
		shprintf("currentspd:%.2f\n", dlu->currentspd);
		shprintf("status_currentspd:%c\n", dlu->status_currentspd);
		shprintf("currentdir:%.2f\n", dlu->currentdir);
		shprintf("status_currentdir:%c\n", dlu->status_currentdir);

		shprintf("online_err:%d\n", dlu->online_err);
		shprintf("transimit_power:%c\n", dlu->transimit_power);
		shprintf("tp_cmd:%c\n", dlu->tp_cmd);
		shprintf("offline_test:%c\n", dlu->offline_test);
		shprintf("test_data:%c\n", dlu->test_data);
		shprintf("operating_access:%c\n", dlu->operating_access);

		shprintf("bgr_ng780:%c\n", dlu->bgr_ng780);
		shprintf("bgr_ng740:%c\n", dlu->bgr_ng740);
		shprintf("bgr_ge160:%c\n", dlu->bgr_ge160);
		shprintf("bgr_ge255:%c\n", dlu->bgr_ge255);
		shprintf("bgr_ge227:%c\n", dlu->bgr_ge227);
		shprintf("bgr_ge259:%c\n", dlu->bgr_ge259);
		shprintf("bgr_ge239:%c\n", dlu->bgr_ge239);
		shprintf("bgr_ge228:%c\n", dlu->bgr_ge228);
		shprintf("bgr_ge050:%c\n", dlu->bgr_ge050);
		shprintf("ncdata_err:%c\n", dlu->ncdata_err);
		shprintf("ute_valid:%c\n", dlu->ute_valid);

		shprintf("distance_btm_longitudinal:%.2f\n", dlu->distance_btm_longitudinal);
		shprintf("distance_btm_trasverse:%.2f\n", dlu->distance_btm_trasverse);
		shprintf("distance_wtr_longitudinal:%.2f\n", dlu->distance_wtr_longitudinal);
		shprintf("distance_wtr_trasverse:%.2f\n", dlu->distance_wtr_trasverse);
		shprintf("distance_cvt_ground:%.2f\n", dlu->distance_cvt_ground);
		shprintf("distance_cvt_water:%.2f\n", dlu->distance_cvt_water);

		shprintf("</UL>");

		shprintf("\n");
		shprintf("<li>Communication...\n");
		shprintf(" <li>To SSC...\n");
		ret += comm_dump(sfd, com);
//		sem_post(&com->sem);
	} else{
//		sem_getvalue(&com->sem, &res);
		shprintf("Busy SSC com:%d\n", res);
	}

	return ret;
}

static int xbt_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
	int res=0;
//	struct timespec tv;
	COM_MSG *com;
	struct xbt *xbt;

	com = &devcom[XBTR_DEV];
	if(com->dataptr==NULL){
		shprintf("No Assign dev...!!!");
		return ret;
	}

	xbt = com->dataptr;

	shprintf("<li>XBT...\n");

//	tv.tv_sec = 1;
//	tv.tv_nsec = 0;
//	res = sem_timedwait(&com->sem, &tv);
	if(res==0){
		shprintf("<UL>");
		shprintf("probetype:%s\n", xbt->probetype);
		shprintf("depth:%.2f\n", xbt->depth);
		shprintf("temp:%.2f\n", xbt->temp);
		shprintf("snd_vel:%.2f\n", xbt->snd_vel);
		shprintf("</UL>");

		shprintf("\n");
		shprintf("<li>Communication...\n");
		shprintf(" <li>To XBT...\n");
		ret += comm_dump(sfd, com);
//		sem_post(&com->sem);
	} else{
//		sem_getvalue(&com->sem, &res);
		shprintf("Busy SSC com:%d\n", res);
	}

	return ret;
}

static int dptsensor1_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
	int res=0;
//	struct timespec tv;
	COM_MSG *com;
	struct dptsensor *dpt;

	com = &devcom[DEPTH1_DEV];
	if(com->dataptr==NULL){
		shprintf("No Assign dev...!!!");
		return ret;
	}

	dpt = com->dataptr;

	shprintf("<li>Depth Sensor1...\n");

//	tv.tv_sec = 1;
//	tv.tv_nsec = 0;
//	res = sem_timedwait(&com->sem, &tv);
	if(res==0){
		shprintf("<UL>");
		shprintf("src:%d\n", dpt->src);
		shprintf("dst:%d\n", dpt->dst);
		shprintf("depth:%.2f\n", dpt->depth);
		shprintf("</UL>");

		shprintf("\n");
		shprintf("<li>Communication...\n");
		shprintf(" <li>To SSC...\n");
		ret += comm_dump(sfd, com);
//		sem_post(&com->sem);
	} else{
//		sem_getvalue(&com->sem, &res);
		shprintf("Busy SSC com:%d\n", res);
	}

	return ret;
}

static int dptsensor3_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
	int res=0;
//	struct timespec tv;
	COM_MSG *com;
	struct dptsensor *dpt;

	com = &devcom[DEPTH2_DEV];
	if(com->dataptr==NULL){
		shprintf("No Assign dev...!!!");
		return ret;
	}

	dpt = com->dataptr;

	shprintf("<li>Depth Sensor3...\n");

//	tv.tv_sec = 1;
//	tv.tv_nsec = 0;
//	res = sem_timedwait(&com->sem, &tv);
	if(res==0){
		shprintf("<UL>");
		shprintf("src:%d\n", dpt->src);
		shprintf("dst:%d\n", dpt->dst);
		shprintf("depth:%.2f\n", dpt->depth);
		shprintf("</UL>");

		shprintf("\n");
		shprintf("<li>Communication...\n");
		shprintf(" <li>To SSC...\n");
		ret += comm_dump(sfd, com);
//		sem_post(&com->sem);
	} else{
//		sem_getvalue(&com->sem, &res);
		shprintf("Busy SSC com:%d\n", res);
	}

	return ret;
}

static int ctd_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
	int res=0;
//	struct timespec tv;
	COM_MSG *com;
	struct ctd *ctd;

	com = &devcom[CTD_DEV];
	if(com->dataptr==NULL){
		shprintf("No Assign dev...!!!");
		return ret;
	}

	ctd = com->dataptr;

	shprintf("<li>CTD probe...\n");

//	tv.tv_sec = 1;
//	tv.tv_nsec = 0;
//	res = sem_timedwait(&com->sem, &tv);
	if(res==0){
		shprintf("<UL>");
		shprintf("id:%d\n", ctd->id);
		shprintf("density:%.2f\n", ctd->density);
		shprintf("sound_vel:%.2f\n", ctd->sound_vel);
		shprintf("salinity:%.2f\n", ctd->salinity);
		shprintf("status_calcdata:%c\n", ctd->status_calcdata);
		shprintf("temp:%.2f, ", ctd->temp);
		shprintf("temp_range:%c, ", ctd->temp_range);
		shprintf("status_temp:%c\n", ctd->status_temp);
		shprintf("conduct:%.2f, ", ctd->conduct);
		shprintf("conduct_range:%c, ", ctd->conduct_range);
		shprintf("status_conduct:%c\n", ctd->status_conduct);
		shprintf("pressure:%.2f, ", ctd->press);
		shprintf("press_range:%c, ", ctd->press_range);
		shprintf("status_press:%c\n", ctd->status_press);
		shprintf("status_probe:%d\n\n", ctd->status_probe);

		shprintf("x:%.2f\n", ctd->x);
		shprintf("y:%.2f\n", ctd->y);
		shprintf("z:%.2f\n", ctd->z);
		shprintf("no:%.2f\n", ctd->serial);
		shprintf("myear:%d\n", ctd->myear);
		shprintf("imonth:%d\n", ctd->imonth);
		shprintf("iyear:%d\n", ctd->iyear);

		shprintf("</UL>");

		shprintf("\n");
		shprintf("<li>Communication...\n");
		shprintf(" <li>To CTD...\n");
		ret += comm_dump(sfd, com);
//		sem_post(&com->sem);
	} else{
//		sem_getvalue(&com->sem, &res);
		shprintf("Busy SSC com:%d\n", res);
	}

	return ret;
}

static int hdr_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
	int res=0;
//	struct timespec tv;
	COM_MSG *com;
//	struct s2vtd *s2vtd;

	com = &devcom[HDR_DEV];
	if(com->dataptr==NULL){
		shprintf("No Assign dev...!!!");
		return ret;
	}

//	s2vtd = com->dataptr;

	shprintf("<li>HDR probe...\n");

//	tv.tv_sec = 1;
//	tv.tv_nsec = 0;
//	res = sem_timedwait(&com->sem, &tv);
	if(res==0){
		shprintf("%s[%d]", com->szbuf, com->szlen);
//		sem_post(&com->sem);
	} else{
//		sem_getvalue(&com->sem, &res);
		shprintf("Busy SSC com:%d\n", res);
	}

	return ret;
}

static int tempsen_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
	int res=0;
//	struct timespec tv;
	COM_MSG *com;
	struct tempsen *tempsen;

	com = &devcom[CONDITION_DEV];
	if(com->dataptr==NULL){
		shprintf("No Assign dev...!!!");
		return ret;
	}

	tempsen = com->dataptr;

	shprintf("<li>TempSensor information...\n");

//	tv.tv_sec = 1;
//	tv.tv_nsec = 0;
//	res = sem_timedwait(&com->sem, &tv);
	if(res==0){
		shprintf("<UL><li>Sensor\n");
		shprintf("temp1 : %.2f\n", tempsen->temp1);
		shprintf("humidity1 : %.2f\n", tempsen->humidity1);
		shprintf("temp2 : %.2f\n", tempsen->temp2);
		shprintf("humidity2 : %.2f\n", tempsen->humidity2);
		shprintf("temp3 : %.2f\n", tempsen->temp3);
		shprintf("fan : %.2f\n", tempsen->fan);
		shprintf("</UL>");

		shprintf("<UL><li>Control\n");
		shprintf("temp1_ctrl : %.0f\n", tempsen->temp1_ctrl);
		shprintf("temp2_ctrl : %.0f\n", tempsen->temp2_ctrl);
		shprintf("humidity_ctrl : %.0f\n", tempsen->humidity_ctrl);
		shprintf("</UL>");

		shprintf("<UL><li>Fault\n");
		shprintf("fan fault : %02X\n", tempsen->fan_fault);
		shprintf("</UL>\n");

		ret += comm_dump(sfd, com);
	} else{
//		sem_getvalue(&com->sem, &res);
		shprintf("Busy TempSensor:%d\n", res);
	}

	return ret;
}

static int sensim_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
	int res=0;
	struct timespec tv;
	COM_MSG *com;
	struct sensim *sensim;

	com = &devcom[SENSIM_DEV];
	if(com->dataptr==NULL){
		shprintf("No Assign dev...!!!");
		return ret;
	}

	sensim = com->dataptr;

	shprintf("<li>S&DC...\n");


	shprintf("\n");
	shprintf("<li>Communication...\n");
	shprintf(" <li>To SSC...\n");

	tv.tv_sec = 1;
	tv.tv_nsec = 0;
//	res = sem_timedwait(&com->sem, &tv);
	if(res==0){
		shprintf("<UL>");
		shprintf("<li>attitude...\n");
		shprintf("src_attitude:%s(%04x)\n", strdev(sensim->src_attitude),sensim->src_attitude);
		shprintf("manual_status:%04x\n", sensim->manual_status);
		shprintf("heading:%.5f\n", sensim->heading);
		shprintf("roll:%.5f\n", sensim->roll);
		shprintf("pitch:%.5f\n", sensim->pitch);
		shprintf("heading_rate:%.5f\n", sensim->heading_rate);
		shprintf("roll_rate:%.5f\n", sensim->roll_rate);
		shprintf("pitch_rate:%.5f\n", sensim->pitch_rate);
		shprintf("heave:%.5f\n", sensim->heave);
		shprintf("heave_rate:%.5f\n", sensim->heave_rate);

		shprintf("<li>position...\n");
		shprintf("src_position:%s(%04x)\n", strdev(sensim->src_position),sensim->src_position);
		shprintf("latitude:%.5f\n", sensim->latitude);
		shprintf("longitude:%.5f\n", sensim->longitude);
		shprintf("stddev_north:%ld\n", sensim->stddev_north);
		shprintf("stddev_east:%ld\n", sensim->stddev_east);

		shprintf("<li>water speed...\n");
		shprintf("src_waterspd:%s(%04x)\n", strdev(sensim->src_waterspd),sensim->src_waterspd);
		shprintf("waterspd_longitudinal:%.5f\n", sensim->waterspd_longitudinal);
		shprintf("waterspd_transverse:%.5f\n", sensim->waterspd_transverse);

		shprintf("<li>ground speed...\n");
		shprintf("src_groundspd:%s(%04x)\n", strdev(sensim->src_groundspd),sensim->src_groundspd);
		shprintf("groundspd_longitudinal:%.5f\n", sensim->groundspd_longitudinal);
		shprintf("groundspd_transverse:%.5f\n", sensim->groundspd_transverse);
		shprintf("groundspd_vertical:%.5f\n", sensim->groundspd_vertical);

		shprintf("<li>propeller speed...\n");
		shprintf("status_propellerspd:%04x\n", sensim->status_propellerspd);
		shprintf("propellerspd_nom:%d\n", sensim->propellerspd_nom);
		shprintf("propellerspd_act:%d\n", sensim->propellerspd_act);

		shprintf("<li>diving depth...\n");
		shprintf("src_divingdep:%s(%04x)\n", strdev(sensim->src_divingdep),sensim->src_divingdep);
		shprintf("divingdep:%.5f\n", sensim->divingdep);

		shprintf("<li>water depth...\n");
		shprintf("src_waterdep:%s(%04x)\n", strdev(sensim->src_waterdep),sensim->src_waterdep);
		shprintf("waterdep:%.5f\n", sensim->waterdep/10.f);

		shprintf("<li>set and drift...\n");
		shprintf("src_setndrift:%s(%04x)\n", strdev(sensim->src_setndrift),sensim->src_setndrift);
		shprintf("direction:%.5f\n", sensim->direction);
		shprintf("currentspd:%.5f\n", sensim->currentspd);

		shprintf("<li>ctd/s2vtd...\n");
		shprintf("status_probes:%04x\n", sensim->status_probes);
		shprintf("salinity1:%.5f\n", sensim->salinity1);
		shprintf("temperature1:%.5f\n", sensim->temperature1);
		shprintf("density1:%.5f\n", sensim->density1);
		shprintf("soundvel1:%.5f\n", sensim->soundvel1);
		shprintf("depth1:%.5f\n", sensim->depth1);
		shprintf("salinity2:%.5f\n", sensim->salinity2);
		shprintf("temperature2:%.5f\n", sensim->temperature2);
		shprintf("density2:%.5f\n", sensim->density2);
		shprintf("soundvel2:%.5f\n", sensim->soundvel2);
		shprintf("depth2:%.5f\n", sensim->depth2);

		shprintf("<li>HMEU status...\n");
		shprintf("status_hmeu:%04x\n", sensim->status_hmeu);
		shprintf("snorkel_status:%04x\n", sensim->snorkel_status);
		shprintf("snorkel_position:%d\n", sensim->snorkel_position);
		shprintf("commast1_status:%04x\n", sensim->commast1_status);
		shprintf("radar_status:%04x\n", sensim->radar_status);
		shprintf("radar_position:%d\n", sensim->radar_position);
		shprintf("attperiscope_status:%04x\n", sensim->attperiscope_status);
		shprintf("attperiscope_position:%d\n", sensim->attperiscope_position);
		shprintf("satcom_status:%04x\n", sensim->satcom_status);
		shprintf("satcom_position:%d\n", sensim->satcom_position);
		shprintf("esmmast_status:%04x\n", sensim->esmmast_status);
		shprintf("esmmast_position:%d\n", sensim->esmmast_position);
		shprintf("commast2_status:%04x\n", sensim->commast2_status);
		shprintf("optronicmast_status:%04x\n", sensim->optronicmast_status);
		shprintf("optronicmast_position:%d\n", sensim->optronicmast_position);
		shprintf("common_retracting:%d\n", sensim->common_retracting);

		shprintf("<li>time, test...\n");
		shprintf("timestamp:%ld\n", sensim->timestamp);
		shprintf("timeoffset:%d\n", sensim->timeoffset);
		shprintf("test:%c\n", sensim->test);
		shprintf("</UL>");


		ret += comm_dump(sfd, com);
//		sem_post(&com->sem);
	} else{
//		sem_getvalue(&com->sem, &res);
		shprintf("Busy SSC com:%d\n", res);
	}

	return ret;
}
*/

extern unsigned char cpulogcnt;
extern unsigned char cpulog[120];
static int rundiag_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
	int i,j;
	char timebuf[300];
	struct tm gmt;
	time_t cur_time;
	struct utsname uts;

	uname(&uts);
	time(&cur_time);
	localtime_r(&cur_time, &gmt);

	memset(timebuf, 0, sizeof(timebuf));
	strftime((char *)timebuf, 300, "%Y.%m.%d %H:%M:%S", &gmt);
	shprintf("\r\n\r\n");

	shprintf(HTML_TABLE_HEADER_);
	shprintf(HTML_TABLE_ROW_"<td><pre>\n\n");
	shprintf("Hyundai intelligent Smart Control Module - Protocol Manager\n");

//	shprintf("             Ver %s\n\n\n", app_version);
	shprintf("      Copyright 2015 HHI     \n");
	shprintf("              by U20\n");
	shprintf("                                                     \n");
	shprintf("</td></tr></table>\r\n\n");

	shprintf("<li><b>Software information...</b>\r\n");
	shprintf(HTML_TABLE_HEADER_400);
	shprintf(HTML_TABLE_ROW_"<td>Linux</td><td>%s %s(%s, %s)</td></tr>\r\n", uts.machine, uts.sysname, uts.release, uts.version);
//	shprintf(HTML_TABLE_ROW_"<td>App Time</td><td>%s</td></tr>\r\n", app_time);
//	shprintf(HTML_TABLE_ROW_"<td>Compiler</td><td>%s</td></tr>\r\n", app_compiler);
//	shprintf(HTML_TABLE_ROW_"<td>By</td><td>%s@%s</td></tr>\r\n\n", app_by, app_host);
	shprintf("</table>\r\n\n");

	shprintf("<li><b>Running information...</b>\r\n");
	shprintf(HTML_TABLE_HEADER_400);
	shprintf(HTML_TABLE_ROW_"<td>Current Time</td><td>%s</td></tr>\n",timebuf);
	shprintf("</table>\r\n\n");

	shprintf("<li><b>System information...</b>\r\n");
	shprintf(HTML_TABLE_HEADER_400);
//	localtime_r((time_t *)&PcuCsr.reg.runtime, &gmt);
	strftime((char *)timebuf, 300, "%Y.%m.%d %H:%M:%S", &gmt);
	shprintf(HTML_TABLE_ROW_"<td>Run time</td><td>%s</td></tr>\n", timebuf);
//	shprintf(HTML_TABLE_ROW_"<td rowspan=2>CPU Usage</td><td>%d.%03d</td></tr>\n", CpuUsage/1000, CpuUsage%1000);
//	shprintf(HTML_TABLE_ROW_"<td align=left><img src=dotimg_5 width=%d%% height=10></td></tr>\r\n", CpuUsage/1000);

//	shprintf(HTML_TABLE_ROW_"<td rowspan=2>ETH0 Usage</td><td>%d.%03d</td></tr>\n", EthUsage[0]/1000, EthUsage[0]%1000);
//	shprintf(HTML_TABLE_ROW_"<td align=left><img src=dotimg_5 width=%d%% height=10></td></tr>\r\n", EthUsage[0]/1000);

//	shprintf(HTML_TABLE_ROW_"<td rowspan=2>ETH1 Usage</td><td>%d.%03d</td></tr>\n", EthUsage[1]/1000, EthUsage[1]%1000);
//	shprintf(HTML_TABLE_ROW_"<td align=left><img src=dotimg_5 width=%d%% height=10></td></tr>\r\n", EthUsage[1]/1000);

	shprintf("</table>\r\n\n");
/*
	shprintf("<li><b>CPU Usage history...</b>\r\n");
	shprintf("<span id='usage'></span>\n");

	shprintf("<script type='text/javascript' src='jquery.js'></script>\n");
	shprintf("<script type='text/javascript' src='sparkline.js'></script>\n");

	shprintf("<script language=JavaScript>\n");
	shprintf("$(function() {\nvar dat = new Array(");
	for(i=0, j=cpulogcnt; i<120; i++){
		if(i==119)	shprintf("%d", cpulog[j]);
		else		shprintf("%d,", cpulog[j]);
		if(++j>=120)	j=0;
	}
	shprintf(");\n$('#usage').sparkline(dat, { height: 200, width: 400, normalRangeMin:0, normalRangeMax:100, normalRangeColor:'black',gridDiv:10,gridColor:'green', lineColor:'yellow', fillColor: false});\n});\n");
	shprintf("</script>\n");
*/
//	graph(sfd, BLACK, LIME, 0, 100, 400, timebuf, 120);

/*
	shprintf("<img src=\"dotimg_0\" width=10 high=10>\n");
	shprintf("<img src=\"dotimg_1\" width=10 high=10>\n");
	shprintf("<img src=\"dotimg_2\" width=10 high=10>\n");
	shprintf("<img src=\"dotimg_3\" width=10 high=10>\n");
	shprintf("<img src=\"dotimg_4\" width=10 high=10>\n");
	shprintf("<img src=\"dotimg_5\" width=10 high=10>\n");
	shprintf("<img src=\"dotimg_6\" width=10 high=10>\n");
	shprintf("<img src=\"dotimg_7\" width=10 high=10>\n");
	shprintf("<img src=\"dotimg_8\" width=10 high=10>\n");
	shprintf("<img src=\"dotimg_9\" width=10 high=10>\n");
	shprintf("<img src=\"dotimg_a\" width=10 high=10>\n");
	shprintf("<img src=\"dotimg_b\" width=10 high=10>\n");
	shprintf("<img src=\"dotimg_c\" width=10 high=10>\n");
	shprintf("<img src=\"dotimg_d\" width=10 high=10>\n");
	shprintf("<img src=\"dotimg_e\" width=10 high=10>\n");
	shprintf("<img src=\"dotimg_f\" width=10 high=10>\n");
*/

	return ret;
}



static int dump_dmesg_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
	FILE *fp;
	char cmd[256], temp[4], *pcmd;
	unsigned int i;
	int tempc;

	if(type){
		shprintf("<form method=\"GET\" action=\"dmesg\">\r\n");
		// 1'st param : command
		shprintf("<input type=\"text\" name=\"&\" size=\"80\" maxlength=\"255\" value=\"\">\r\n");
		shprintf("</form>\n");
	}

	if(argc>=2){
		pcmd = cmd;
		for(i=0;i<strlen(argv[1]);i++){
			if(argv[1][i] == '+')	*pcmd++ = ' ';
			else if(argv[1][i] == '%'){
				strncpy(temp, &argv[1][i+1], 2);
				temp[2] = '\0';
				sscanf(temp, "%X", &tempc);
				*pcmd++ = tempc;
				i += 2;
			} else{
				*pcmd++ = argv[1][i];
			}
		}
		*pcmd = '\0';
	} else{
		strncpy(cmd, "dmesg", 256);
	}
	fp = popen(cmd, "r");
	if(fp){
		while(1){
			ret = fread(shbuff, sizeof(char), MAX_SHBUFF, fp);
			if(ret<=0) break;
			write(sfd, shbuff, ret);
		}
		pclose(fp);
	}

	return ret;
}


static int timesync_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
//	unsigned int i;
//	int stratum=0;
//	float offset=0;
//	float delay=0;
/*
	if(argc>=3){
		sscanf(argv[2], "%d", &i);
		if(CmdLink[i].fd != 0){
			ntpinfo(sfd, &CmdLink[i].addr, 1, &stratum, &offset, &delay);

			shprintf("NTP Synchronization from %s(Dev:%d) offset:%f\n",
				inet_ntoa(CmdLink[i].addr.sin_addr), CmdLink[i].dev, offset);

			return ret;
		}
	}
*/
//	shprintf("<li> Current NTP Server : %s\n\n", inet_ntoa(NTPServerAddr.sin_addr));
	shprintf(HTML_TABLE_HEADER_400);
	shprintf(HTML_TABLE_ROW_"<td>IP</td><td>Dev#</td><td>stratum</td><td>Offset</td><td>Delay</td><td>Set</td></tr>\n");
/*
	for(i=0;i<MAX_CMD_LINK;i++){
		if(CmdLink[i].fd != 0){
			shprintf(HTML_TABLE_ROW_"<td>%s</td><td>%d</td>", inet_ntoa(CmdLink[i].addr.sin_addr), CmdLink[i].dev);
			ntpinfo(sfd, &CmdLink[i].addr, 0, &stratum, &offset, &delay);

			if(stratum!=0)
				shprintf("<td>%d</td><td>%f</td><td>%f</td>", stratum, offset, delay);
			else
				shprintf("<td colspan=3>no ntp server</td>");

			shprintf("<td><form method=\"GET\" action=\"timeSync\">\r\n");
			shprintf("<input type=\"submit\" name=\"_\" value=\"sync_%d\">\n", i);
			shprintf("</form></td></tr>\n");
		}
	}
*/
	shprintf("</table>");

	return ret;
}

#ifndef offsetof
 #define offsetof(type, member) ((size_t) &((type *)0)->member)
#endif
/*
static int view_ipmsst_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;

	shprintf("total size:%d\n", sizeof(HEADER) + sizeof(struct ipms_send)+ sizeof(TAIL));
	shprintf("src_attitude:%d\n", offsetof(struct ipms_send, src_attitude) + sizeof(HEADER)+1);
	shprintf("manual_status:%d\n", offsetof(struct ipms_send, manual_status) + sizeof(HEADER)+1);
	shprintf("heading:%d\n", offsetof(struct ipms_send, heading) + sizeof(HEADER)+1);
	shprintf("roll:%d\n", offsetof(struct ipms_send, roll) + sizeof(HEADER)+1);
	shprintf("pitch:%d\n", offsetof(struct ipms_send, pitch) + sizeof(HEADER)+1);
	shprintf("heading_rate:%d\n", offsetof(struct ipms_send, heading_rate) + sizeof(HEADER)+1);
	shprintf("roll_rate:%d\n", offsetof(struct ipms_send, roll_rate) + sizeof(HEADER)+1);
	shprintf("pitch_rate:%d\n", offsetof(struct ipms_send, pitch_rate) + sizeof(HEADER)+1);
	shprintf("src_position:%d\n", offsetof(struct ipms_send, src_position) + sizeof(HEADER)+1);
	shprintf("src_waterspd:%d\n", offsetof(struct ipms_send, src_waterspd) + sizeof(HEADER)+1);
	shprintf("src_groundspd:%d\n", offsetof(struct ipms_send, src_groundspd) + sizeof(HEADER)+1);
	shprintf("src_divingdep:%d\n", offsetof(struct ipms_send, src_divingdep) + sizeof(HEADER)+1);
	shprintf("com_mast_status:%d\n", offsetof(struct ipms_send, com_mast_status) + sizeof(HEADER)+1);
	shprintf("timestamp:%d\n", offsetof(struct ipms_send, timestamp) + sizeof(HEADER)+1);


	return ret;
}
*/


#define SOCK_PORT_NO_COMBASE	7000
#define COMBASE_OFFSET			8

#ifdef COM
#define MAX_INF_PORT		14
#else
#define MAX_INF_PORT		4
#endif
static int comsockfd[4];
static char thrarg[2];
pthread_t ComMsgthr=0;
// 0 : primary tx
// 1 : secondary tx
// 2 : primary rx
// 3 : secondary rx

static int open_comsocket(int com)
{
	int sockfd;
	int port;
	int i;
	struct sockaddr_in address;

	port = SOCK_PORT_NO_COMBASE + COMBASE_OFFSET*(com-1);
	for(i=0; i<4; i++){
		sockfd = socket(AF_INET, SOCK_DGRAM, 0);
		memset(&address,0,sizeof(address));
		address.sin_family = AF_INET;
		address.sin_addr.s_addr = htonl(INADDR_ANY);
		address.sin_port = htons(port+i);
		if(bind(sockfd, (struct sockaddr *)&address, sizeof(address)) < 0){
			return -1;
		}
		comsockfd[i] = sockfd;
	}

	return 0;
}

static int hexdump(int sfd, char *ptr, int len)
{
	int i, j;
	char *buf, *cbuf, *eaddr;
	int ret=0;

	eaddr = ptr+len;
	buf = ptr;
	for(i=0;i<len;){
		cbuf = buf;
		shprintf("\r\n[%03X0]", i/16);
		for(j=0;j<0x10;j++){
			if(!(j%8))		shprintf("  ");
			else if(!(j%4))	shprintf("- ");

			shprintf("%02X ", (unsigned char) *(buf++));
			if(buf >= eaddr){
				j++;
				break;
			}
		}
		for(;j<0x10;j++){
			if(!(j%4))	shprintf("  ");
			shprintf("   ");
		}

		shprintf("   ");
		for(j=0;j<16;j++){
			if(*cbuf <= 0x20)	shprintf(".");
			else if(*cbuf<0x80)	shprintf("%c", (unsigned char) *(cbuf));
			else				shprintf(".");
			cbuf++;
			if(cbuf >= eaddr)	break;
		}
		for(;j<0x10;j++)	shprintf(" ");
		i += 0x10;
	}

	return ret;
}


static void ComMessageThr(void *arg)
{
	int timeout, port;
	fd_set readfds, testfds;
	struct timeval timev, timep;
	int i;
	int nread, len;
	char buff[4096];
	time_t timet;
	int sfd;
	char fname[256];
	int ret=0;
	char *ptr;
	struct tm gmt;

	ptr = arg;
	port = ptr[0];
	timeout = ptr[1];

	if(open_comsocket(port) < 0){
		ComMsgthr = 0;
		return;
	};

	timet = time(NULL);

	FD_ZERO(&readfds);
	FD_SET(comsockfd[0],&readfds);
	FD_SET(comsockfd[1],&readfds);
	FD_SET(comsockfd[2],&readfds);
	FD_SET(comsockfd[3],&readfds);

	strcpy(fname, log_path_name);
	strcat(fname, "infmsg");

	remove(fname);
	sfd = open(fname, O_WRONLY|O_CREAT, 0444);

	while(timeout){
		testfds = readfds;

		timev.tv_sec = 1;
		timev.tv_usec = 0;

		if( select(FD_SETSIZE, &testfds,(fd_set*)0,(fd_set*)0,(struct timeval*)&timev) == 0 ){
			timeout--;
		} else{
			for(i=0; i<4; i++){
				if(FD_ISSET(comsockfd[i], &testfds)){
					ioctl(comsockfd[i], FIONREAD, &nread);
					if(nread > 0){
						len = read(comsockfd[i], buff, sizeof(buff));
						if(len <= 0) break;
						if(i < 2){
							shprintf(">> Tx");
						} else shprintf("<< Rx");
						if(i&1)	shprintf("S");
						else	shprintf("P");

						gettimeofday(&timep, NULL);
						localtime_r((const time_t*)&timep.tv_sec, &gmt);
						shprintf(" [%4d.%02d.%02d %02d:%02d:%02d.%06d]", gmt.tm_year+1900, gmt.tm_mon+1, gmt.tm_mday,
									gmt.tm_hour, gmt.tm_min, gmt.tm_sec, timep.tv_usec);
						hexdump(sfd, buff, len);

						shprintf("\n\n");
					}
				}
			}
		}

		if( (timet+timeout) < time(NULL))	break;
	}

	close(sfd);
	for(i=0; i<4; i++){
		close(comsockfd[i]);
		comsockfd[i] = 0;
	}
	ComMsgthr = 0;

	return;
}

static int fdump(int sfd, char *fname)
{
	int fd, len;
	struct stat sb;
	int ret=0;

	fd = open(fname, O_RDONLY);
	if(fd > 0){
		fstat(fd, &sb);
		len = sb.st_size;

		while(len>0){
			ret = read(fd, shbuff, MAX_SHBUFF);
			if(ret<=0)	break;
			write(sfd, shbuff, ret);
			len -= ret;
		}
		close(fd);
		ret = len;
	}

	return ret;
}

static int view_com_status_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int ret = 0;
	int fd;
	char fname[256];

	strcpy(fname, log_path_name);
	strcat(fname, "inf_comstatus");

	if(argc>=2){
		if(strcmp(argv[1], "Clear") == 0){
			shprintf("Clear");
			remove(fname);
			return 0;
		}
	}

	fdump(sfd, fname);
	shprintf(" CAN ID : %02d\n", myID);
	shprintf("<form method='GET' action='viewComStatus'><input type='submit' name='' value='Clear'></form>");

	return ret;
}

#define HOME_DIR	"/home/"
static int view_port_config_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int ret = 0;
	char fname[256];
	int port = 0;
	int i;

	if(argc >= 2){
		port = atoi(argv[1]);		// port
	}

	shprintf("<form method=\"GET\" action=\"viewPortConf\">\r\n");
//	shprintf("Com port : <select name=''>\r\n");
	shprintf("Com port : <select name=&>\r\n");
	shprintf("<option value=0>Port Manager</option>");
	for(i=1; i<=MAX_INF_PORT; i++){	// com1~8, eth9,10
		shprintf("<option value=%d>Port %d</option>", i, i);
	}
	shprintf("</select> ");
//	shprintf("<input type='submit' name='' value=\"View\">\r\n");
	shprintf("<input type=\"submit\" name=\"&\" value=\"View\">\r\n");
	shprintf("</form>\n");

	if(port == 0)	shprintf("<b>Port Manager View</b>\n");
	else			shprintf("<b>Port %d View</b>\n", port);

	if(port==0){
		shprintf("<li>Port Manager Configuration...\n");
		shprintf(HTML_TABLE_HEADER_700"<tr><td><pre>");
		// /home/sysconfig/pm.conf
		snprintf(fname, sizeof(fname), HOME_DIR"sysconfig/pm.conf");
		fdump(sfd, fname);

		shprintf("</pre></td></tr></table>\n<hr>\n<li>NMEA Definition...\n");
		shprintf(HTML_TABLE_HEADER_700"<tr><td><pre>");
		// /home/sysconfig/NMEA.def
		snprintf(fname, sizeof(fname), HOME_DIR"sysconfig/NMEA.def");
		fdump(sfd, fname);
		shprintf("</pre></td></tr></table>\n");
	} else{
		shprintf("<li>Port Configuration...\n");
		// /home/sysconfig/portxx.conf
		shprintf(HTML_TABLE_HEADER_700"<tr><td><pre>");
		snprintf(fname, sizeof(fname), HOME_DIR"sysconfig/port%02d.conf", port);
		fdump(sfd, fname);

		shprintf("</pre></td></tr></table>\n<hr>\n<li>Protocol Configuration...\n");
		shprintf(HTML_TABLE_HEADER_700"<tr><td><pre>");
		// /home/sysconfig/ptcxx.conf
		snprintf(fname, sizeof(fname), HOME_DIR"sysconfig/ptc%02d.conf", port);
		fdump(sfd, fname);
		shprintf("</pre></td></tr></table>\n");
	}

	return ret;
};

static int set_com_msg_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int ret = 0;
	int i;

	if(ComMsgthr != 0){
		shprintf("Communication Message capture process is activated...\n");
		shprintf("Com port : %d, Duration : %d sec\n", thrarg[0], thrarg[1]);

		return 0;
	}

	if(argc >= 4){
		if(ComMsgthr == 0){
			thrarg[0] = atoi(argv[1]);		// port
			thrarg[1] = atoi(argv[2]);	// capture time
			if(thrarg[0]<1 || thrarg[0]>MAX_INF_PORT)	thrarg[0] = 1;	// default port
			if(thrarg[1]<1 || thrarg[1]>100)thrarg[1] = 10;	// default duration

			pthread_create(&ComMsgthr, NULL, (void *)&ComMessageThr, &thrarg);

			shprintf("Communication Message capture process start...\n");
			shprintf("Com port : %d, Duration : %d sec\n", thrarg[0], thrarg[1]);
		}
	} else{
		shprintf("<form method=\"GET\" action=\"setComMsg\">\r\n");
		// 1'st param : com port
		shprintf("Com port : <select name=&>\r\n");
		for(i=1; i<=MAX_INF_PORT; i++){	// com1~8, eth9,10
			shprintf("<option value=%d>Port %d</option>", i, i);
		}
		shprintf("</select>\n");
		// 2'nd param : duration
		shprintf("Duration time : <input type=\"text\" name=\"&\" size=\"3\" maxlength=\"3\" value=\"10\">Second\n");
		// 3'rd param : Set
		shprintf("<input type=\"submit\" name=\"\" value=\"Set\">\r\n");
		shprintf("</form>\n");
	}


	return ret;
}

static int view_com_msg_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int ret = 0;
	int fd;
	char buf[256];
	struct stat sb;
	int len;
//	int timeout = 10;	// default 10 second

	strcpy(buf, log_path_name);
	strcat(buf, "infmsg");
	fd = open(buf, O_RDONLY);
	if(fd < 0){
		shprintf("Communication Message not found\n");
		shprintf("Run setComMsg\n");
		return ret;
	}

	fstat(fd, &sb);
	len = sb.st_size;
	if(S_ISFIFO(sb.st_mode)){
		shprintf("FIFO...\n");
		// nonblock mode...
		fcntl(fd, F_SETFL, O_NONBLOCK);
		ret = read(fd, shbuff, MAX_SHBUFF);
		write(sfd, shbuff, ret);
	} else{
		while(len>0){
			ret = read(fd, shbuff, MAX_SHBUFF);
			if(ret<=0)	break;
			write(sfd, shbuff, ret);
			len -= ret;
		}
	}

	if(fd > 0)	close(fd);

	return ret;
}

static int view_inf_log_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int ret = 0;
	char fname[256];
	int port = 0;
	int i;

	if(argc >= 2){
		port = atoi(argv[1]);		// port
	}

	if(type){
		shprintf("<form method=\"GET\" action=\"viewInfLog\">\r\n");
//		shprintf("Com port : <select name=''>\r\n");
		shprintf("Com port : <select name=&>\r\n");
		shprintf("<option value=0>Port Manager</option>");
		for(i=1; i<=MAX_INF_PORT; i++){	// com1~8, eth9,10
			shprintf("<option value=%d>Port %d</option>", i, i);
		}
		shprintf("</select> ");
//		shprintf("<input type='submit' name='' value=\"View\">\r\n");
		shprintf("<input type=\"submit\" name=\"&\" value=\"View\">\r\n");
		shprintf("</form>\n");

		if(port == 0)	shprintf("<b>Port Manager View</b>\n");
		else			shprintf("<b>Port %d View</b>\n", port);

		if(port==0){
			shprintf("<li>Port Manager Log...\n");
			shprintf(HTML_TABLE_HEADER_700"<tr><td><pre>");
			// /home/sysconfig/pm.conf
			snprintf(fname, sizeof(fname), "/mnt/pm/pm");
			if(access(fname,F_OK) != 0){
				snprintf(fname, sizeof(fname), HOME_DIR"log/pm/pm");
			}
			fdump(sfd, fname);

			shprintf("</pre></td></tr></table>\n");
		} else{
			shprintf("<li>Interface Log...\n");
			// /home/sysconfig/portxx.conf
			shprintf(HTML_TABLE_HEADER_700"<tr><td><pre>");
			snprintf(fname, sizeof(fname), "/mnt/inf/inf%02d", port);
			if(access(fname,F_OK) != 0){
				snprintf(fname, sizeof(fname), HOME_DIR"log/inf/inf%02d", port);
			}
			fdump(sfd, fname);
			shprintf("</pre></td></tr></table>\n");
		}
	}
	return ret;

}





static int set_refresh_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
	int time, i;

	if(argc>1)	time = atoi(argv[1]);
	else 		time = 0;

	shprintf("\nCommand refresh time : %d\n", time);
	refresh_time = time;

	if(type){
		shprintf("<form method=\"GET\" action=\"refresh\">\r\n");

		// 1'st param : second
		shprintf("<select name=_>\r\n");
		shprintf("<option value=0>Disable</option>");
		shprintf("<option value=1>0.1 sec</option>");
		shprintf("<option value=5>0.5 sec</option>");
		for(i=1; i<11; i++){
			shprintf("<option value=%d>%d sec</option>", i*10, i);
		}
		shprintf("</select> ");

		// 2'nd param : set
		shprintf("<input type=\"submit\" name=\"&\" value=\"set\">\r\n");
		shprintf("</form>\n");
	}

	return ret;
}

static int view_pm_log_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int ret = 0;

	int fd;
	char buf[256];
	struct stat sb;
	int len;

	strcpy(buf, log_path_name);
	strcat(buf, log_name);
	fd = open(buf, O_RDONLY);
	if(fd < 0)	return 0;
	fstat(fd, &sb);
	len = sb.st_size;

	while(len>0){
		ret = read(fd, shbuff, MAX_SHBUFF);
		if(ret<=0)	break;
		write(sfd, shbuff, ret);
		len -= ret;
	}

	if(fd > 0)	close(fd);

	return ret;
}

static int ifpoint_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0, i,j, Inf_num = MAX_INF_PORT+1;
	IO_DATA *inf;
	unsigned short *ptag;

	if(argc >= 2){
		Inf_num = atoi(argv[1]);		// port
	}

	shprintf("<form method=\"GET\" action=\"ifpoint\">\r\n");
	shprintf("Com port : <select name=&>\r\n");

	for(i=0; i<=MAX_INF_PORT; i++){	// com1~8, eth9,10
		shprintf("<option value=%d>Port %d</option>", i, i);
	}
	shprintf("<option value=%d>Port All</option>", i);
	shprintf("</select> ");
	shprintf("<input type=\"submit\" name=\"&\" value=\"View\">\r\n");
	shprintf("</form>\n");

	inf = &io_data[0];//&io_data[port_num].INF_mem.INF_data.tag[address]
	for(i=0; i<12; i++){
		shprintf(" Port#%02d : %04x\n", i+1, htons(inf->INF_mem.INF_data.tag[i]));
//		shprintf(" Port#%d : %04x\n", i+1, inf->INF_mem.INF_data.tag[i]);
	}
	j = 0; 
//	shprintf(" LAN #%02d : %04x\n", j, htons(inf->INF_mem.INF_data.tag[12]));
//	shprintf(" LAN #%02d : %04x\n", j+1, htons(inf->INF_mem.INF_data.tag[13]));
	for(; i<14; i++){ 
		shprintf(" LAN #%02d : %04x\n", i-11, htons(inf->INF_mem.INF_data.tag[i]));
//		shprintf(" LAN#%d : %04x\n", i-7, inf->INF_mem.INF_data.tag[i]);
	}
	shprintf("\n<hr>");

	if(Inf_num == (MAX_INF_PORT+1))
	{
		shprintf("<b>Port All View</b>\n");
		for(Inf_num=0; Inf_num<=MAX_INF_PORT; Inf_num++){
			inf = &io_data[0 + Inf_num];
			ptag = &inf->INF_mem.INF_data.tag[0];

			shprintf("\nIF#%d",Inf_num);
			shprintf("\r\n----------------------------------------------\n");

			for(i=0; i<2048; i++){
				if(i%10==0){
					shprintf("\n%4d ", i);
				}
				shprintf("%04x ", htons(*ptag++));
	//			shprintf("%04x ", *ptag++);
			}
			shprintf("\n<hr>");
		}
	}
	else
	{
		inf = &io_data[0 + Inf_num];
		ptag = &inf->INF_mem.INF_data.tag[0];

		shprintf("\nIF#%d",Inf_num);
		shprintf("\r\n----------------------------------------------\n");

		for(i=0; i<2048; i++){
			if(i%10==0){
				shprintf("\n%4d ", i);
			}
			shprintf("%04x ", htons(*ptag++));
//			shprintf("%04x ", *ptag++);
		}
		shprintf("\n<hr>");
	}
/*
	for(Inf_num=0; Inf_num<=MAX_INF_PORT; Inf_num++){
		inf = &io_data[0 + Inf_num];
		ptag = &inf->INF_mem.INF_data.tag[0];

		shprintf("\nIF#%d",Inf_num);
		shprintf("\r\n----------------------------------------------\n");

		for(i=0; i<2048; i++){
			if(i%10==0){
				shprintf("\n%4d ", i);
			}
			shprintf("%04x ", htons(*ptag++));
//			shprintf("%04x ", *ptag++);
		}

		shprintf("\n<hr>");

	}
	*/
/*	int RtuNo, i;
	HICSR_CARD *card;
	unsigned short *ptag;

	if(ModbusQuery[0].cnt <= 0){
		shprintf("No interface...\r\n");
		return ret;
	}

	card = &IoData[INF_ST];
	shprintf("Interface device alarm...\n");
	for(i=0; i<8; i++){
		shprintf(" Port#%d : %04x\n", i+1, card->avail.data.interface.tag[i]);
	}
	for(; i<10; i++){
		shprintf(" LAN#%d : %04x\n", i-7, card->avail.data.interface.tag[i]);
	}

	shprintf("\n<hr>");
	for(RtuNo=0; RtuNo<=MAX_MPM_IF_SLOT; RtuNo++){
		if(ModbusQuery[RtuNo].cnt==0)	continue;
		card = &IoData[INF_ST + RtuNo];

		ptag = &card->avail.data.interface.tag[0];

		shprintf("\nQuery for IF#%d",RtuNo);
		shprintf("\r\n----------------------------------------------\n");

		for(i=0; i<2048; i++){
			if(i%10==0){
				shprintf("\n%4d ", i);
			}
			shprintf("%04x ", *ptag++);

*/
/*
			shprintf("\r\n  RES : [%02X] [%02X] [%04X] [%04X]",
			ModBus[RtuNo].Answer[i].Address,
			ModBus[RtuNo].Answer[i].Function,
			htons(ModBus[RtuNo].Answer[i].Length),
			htons(ModBus[RtuNo].Answer[i].CRC) );
*/
/*		}

		shprintf("\n<hr>");

	}
*/
	return ret;
}

static int view_command_log_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int ret = 0;
/*
	int fd;
	char buf[256];
	struct stat sb;
	int len;

	strcpy(buf, log_path_name);
	strcat(buf, command_log_name);
	fd = open(buf, O_RDONLY);
	if(fd < 0)	return 0;
	fstat(fd, &sb);
	len = sb.st_size;

	while(len>0){
		ret = read(fd, shbuff, MAX_SHBUFF);
		if(ret<=0)	break;
		write(sfd, shbuff, ret);
		len -= ret;
	}

	if(fd > 0)	close(fd);
*/
	return ret;
}
/*
static int view_error_log_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int ret = 0;

	int fd;
	char buf[256];
	struct stat sb;
	int len;

	strcpy(buf, log_path_name);
	strcat(buf, error_log_name);
	fd = open(buf, O_RDONLY);
	if(fd < 0)	return 0;
	fstat(fd, &sb);
	len = sb.st_size;

	while(len>0){
		ret = read(fd, shbuff, MAX_SHBUFF);
		if(ret<=0)	break;
		write(sfd, shbuff, ret);
		len -= ret;
	}

	if(fd > 0)	close(fd);

	return ret;
}


static int view_alarm_log_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int ret = 0;

	int fd;
	char buf[256];
	struct stat sb;
	int len;

	strcpy(buf, log_path_name);
	strcat(buf, alarm_log_name);
	fd = open(buf, O_RDONLY);
	if(fd < 0)	return 0;
	fstat(fd, &sb);
	len = sb.st_size;

	while(len>0){
		ret = read(fd, shbuff, MAX_SHBUFF);
		if(ret<=0)	break;
		write(sfd, shbuff, ret);
		len -= ret;
	}

	if(fd > 0)	close(fd);

	return ret;
}
*/

/*
static int ShHexDataDisp(int count, char *buffer, char *str)
{
	int ret=0;
	int i;

	shprintf("       0  1  2  3    4  5  6  7   8  9  a  b    c  d  e  f\r\n");
	shprintf("      ----------------------------------------------------");
	for(i=0; i<count; i++){
		if(!(i%16)){
			shprintf("\r\n[%03X0]", i/16);
		}
		if(!(i%8)){
			shprintf(" ");
		} else if(!(i%4)){
			shprintf("- ");
		}
		shprintf("%02X ", buffer[i] & 0xff);
	}
	shprintf("\r\n");

	return(ret);
}
*/

static int pstate_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
	int i;

	struct pid_info_t *pinfo;

	shprintf("<li><b>Thread state</b>\n");
	shprintf(HTML_TABLE_HEADER_400);
	shprintf(HTML_TABLE_ROW_"<td>NAME</td><td>TID</td><td>OLD</td><td>NEW</td></tr>");
/*
	for(i=0;i<PID_UNKNOWN;i++){
		pinfo = &pidinfo[i];
		shprintf(HTML_TABLE_ROW_"<td>%s</td><td>%d</td><td>%d</td><td>%d</td></tr>",
				strpid(i), pinfo->pidno, pinfo->old, pinfo->loop);

		pinfo->old = pinfo->loop;
	}

*/
	shprintf("</table>");

	return ret;
}

#define MAXHOSTNAMELEN 256
static int backup_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
	int i;
	FILE *fp;
	char cmd[256];
	char myhostname[MAXHOSTNAMELEN+1];

	gethostname(myhostname, sizeof(myhostname));

	for(i=0;i<256;i++){
		if(myhostname[i]==0)	break;
		if(myhostname[i]=='_')	myhostname[i] = '-';
	}

	if(argc>=2){
		if(strncmp(argv[1], "Make", 4) == 0){
			shprintf("");
			snprintf(cmd, sizeof(cmd), "tar cpz -f /tmp/%s.tgz /home/app/ /home/db/ /home/modules/ /home/www/ /home/util/ /home/sysconfig/ > /dev/null 2>&1", myhostname);
// tar cpz -f /tmp/%s.tgz /home/app/ /home/db/ /home/modules/ /home/www/ /home/util/ /home/sysconfig/p* /home/sysconfig/NMEA.def
			fp = popen(cmd, "r");
			if(fp){
				while(1){
					ret = fread(shbuff, sizeof(char), MAX_SHBUFF, fp);
					if(ret<=0) break;
					write(sfd, shbuff, ret);
				}
				pclose(fp);
			}

			shprintf("<b>Make Backup File... ok</b>\n\n");
		}

		if(strncmp(argv[1], "Delete", 6) == 0){
			sprintf(cmd, "rm -rf /tmp/%s*", myhostname);	// delete all
			i = system(cmd);
			shprintf("<b>Delete Backup File...</b>\n\n");
		}
	}
	snprintf(cmd, sizeof(cmd), "/tmp/%s.tgz", myhostname);

	shprintf(HTML_TABLE_HEADER_400"<tr><td>1</td><td>");
	shprintf("<form method=\"GET\" action=\"Backup\">\r\n");
	shprintf("<input type=\"submit\" name=\"&\" value=\"Make Backup\" onclick='alert(\"Wait until ok...\")'>\r\n");
	shprintf("</form>\n");
	shprintf("</td></tr>\n");

	if(access(cmd, F_OK) == 0){
		shprintf("<tr><td>2</td><td>");
		shprintf("<form method=\"GET\" action=\"%s.tgz\">\r\n", myhostname);
		shprintf("<input type=\"submit\" name=\"&\" value=\"Download Backup\">\r\n");
		shprintf("</form>\n");
		shprintf("</td></tr>\n");

		shprintf("<tr><td>3</td><td>");
		shprintf("<form method=\"GET\" action=\"Backup\">\r\n");
		shprintf("<input type=\"submit\" name=\"&\" value=\"Delete Backup\" onclick='alert(\"Delete file\")'>\r\n");
		shprintf("</form>\n");
		shprintf("</td></tr>\n");
	}
	shprintf("</table>\n");
	return ret;
}

#include <dirent.h>
static int restore_file(char *myhostname, char *filename)
{
	DIR *directory;
	struct dirent *entry = NULL;
	int len = 0;

	if ((directory = opendir("/tmp")) == NULL){
		snprintf(filename, MAXHOSTNAMELEN, "/tmp/%s.tgz", myhostname);
		return 0;
	}

	len = strlen(myhostname);
	while((entry = readdir(directory)) != NULL){
//log_access("%s-%s : %d", entry->d_name, myhostname, len);
		if (strncmp(entry->d_name, myhostname, len) == 0){
			snprintf(filename, MAXHOSTNAMELEN, "/tmp/%s", entry->d_name);
//			log_access("File found:%s", entry->d_name);
			break;
		}
	}
	closedir(directory);

	return 0;
}

static int restore_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
	char cmd[256];
	char myhostname[MAXHOSTNAMELEN+1];
	char filename[MAXHOSTNAMELEN+1];
	FILE *fp;
	int i;

	gethostname(myhostname, sizeof(myhostname));
	for(i=0;i<256;i++){
		if(myhostname[i]==0)	break;
		if(myhostname[i]=='_')	myhostname[i] = '-';
	}

	shprintf(HTML_TABLE_HEADER_500"<tr><td>1</td><td>");
	shprintf("<form method=post enctype=\"multipart/form-data\" name=image_upload>");
	shprintf("FILE : <input type=\"file\" name=\"dir=/tmp\" size=\"40\" maxlength=\"255\" value=\"himpm\">\n");
	shprintf("<input type=\"submit\" name=\"&\" value=\"Upload\" onclick='alert(\"Wait a second\")'>\r\n");
	shprintf("</form>\n");
	shprintf("</td></tr>\n");


	restore_file(myhostname, filename);

	if(argc>=2){
		if(access(filename, F_OK) == 0){
			if(strncmp(argv[1], "Restore", 7) == 0){
				sprintf(cmd, "rm -rf /home/Backup");		// delete all
				system(cmd);

				sprintf(cmd, "mkdir /home/Backup");			// mkdir
				system(cmd);

				sprintf(cmd, "mv /home/* /home/Backup");	// move
				system(cmd);

				sprintf(cmd, "tar xzf '%s' -C /", filename);	// extract
				system(cmd);

				sprintf(cmd, "rm -f /tmp/%s*.tgz", myhostname);	// rm
				system(cmd);

				// keep file...
				sprintf(cmd, "cp /home/Backup/sysconfig/HOSTNAME /home/sysconfig/");	// hostname
				system(cmd);
				sprintf(cmd, "cp /home/Backup/sysconfig/ifcfg-eth0 /home/sysconfig/");	// ifcfg-eth0
				system(cmd);
				sprintf(cmd, "cp /home/Backup/sysconfig/ifcfg-eth1 /home/sysconfig/");	// ifcfg-eth1
				system(cmd);

				shprintf("<b>Restore file... ok</b>\n");
				return ret;
			}
		}

		if(strncmp(argv[1], "Rollback", 8) == 0){
			sprintf(cmd, "cp -a /home/Backup/* /home/");	// rollback
			i = system(cmd);
			log_access("%s:%d", cmd, i);

			sprintf(cmd, "rm -rf /home/Backup");			// delete all
			i = system(cmd);
			log_access("%s:%d", cmd, i);

			shprintf("<b>Rollback file... ok</b>\n");
			return ret;
		}
		if(strncmp(argv[1], "Delete", 6) == 0){
			sprintf(cmd, "rm -rf /tmp/%s*", myhostname);	// delete all
			i = system(cmd);
			shprintf("<b>Delete Backup File...</b>\n\n");
			return ret;
		}
	}

	if(access(filename, F_OK) == 0){
		shprintf("<tr><td>2</td><td>");
		shprintf("<b>Backup File : %s</b>\n", filename);

		shprintf("<form method=\"GET\" action=\"Restore\">\r\n");
		shprintf("<input type=\"submit\" name=\"&\" value=\"Restore ?\" onclick='alert(\"Wait until ok...\")'>\r\n");
		shprintf("</form>\n");

		shprintf("<form method=\"GET\" action=\"Restore\">\r\n");
		shprintf("<input type=\"submit\" name=\"&\" value=\"Delete File\" onclick='alert(\"Wait a second\")'>\r\n");
		shprintf("</form>\n");
		shprintf("</td></tr>\n");
	}

	snprintf(cmd, sizeof(cmd), "/home/Backup");
	if(access(cmd, F_OK) == 0){
		shprintf("<tr><td>3</td><td>");
		shprintf("<form method=\"GET\" action=\"Restore\">\r\n");
		shprintf("<input type=\"submit\" name=\"&\" value=\"Rollback ?\" onclick='alert(\"Wait until ok...\")'>\r\n");
		shprintf("</form>\n");
		shprintf("</td></tr>\n");
	}
	shprintf("</table>\n");

	if(access(filename, F_OK) == 0){
		shprintf("File list...\n");
		shprintf(HTML_TABLE_HEADER_700"<tr><td><pre>");
		snprintf(cmd, sizeof(cmd), "tar tvf '%s'", filename);

		fp = popen(cmd, "r");
		if(fp == 0)	return 0;

		while(1){
			ret = fread(shbuff, sizeof(char), MAX_SHBUFF, fp);
			if(ret<=0)	break;
			write(sfd, shbuff, ret);
		}

		if(fp != 0)	pclose(fp);

		shprintf("</pre></td></tr></table>\n");
	}

	return ret;
}

static int appup_cmd(int sfd, int argc, char **argv, char *str, char type)
{
	int	ret	= 0;
	shprintf(HTML_TABLE_HEADER_500"<tr><td>");
//	shprintf("<form method=\"POST\" action=\"appup\">\r\n");
	shprintf("<form method=post enctype=\"multipart/form-data\" name=image_upload>");
	shprintf("Directory :<input type=\"text\" name=\"dir\" size=\"64\" maxlength=\"256\" value=\"/home/app/\">\r\n");
	shprintf("FILE : <input type=\"file\" name=\"&\" size=\"40\" maxlength=\"255\" value=\"himpm\">\n");

	// 5'th param : set / reset
	shprintf("<input type=\"submit\" name=\"&\" value=\"Upload\">\r\n");
	shprintf("</form>\n");
	shprintf("</td></tr>\n");
	shprintf("</table>\n");
	return ret;
}

int help_cmd(int sfd, int argc, char **argv, char *str, char type);

int http_frame(int sfd, char *str, char type);
int http_header(int sfd, int argc, char **argv, char *str, char type);
int http_command(int sfd, int argc, char **argv, char *str, char type);


struct command cmd_list[]={
	//	 cmd   func	   usage  help
	{"", NULL, "RUN State", "", 0},
	{"rundiag", rundiag_cmd, "rundiag", "Version Information", CMDF_REFRESH},
	{"pstate", pstate_cmd, "pstate", "Process Information", CMDF_REFRESH},
	{"timeSync", timesync_cmd, "timeSync", "Time Synchronous Information", CMDF_REFRESH},

	{"", NULL, "Interface", "", 0},
	{"viewComStatus", view_com_status_cmd, "viewComStatus", "View Interface Communication Status", CMDF_REFRESH, 0},
	{"viewPortConf", view_port_config_cmd, "viewPortConf", "View Port configuration", 0, 0},
	{"setComMsg", set_com_msg_cmd, "setComMsg", "Set Communication Message", CMDF_REFRESH, 0},
	{"viewComMsg", view_com_msg_cmd, "viewComMsg", "View Communication Message", CMDF_REFRESH, 0},
	{"viewInfLog", view_inf_log_cmd, "viewInfLog", "View Interface Log", 0, 0},
	{"ifpoint", ifpoint_cmd, "ifpoint", "Interface point", CMDF_REFRESH, 0},

	{"", NULL, "Logs", "", 0},
	{"viewPMLog", view_pm_log_cmd, "viewAccessLog", "View Access Log", 0},

//	{"viewCommandLog", view_command_log_cmd, "viewCommandLog", "View Command Log", 0},
//	{"viewErrorLog", view_error_log_cmd, "viewErrorLog", "View Error Log", 0},
//	{"viewAlarmLog", view_alarm_log_cmd, "viewAlarmLog", "View Alarm Log", 0},

//	{"", NULL, "Debug", "", 0},
//	{"viewIpmsSt", view_ipmsst_cmd, "viewIpmsSt", "View Ipms struct", 0},

	{"", NULL, "Others", "", 0},
	{"refresh", set_refresh_cmd, "refresh [time second]", "Set Command Refresh Time", 0},
	{"dmesg", dump_dmesg_cmd, "dmesg", "Linux dmesg", 0},
	{"help", help_cmd, "help [command]", "Help", 0},

	{"", NULL, "Backup/Restore", "", 0, 0},
	{"Backup", backup_cmd, "Backup", "Backup", 0, 0},
	{"Restore", restore_cmd, "Restore", "Restore", 0, 0},
	{"appup", appup_cmd, "appup", "application update", 0, 0},

//	{"GET", http_cmd, "http", "Internet Explorer", CMDF_HIDDEN},
	{"header", http_header, "", "", CMDF_HIDDEN},
	{"command", http_command, "", "", CMDF_HIDDEN},
	{0, 0, 0, 0, 0}
};
