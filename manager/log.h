#ifndef	_LOG_H
#define	_LOG_H


void log_error(char *format, ...);
void log_access(char *format, ...);

void open_logs(int mode);


extern char *log_path_name;
extern char *log_name;

#endif		// _LOG_H
