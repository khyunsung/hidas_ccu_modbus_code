#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#include <asm/types.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <termios.h>

#include "common.h"

#include "app.h"
#include "interface.h"
#include "modbus.h"
#include "utils.h"
#include "log.h"


//#define	DEBUG
#ifndef	DEBUG
	#define	dprintf(format, args...) log_access(format, ## args)
#else
	#define	dprintf(format, args...) log_access(format, ## args); printf(format, ## args)
#endif	// DEBUG

//#define		WRITE_DEBUG

#define BASE 			3000

#define DEBUG_INF
#define PULSE_CHECK

#define UART_REOPEN	// UART_REOPEN 2012-12-03
#define a()    printf(">>>> Function:%s, Line:%d\n", __FUNCTION__, __LINE__) //khs

unsigned int inf_tx_cnt[2], inf_rx_cnt[2], inf_thr_cnt;
extern __u8 inf_alive;

extern INF_ST INF_st;
extern CMD_Q CMD_q;

extern pthread_mutex_t mutex_inf;
extern pthread_mutex_t mutex_mem;

extern int fd_index, read_index;
extern unsigned short INF_mem[MAX_INF_TAG];
extern char sendbuff[DEFAULT_BUFF_SIZE];
extern char recvbuff[DEFAULT_BUFF_SIZE];
extern unsigned short mflag;

extern Inf_sts_cmd_r_bit Inf_s_cmd_b;
extern int Comm_Open_Err;

int main_line, rev_line;
char g_sendSize;
char endian_type;
static int select_nextquery(int port_num, int mode);

int port0 = 0, port1 = 0, port_ps = 0;
int p_open[2] = {0,};
char redundancy_on;

static int modbus_write(int in, int out, int port_num, int mode)
{
	unsigned short crc;
	int i, len, sendSize;
	int transmited_bytes;
	int return_mode;
	char writebuff[DEFAULT_BUFF_SIZE];
	__u8 *pbyte;
	__u8 byte_count;
	modbus_res_hdr_ext *pext;
	
	modbus_req_hdr *mreq = (modbus_req_hdr *)sendbuff;
	modbus_req_hdr *wreq = (modbus_req_hdr *)recvbuff;

	switch(mode)
	{
	case WRITE_REQ:
		dprintf("[MM %d] HMI write address: %d\n",port_num,wreq->address);
		for(i = 0; i < INF_st.num_write_cmd; i++)
		{
			if(INF_st.write_cmd[i].address == wreq->address)
			{
/*				dprintf("\tMatch write address: %d, fc: %d\n"
				,INF_st.write_cmd[i].address, INF_st.write_cmd[i].write_function_code);
				dprintf("\tReceived data :[%x][%x][%x][%x][%x][%x][%x][%x][%x][%x]\n"
				,recvbuff[0],recvbuff[1],recvbuff[2],recvbuff[3]
				,recvbuff[4],recvbuff[5],recvbuff[6],recvbuff[7]
				,recvbuff[8],recvbuff[9]
				);*/
				break;
			}		
		}
		if(i >= INF_st.num_write_cmd){
			dprintf("\twrite no match! - forced query selection\n");
			
			select_nextquery(port_num,PERIODIC_REQ_PRI);

			//return PERIODIC_REQ; // go to PERIODIC_REQ directly
		}
		else
		{
			if( wreq->function_code == 0x05 || wreq->function_code == 0x06 ||
				wreq->function_code == 0x0F || wreq->function_code == 0x10 )
			{
				mreq->unit_id = INF_st.write_cmd[i].write_unit_id;
				mreq->function_code = INF_st.write_cmd[i].write_function_code;
				mreq->address = htons(INF_st.write_cmd[i].write_address);
	
				if(mreq->function_code == 0x0F)
				{
					mreq->points = htons(16);
					sendbuff[6] = 2;
					if(endian_type == MD_LITTLE_ENDIAN)
					{
						sendbuff[7] = (wreq->points)>>8;
						sendbuff[8] = (wreq->points)&0x00FF;					
					}
					else
					{
						sendbuff[7] = (wreq->points)&0x00FF;
						sendbuff[8] = (wreq->points)>>8;					
					}
					crc = CRC16((__u8*)mreq,9)&0xFFFF;
					sendbuff[9] = crc>>8;
					sendbuff[10] = crc&0x00FF;
					sendSize = 11;
				}
				else if(mreq->function_code == 0x10)
				{
					if( wreq->function_code == 0x06 )
					{
						mreq->points = htons(1);
						sendbuff[6] = 2;
						if(endian_type == MD_LITTLE_ENDIAN)
						{
							sendbuff[7] = (wreq->points)&0x00FF;
							sendbuff[8] = (wreq->points)>>8;												
						}
						else
						{
							sendbuff[7] = (wreq->points)>>8;
							sendbuff[8] = (wreq->points)&0x00FF;							
						}
						len = 9;
					}
					else if( wreq->function_code == 0x10 )
					{
dprintf("fcode:16 length:%d\n", INF_st.write_cmd[i].write_data);
						byte_count = INF_st.write_cmd[i].write_data;
						mreq->points = htons(byte_count);
						byte_count = byte_count*2;
						sendbuff[6] = byte_count;
						if(endian_type == MD_LITTLE_ENDIAN)
						{
							sendbuff[7] = recvbuff[8];
							sendbuff[8] = recvbuff[7];
							sendbuff[9] = recvbuff[10];
							sendbuff[10] = recvbuff[9];
						}
						else
							memcpy(&sendbuff[7],&recvbuff[7],4);					
						len = 7 + byte_count;
					}
					else
						return PERIODIC_REQ;
			
					crc = CRC16((__u8*)mreq,len)&0xFFFF;
					sendbuff[len++] = crc>>8;
					sendbuff[len++] = crc&0x00FF;
					sendSize = len;
				}
				else if(mreq->function_code == 0x05)
				{
					if(wreq->points != 0)
						if(endian_type == MD_LITTLE_ENDIAN)
							mreq->points = htons(0x00FF);
						else 
							mreq->points = htons(0xFF00);
					else
						mreq->points = 0x0000;
			
					crc = CRC16((__u8*)mreq,6)&0xFFFF;
					sendbuff[6] = crc>>8;
					sendbuff[7] = crc&0x00FF;
					sendSize = 8;
				}
				else if( mreq->function_code == 0x06)
				{
					mreq->points = htons(wreq->points);

					if( INF_st.write_cmd[i].write_data )
					{
						if( wreq->function_code == 0x06 ){
							if(wreq->points > 0)
								if(endian_type == MD_LITTLE_ENDIAN)
								{
									sendbuff[4] = recvbuff[5];
									sendbuff[5] = recvbuff[4];
								}
									//mreq->points = INF_st.write_cmd[i].write_data;
								else
								{
	#ifdef VRCS
									if(recvbuff[5] == 1) sendbuff[5] = 101;
									else
									if(recvbuff[5] == 2) sendbuff[5] = 103;
									else
									   sendbuff[5] = recvbuff[4];
									sendbuff[4] = 0;
	#else
									sendbuff[4] = recvbuff[4];
									sendbuff[5] = recvbuff[5];
	#endif
								}

						}
						else if( wreq->function_code == 0x10 ){
							if(wreq->points > 0)
								if(endian_type == MD_LITTLE_ENDIAN)
								{
									sendbuff[4] = recvbuff[8];
									sendbuff[5] = recvbuff[7];
								}
									//mreq->points = INF_st.write_cmd[i].write_data;
								else
								{
	#ifdef VRCS
									if(recvbuff[5] == 1) sendbuff[5] = 101;
									else
									if(recvbuff[5] == 2) sendbuff[5] = 103;
									else
									   sendbuff[5] = recvbuff[4];
									sendbuff[4] = 0;
	#else
									sendbuff[4] = recvbuff[7];
									sendbuff[5] = recvbuff[8];
	#endif
								}

						}


								//mreq->points = htons(INF_st.write_cmd[i].write_data);
					}
	
					crc = CRC16((__u8*)mreq,6)&0xFFFF;
					sendbuff[6] = crc>>8;
					sendbuff[7] = crc&0x00FF;
					sendSize = 8;
				}
				else 
					return PERIODIC_REQ;

				INF_st.write_cmd[i].repository = wreq->points;  // for pulse check (03/26/2012)
				return_mode = WAIT_EVENT_RES;
			}
			else
				return PERIODIC_REQ;

			break;
		}
	case PERIODIC_REQ:
		if( read_index < 0 ) return PERIODIC_REQ;

		mreq->unit_id = INF_st.read_cmd[read_index].unit_id;
		mreq->function_code = INF_st.read_cmd[read_index].function_code;
		mreq->address = htons(INF_st.read_cmd[read_index].address);
		mreq->points = htons(INF_st.read_cmd[read_index].points);

		if( mreq->function_code == 0x0F || mreq->function_code == 0x10 || mreq->function_code == 0x06 )
		{
			pext = (modbus_res_hdr_ext *)writebuff;
			pext->unit_id = port_num;
			if( mreq->function_code == 0x0F )
			{
				pext->function_code = mreq->function_code;
				byte_count = (INF_st.read_cmd[read_index].points+7)>>3;
				pbyte = (__u8 *)(sendbuff+6);
				*pbyte++ = byte_count;
			}
			else if( mreq->function_code == 0x10 )
			{
				pext->function_code = mreq->function_code;
				byte_count = INF_st.read_cmd[read_index].points*2;
				pbyte = (__u8 *)(sendbuff+6);
				*pbyte++ = byte_count;
			}
			else
			{
				pext->function_code = 0x10;
				byte_count = 2;
				pbyte = (__u8 *)(sendbuff+4);
			}
			pext->address = INF_st.read_cmd[read_index].mapping;
			pext->byte_count = byte_count;

			crc = CRC16((__u8*)pext,6)&0xFFFF;
			writebuff[6] = crc>>8;
			writebuff[7] = crc&0x00FF;
			sendSize = 8;
			g_sendSize = sendSize;

			port_up(INF_st.sockfd,writebuff,sendSize,port_num);

			usleep(10000);

pthread_mutex_lock(&mutex_mem);
			if(endian_type == MD_LITTLE_ENDIAN)
				for(i = 0; i < byte_count/2 ; i = i + 2){
					pbyte[i] = (INF_mem[pext->address] & 0xFF00) >> 8;
					pbyte[i+1] = INF_mem[pext->address];}	
			else
				memcpy(pbyte,&INF_mem[pext->address],byte_count);
pthread_mutex_unlock(&mutex_mem);

			if( mreq->function_code == 0x0F || mreq->function_code == 0x10 )
			{
				crc = CRC16((__u8*)mreq,7+byte_count)&0xFFFF;
				sendbuff[7+byte_count] = crc>>8;
				sendbuff[8+byte_count] = crc&0x00FF;
				sendSize = 9+byte_count;

				return_mode = WAIT_READ_RES;
				break;
			}
		}

		crc = CRC16((__u8*)mreq,6)&0xFFFF;
		sendbuff[6] = crc>>8;
		sendbuff[7] = crc&0x00FF;
		sendSize = 8;
		
		return_mode = WAIT_READ_RES;
		break;
	case DIAGNOSIS:
		if( read_index < 0 ) return PERIODIC_REQ;

		writebuff[0] = INF_st.read_cmd[read_index].unit_id;
		writebuff[1] = 0x08;
		writebuff[2] = 0x00;
		writebuff[3] = 0x00;
		writebuff[4] = 0x00;
		writebuff[5] = 0x00;

		crc = CRC16((__u8*)writebuff,6)&0xFFFF;
		writebuff[6] = crc>>8;
		writebuff[7] = crc&0x00FF;
		sendSize = 8;

		transmited_bytes = inf_write(out,writebuff,sendSize,PRIMARY);

		return PERIODIC_REQ;
	default:
		return PERIODIC_REQ;
	}

	g_sendSize = sendSize;
	transmited_bytes = inf_write(out,sendbuff,sendSize,PRIMARY);
	
	if(transmited_bytes > 0) update_line_info(main_line, TX);
	
	return return_mode;
}

static int modbus_read(int in, int out,int port_num)
{
	char *buff;
	char rbuff[DEFAULT_BUFF_SIZE]={0,};
	char writebuff[DEFAULT_BUFF_SIZE];
	int i, len;
	unsigned char temp;
	__u16 crc, r_crc;
	modbus_req_hdr *preq;
	modbus_res_hdr *pres;
	modbus_res_hdr_ext *pext;
	char str[500], tmp[8];

//	memset(writebuff,0,DEFAULT_BUFF_SIZE);
	len = inf_read(in,rbuff,DEFAULT_BUFF_SIZE-16,PRIMARY);

#ifndef MODBUS_TCP
	//+++++ UART Error +++++
	if(rbuff[0] == 0) {
	    buff = (char *) &rbuff[1];
	}
	else { 
		buff = (char *) &rbuff[0];
	}
	//++++++++++++++++++++++
#else
	buff = (char *) &rbuff[0];
#endif
	if(len < 5 )
	{
		dprintf("[MM %d] Slave Read Too Short [%d]!\n",port_num,len);
		return -1;
	}
	if(len >= DEFAULT_BUFF_SIZE-16)
	{
		dprintf("[MM %d] Slave Read Too Long [%d]!\n",port_num,len);
		return -1;
	}
	
#ifndef MODBUS_TCP
	pres = (modbus_res_hdr *)buff;
	/*
	crc = CRC16(buff,len-2)&0xFFFF;
	r_crc = (buff[len-2]<<8)|buff[len-1];
	if(crc!=r_crc){
		dprintf("[MM %d] Slave CRC error!\n",port_num);
		return -1;
	}
	*/
	switch(pres->function_code){
//	case 1: case 2: case 3:
	case 1: case 2: case 3: case 4:
		len = 5 + pres->byte_count;
		break;
//	case 5: case 6:
	case 5: case 6: case 15: case 16:
		len = 8;
		break;
	}

#if 0
	dprintf("[MM %d] buff[0]:0x%02X,fc:0x%02X, len:%d\n",port_num,buff[0],pres->function_code,len);
		sprintf(str,"\t");
		for(i = 0; i < len; i++)
		{
			sprintf(tmp,"%02X ",buff[i]);
			strcat(str,tmp);
		}
		dprintf("%s\n",str);
#endif

	crc = CRC16(buff,len-2)&0xFFFF;
	r_crc = (buff[len-2]<<8)|buff[len-1];
	if(crc!=r_crc){
		dprintf("[MM %d] Slave CRC error!, cal:0x%04X, crc:0x%04X\n",port_num,crc,r_crc);
		return -1;
	}

#else
	pres = (modbus_res_hdr *)(buff+6);
#endif

	preq = (modbus_req_hdr *)sendbuff;

	if( preq->unit_id != pres->unit_id)
	{
		dprintf("[MM %d] Slave address [%d] mismatch!\n",port_num,pres->unit_id);
		return -1;
	}
	
	if( preq->function_code != pres->function_code )
	{
		dprintf("[MM %d] Slave function_code [%d] mismatch!\n",port_num,pres->function_code);
#ifdef HIMAP_IV
		if( preq->function_code == (pres->function_code-0x80) ) // HiMAP-IV compatible code
		{
			switch(pres->byte_count)
			{
			case illegal_function: break;
			case illegal_address: break;
			case illegal_value: dprintf("[MM %d] HiMAP-IV: Response Ignorance\n",port_num); return 1;
			case illegal_response: break;
			default: break;
			}
		}
#endif
		return -1;
	}

	if( pres->function_code == 0x05 ||
		pres->function_code == 0x06 ||
		pres->function_code == 0x0F ||
		pres->function_code == 0x10 )
	{
#ifdef DEBUG	
		dprintf("[MM %d] Write command [%02X]\n",port_num,pres->function_code);
#endif		
		return 1;
	}
	if( pres->function_code == 0x01 ||
		pres->function_code == 0x02 )
	{
		if( (htons(preq->points)+7)/8 != pres->byte_count )
		{
			dprintf("[MM %d] FC:[%02X] points [%d] - byte_count [%d] mismatch!\n",port_num,
				pres->function_code,htons(preq->points),pres->byte_count);
			return -1;
		}

		for(i = 0; i < pres->byte_count/2 ; i++)
			pres->data[i] = htons(pres->data[i]);
	}
	else if( pres->function_code == 0x03 ||
		pres->function_code == 0x04 )
	{
		if( htons(preq->points)*2 != pres->byte_count )
		{
			dprintf("[MM %d] FC:[%02X] points [%d] - byte_count [%d] mismatch!\n",port_num,
				pres->function_code,htons(preq->points),pres->byte_count);
			return -1;
		}
	}
	else return -1;

	pext = (modbus_res_hdr_ext *)writebuff;
	pext->unit_id = port_num;
	pext->function_code = 0x03;
	pext->address = INF_st.read_cmd[read_index].mapping;
	pext->byte_count = pres->byte_count;
	if(endian_type == MD_LITTLE_ENDIAN)
	{
		for(i = 0; i < pres->byte_count/2 ; i++)
			pext->data[i] = htons(pres->data[i]);
	}
	else {

#ifdef VRCS
		for(i = 0; i < pres->byte_count/2 ; i++)
		{
			temp =  (pres->data[i] & 0xFF00) >> 8;
			if(temp > 100)
			{
			   switch(temp)
			   {
			   case 101: pext->data[i] = 0x0100; break;
			   case 103: pext->data[i] = 0x0200; break;
			   case 107: pext->data[i] = 0x0400; break;
			   case 108: pext->data[i] = 0x0800; break;
			   }
			   pext->data[i] |= (pres->data[i] & 0x00FF);
			}
			else
			   pext->data[i] = pres->data[i];
		}
#else
		memcpy(pext->data,pres->data,pres->byte_count);
#endif
	}

	crc = CRC16((__u8*)pext,6+pres->byte_count)&0xFFFF;
	writebuff[6+pres->byte_count] = crc>>8;
	writebuff[7+pres->byte_count] = crc&0x00FF;

	port_up(out,writebuff,8+pres->byte_count,port_num);
for(i = 0; i < 8+pres->byte_count; i++) printf("%02x ", writebuff[i] & 0xff);
printf("\n");

	return 1;
}

static int redundancy_write(int in, int out, int port_num, int mode)
{
	unsigned short crc;
	int sendSize;
	int transmited_bytes;
	char writebuff[DEFAULT_BUFF_SIZE];

	if( read_index < 0 ) return -1;
	modbus_req_hdr *mreq = (modbus_req_hdr *)writebuff;

	if( mode == DIAGNOSIS )
	{
		writebuff[0] = INF_st.read_cmd[read_index].unit_id;
		writebuff[1] = 0x08;
		writebuff[2] = 0x00;
		writebuff[3] = 0x00;
		writebuff[4] = 0x00;
		writebuff[5] = 0x00;
	}
	else if( mode == PERIODIC_REQ )
	{
		if(INF_st.read_cmd[read_index].function_code == 0x01 ||
		   INF_st.read_cmd[read_index].function_code == 0x02 ||
		   INF_st.read_cmd[read_index].function_code == 0x03 ||
		   INF_st.read_cmd[read_index].function_code == 0x04 )
		{
			mreq->unit_id = INF_st.read_cmd[read_index].unit_id;
			mreq->function_code = INF_st.read_cmd[read_index].function_code;
			mreq->address = htons(INF_st.read_cmd[read_index].address);
			mreq->points = htons(INF_st.read_cmd[read_index].points);
		}
		else
			return 1;
	}
	else if( mode == WRITE_REQ )
	{
		sendSize = g_sendSize;
		
		if((INF_st.read_cmd[read_index].function_code == 0x0F ||
		   INF_st.read_cmd[read_index].function_code == 0x10) &&
		   (sendSize <= 8))
			return 1;

		memcpy(writebuff,sendbuff,sendSize);
		goto transmit; 
	}
	else
		return -1;
    
	crc = CRC16((__u8*)writebuff,6)&0xFFFF;
	writebuff[6] = crc>>8;
	writebuff[7] = crc&0x00FF;
	sendSize = 8;
transmit:
	transmited_bytes = inf_write(out,writebuff,sendSize,SECONDARY);
	if(transmited_bytes > 0)	update_line_info(rev_line, TX);
	return 1;
}

static int redundancy_read(int in, int out,int port_num)
{
	char buff[DEFAULT_BUFF_SIZE];
	int i, len;
	__u16 crc, r_crc;
	modbus_req_hdr *preq;
	modbus_res_hdr *pres;

	len = inf_read(in,buff,DEFAULT_BUFF_SIZE-16,SECONDARY);
	if(len < 5 ) return -1;
	if(len >= DEFAULT_BUFF_SIZE-16) return -1;

#ifndef MODBUS_TCP
	crc = CRC16(buff,len-2)&0xFFFF;
	r_crc = (buff[len-2]<<8)|buff[len-1];
	if(crc!=r_crc){
		dprintf("[MM %d] Redundancy Slave CRC error!\n",port_num);
		return -1;
	}

	pres = (modbus_res_hdr *)buff;
#else
	pres = (modbus_res_hdr *)(buff+6);
#endif

#if 0
	if( preq->unit_id != pres->unit_id)
	{
		printf("Slave address [%d] mismatch!\n",pres->unit_id);
		return -1;
	}
#endif
	
	if( pres->function_code == 0x08 )
	{
		dprintf("[MM %d] Diagnosis [ID: %02X]\n",port_num,pres->unit_id);
	}
	else
	{
//		dprintf("[MM %d] Both [ID: %02X]\n",port_num,pres->unit_id);
	}

	return 1;
}

static int select_nextquery(int port_num, int mode)
{
	int i;
	int next_mode;
	char head,tail,length;
	char str[40], temp[8];
	int f_code;

pthread_mutex_lock(&mutex_inf);
	head = CMD_q.write_head;
	tail = CMD_q.write_tail;
pthread_mutex_unlock(&mutex_inf);

//	if( mode != PERIODIC_REQ_PRI && head != tail )
 	if( mode != PERIODIC_REQ_PRI && head != tail && Inf_s_cmd_b.bit.tx_valid == 3) // LGE
	{
pthread_mutex_lock(&mutex_inf);
		length = CMD_q.write_length[head];
		memcpy(recvbuff,CMD_q.write_buff[head],length);
		CMD_q.write_head++;
		CMD_q.write_head %= MAX_Q_SIZE;
pthread_mutex_unlock(&mutex_inf);
#if 0
		dprintf("[CMD dequeue inf:%d] head:%d tail:%d\n",port_num,
			CMD_q.write_head,tail);
		sprintf(str,"\t");
		for(i = 0; i < length; i++)
		{ 
			sprintf(temp,"%02X ",recvbuff[i]);
			strcat(str,temp);
		}
		dprintf("%s\n",str);
#endif

		next_mode = WRITE_REQ;
	}
	else
	{
CHECK_read_index:
		for( i = 0 ; i < INF_st.num_read_cmd ; i++ )
		{
			read_index = ++read_index % INF_st.num_read_cmd;

// LGE
			if( Inf_s_cmd_b.bit.tx_valid != 3 )
			{
				f_code = INF_st.read_cmd[read_index].function_code;
				if( f_code == 0x05 || f_code == 0x06 || f_code == 0x0F || f_code == 0x10 )
				{
					goto CHECK_read_index;
				}
			}
// --
//			dprintf("[INF %d] read index: %d priority: %d\n",port_num,
//				read_index,INF_st.read_cmd[read_index].aging);

			if( INF_st.read_cmd[read_index].aging == 0 )
			{
//				dprintf("[INF %d] Query select: %d priority: %d\n",port_num,
//					read_index,INF_st.read_cmd[read_index].priority);
				INF_st.read_cmd[read_index].aging = INF_st.read_cmd[read_index].priority;
				break;
			}
			else
			{
				INF_st.read_cmd[read_index].aging--;
				if( INF_st.read_cmd[read_index].aging < 0 )
					INF_st.read_cmd[read_index].aging = 0;
			}
		}
		if( i >= INF_st.num_read_cmd )
		{
			dprintf("[MM %d] Error: Query not selected!\n",port_num);
			read_index = -1;
		}

		next_mode = PERIODIC_REQ;
	}

	return next_mode;
}

static int enqueue_cmd(int port_num,int index)
{
	char i,head,tail;
	char tempbuff[10];
	char str[32], temp[8];
	unsigned short crc;
	modbus_req_hdr *mreq = (modbus_req_hdr *)tempbuff;

pthread_mutex_lock(&mutex_inf);
	head = CMD_q.write_head;
	tail = CMD_q.write_tail;
pthread_mutex_unlock(&mutex_inf);

	if( (tail+1)%MAX_Q_SIZE == head ) return -1;
	else
	{
		mreq->unit_id = port_num;
		mreq->function_code = 0x06;
		mreq->address = INF_st.write_cmd[index].address;
		mreq->points = 0; // clear

		crc = CRC16((__u8*)mreq,6)&0xFFFF;
		tempbuff[6] = crc>>8;
		tempbuff[7] = crc&0x00FF;

pthread_mutex_lock(&mutex_inf);
		memcpy(CMD_q.write_buff[tail],tempbuff,8);
		CMD_q.write_length[tail]=8;
		CMD_q.write_tail++;
		CMD_q.write_tail %= MAX_Q_SIZE;
pthread_mutex_unlock(&mutex_inf);
#if 0
		dprintf("[CMD queue inf:%d by checker] head:%d tail:%d\n",port_num,
			head,CMD_q.write_tail);
		sprintf(str,"\t");
		for(i = 0; i < 8; i++)
		{
			sprintf(temp,"%02X ",CMD_q.write_buff[tail][i]);
			strcat(str,temp);
		}
		dprintf("%s\n",str);
#endif
	}

	return 1;
}

void modbusm_bufm(void *arg)	// added on 21/11/11
{
	int i,sockfd,flags;
	int port_num, len, check;
	fd_set readfds,	testfds;
	char tempbuff[DEFAULT_BUFF_SIZE],writebuff[16];
	char str[40], temp[8];
	char head,tail;
	__u8 delay = 0;
//	__u16 pulse_type = 1<<4, crc;
//	__u16 pre_flag = mflag;
	__u8 unit_id, func_code;
	__u16 address, points, byte_count;
	struct timeval timeout;
	modbus_res_hdr_ext *preq;
	modbus_res_hdr_ext *pext;

	port_num = (int)arg;
	pthread_detach(pthread_self());

	dprintf(" - Buffer Manager Start [Port: %d]\n",port_num);
	
//	sleep(5);

	sockfd = INF_st.sockfd;
//	flags = fcntl(sockfd, F_GETFL, 0);
//	fcntl(sockfd, F_SETFL, flags|O_NONBLOCK);

	FD_ZERO(&readfds);
	FD_SET(sockfd, &readfds);

	preq = (modbus_res_hdr_ext *)tempbuff;

	while(1)
	{
		testfds = readfds;

		timeout.tv_sec = 30;
		timeout.tv_usec = 0;

		if(select(sockfd+1, &testfds, (fd_set*)0, (fd_set*)0, (struct timeval*)&timeout) != 0)
		{
			if( FD_ISSET(sockfd,&testfds) ) {  // Write Command
				len = read(sockfd,tempbuff,MAX_CMD_SIZE);
				if( len >= 6 && len < 256 )
				{
					unit_id = preq->unit_id;
					func_code = preq->function_code;
					address = preq->address;

					if( func_code == 0xFF )  // for modbus read response  added on 07/05/12
					{
						byte_count = preq->byte_count;
						points = byte_count/2;
						check = 1;

						if( unit_id != port_num )
						{
						//	dprintf("[MM CMD %d] Precheck error - id [%d]\n",port_num,unit_id);
							check = -1;
						}
		
						if(address>=2048)
						{
						//	dprintf("[MM CMD %d] Precheck error - address [%d]\n",port_num,address);
							check = -1;
						}

						if((address+points)>2048)
						{
						//	dprintf("[MM CMD %d] Precheck error - Data Space [%d]-[%d]\n",port_num,
						//		address,address+points-1);
							check = -1;
						}

						if( byte_count > 128 )
						{
						//	dprintf("[MM CMD %d] Precheck error - Too large [%d]\n",port_num,byte_count);
							check = -1;
						}

						if( check > 0 )
						{
							dprintf("[MM CMD %d] MEM ADDR:%04X NO:%04X(%d)\n",port_num,address,points,byte_count);
pthread_mutex_lock(&mutex_mem);
							memcpy(&INF_mem[address],preq->data,byte_count);
pthread_mutex_unlock(&mutex_mem);
							dprintf("DATA : %d-->[%d][%d][%d][%d][%d]\n",address,preq->data[0],preq->data[1],preq->data[2],preq->data[3],preq->data[4]);
						}
					}
					else if( func_code == write_single_reg || func_code == write_mult_regs )
					{
pthread_mutex_lock(&mutex_inf);
						head = CMD_q.write_head;
						tail = CMD_q.write_tail;
pthread_mutex_unlock(&mutex_inf);
						if( (tail+1)%MAX_Q_SIZE == head )
						{
						//	dprintf("[MM CMD %d] Write buff is full!!\n",port_num);
							mflag |= (1<<14);
							delay = 0;
						}
						else
						{	
							if( len == 6 || len == 11 )
							{
pthread_mutex_lock(&mutex_inf);
								memcpy(CMD_q.write_buff[tail],tempbuff,len);
								CMD_q.write_length[tail]=len;
								CMD_q.write_tail++;
								CMD_q.write_tail %= MAX_Q_SIZE;
pthread_mutex_unlock(&mutex_inf);
#if 0
								dprintf("[CMD queue inf:%d] head:%d tail:%d (len:%d)\n",port_num,
									head,CMD_q.write_tail,len);
								sprintf(str,"\t");
								for(i = 0; i < len; i++)
								{
									sprintf(temp,"%02X ",CMD_q.write_buff[tail][i]);
									strcat(str,temp);
								}
								dprintf("%s\n",str);
#endif

								if( delay > 10 )
								{
									mflag &= ~(1<<14);
									delay = 0;
								}
								else delay++;
							}
							else
							{
							//	dprintf("[MM CMD %d] CMD Length error [%d]\n",port_num,len);
							}

						//	cmd_handshake(sockfd,port_num,0x06);
						}
					}
					else // func_code error
					{
					//	dprintf("[MM CMD %d] Precheck error - Func code [%d]\n",port_num,func_code);
					}
				} // length error
			} // 
		}

		usleep(10000);
	}
}

void modbusm_pulse(void *arg)
{
	int i,sockfd,flags;
	int port_num;
	char writebuff[16];
	__u16 pulse_type = 1<<4, crc;
	__u16 pre_flag = mflag;
	modbus_res_hdr_ext *pext;

	port_num = (int)arg;
	pthread_detach(pthread_self());

	sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);	
	if(sockfd < 0)
	{
		dprintf("[MM PULSE %d] socket open error!!!\n",port_num);
		return;
	}
//	flags = fcntl(sockfd, F_GETFL, 0);
//	fcntl(sockfd, F_SETFL, flags|O_NONBLOCK);

	dprintf(" - Pulse Manager Start [Port: %d]\n",port_num);

	sleep(1);

	while(1)
	{
#ifdef PULSE_CHECK 
#ifdef DEBUG
		dprintf("[MM CMD %d] Checking Pulse-type Command...\n",port_num);
#endif
		for(i = 0; i < INF_st.num_write_cmd; i++)
		{
			if(INF_st.write_cmd[i].write_type & pulse_type)
			{
				if(INF_st.write_cmd[i].repository)
				{
				//	dprintf("[MM CMD %d] pulse-type command error protected!!\n",port_num);
					enqueue_cmd(port_num,i);
					mflag |= (1<<15);
					break;
				}
			}
		}
		if(i >= INF_st.num_write_cmd) mflag &= ~(1<<15);
#endif

#if 1
		if( mflag != pre_flag )
		{
			pext = (modbus_res_hdr_ext *)writebuff;
			pext->unit_id = 0;
			pext->function_code = 0x03;
			pext->address = 30+(port_num-1);
			pext->byte_count = 2;
			pext->data[0] = htons(mflag);	
			crc = CRC16((__u8*)pext,8)&0xFFFF;
			writebuff[8] = crc>>8;
			writebuff[9] = crc&0x00FF;

			port_up(sockfd,writebuff,10,0);
			pre_flag = mflag;
		}
#endif
		
		sleep(30);
	}
}

void modbusm_thread(void *arg)
{
	int slavefd,slavefds[2],tempfd;
	int slave_index;
	int optval=1;
	fd_set readfds,	testfds;
	int fd, sockfd, flags;
	int	nread;
	int port_num, mode, result, result_2, retries;
	int switch_no, reset_no, system_no;
	struct timeval timeout;
	char write_flag;
	int tx_time,rx_time,timeout_time;
	int num_port, num_retry, num_read;
	char redundancy_on;
	char isrestart = 0;

	//+++++ Redundancy ++++
	int retries_2 = 0, switch_no_2 = 0;
	//++++++++++++++++++++++

	port_num = (int)arg;
	pthread_detach(pthread_self());
	dprintf("Modbus Master Start [INF: %d]\n",port_num);
	sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(sockfd < 0)
	{
		dprintf("[MM %d] socket open error!!!\n",port_num);
		return;
	}

//	slavefds[0] = 0;
//	slavefds[1] = 0;
	num_port = INF_st.num_port;
	slavefds[0] = p_open[0] = open_link(0);

	if(INF_st.num_port == 2)
	{
		p_open[1] = open_link(1);
	}
	dprintf("[INF: %d] id0:%d, id1:%d \n",port_num,p_open[0],p_open[1]);
	cmd_handshake(sockfd,port_num,0xAA);
	cmd_clear(sockfd);

	retries = 0;
	switch_no = 0;
	reset_no = 0;
	write_flag = 0;
	fd_index = 0;
	main_line = fd_index ;	
	rev_line = (fd_index + 1) % num_port;	
	slave_index = 0;
	redundancy_on = 0;
	slavefd = slavefds[fd_index];

	tx_time = INF_st.tx_time*1000;
	rx_time = INF_st.rx_time*1000;
	timeout_time = INF_st.timeout*1000;
	num_retry = INF_st.num_retry;
	num_read  = INF_st.num_read_cmd;
	
	if( INF_st.num_addr_map > 0 && INF_st.num_addr_map <= 16 )
		system_no = INF_st.num_addr_map;
	else
		system_no = 1;
	mode = PERIODIC_REQ;

pthread_mutex_lock(&mutex_inf);
	CMD_q.write_head = 0;
	CMD_q.write_tail = 0;	// added on 21/11/11
pthread_mutex_unlock(&mutex_inf);

	dprintf("Modbus Master Start [INF: %d]\n",port_num);

	FD_ZERO(&readfds);
	FD_SET(slavefd, &readfds);

	if(INF_st.num_port == 2)
	{
		slave_index = 1;
		if(INF_st.redundancy >= LINECHECK_REDUNDANCY )
		{
			FD_SET(slavefds[slave_index], &readfds);
			redundancy_on = 1;
		}
	}

	Comm_Open_Err = 0;

	while(1){
		testfds	= readfds;
		slavefds[0] = p_open[0];
		slavefds[1] = p_open[1];
		if(p_open[port_ps] >= 0)
		{
			switch(mode)
			{
			case WRITE_REQ:
				mode = modbus_write(sockfd,slavefds[port_ps],port_num,mode);
				break;
			case PERIODIC_REQ:
				mode = modbus_write(sockfd,slavefds[port_ps],port_num,mode);
				break;
			default:
				mode = PERIODIC_REQ;
				continue;
			}
		}

		if( redundancy_on && p_open[port_ps^1]>=0 )
		{
			if( INF_st.redundancy == LINEBOTH_REDUNDANCY )
				redundancy_write(sockfd,slavefds[port_ps^1],port_num,WRITE_REQ);
			else if( INF_st.redundancy == LINEREAD_REDUNDANCY )
				redundancy_write(sockfd,slavefds[port_ps^1],port_num,PERIODIC_REQ);
			else
				redundancy_write(sockfd,slavefds[port_ps^1],port_num,DIAGNOSIS);
			result_2 = 0;
		}

		usleep(rx_time);
		
		result = -1;
		result_2 = -1;

		timeout.tv_sec = 0;
		timeout.tv_usec = timeout_time;

		if(select(FD_SETSIZE, &testfds, (fd_set*)0, (fd_set*)0, (struct timeval*)&timeout) != 0){
			if( FD_ISSET(slavefds[port_ps],&testfds) ) {
				ioctl(slavefds[port_ps], FIONREAD,&nread);
				if(nread > 0){
					result = modbus_read(slavefds[port_ps],sockfd,port_num);
				}
//				usleep(tx_time);
			}

			if( redundancy_on )
			{
				if( FD_ISSET(slavefds[port_ps^1],&testfds) ) {
					ioctl(slavefds[port_ps^1], FIONREAD,&nread);
					if(nread > 0){
						result_2 = redundancy_read(slavefds[port_ps^1],sockfd,port_num);
					}
				}
			}
		}
//++++++++++++++++ Secondary +++++++++++++++++
		if( redundancy_on )
		{
			if( result_2 > 0 )
			{
				main_line = port_ps^1;
				line_check(sendbuff[0],sockfd,port_num,ALIVE);
				update_line_info(main_line, RX);
				retries_2 = 0;
				switch_no_2 = 0;
			}
			else if( result_2 < 0 )
			{
				main_line = port_ps^1;
				retries_2++;
				line_check(sendbuff[0],sockfd,port_num,BAD);

				if( retries_2 >= num_retry )
				{
					switch_no_2++;
					if(switch_no_2 >= 2)
					{
						line_check(sendbuff[0],sockfd,port_num,DEAD);
					}
				}
			}
		}  // Redundancy Check

//++++++++++++++++ Primary +++++++++++++++++
		if( result > 0 )
		{
			main_line = port_ps;
			line_check(sendbuff[0],sockfd,port_num,ALIVE);

			update_line_info(main_line, RX);
		
			mode = select_nextquery(port_num,mode);

			retries = 0;
			switch_no = 0;
			reset_no = 0;
		}
		else if( result == 0 )
		{
			mode = select_nextquery(port_num,mode);

			retries = 0;
			switch_no = 0;
			reset_no = 0;
		}
		else
		{
#if 1
			if( mode == WAIT_READ_RES ) mode = PERIODIC_REQ;
			else if( mode == WAIT_WRITE_RES ) mode = WRITE_REQ;
			else if( mode == WAIT_EVENT_RES ) mode = WRITE_REQ;
			else mode = PERIODIC_REQ;
#endif
			main_line = port_ps;
			retries++;
			line_check(sendbuff[0],sockfd,port_num,BAD);

			if( retries >= num_retry )
			{
				switch_no++;
				if(switch_no >= 2)
				{

#ifndef MODBUS_TCP
#ifdef DEBUG
					dprintf("[MM %d] Query Conversion!\n",port_num);
#endif
					mode = select_nextquery(port_num,mode);

					switch_no = 0;

#ifdef UART_REOPEN
					if( reset_no++ >= (num_read/system_no) )
					{
						dprintf("[MM %d] UART Reconnect!\n",port_num);
						close(slavefds[0]);
						sleep(1);
						if(num_port == 2)
						{
							close(slavefds[1]);
							sleep(1);
						}
					}
#endif
		
#else
#ifdef DEBUG
					dprintf("[MM %d] Socket Reconnect!\n",port_num);
#endif
//
//					close(slavefds[0]);
//					sleep(5);
//					if(num_port == 2)
///					{
//						close(slavefds[1]);
//						sleep(5);
//					}
//					
//					line_check(sendbuff[0],sockfd,port_num,DEAD);
//					port_ps ^= 1;
//					dprintf("[MM %d] Port Conversion!\n",port_num);
//					goto restart; 

#endif
				}
#ifdef DEBUG
				dprintf("[MM %d] Port Conversion!\n",port_num);
#endif

				retries = 0;
			} 
		}

#ifdef ECP_PHY
		line_check(sendbuff[0],sockfd,port_num,PHY_CHECK);
#endif
	}

	close(slavefd);
	close(sockfd);
}
 
void modbusm_reopen_thread(void *arg)
{
	int port_num, slavefds[2];
	port_num = (int)arg;

	dprintf("Modbus Master Reopen Thread Start [INF: %d]\n",port_num);
	while(1)
	{
		sleep(10);
		if((p_open[0] < 1000)&&(p_open[1] < 1000))
		{
			if(INF_st.num_port == 2){
				if((port0 == 0)||(p_open[0]==0)){
					close(p_open[0]);
					p_open[0] = open_link(0);
				}
				if((port1 == 0)||(p_open[1]==0)){
					close(p_open[1]);
					p_open[1] = open_link(1);
				}
			}
			else if(port0 == 0){
				close(p_open[0]);
				p_open[0] = open_link(0);
			}
		}

		if((INF_st.num_port == 2)&&((port0 == 0)||(port1 == 0))){
//		if((port0 == 0)||(port1 == 0)){
//			dprintf("[INF: %d] port0 : %d, port1 : %d \n",port_num,port0,port1);
//			dprintf("[INF: %d] open0 : %d, open1 : %d \n",port_num,p_open[0],p_open[1]);
//			dprintf("[INF: %d] port_ps : %d, redundancy_on : %d \n",port_num,port_ps, redundancy_on);
			dprintf("1[INF: %d] ps:%d, p0:%d, p1:%d, id0:%d, id1:%d \n",port_num,port_ps,port0,port1,p_open[0],p_open[1]);
		}

		if((INF_st.num_port == 2)&&((port0 == 1)||(port1 == 1))){
			dprintf("2[INF: %d] ps:%d, p0:%d, p1:%d, id0:%d, id1:%d \n",port_num,port_ps,port0,port1,p_open[0],p_open[1]);
		}

	}
}
