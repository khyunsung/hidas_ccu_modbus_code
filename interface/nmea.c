#include <stdio.h>

#include <asm/types.h>
#include <sys/types.h>

#include "nmea.h"

#if 0
// Standard NMEA Sentence
#define	NMEA_SEN_ALR	0x524C41
#define	NMEA_SEN_DPT	0x545044
#define	NMEA_SEN_DTM	0x4D5444
#define	NMEA_SEN_GLL	0x4c4c47
#define	NMEA_SEN_GGA	0x414747
#define	NMEA_SEN_HDT	0x544448
//#define	NMEA_SEN_HTD	0x445448
//#define	NMEA_SEN_HTC	0x435448
#define	NMEA_SEN_MTW	0x57544D
#define	NMEA_SEN_MWV	0x56574D
#define	NMEA_SEN_ROT	0x544F52
#define	NMEA_SEN_RPM	0x4D5052
#define	NMEA_SEN_RSA	0x415352
#define	NMEA_SEN_VBW	0x574256
#define	NMEA_SEN_VTG	0x475456
#define	NMEA_SEN_XDR	0x524458
#define	NMEA_SEN_ZDA	0x41445A

// Radar/ARPA NMEA Sentence
#define NMEA_SEN_AAM	0x4D4141	// Waypoint Arrival Alarm
#define NMEA_SEN_OSD	0x44534F	// Own Ship Data
#define NMEA_SEN_RSD	0x445352	// Radar System Data
#define NMEA_SEN_TLL	0x4C4C54	// Target Latitude and Longitude
#define NMEA_SEN_TTM	0x4D5454	// Tracked Target Message

#define	NMEA_SEN_HTC	0x435448
#define	NMEA_SEN_HTD	0x445448
#define	NMEA_SEN_VTG	0x475456
#define NMEA_SEN_WPL	0x4C5057

// Proprietary NMEA Sentence
#define	NMEA_SEN_BAL	0x4C4142
#define	NMEA_SEN_DPO	0x4F5044
#define	NMEA_SEN_DPR	0x525044
#define	NMEA_SEN_DYN	0x4E5944
#define	NMEA_SEN_HAS	0x534148
#define	NMEA_SEN_HFO	0x4F4648
#define	NMEA_SEN_HAO	0x4F4148
#define	NMEA_SEN_POS	0x534F50
#define	NMEA_SEN_PTR	0x525450
#define	NMEA_SEN_RAS	0x534152
#define	NMEA_SEN_RPO	0x4F5052
#define	NMEA_SEN_SPO	0x4F5053
#define	NMEA_SEN_SRO	0x4F5253
#define	NMEA_SEN_TRO	0x4F5254
#define	NMEA_SEN_SYS	0x535953
#define	NMEA_SEN_HMI	0x494D48
#define	NMEA_SEN_MOD	0x444F4D
#define	NMEA_SEN_OTE	0x45544F

#define	NMEA_SEN_MST	0x54534D
#define	NMEA_SEN_FTD	0x445446
#endif

int decstr2num(char *s, int len)
{
	int i;
	int val=0;
	unsigned char sign = 0;

	if(*s=='-'){
		sign = 1;
		s++;
	}
	if(*s=='+') s++;
	for(i=0; i<len; i++){
		if(*s>='0' && *s<='9'){
			val *= 10;
			val += *s-'0';
			s++;
		} else{
			break;
		}
	}
	if(sign)	val = val * (-1);

	return val;
}

float decstr2float(char *s, int len)
{
	int i;
	float val=0.0, div=0;
	unsigned char sign = 0;

	if(*s=='-'){
		sign = 1;
		s++;
	}
	if(*s=='+') s++;
	for(i=0; i<len; i++){
		if(*s>='0' && *s<='9'){
			val *= 10;
			div *= 10;
			val += *s-'0';
			s++;
		} else if(*s=='.'){
			div = 1;
			s++;
		} else{
			break;
		}
	}
	if(div != 0)	val = val / div;
	if(sign)	val = val * (-1.);
	
	return val;
}


int hexstr2num(char *s, int len)
{
	int i;
	int val=0;

	for(i=0; i<len; i++){
		if(*s>='0' && *s<='9'){
			val <<= 4;
			val += *s-'0';
			s++;
		} else if(*s>='a' && *s<='f'){
			val <<= 4;
			val += *s-'a'+10;
			s++;
		} else if(*s>='A' && *s<='F'){
			val <<= 4;
			val += *s-'A'+10;
			s++;
		} else{
			break;
		}
	}

	return val;
}

typedef struct {
	    unsigned short a;
		unsigned short b;
		unsigned short c;
} retType;

retType my_get_nmea_utc(char *s)
{
    retType ret;

	ret.a = decstr2num(s, 2);
	s += 2;
	
	ret.b = decstr2num(s, 2);
	s += 2;
	
	ret.c = decstr2num(s, 2);
	return ret;
}
void get_nmea_utc(char *s, char *ret)
{
	int hour, min, sec, msec=0;
	int len;

	len = strlen(s);
	hour = decstr2num(s, 2);
	s += 2;
	len -= 2;
	min = decstr2num(s, 2);
	s += 2;
	len -= 2;
	sec = decstr2num(s, 2);
	s += 2;
	len -= 2;
	if(*s=='.'){
		s++;
		msec = decstr2num(s, len);
	}

	sprintf(ret,"%02d:%02d:%02d.%03d",hour,min,sec,msec);
}


retType my_get_nmea_latitude(char *s)
{
	 retType ret; 

	 ret.a = decstr2num(s,2);

     s+=2;
	 ret.b = decstr2num(s,2);

     s+=3;

	 ret.c = decstr2num(s,4);
	 return ret;
}

float get_nmea_latitude(char *s, char *s1)
{
	float degree, minute;
	int len;

	len = strlen(s);
	degree = decstr2float(s,len);
#if 0
	degree = decstr2num(s, 2);
	s += 2;
	len -= 2;
	minute = decstr2float(s, len);
	degree = degree + minute/60.0;
#endif
	if(*s1 == 'S')	degree *= -1;	// 'N':+, 'S':-

	return degree;
}


retType my_get_nmea_longitude(char *s)
{
	 retType ret; 

	 ret.a = decstr2num(s,3);

     s+=3;
	 ret.b = decstr2num(s,2);

	 s+=3;

	 ret.c = decstr2num(s,4);

	 return ret;
}
float get_nmea_longitude(char *s, char *s1)
{
	float degree, minute;
	int len;

	len = strlen(s);
	degree = decstr2float(s,len);
#if 0
	degree = decstr2num(s, 3);
	s += 3;
	len -= 3;
	minute = decstr2float(s, len);
	degree = degree + minute/60.0;
#endif
	if(*s1 == 'W')	degree *= -1;	// 'E':+, 'W':-

	return degree;
}

float get_nmea_angle(char *s, char *s1)
{
	float degree;

	degree = decstr2float(s, strlen(s));
	if(*s1 == 'U')	degree *= -1;		// 'D':+, 'U':-
	else if(*s1 == 'L')	degree *= -1;	// 'P':+, 'L':-

	//dprintf("An:%7.4f ", degree);

	return degree;
}

int get_args(char *s, char **argv)
{
	int	argc = 0;
	
	argv[argc++] = s;
	while(*s!='\0'){
		if(*s == ','){
			*s = '\0';
			argv[argc++] = ++s;
		} else if(*s == '*'){
			*s = '\0';
			break;
		} else{
			s++;
		}
	}
	return argc;
}

int get_args1(char *s, char **argv)
{
	int	argc = 0;

	argv[argc++] = s;
	while(*s!='\0'){
		if(*s == ','){
			*s = '\0';
			argv[argc++] = ++s;
		} else if(*s == '\r'){
			*s = '\0';
			break;
		} else{
			s++;
		}
	}
	return argc;
}
