#ifndef	_LOG_H
#define	_LOG_H


void log_error(char *format, ...);
void log_access(char *format, ...);

void open_logs(int mode);

#endif		// _LOG_H
