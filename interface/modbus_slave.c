#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#include <asm/types.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>

#include "common.h"

#include "app.h"
#include "interface.h"
#include "modbus.h"
#include "utils.h"
#include "log.h"


//#define	DEBUG
#ifndef	DEBUG
	#define	dprintf(format, args...) log_access(format, ## args)
#else
	#define	dprintf(format, args...) log_access(format, ## args); printf(format, ## args)
#endif	// DEBUG

//#define		WRITE_DEBUG

#define BASE 			3000
#define LIMIT_CLIENT_NUMBER 16
#define DEBUG_INF

extern int inf_tx_cnt, inf_rx_cnt, inf_thr_cnt;
extern __u8 inf_alive;

extern INF_ST INF_st;

extern pthread_mutex_t mutex_mem;
pthread_t clientch_thr; 

extern int fd_index, read_index;
extern unsigned short INF_mem[MAX_INF_TAG];
extern char sendbuff[DEFAULT_BUFF_SIZE];
extern char recvbuff[DEFAULT_BUFF_SIZE];

__u16 trans_id;
__u16 ptc_id;
unsigned char client_number;
char endian_type;
int Port_Number;//2016.12.08-23:53,khs

static int _open_server(int port)
{
	return -1;
}
static int _open_link(int port)
{
	return -1;
}

static __u16 make_mask(__u8 base, __u16 to)
{
	__u16 mask1, mask2=0xFFFF;

	switch(to)
	{
	case 0: mask1 = 0x0000; break;
	case 1: mask1 = 0x0001; break;
	case 2: mask1 = 0x0003; break;
	case 3: mask1 = 0x0007; break;
	case 4: mask1 = 0x000F; break;
	case 5: mask1 = 0x001F; break;
	case 6: mask1 = 0x003F; break;
	case 7: mask1 = 0x007F; break;
	case 8: mask1 = 0x00FF; break;
	case 9: mask1 = 0x01FF; break;
	case 10: mask1 = 0x03FF; break;
	case 11: mask1 = 0x07FF; break;
	case 12: mask1 = 0x0FFF; break;
	case 13: mask1 = 0x1FFF; break;
	case 14: mask1 = 0x3FFF; break;
	case 15: mask1 = 0x7FFF; break;
	case 16:
	default: mask1 = 0xFFFF;
	}

	if( base == 0 ) return mask1;

	switch(base)
	{
	case 1: mask2 = 0xFFFE; break;
	case 2: mask2 = 0xFFFC; break;
	case 3: mask2 = 0xFFF8; break;
	case 4: mask2 = 0xFFF0; break;
	case 5: mask2 = 0xFFE0; break;
	case 6: mask2 = 0xFFC0; break;
	case 7: mask2 = 0xFF80; break;
	case 8: mask2 = 0xFF00; break;
	case 9: mask2 = 0xFE00; break;
	case 10: mask2 = 0xFC00; break;
	case 11: mask2 = 0xF800; break;
	case 12: mask2 = 0xF000; break;
	case 13: mask2 = 0xE000; break;
	case 14: mask2 = 0xC000; break;
	case 15: mask2 = 0x8000; break;
	default: mask2 = 0x0000;
	}
	
	return mask1&mask2;
}

int add_mbapheader(char *buff, unsigned int length)
{	
	modbus_mbap_hdr MBAPHeader_snd;
	char buff_t[DEFAULT_BUFF_SIZE];
	
	//2016.11.25-03:43,khs  MBAPHeader_snd.trans_id = htons(trans_id);
	MBAPHeader_snd.trans_id = trans_id;
	MBAPHeader_snd.ptc_id = htons(ptc_id);	
	MBAPHeader_snd.length = htons(length);

	memcpy(buff_t,buff,length);	
	memcpy(buff,&MBAPHeader_snd,6);
	memcpy(buff+6,buff_t,length);
	
	return length+6;
}

static int modbus_error(int out, __u8 id, __u8 func, __u8 exc,int ps)
{
	char buff[8];
	int index = 0, buff_length;
	__u16 crc;

	buff[index++] = id;
	buff[index++] = 0x80+func;
	buff[index++] = exc;


	
#ifdef MODBUS_TCP
	buff_length = add_mbapheader(buff, index);
#else
	crc = CRC16(buff,3)&0xFFFF;
	buff[index++] = crc>>8;
	buff[index++] = crc&0x00FF;
	buff_length = index;
#endif

	inf_write(out,buff,buff_length,ps);
	return 1;
}


static int modbus_write(int out, __u8 id, __u8 func, __u16 address, __u16 points, __u16 org, int ps)
{
	int i, index, len, sendSize;
	__u16 crc, r_crc;
	__u8 byte_count, base;
	__u16 temp, mask;
	
	modbus_res_hdr *pres;
	pres = (modbus_res_hdr *)sendbuff;


	if(address>=2048)
	{
		dprintf("[MS Res] Precheck error - map address [%d]\n",address);
		return -1;
	}

	pres->unit_id = id;
	pres->function_code = func;
	if( func == 0x01 || func == 0x02 )
		byte_count = ((points+7)>>3)&0x00FF;
	else
		byte_count = (points*2)&0x00FF;
	pres->byte_count = byte_count;

	if( func == 0x01 || func == 0x02 )
	{
		if((address*16+points)>32768)
		{
			dprintf("[MS Res] Precheck error - Data Space [%d]-[%d]\n",
				address,address+byte_count/2-1);
			return -1;
		}

		base = org%16;
pthread_mutex_lock(&mutex_mem);
		for( i = 0, index = 0 ; i < points ; i+=16 )
		{
			mask = make_mask(base,points-i); base = 0;
			temp = INF_mem[address+index];
			if( mask != 0xFFFF ) temp &= mask;
			pres->data[index++] = temp;
		}
pthread_mutex_unlock(&mutex_mem);
	}
	else if( func == 0x10 )
	{
		if((address+points)>2048)
		{
			dprintf("[MS Res] Precheck error - Data Space [%d]-[%d]\n",
				address,address+points-1);
			return -1;
		}

pthread_mutex_lock(&mutex_mem);
		if(endian_type == MD_LITTLE_ENDIAN)
			for(i = 0; i < byte_count/2 ; i++)
				pres->data[i] = INF_mem[address + i];
		else
			memcpy(pres->data,&INF_mem[address],byte_count);
pthread_mutex_unlock(&mutex_mem); 
		return 1; 
	}
	else
	{
		if((address+points)>2048)
		{
			dprintf("[MS Res] Precheck error - Data Space [%d]-[%d]\n",
				address,address+points-1);
			return -1;
		}

pthread_mutex_lock(&mutex_mem);
		if(endian_type == MD_LITTLE_ENDIAN)
			for(i = 0; i < byte_count/2 ; i++)
				pres->data[i] = INF_mem[address + i];
		else
			memcpy(pres->data,&INF_mem[address],byte_count);
pthread_mutex_unlock(&mutex_mem);
	}
	
#ifndef MODBUS_TCP
	crc = CRC16((__u8*)pres,3+byte_count)&0xFFFF;
	sendbuff[3+byte_count] = crc>>8;
	sendbuff[4+byte_count] = crc&0x00FF;
	sendSize = 5+byte_count;
#else
	sendSize = add_mbapheader(sendbuff,byte_count+3);
#endif
	
	inf_write(out,sendbuff,sendSize,ps);
	return 1;
}

char membuff[32];
int  mem_len;	//MODBUS_TCP 에서 총 127포인트 일때 문제가 생겨 int형으로 변환
//char mem_len;	

static int handle_modbus_slave(int in, int out,int port_num, int ps)
{
	char *buff = NULL, *tempbuff;
	char tempbuf[512], catbuff[512];
	char writebuff[DEFAULT_BUFF_SIZE];
	char resbuff[DEFAULT_BUFF_SIZE];
	int i, len;
	__u16 crc, r_crc;
	modbus_req_hdr *preq;
	modbus_res_hdr *pres;
	modbus_res_hdr_ext *pext;
	__u8 unit_id, func_code;
	__u16 address, points;
	__u16 mapping, range;
	int tx_time = INF_st.tx_time*1000;
	mapping = 0;
	len = inf_read(out,tempbuf,512,ps);

#ifndef MODBUS_TCP
	//+++++ UART Error +++++
	if(tempbuf[0] == 0) {
		tempbuff = (char *) &tempbuf[1];
	}
	else {
		tempbuff = (char *) &tempbuf[0];
	}
	//++++++++++++++++++++++

#else
	tempbuff = (char *) &tempbuf[0];
#endif

	if(len == 0)
	{
		return -1;	//close
	}
	if(len < 8 )
	{
		if( mem_len <= 0 )
		{
			dprintf("[MS %d] Slave Read Concatenation!\n",port_num);
			dprintf("\tSave Mem:%d\n",len);
			//mem_len = len;
			//memcpy(membuff,tempbuff,mem_len);		//read에 실패 한경우 0을 리턴하도록함. 실패하면 len -1이되면서 memcpy하면 멈춤.
			return 0;
		}
	}
	if(len >= 512 )
	{
		dprintf("[MS %d] Slave Read Too Long [%d]!\n",port_num,len);
		dprintf("\tClear Mem\n");
		mem_len = 0;
		return -1;
	}

	if( mem_len > 0 )
	{
		if((mem_len + len) >= 8 )
		{
			dprintf("\tConcat Mem:%d + Read:%d\n",mem_len,len);
			memcpy(catbuff,membuff,mem_len);
			memcpy(&catbuff[mem_len],tempbuff,len);
			len += mem_len;
			mem_len = 0;
			buff = &catbuff[0];
		}
		else
		{
			dprintf("\t-Add Mem:%d + %d\n",mem_len,len);
			memcpy(&membuff[mem_len],tempbuff,len);
			mem_len += len;
			return 0;
		}
	}
#ifdef MODBUS_TCP
	else
	{
		modbus_mbap_hdr *MBAPHeader_rec;

		MBAPHeader_rec = (modbus_mbap_hdr *)tempbuff;
		trans_id = MBAPHeader_rec->trans_id;
		ptc_id = MBAPHeader_rec->ptc_id;

	
#ifdef DEBUG
		dprintf("[MS %d] Query MBAPHeader: %02X %02X %03X\n",
		port_num, trans_id, ptc_id, htons(MBAPHeader_rec->length));
#endif
  		buff = &tempbuff[sizeof(MBAPHeader_rec[0])];
    }
#else	
	else buff = &tempbuff[0];
#endif		
	if( buff == NULL )
	{
		dprintf("[MS %d] NULL Buff error!\n",port_num);
		return -1;
	}
#ifndef MODBUS_TCP
	crc = CRC16(buff,len-2)&0xFFFF;
	r_crc = (buff[len-2]<<8)|buff[len-1];
	if(crc!=r_crc){
		dprintf("[MS %d] Slave CRC error!\n",port_num);
		return -1;
	}
#endif
	
	preq = (modbus_req_hdr *)buff;
	pres = (modbus_res_hdr *)resbuff;
	pext = (modbus_res_hdr_ext *)writebuff;

	unit_id = preq->unit_id;
	func_code = preq->function_code;
	address = htons(preq->address);
	points = htons(preq->points);
	
	
#ifdef DEBUG
	dprintf("[MS %d] Query: %02X %02X %04X %04X\n",port_num,unit_id,func_code,address,points);
#endif

	if( func_code == 0x03 || func_code == 0x04 || func_code == 0x06 || func_code == 0x10 ) // in case of analog data
	{
		if(address>=20480){
			dprintf("[MS %d] Precheck f - Address [%d]\n",port_num,address);
			modbus_error(out,unit_id,func_code,illegal_address,ps);
			return -1;
		}
		
		if( func_code == 0x03 || func_code == 0x04 || func_code == 0x10 )
		{
			if(points>127){	
				dprintf("[MS %d] Precheck error - Point Number [%d]\n",port_num,points);
				modbus_error(out,unit_id,func_code,illegal_address,ps);
				return -1;
			}
		}
	}
	else if( func_code == 0x01 || func_code == 0x02 ) //in case of digital data
	{
		if(address>=32768){
			dprintf("[MS %d] Precheck error - Address [%d]\n",port_num,address);
			modbus_error(out,unit_id,func_code,illegal_address,ps);
			return -1;
		}
		
		if(points>127*16){
			dprintf("[MS %d] Precheck error - Point Number [%d]\n",port_num,points);
			modbus_error(out,unit_id,func_code,illegal_address,ps);
			return -1;
		}
	}
	else	// etc.
	{
		dprintf("[MS %d] Modbus Slave - Function Code Error!!!\n",port_num);
		modbus_error(out,unit_id,func_code,illegal_function,ps);
		return -1;
	}
	// 0x01, 0x02, 0x03, 0x04라면 메모리 요청을 하고 일정시간 기다렸다가 외부로 전송
	if( func_code == 0x01 || func_code == 0x02 || func_code == 0x03 || func_code == 0x04 )	
	{
		for(i = 0; i < INF_st.num_read_cmd; i++)
		{
			if( (INF_st.read_cmd[i].address <= address) &&
				((INF_st.read_cmd[i].address+INF_st.read_cmd[i].points) > address) &&
				(INF_st.read_cmd[i].function_code == func_code) &&
				(INF_st.read_cmd[i].unit_id == unit_id) )
			{ // check address and area, upload the message for data, response
#ifdef DEBUG
				dprintf("[MS %d] Match read fc: %d address area: %d - %d\n",port_num,
						INF_st.read_cmd[i].function_code,
						INF_st.read_cmd[i].address,
						INF_st.read_cmd[i].address + INF_st.read_cmd[i].points);
#endif
				break;
			}	
			else
			{
#ifdef DEBUG
				dprintf("[MS %d] Not Match read fc: %d address area: %d - %d\n",port_num,
						INF_st.read_cmd[i].function_code,
						INF_st.read_cmd[i].address,
						INF_st.read_cmd[i].address + INF_st.read_cmd[i].points);
#endif
			}
		}
		if(i >= INF_st.num_read_cmd){
			dprintf("[MS %d] Modbus Slave Read - No Match Error!!!\n",port_num);
			modbus_error(out,unit_id,func_code,illegal_address,ps);
			return -1;
		}
		if( (address+points) > (INF_st.read_cmd[i].address+INF_st.read_cmd[i].points) )
		{
			dprintf("[MS %d] Modbus Slave Read - Boundary Error!!!\n",port_num);
			modbus_error(out,unit_id,func_code,illegal_address,ps);
			return -1;
		}			

		pext->unit_id = port_num;
		if( func_code == 0x01 || func_code == 0x02 )
		{
			pext->function_code = 0x0F;
			mapping = ((address-INF_st.read_cmd[i].address)>>4)+INF_st.read_cmd[i].mapping;
			pext->byte_count = (points+7)>>3;
		}
		else
		{
			pext->function_code = 0x10;
			mapping = (address-INF_st.read_cmd[i].address)+INF_st.read_cmd[i].mapping;
			pext->byte_count = points*2;
		}
		pext->address = mapping;
		crc = CRC16((__u8*)pext,6)&0xFFFF;
		writebuff[6] = crc>>8;
		writebuff[7] = crc&0x00FF;

		port_up(in,writebuff,8,port_num);
		
// port_up 하고 기다렸다가 외부로 전송함
		usleep(5*tx_time);	// protection
		if( modbus_write(out,unit_id,func_code,mapping,points,address,ps) < 0 )
		{
			dprintf("[MS %d] Response Error!!!\n",port_num);
			modbus_error(out,unit_id,func_code,illegal_response,ps);
			return -1;
		}
		else update_line_info(ps,TX);

		return 1;
	}
	//0x06이라면 응답을 해줌(내부로)
	else if( func_code == 0x06 )
	{ // upload the message and response
		//dprintf("[MS %d] INF write address: %d\n",port_num,address);
		for(i = 0; i < INF_st.num_write_cmd; i++)
		{
			if( INF_st.write_cmd[i].write_address == address &&
				INF_st.write_cmd[i].write_function_code == func_code &&
				INF_st.write_cmd[i].write_unit_id == unit_id )
			{
#ifdef DEBUG
				dprintf("\tMatch write address: %d\n",INF_st.write_cmd[i].address);
#endif
				break;
			}
			else
			{
#ifdef DEBUG
				dprintf("\tNot Match write address: %d\n",INF_st.write_cmd[i].address);
#endif
			}
		}
		if(i >= INF_st.num_write_cmd){
			dprintf("[MS %d] Modbus Slave Write - No Match Error!!!\n",port_num);
			modbus_error(out,unit_id,func_code,illegal_address,ps);
			return -1;
		}

		pext->unit_id = port_num;
		pext->function_code = 0x03;
		pext->address = INF_st.write_cmd[i].address;
		pext->byte_count = 2;
		if(endian_type == MD_LITTLE_ENDIAN)
			pext->data[0] = points;
		else		
			pext->data[0] = htons(points);
		crc = CRC16((__u8*)pext,8)&0xFFFF;
		writebuff[8] = crc>>8;
		writebuff[9] = crc&0x00FF;

		if( port_up(in,writebuff,10,port_num) > 0 )
		{
			memcpy(pres,preq,8);

#ifndef MODBUS_TCP
			inf_write(out,resbuff,8,PRIMARY);
#else			
			add_mbapheader(resbuff,6);
			inf_write(out,resbuff,12,PRIMARY);
#endif			
			update_line_info(ps,TX);
			return 1;
		}
		else
		{
			dprintf("[MS %d] Data Up Error!!!\n",port_num);
			modbus_error(out,unit_id,func_code,illegal_response,ps);
			return -1;
		}		
	}
	else if( func_code == 0x10 )
	{
		//dprintf("[MS %d] INF write address: %d\n",port_num,address);
		for(i = 0; i < INF_st.num_write_cmd; i++)
		{
			if( INF_st.write_cmd[i].write_data == 0 )
				range = 4;
			else
				range = INF_st.write_cmd[i].write_data;

			if( INF_st.write_cmd[i].write_address == address &&
				INF_st.write_cmd[i].write_function_code == func_code &&
				INF_st.write_cmd[i].write_unit_id == unit_id )
			{
#ifdef DEBUG
				dprintf("\tMatch write address area: %d - %d\n",
						INF_st.write_cmd[i].address,INF_st.write_cmd[i].address+range);
#endif
				break;
			}	
			else
			{
#ifdef DEBUG
				dprintf("\tNot Match write address area: %d - %d\n",
						INF_st.write_cmd[i].address,INF_st.write_cmd[i].address+range);
#endif
			}
		}
		if(i > INF_st.num_write_cmd){
			dprintf("[MS %d] Modbus Slave Write - No Match Error!!!\n",port_num);
			modbus_error(out,unit_id,func_code,illegal_address,ps);
			return -1;
		}
		if( (INF_st.write_cmd[i].address+points) > (INF_st.write_cmd[i].address+range) )
		{
			dprintf("[MS %d] Modbus Slave Write - Boundary Error!!!\n",port_num);
			modbus_error(out,unit_id,func_code,illegal_address,ps);
			return -1;
		}

		pext->unit_id = port_num;
		pext->function_code = 0x3;
		pext->address = INF_st.write_cmd[i].address;
		pext->byte_count = points*2;
		if(endian_type == MD_LITTLE_ENDIAN)
			for(i = 0; i < buff[6] ; i++)
				pext->data[i] = buff[7 + i];
		else			
			memcpy(pext->data,&buff[7],buff[6]);
		crc = CRC16((__u8*)pext,7+buff[6])&0xFFFF;
		writebuff[7+buff[6]] = crc>>8;
		writebuff[8+buff[6]] = crc&0x00FF;

		if( port_up(in,writebuff,9+buff[6],port_num) > 0 )
		{
			memcpy(pres,preq,6);
			crc = CRC16((__u8*)pres,6)&0xFFFF;
			resbuff[6] = crc>>8;
			resbuff[7] = crc&0x00FF;

			i = add_mbapheader(resbuff, 8);
			inf_write(out, resbuff, i, PRIMARY);
			update_line_info(PRIMARY,TX);
			return 1;
			/*if( modbus_write(out,unit_id,func_code,mapping,points,address,ps) < 0 ) {
				dprintf("[MS %d] Data Up Error1!!\n",port_num);
				modbus_error(out,unit_id,func_code,illegal_response,ps);
				return -1;
			}*/
		}
		else
		{
			update_line_info(ps,TX);
			return 1;
		}			
	}
	else
	{
		dprintf("[MS %d] Modbus Slave - Internal Function Code Error!!!\n",port_num);
		modbus_error(out,unit_id,func_code,illegal_response,ps);
		return -1;
	}

	return 1;
  
}

void modbuss_bufm(void *arg)	// added on 27/04/12
{
	int sockfd, len;
	int port_num;
	fd_set readfds,	testfds;
	char tempbuff[DEFAULT_BUFF_SIZE];
	__u8 unit_id, func_code;
	__u16 address, points, byte_count;
	struct timeval timeout;
	modbus_res_hdr_ext *pext;

	port_num = (int)arg;
	Port_Number = port_num;//2016.12.08-23:54,khs
	pthread_detach(pthread_self());

	dprintf(" - Buffer Manager Start [Port: %d]\n",port_num);
	
	sleep(5);

	sockfd = INF_st.sockfd;
	FD_ZERO(&readfds);
	FD_SET(sockfd, &readfds);

	pext = (modbus_res_hdr_ext *)tempbuff;

	while(1)
	{
		testfds = readfds;

		timeout.tv_sec = 20;
		timeout.tv_usec = 0;

		if(select(sockfd+1, &testfds, (fd_set*)0, (fd_set*)0, (struct timeval*)&timeout) != 0){
			
			if( FD_ISSET(sockfd,&testfds) ) {
				len = read(sockfd,tempbuff,DEFAULT_BUFF_SIZE);
				if( len >= 6 && len <= 512 )
				{
					func_code = pext->function_code;
					if( func_code == write_single_reg || func_code == write_mult_regs )
					{
						cmd_handshake(sockfd,port_num,0x06); // cmd_handshake implementation
					}
					
					if( func_code == 0xFF )  // for modbus read response
					{
						unit_id = pext->unit_id;
						address = pext->address;
						byte_count = pext->byte_count;
						points = byte_count/2;

						if( unit_id != port_num )
						{
							dprintf("[MS CMD %d] Precheck error - id [%d]\n",port_num,unit_id);
							continue;
						}

						if(address>=2048)
						{
							dprintf("[MS CMD %d] Precheck error - address [%d]\n",port_num,address);
							continue;
						}

						if((address+points)>2048)
						{
							dprintf("[MS CMD %d] Precheck error - Data Space [%d]-[%d]\n",port_num,
								address,address+points-1);
							continue;
						}

pthread_mutex_lock(&mutex_mem);
						memcpy(&INF_mem[address],pext->data,byte_count);
pthread_mutex_unlock(&mutex_mem);
					}
				}
			}
		}

		usleep(10000);
	}
}

void clientch_thread(void *arg)
{
	fd_set readfds,	testfds;
	struct timeval timeout;
	int tx_time,rx_time;
	int client_fd;
	int fd, sockfd;
	int mode, result;
	
	
	sockfd = INF_st.sockfd;
	client_fd = (int)arg;	
	
	FD_ZERO(&readfds);
	FD_SET(client_fd, &readfds);	
	
	while(1)
	{
		testfds	= readfds;		
	
		timeout.tv_sec = 0;
		timeout.tv_usec = INF_st.timeout*1000000L;		
		
		if(select(FD_SETSIZE, &testfds, (fd_set*)0, (fd_set*)0, (struct timeval*)&timeout) != 0)
		{
			if(FD_ISSET(client_fd,&testfds))
			{	
				mode = PRIMARY;														
				result = handle_modbus_slave(sockfd,client_fd,Port_Number,mode);														
				if(result < 0)
				{					
						FD_CLR(client_fd, &readfds);	
						close(client_fd);	
						client_number--;
						dprintf( "[MS CH] Close : socket discript[%d]\n", client_fd);	
						pthread_exit(0);									
				}			
				if(result > 0)		update_line_info(mode,RX);
			}	
		}
		else
		{
			FD_CLR(client_fd, &readfds);					
			close(client_fd);
			client_number--;
			dprintf( "[MS CH] Timeout : socket discript[%d]\n", client_fd);		
			pthread_exit(0);			
		}		
	}
}


void modbuss_thread(void *arg)
{
	int slavefd,slavefds[2];
	int len, optval=1;
	fd_set readfds,	testfds;
	int fd, sockfd;
	int	nread;
	int port_num, mode, result, retries;
	int switch_no, system_no;
	struct timeval timeout;
	char rx_flag;
	int tx_time,rx_time;
	int num_retry;
#ifdef MODBUS_TCP
	int 	clientsockfd;
	int		i;
	int 	clientAddr_size;	
	struct sockaddr_in clientAddr;
  char   buff_rcv[1024+5];
#endif
	
	port_num = (int)arg;
	pthread_detach(pthread_self());

restart:
#ifdef MODBUS_TCP
	slavefds[0] = open_server(502);		
	//slavefds[0] = open_server(503);		
	INF_st.num_port = 1;
#else
	slavefds[0] = open_link(0);	
#endif
	if( slavefds[0] < 0 ) goto restart;
	if(INF_st.num_port == 2)
	{
		slavefds[1] = open_link(1);
		if( slavefds[1] < 0 )
			INF_st.num_port = 1;
	}

	read_index = 0;
	retries = 0;
	switch_no = 0;
	rx_flag = 1;
	fd_index = 0;
	slavefd = -1;
	sockfd = INF_st.sockfd;
	tx_time = INF_st.tx_time*1000;
	rx_time = INF_st.rx_time*1000;
	
	num_retry = INF_st.num_retry;
	mode = PRIMARY;

	dprintf("Modbus Slave Start [INF: %d]\n",port_num);

	FD_ZERO(&readfds);
	FD_SET(slavefds[0], &readfds);
	if(INF_st.num_port == 2) FD_SET(slavefds[1], &readfds);
		
#ifdef MODBUS_TCP
     if( -1 == listen(slavefds[0], 5))
     {
        exit( 1);
     }
#endif

	nread = 1;
	while(1){
		testfds	= readfds;		
		slavefd = -1;
		result = 0;

		if(rx_flag) usleep(rx_time);	// for concatenation time
		else rx_flag = 1;

		timeout.tv_sec = 0;
		timeout.tv_usec = INF_st.timeout*1000;
				
#ifdef MODBUS_TCP

	if (client_number < LIMIT_CLIENT_NUMBER)
	{
		if(select(FD_SETSIZE, &testfds, (fd_set*)0, (fd_set*)0, (struct timeval*)&timeout) != 0)
		{     
			if( FD_ISSET(slavefds[0],&testfds) ) 
			{	
				ioctl(slavefds[0], FIONREAD,&nread);	//2016.11.25-03:43,khs
				if(nread > 0){		
					clientAddr_size  = sizeof( clientAddr );
					clientsockfd      = accept( slavefds[0], (struct sockaddr*)&clientAddr,
												 &clientAddr_size);
					if ( -1 == clientsockfd)
					{        		
						exit( 1);
					} 
					
					pthread_create(&clientch_thr, NULL, (void *)&clientch_thread,(void *)clientsockfd);	
					client_number++;
					dprintf( "[MS %d] Connected : %d/%d socket discript[%d]\n",port_num,client_number,LIMIT_CLIENT_NUMBER,clientsockfd);	   
					
				}	
			}
			else
			{
			
			}
		} 
	}
#else      
      
		if(select(FD_SETSIZE, &testfds, (fd_set*)0, (fd_set*)0, (struct timeval*)&timeout) != 0){
			
			if( FD_ISSET(slavefds[0],&testfds) ) {
				ioctl(slavefds[0], FIONREAD,&nread);
				if(nread > 0){
					slavefd = slavefds[0];
					mode = PRIMARY;
					result = handle_modbus_slave(sockfd,slavefd,port_num,mode);
				}
			}
			else if( INF_st.num_port == 2)
			{
				if( FD_ISSET(slavefds[1],&testfds) ) {
					ioctl(slavefds[1], FIONREAD,&nread);
					if(nread > 0){
						slavefd = slavefds[1];
						mode = SECONDARY;
						result = handle_modbus_slave(sockfd,slavefd,port_num,mode);
					}
				}
			}
			else result = -1;
		}
		else
		{
			result = -1;
		}

		if( result > 0 )
		{
			line_check(1,sockfd,port_num,ALIVE);
			retries = 0;
			update_line_info(mode,RX);
		}
		else if( result < 0 )
		{
			retries++;
			line_check(1,sockfd,port_num,BAD);

			if( retries >= num_retry ) retries = 0;
		}
		else
		{
			rx_flag = 0;
		}

		usleep(tx_time);	// protection
#endif
	}
	//close(slavefds[0]);
	//if(INF_st.num_port == 2) close(slavefds[1]);
}

