#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#include <asm/types.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <termios.h>

#include "common.h"

#include "app.h"
#include "interface.h"
#include "modbus.h"
#include "nmea.h"
#include "utils.h"
#include "log.h"


//#define	DEBUG
#ifndef	DEBUG
	#define	dprintf(format, args...) log_access(format, ## args)
#else
	#define	dprintf(format, args...) log_access(format, ## args); printf(format, ## args)
#endif	// DEBUG

//#define		WRITE_DEBUG

#define BASE 			3000

#define DEBUG_INF

extern int inf_tx_cnt, inf_rx_cnt, inf_thr_cnt;
extern __u8 inf_alive;

extern INF_ST INF_st;
extern NMEA_DEF NMEA_def[MAX_NMEA_DEFINE];

extern int fd_index, read_index;
extern char sendbuff[DEFAULT_BUFF_SIZE];
extern char recvbuff[DEFAULT_BUFF_SIZE];


static int _open_link(int port)
{
	return -1;
}

typedef struct {
	    unsigned short a;
		unsigned short b;
		unsigned short c;
} retType;

extern retType my_get_nmea_utc(char *s);
extern retType my_get_nmea_latitude(char *s);
extern retType my_get_nmea_longitude(char *s);


#ifdef ORG
static int nmea_read(int in, int out,int port_num)
{
	char buff[DEFAULT_BUFF_SIZE];
	char rzbuff[DEFAULT_BUFF_SIZE];
	char tmpbuff[DEFAULT_BUFF_SIZE];
	char writebuff[DEFAULT_BUFF_SIZE];
	char str[64], tempstr[24];

	retType ret;
	int i, j, len, slen;
	__u8 index1, index2, index3, type;
	__u8 chksum, rchksum;
	__u8 result, value;
	char c, *p;
	char format[10], talker[10];
	modbus_res_hdr_ext *pext;
	__u16 address, byte_count, crc;
	__u8 data[16];
	int index = 0;
	int length, start = -1, end = -1;
	int temp1, temp3;
	float temp2;
	unsigned short myTemp1,myTemp2;

	int argc;
	char *argv[64];

	struct sockaddr_in bypass;

	len = inf_read(in,buff,DEFAULT_BUFF_SIZE-16,PRIMARY);
	if(len <= 0 ) return -1;
	if(len > DEFAULT_BUFF_SIZE-16 ) return -1;
	buff[len] = 0;

#if 0
	dprintf("\n<======= %s\n",buff);
#endif

	while(index < len)
	{
		if(buff[index] == '$')
			start = index;

		if(buff[index] == '\n' && start >= 0 )
			end = index;

		if( start < end )
		{
			length = end-start+1;
			memcpy(rzbuff,&buff[start],length);
			rzbuff[length] = 0;

			start = end = -1;

			if(rzbuff[0] != '$' || rzbuff[length-1] != '\n')
			{
				dprintf("[NMEA %d] NMEA Read - Sentence Error[1]!\n",port_num);
				continue;
			}

#ifdef DEBUG
	printf("\n<== %s\n",rzbuff);
#endif

			chksum = 0;
			for( i = 1 ; i < length-2 ; i++ )
			{
				if(rzbuff[i] == '*') break;
				chksum ^= rzbuff[i];
			}
			if(i >= length-2)
			{
				dprintf("[NMEA %d] NMEA Read - Sentence Error[2]!\n",port_num);
				continue;
			}

			rchksum = hexstr2num(&rzbuff[i+1],2);
			if(chksum != rchksum)
			{
				dprintf("[NMEA %d] NMEA Checksum Error: %02x -> %02x\n",port_num,rchksum,chksum);
				continue;
			}

//#ifdef DEBUG
//	printf("\n<== %s\n",rzbuff);
//#endif
			memcpy(tmpbuff,rzbuff,DEFAULT_BUFF_SIZE);
			argc = get_args(&tmpbuff[1],argv);

			slen = strlen(argv[0]);
			if( slen < 3 ) continue;
			strncpy(format,&argv[0][slen-3],3);
			strncpy(talker,&argv[0][0],slen-3);
			format[3] = 0;
			talker[slen-3] = 0;

#ifdef DEBUG
	dprintf("[NMEA %d]	Format: %s Talker: %s\n",port_num,format,talker);
#endif

			for(i = 0; i < INF_st.num_read_cmd; i++)
			{
				if(INF_st.nmea_read[i].option == OPT_SECOND_FORMAT )
				{
					strcpy(format,&argv[1][0]);
					strcpy(talker,&argv[0][0]);
#ifdef DEBUG
	dprintf("[NMEA %d]	____Format: %s Talker: %s (by Option %s)\n",port_num,format,talker,"OPT_SECOND_FORMAT");
#endif
				}

				if(INF_st.nmea_read[i].option == OPT_REDIRECT3 )
				{
					if(strcmp(&argv[3][0],"A") == 0) strcpy(format,"RD1");
					if(strcmp(&argv[3][0],"B") == 0) strcpy(format,"RD2");
					if(strcmp(&argv[3][0],"C") == 0) strcpy(format,"RD3");
					if(strcmp(&argv[3][0],"D") == 0) strcpy(format,"RD4");
#ifdef DEBUG
	dprintf("[NMEA %d]	____Format: %s Talker: %s (by Option %s)\n",port_num,format,talker,"OPT_REDIRECT3");
#endif
				}

				if(strcmp(format,INF_st.nmea_read[i].format) == 0)
				{
#ifdef DEBUG
	dprintf("[NMEA %d]	The format [%s] is accepted!\n",port_num,format);
#endif
					break;
				}
			}
			if(i >= INF_st.num_read_cmd){
				dprintf("[NMEA %d] NMEA Read - '%s' No Definition Error[1]!!!\n",port_num, format);
				continue;
			}
			index1 = i;

			if( INF_st.nmea_read[index1].option == OPT_BYPASS ) // Bypass
			{
				memset(&bypass,0,sizeof(bypass));
				bypass.sin_family=AF_INET;
				bypass.sin_addr.s_addr=inet_addr(INF_st.bypass.ip);
				bypass.sin_port = htons(INF_st.bypass.port);

				if(sendto(out,rzbuff,length,0,(struct sockaddr *)&bypass,sizeof(bypass))
					== length)
					result = 1;
				else
					result = 0;
#ifdef DEBUG
				if( result ) { dprintf("[NMEA %d]   BYPASS: OK\n",port_num); }
				else { dprintf("[NMEA %d]   BYPASS: Error!\n",port_num); }
#endif
			}

			for(i = 0 ; i < MAX_NMEA_DEFINE; i++)
			{
				if( NMEA_def[i].o )
				{
					if(strcmp(format,NMEA_def[i].format) == 0)
					{
#ifdef DEBUG
	dprintf("[NMEA %d]	The format [%s] is defined!!\n",port_num,format);
#endif
						break;
					}
				}
			}
			if( i >= MAX_NMEA_DEFINE )
			{
				dprintf("[NMEA %d] NMEA READ - No Definition Error[2]!!!\n",port_num);
				continue;
			}
			index2 = i;

			for(i = 0; i < 15; i++)
			{
				index3 = INF_st.nmea_read[index1].field[i].position;

				if( index3 != 0 )
				{
					address = INF_st.nmea_read[index1].field[i].address;
					type = NMEA_def[index2].field[index3-1].type;

					switch(type)
					{
					case TYPE_LAT:
						
						byte_count = 6;
						ret = my_get_nmea_latitude(argv[index3]);	
						myTemp1 = htons(ret.a);
						memcpy(&data[0],&myTemp1,2);
						myTemp1 = htons(ret.b);
						memcpy(&data[2],&myTemp1,2);
						myTemp1 = htons(ret.c);
						memcpy(&data[4],&myTemp1,2);
						/*
						byte_count = 4;
						temp2 = get_nmea_latitude(argv[index3],argv[index3+1]);
						memcpy(&temp1,&temp2,byte_count);
						temp3 = htonl(temp1);
					 	memcpy(data,&temp3,byte_count);
					//	memcpy(data,&temp2,byte_count);
					    */
						break;
					case TYPE_LONG: 
						/*
						byte_count = 4;
						temp2 = get_nmea_longitude(argv[index3],argv[index3+1]);
						memcpy(&temp1,&temp2,byte_count);
						temp3 = htonl(temp1);
					 	memcpy(data,&temp3,byte_count);
						*/
						byte_count = 6;
						ret = my_get_nmea_longitude(argv[index3]);
						myTemp1 = htons(ret.a);
						memcpy(&data[0],&myTemp1,2);
						myTemp1 = htons(ret.b);
						memcpy(&data[2],&myTemp1,2);
						myTemp1 = htons(ret.c);
						memcpy(&data[4],&myTemp1,2);
						break;
					case TYPE_UTC:					
						byte_count = 6;
						ret = my_get_nmea_utc(argv[index3]);
						myTemp1 = htons(ret.a);
						memcpy(&data[0],&myTemp1,2);
						myTemp1 = htons(ret.b);
						memcpy(&data[2],&myTemp1,2);
						myTemp1 = htons(ret.c);
						memcpy(&data[4],&myTemp1,2);						
						/*
						byte_count = 4;
						temp2 = decstr2float(argv[index3],strlen(argv[index3]));
						memcpy(&temp1,&temp2,byte_count);
						temp3 = htonl(temp1);
					 	memcpy(data,&temp3,byte_count);
						*/
						break;
					case TYPE_STATUS: byte_count = 1;
						memcpy(data,&argv[index3][0],byte_count);
						break;
					case TYPE_DEFINED: byte_count = 1;
						memcpy(data,&NMEA_def[index2].field[index3].c,byte_count);
						break;
					case TYPE_NUM_VAR: byte_count = 2;
						temp2 = decstr2float(argv[index3],strlen(argv[index3]));
						myTemp1 = temp2 * 10;

						myTemp2 = htons(myTemp1);
						memcpy(data,&myTemp2,2);

						//memcpy(&temp1,&temp2,byte_count);
						//temp3 = htonl(temp1);
					 	//memcpy(data,&temp3,byte_count);
						break;
					case TYPE_HEX_VAR: byte_count = 2;
						temp1 = hexstr2num(argv[index3],strlen(argv[index3]));
						temp3 = htons(temp1);
						memcpy(data,&temp3,byte_count);
						break;
				//	case TYPE_TEXT_VAR: break;
					case TYPE_ALPHA: 
						byte_count = 1;
						switch(argv[index3][0])
						{
						case 'S': value = 1; break;
						case 'N': value = 0; break;
						case 'E': value = 0; break;
						case 'W': value = 1; break;
						case 'A': value = 0; break;
						case 'V': value = 1; break;
						case 'R': value = 0; break;
						case 'T': value = 1; break;
						case 'K': value = 2; break;
						case 'M': value = 1; break;
						}
						memcpy(data,&value,byte_count);
						//memcpy(data,&argv[index3][0],byte_count);
						break;
					case TYPE_NUM_1: 
					case TYPE_NUM_2: byte_count = 2;
						temp1 = decstr2num(argv[index3],strlen(argv[index3]));
						temp3 = htons(temp1);
						memcpy(data,&temp3,byte_count);
						break;
				//	case TYPE_TEXT_2: break;
					default:
						byte_count = 0;
						break;
					}

					if( byte_count > 0 )
					{
						pext = (modbus_res_hdr_ext *)writebuff;
						pext->unit_id = port_num;
						pext->function_code = 0x03;
						pext->address = address;
						pext->byte_count = byte_count;
						memcpy(pext->data,data,byte_count);
						crc = CRC16((__u8*)pext,6+byte_count)&0xFFFF;
						writebuff[6+byte_count] = crc>>8;
						writebuff[7+byte_count] = crc&0x00FF;
	
						port_up(out,writebuff,8+byte_count,port_num);
//#ifdef DEBUG
						sprintf(str,"[NMEA %d] DATA UP [ADDR:%04X ",port_num,address);
						if( byte_count > 2 ) sprintf(tempstr,"DATA:%.2f] ",temp2);
						else sprintf(tempstr,"DATA:%d] ",temp1);
						strcat(str,tempstr);
						for( j = 0 ; j < 8+byte_count; j++)
						{
							sprintf(tempstr,"|%02X",writebuff[j]);
							strcat(str,tempstr);
						}	
						dprintf("%s|\n",str);
//#endif
					}
				}
			}
		}

		index++;
	}

	return 1;
}
#else
static int nmea_read(int in, int out,int port_num)
{
	char buff[DEFAULT_BUFF_SIZE];
	char rzbuff[DEFAULT_BUFF_SIZE];
	char tmpbuff[DEFAULT_BUFF_SIZE];
	char writebuff[DEFAULT_BUFF_SIZE];
	char str[64], tempstr[24];

	retType ret;
	int i, j, len, slen;
	__u8 index1, index2, index3, type;
	__u8 chksum, rchksum;
	__u8 result, value;
	char c, *p;
	char format[10], talker[10];
	modbus_res_hdr_ext *pext;
	__u16 address, byte_count, crc;
	__u8 data[16];
	int index = 0;
	int length, start = -1, end = -1;
	int temp1, temp3;
	float temp2;
	unsigned short myTemp1,myTemp2;

	int argc;
	char *argv[64];
	char ch;

	struct sockaddr_in bypass;
/*
	len = inf_read(in,buff,DEFAULT_BUFF_SIZE-16,PRIMARY);
	if(len <= 0 ) return -1;
	if(len > DEFAULT_BUFF_SIZE-16 ) return -1;
	buff[len] = 0;
*/

	len = 0;
	while(1){
		slen = read(in, &ch, 1);
		if(slen <= 0){
			return 0;
		}
		if(ch=='$')	len = 0;	// Start NMEA
		if(ch=='\n'){			// End NMEA
			buff[len++] = ch;
			if(len>=DEFAULT_BUFF_SIZE)	len = DEFAULT_BUFF_SIZE-1;
			break;
		} else{
			if(len<DEFAULT_BUFF_SIZE)	buff[len++] = ch;
		}
	}

#ifdef DEBUG_INF
	if(INF_st.debugfd.valid)
	{
		index = fd_index;

		write(INF_st.debugfd.rxfd[index],buff,len);
	}
#endif


#if 0
	dprintf("\n<======= %s\n",buff);
#endif

	while(index < len)
	{
		if(buff[index] == '$')
			start = index;

		if(buff[index] == '\n' && start >= 0 )
			end = index;

		if( start < end )
		{
			length = end-start+1;
			memcpy(rzbuff,&buff[start],length);
			rzbuff[length] = 0;

			start = end = -1;

			if(rzbuff[0] != '$' || rzbuff[length-1] != '\n')
			{
				dprintf("[NMEA %d] NMEA Read - Sentence Error[1]!\n",port_num);
				continue;
			}

#ifdef DEBUG
	printf("\n<== %s\n",rzbuff);
#endif

			chksum = 0;
			for( i = 1 ; i < length-2 ; i++ )
			{
				if(rzbuff[i] == '*') break;
				chksum ^= rzbuff[i];
			}
			if(i >= length-2)
			{
				dprintf("[NMEA %d] NMEA Read - Sentence Error[2]!\n",port_num);
				continue;
			}

			rchksum = hexstr2num(&rzbuff[i+1],2);
			if(chksum != rchksum)
			{
				dprintf("[NMEA %d] NMEA Checksum Error: %02x -> %02x\n",port_num,rchksum,chksum);
				continue;
			}

//#ifdef DEBUG
//	printf("\n<== %s\n",rzbuff);
//#endif
			memcpy(tmpbuff,rzbuff,DEFAULT_BUFF_SIZE);
			argc = get_args(&tmpbuff[1],argv);

			slen = strlen(argv[0]);
			if( slen < 3 ) continue;
			strncpy(format,&argv[0][slen-3],3);
			strncpy(talker,&argv[0][0],slen-3);
			format[3] = 0;
			talker[slen-3] = 0;

#ifdef DEBUG
	dprintf("[NMEA %d]	Format: %s Talker: %s\n",port_num,format,talker);
#endif

			for(i = 0; i < INF_st.num_read_cmd; i++)
			{
				if(INF_st.nmea_read[i].option == OPT_SECOND_FORMAT )
				{
					strcpy(format,&argv[1][0]);
					strcpy(talker,&argv[0][0]);
#ifdef DEBUG
	dprintf("[NMEA %d]	____Format: %s Talker: %s (by Option %s)\n",port_num,format,talker,"OPT_SECOND_FORMAT");
#endif
				}

				if(INF_st.nmea_read[i].option == OPT_REDIRECT3 )
				{
					if(strcmp(&argv[3][0],"A") == 0) strcpy(format,"RD1");
					if(strcmp(&argv[3][0],"B") == 0) strcpy(format,"RD2");
					if(strcmp(&argv[3][0],"C") == 0) strcpy(format,"RD3");
					if(strcmp(&argv[3][0],"D") == 0) strcpy(format,"RD4");
#ifdef DEBUG
	dprintf("[NMEA %d]	____Format: %s Talker: %s (by Option %s)\n",port_num,format,talker,"OPT_REDIRECT3");
#endif
				}

				if(strcmp(format,INF_st.nmea_read[i].format) == 0)
				{
#ifdef DEBUG
	dprintf("[NMEA %d]	The format [%s] is accepted!\n",port_num,format);
#endif
					break;
				}
			}
			if(i >= INF_st.num_read_cmd){
				dprintf("[NMEA %d] NMEA Read - '%s' No Definition Error[1]!!!\n",port_num, format);
				continue;
			}
			index1 = i;

			if( INF_st.nmea_read[index1].option == OPT_BYPASS ) // Bypass
			{
				memset(&bypass,0,sizeof(bypass));
				bypass.sin_family=AF_INET;
				bypass.sin_addr.s_addr=inet_addr(INF_st.bypass.ip);
				bypass.sin_port = htons(INF_st.bypass.port);

				if(sendto(out,rzbuff,length,0,(struct sockaddr *)&bypass,sizeof(bypass))
					== length)
					result = 1;
				else
					result = 0;
#ifdef DEBUG
				if( result ) { dprintf("[NMEA %d]   BYPASS: OK\n",port_num); }
				else { dprintf("[NMEA %d]   BYPASS: Error!\n",port_num); }
#endif
			}

			for(i = 0 ; i < MAX_NMEA_DEFINE; i++)
			{
				if( NMEA_def[i].o )
				{
					if(strcmp(format,NMEA_def[i].format) == 0)
					{
#ifdef DEBUG
	dprintf("[NMEA %d]	The format [%s] is defined!!\n",port_num,format);
#endif
						break;
					}
				}
			}
			if( i >= MAX_NMEA_DEFINE )
			{
				dprintf("[NMEA %d] NMEA READ - No Definition Error[2]!!!\n",port_num);
				continue;
			}
			index2 = i;

			for(i = 0; i < 15; i++)
			{
				index3 = INF_st.nmea_read[index1].field[i].position;

				if( index3 != 0 )
				{
					address = INF_st.nmea_read[index1].field[i].address;
					type = NMEA_def[index2].field[index3-1].type;

					switch(type)
					{
					case TYPE_LAT:

						byte_count = 6;
						ret = my_get_nmea_latitude(argv[index3]);
						myTemp1 = htons(ret.a);
						memcpy(&data[0],&myTemp1,2);
						myTemp1 = htons(ret.b);
						memcpy(&data[2],&myTemp1,2);
						myTemp1 = htons(ret.c);
						memcpy(&data[4],&myTemp1,2);
						/*
						byte_count = 4;
						temp2 = get_nmea_latitude(argv[index3],argv[index3+1]);
						memcpy(&temp1,&temp2,byte_count);
						temp3 = htonl(temp1);
					 	memcpy(data,&temp3,byte_count);
					//	memcpy(data,&temp2,byte_count);
					    */
						break;
					case TYPE_LONG:
						/*
						byte_count = 4;
						temp2 = get_nmea_longitude(argv[index3],argv[index3+1]);
						memcpy(&temp1,&temp2,byte_count);
						temp3 = htonl(temp1);
					 	memcpy(data,&temp3,byte_count);
						*/
						byte_count = 6;
						ret = my_get_nmea_longitude(argv[index3]);
						myTemp1 = htons(ret.a);
						memcpy(&data[0],&myTemp1,2);
						myTemp1 = htons(ret.b);
						memcpy(&data[2],&myTemp1,2);
						myTemp1 = htons(ret.c);
						memcpy(&data[4],&myTemp1,2);
						break;
					case TYPE_UTC:
						byte_count = 6;
						ret = my_get_nmea_utc(argv[index3]);
						myTemp1 = htons(ret.a);
						memcpy(&data[0],&myTemp1,2);
						myTemp1 = htons(ret.b);
						memcpy(&data[2],&myTemp1,2);
						myTemp1 = htons(ret.c);
						memcpy(&data[4],&myTemp1,2);
						/*
						byte_count = 4;
						temp2 = decstr2float(argv[index3],strlen(argv[index3]));
						memcpy(&temp1,&temp2,byte_count);
						temp3 = htonl(temp1);
					 	memcpy(data,&temp3,byte_count);
						*/
						break;
					case TYPE_STATUS: byte_count = 1;
						memcpy(data,&argv[index3][0],byte_count);
						break;
					case TYPE_DEFINED: byte_count = 1;
						memcpy(data,&NMEA_def[index2].field[index3].c,byte_count);
						break;
					case TYPE_NUM_VAR: byte_count = 2;
						temp2 = decstr2float(argv[index3],strlen(argv[index3]));
						myTemp1 = temp2 * 10;

						myTemp2 = htons(myTemp1);
						memcpy(data,&myTemp2,2);

						//memcpy(&temp1,&temp2,byte_count);
						//temp3 = htonl(temp1);
					 	//memcpy(data,&temp3,byte_count);
						break;
					case TYPE_HEX_VAR: byte_count = 2;
						temp1 = hexstr2num(argv[index3],strlen(argv[index3]));
						temp3 = htons(temp1);
						memcpy(data,&temp3,byte_count);
						break;
				//	case TYPE_TEXT_VAR: break;
					case TYPE_ALPHA:
						byte_count = 1;
						switch(argv[index3][0])
						{
						case 'S': value = 1; break;
						case 'N': value = 0; break;
						case 'E': value = 0; break;
						case 'W': value = 1; break;
						case 'A': value = 0; break;
						case 'V': value = 1; break;
						case 'R': value = 0; break;
						case 'T': value = 1; break;
						case 'K': value = 2; break;
						case 'M': value = 1; break;
						}
						memcpy(data,&value,byte_count);
						//memcpy(data,&argv[index3][0],byte_count);
						break;
					case TYPE_NUM_1:
					case TYPE_NUM_2: byte_count = 2;
						temp1 = decstr2num(argv[index3],strlen(argv[index3]));
						temp3 = htons(temp1);
						memcpy(data,&temp3,byte_count);
						break;
				//	case TYPE_TEXT_2: break;
					default:
						byte_count = 0;
						break;
					}

					if( byte_count > 0 )
					{
						pext = (modbus_res_hdr_ext *)writebuff;
						pext->unit_id = port_num;
						pext->function_code = 0x03;
						pext->address = address;
						pext->byte_count = byte_count;
						memcpy(pext->data,data,byte_count);
						crc = CRC16((__u8*)pext,6+byte_count)&0xFFFF;
						writebuff[6+byte_count] = crc>>8;
						writebuff[7+byte_count] = crc&0x00FF;

						port_up(out,writebuff,8+byte_count,port_num);
//#ifdef DEBUG
						sprintf(str,"[NMEA %d] DATA UP [ADDR:%04X ",port_num,address);
						if( byte_count > 2 ) sprintf(tempstr,"DATA:%.2f] ",temp2);
						else sprintf(tempstr,"DATA:%d] ",temp1);
						strcat(str,tempstr);
						for( j = 0 ; j < 8+byte_count; j++)
						{
							sprintf(tempstr,"|%02X",writebuff[j]);
							strcat(str,tempstr);
						}
						dprintf("%s|\n",str);
//#endif
					}
				}
			}
		}

		index++;
	}

	return 1;
}
#endif

void nmeas_thread(void *arg)
{
	int slavefd,slavefds[2];
	int len, optval=1;
	fd_set readfds,	testfds;
	int fd, sockfd;
	int	nread;
	int port_num, mode, result, retries;
	int switch_no, system_no;
	struct timeval timeout;
	char write_flag;
	int tx_time,rx_time, timeout_time;
	int num_retry;

	port_num = (int)arg;
	pthread_detach(pthread_self());

restart:
	slavefds[0] = open_link(0);
	if( slavefds[0] < 0 ) goto restart;
	if(INF_st.num_port == 2)
	{
		slavefds[1] = open_link(1);
		if( slavefds[1] < 0 )
			INF_st.num_port = 1;
	}

	read_index = 0;
	retries = 0;
	switch_no = 0;
	write_flag = 0;
	fd_index = 0;
	slavefd = slavefds[fd_index];
	sockfd = INF_st.sockfd;
	tx_time = INF_st.tx_time*1000;
	rx_time = INF_st.rx_time*1000;
	timeout_time = INF_st.timeout*1000;
	num_retry = INF_st.num_retry;
	mode = PERIODIC_REQ;

	dprintf("NMEA Slave Start [INF: %d]\n",port_num);
	FD_ZERO(&readfds);
//	FD_SET(sockfd, &readfds);
	FD_SET(slavefds[0], &readfds);
	if(INF_st.num_port == 2) FD_SET(slavefds[1], &readfds);

	while(1){
		testfds	= readfds;

		usleep(rx_time);
		
		result = 0;

		timeout.tv_sec = 0;
		timeout.tv_usec = timeout_time;

		if(select(FD_SETSIZE, &testfds, (fd_set*)0, (fd_set*)0, (struct timeval*)&timeout) != 0){

			if( FD_ISSET(slavefds[0],&testfds) ) {
				ioctl(slavefds[0], FIONREAD,&nread);
				if(nread > 0){
			//		usleep(tx_time);
					slavefd = slavefds[0];
					result = nmea_read(slavefd,sockfd,port_num);
				}
			}
			
			if( INF_st.num_port == 2)
			{
				if( FD_ISSET(slavefds[1],&testfds) ) {
					ioctl(slavefds[1], FIONREAD,&nread);
					if(nread > 0){
			//			usleep(tx_time);
						slavefd = slavefds[1];
						result = nmea_read(slavefd,sockfd,port_num);
					}
				}
			}
		}

		if( result > 0 )
		{
			line_check(0,sockfd,port_num,ALIVE);
			retries = 0;
			update_line_info(PRIMARY,RX);
		}
		else
		{
			retries++;
			line_check(0,sockfd,port_num,BAD);

			if( retries >= num_retry )
			{
//				line_check(0,sockfd,port_num,DEAD);
				retries = 0;
			}
		}
	}

	close(slavefds[0]);
	if(INF_st.num_port == 2) close(slavefds[1]);
}

