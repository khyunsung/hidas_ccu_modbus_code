#ifndef	_MODBUS_H
#define	_MODBUS_H


/* Modbus protocol function codes */
#define read_coils				1
#define read_input_discretes	2
#define read_mult_regs			3
#define read_input_regs			4
#define write_coil				5
#define write_single_reg		6
#define read_except_stat		7
#define diagnostics				8
#define program_484				9
#define poll_484				10
#define get_comm_event_ctrs		11
#define get_comm_event_log		12
#define program_584_984			13
#define poll_584_984			14
#define force_mult_coils		15
#define write_mult_regs			16
#define report_slave_id			17
#define program_884_u84			18
#define reset_comm_link			19
#define read_genl_ref			20
#define write_genl_ref			21
#define mask_write_reg			22
#define read_write_reg			23
#define read_fifo_queue			24
#define program_ConCept			40
#define firmware_replace		125
#define program_584_984_2		126
#define report_local_addr_mb	127

/* Modbus protocol exception codes */
#define illegal_function		0x01
#define illegal_address			0x02
#define illegal_value			0x03
#define illegal_response		0x04
#define acknowledge				0x05
#define slave_busy				0x06
#define negative_ack			0x07
#define memory_err				0x08
#define gateway_unavailable		0x0a
#define gateway_trgt_fail		0x0b

/* return codes of function classifying packets as query/response */
#define query_packet			0
#define response_packet			1
#define cannot_classify			2

#pragma pack(1)
/* Modbus header */
typedef struct _modbus_req_hdr {
	__u8		unit_id;
	__u8		function_code;
	__u16		address;
	__u16		points;
} modbus_req_hdr;

typedef struct _modbus_res_hdr {
	__u8		unit_id;
	__u8		function_code;
	__u8		byte_count;
	__u16		data[128];
} modbus_res_hdr;

typedef struct _modbus_mbap_hdr {
	__u16		trans_id;
	__u16		ptc_id;
	__u16		length;
} modbus_mbap_hdr;

typedef struct _modbus_wreq_hdr {
	__u8		unit_id;
	__u8		function_code;
	__u16		address;
	__u16		points;
	__u8		byte_count;
	__u16		data[512];
} modbus_wreq_hdr;

#if 0
typedef struct _modbus_res_hdr_ext {
	__u8		unit_id;
	__u16		address;
	__u16		byte_count;
	__u16		data[512];
} modbus_res_hdr_ext;
#endif
#pragma pack ()

#endif		// _MODBUS_H
