#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#include <asm/types.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <termios.h>

#include "common.h"

#include "app.h"
#include "interface.h"
#include "modbus.h"
#include "nmea.h"
#include "utils.h"
#include "log.h"

//#define	DEBUG
#ifndef	DEBUG
	#define	dprintf(format, args...) log_access(format, ## args)
#else
	#define	dprintf(format, args...) log_access(format, ## args); printf(format, ## args)
#endif	// DEBUG

//#define		WRITE_DEBUG

#define BASE 			3000

#define DEBUG_INF

extern int inf_tx_cnt, inf_rx_cnt, inf_thr_cnt;
extern __u8 inf_alive;

extern INF_ST INF_st;
extern NMEA_DEF NMEA_def[MAX_NMEA_DEFINE];

extern pthread_mutex_t mutex_mem;

extern int fd_index, read_index;
extern unsigned short INF_mem[MAX_INF_TAG];
extern char sendbuff[DEFAULT_BUFF_SIZE];
extern char recvbuff[DEFAULT_BUFF_SIZE];

extern Inf_sts_cmd_r_bit Inf_s_cmd_b;
extern int Comm_Open_Err;

static int _open_link(int port)
{
	return -1;
}

static int nmea_write(int in, int out, int port_num, int mode)
{
	char buff[DEFAULT_BUFF_SIZE];
	char writebuff[DEFAULT_BUFF_SIZE];
	char str[64],tempstr[8];

	int i, j, k, len;
	__u8 index1, index2, index3, type;
	__u8 chksum;
	__u8 *pbyte;
	char format[5], talker[5], tail[12];
	modbus_res_hdr_ext *pext;
	__u16 address, byte_count, crc;
	int index = 0;
	int length;
	__u16 temp1;
	char temp1c;
	float temp2;
	int temp3;
	fd_set fds;
	struct timeval timeout;
	char ok = 0;
	__u16 mapping;
    
	int argc;
	char argv[32][12];

// LGE
	cmd_reg_check(sockfd_cmd);
	if(Inf_s_cmd_b.bit.tx_valid != 3) return 1;
// --

	modbus_res_hdr_ext *wres = (modbus_res_hdr_ext *)recvbuff;
	pext = (modbus_res_hdr_ext *)writebuff;
	
	for( i = 0 ; i < INF_st.num_write_cmd ; i++ )
	{
		strncpy(format,INF_st.nmea_write[i].format,5);
		strncpy(talker,INF_st.nmea_write[i].talker,5);

		//
		pext->unit_id = port_num;
		pext->function_code = 0x10;
		mapping = (INF_st.nmea_write[i].field[0].position / 128 ) * 128;
		pext->byte_count = 128;//;
		pext->address = mapping;
		crc = CRC16((__u8*)pext,6)&0xFFFF;
		writebuff[6] = crc>>8;
		writebuff[7] = crc&0x00FF;
		port_up(in,writebuff,8,port_num);
		
		for(j = 0 ; j < MAX_NMEA_DEFINE; j++)
		{
			if( NMEA_def[j].o )
			{
				if(strcmp(format,NMEA_def[j].format) == 0)
				{
#ifdef DEBUG
	dprintf("[NMEAW %d]	The format [%s] is defined!!\n",port_num,format);
#endif
					break;
				}
			}
		}
		if( j >= MAX_NMEA_DEFINE )
		{
			dprintf("[NMEAW %d] NMEA READ - No Definition Error[2]!!!\n",port_num);
			continue;
		}
		index2 = j;

		argc = NMEA_def[index2].max+1;
		for(j = 0; j < argc ; j++ )
			memset(&argv[j][0],0,12); // argv[j][0] = 0;

		sprintf(argv[0],"$%s%s",talker,format);

		for(j = 0; j < MAX_NO_FIELD; j++)
		{
			index3 = INF_st.nmea_write[i].field[j].position;

			if( index3 != 0 )
			{
				address = INF_st.nmea_write[i].field[j].address;
				type = NMEA_def[index2].field[index3-1].type;

				switch(type)
				{
				case TYPE_LAT:      byte_count = 4;	break;
				case TYPE_LONG:     byte_count = 4; break;
				case TYPE_UTC:	    byte_count = 4; break;
				case TYPE_STATUS:   byte_count = 1; break;
				case TYPE_DEFINED:  byte_count = 1; break;
				case TYPE_NUM_VAR:  byte_count = 4; break;
				case TYPE_HEX_VAR:  byte_count = 2; break;
				case TYPE_TEXT_VAR: byte_count = 0; break;
				case TYPE_ALPHA:    byte_count = 1; break;
				case TYPE_NUM_1: 
				case TYPE_NUM_2:    byte_count = 2; break;
				case TYPE_TEXT_2:   byte_count = 0; break;
				default: byte_count = 0; break;
				}

				if( byte_count > 0 )
				{
					switch(type)
					{
					case TYPE_LAT:
pthread_mutex_lock(&mutex_mem);
						memcpy(&temp3,&INF_mem[address],4);
pthread_mutex_unlock(&mutex_mem);
						temp3 = htonl(temp3);
						memcpy(&temp2,&temp3,4);

						if( temp2 >= 0 )
						{
							sprintf(argv[index3],"%07.2f",temp2);
							argv[index3+1][0] = 'N';
						}
						else
						{
							sprintf(argv[index3],"%07.2f",-temp2);
							argv[index3+1][0] = 'S';
						}
						argv[index3+1][1] = 0;
						break;
					case TYPE_LONG:
pthread_mutex_lock(&mutex_mem);
						memcpy(&temp3,&INF_mem[address],4);
pthread_mutex_unlock(&mutex_mem);
						temp3 = htonl(temp3);
						memcpy(&temp2,&temp3,4);

						if( temp2 >= 0 )
						{
							sprintf(argv[index3],"%08.2f",temp2);
							argv[index3+1][0] = 'E';
						}
						else
						{
							sprintf(argv[index3],"%08.2f",-temp2);
							argv[index3+1][0] = 'W';
						}
						argv[index3+1][1] = 0;
						break;
					case TYPE_UTC:
pthread_mutex_lock(&mutex_mem);
						memcpy(&temp3,&INF_mem[address],4);
pthread_mutex_unlock(&mutex_mem);
						temp3 = htonl(temp3);
						memcpy(&temp2,&temp3,4);
						sprintf(argv[index3],"%09.2f",temp2);
						break;
					case TYPE_STATUS: break;
					case TYPE_DEFINED: break;
					case TYPE_NUM_VAR:
pthread_mutex_lock(&mutex_mem);
						memcpy(&temp3,&INF_mem[address],4);
pthread_mutex_unlock(&mutex_mem);
						temp3 = htonl(temp3);
						memcpy(&temp2,&temp3,4);
						sprintf(argv[index3],"%.1f",temp2);
						break;
					case TYPE_HEX_VAR:
pthread_mutex_lock(&mutex_mem);
						memcpy(&temp1,&INF_mem[address],2);
pthread_mutex_unlock(&mutex_mem);
						temp1 = htons(temp1);
						sprintf(argv[index3],"%x",temp1);
						break;
				//	case TYPE_TEXT_VAR: break;
					case TYPE_ALPHA:
pthread_mutex_lock(&mutex_mem);
						 memcpy(&temp1c,&INF_mem[address],1);
pthread_mutex_unlock(&mutex_mem);
                         temp1c = 'A';
						 sprintf(argv[index3],"%c",temp1c);			
						 break;
					case TYPE_NUM_1: 
					case TYPE_NUM_2:
pthread_mutex_lock(&mutex_mem);
						memcpy(&temp1,&INF_mem[address],2);
pthread_mutex_unlock(&mutex_mem);
						temp1 = htons(temp1);
						sprintf(argv[index3],"%d",temp1);
						break;
				//	case TYPE_TEXT_2: break;
						default: break;
					} // switch
				} // if(byte_count > 0)
			} // if(index3 != 0)
		} // for
		
		memset(buff,0,DEFAULT_BUFF_SIZE); // 2013-01-08
		strcpy(buff,argv[0]);
		for(j = 1; j < argc ; j++ )
		{
			strcat(buff,",");
			if( NMEA_def[index2].field[j-1].type == TYPE_DEFINED )
			{
				tail[0] = NMEA_def[index2].field[j-1].c;
				tail[1] = 0;
				strcat(buff,tail);
			}
			else if( NMEA_def[index2].field[j-1].type == TYPE_TEXT_VAR )
			{
				memset(tail,0,12);
				strcpy(tail,INF_st.nmea_write[i].field[j-1].s);
				strcat(buff,tail);
			}
			else if( NMEA_def[index2].field[j-1].type == TYPE_TEXT_2 )
			{
				strncpy(tail,INF_st.nmea_write[i].field[j-1].s,2);
				tail[2] = 0;
				strcat(buff,tail);
			}
			/*
			else if( NMEA_def[index2].field[j-1].type == TYPE_ALPHA )
			{
				tail[0] = INF_st.nmea_write[i].field[j-1].s[0];
				tail[1] = 0;
				strcat(buff,tail);
				dprintf("buffer %s\n",buff);
			}
			*/
			else
				strcat(buff,argv[j]);
		}
		length = strlen(buff);

		chksum = 0;
		for( j = 1 ; j < length ; j++ )
			chksum ^= buff[j];

		sprintf(tail,"*%02X\r\n",chksum);
		strcat(buff,tail);

		length = strlen(buff);

		inf_write(out,buff,length,PRIMARY);

#ifdef DEBUG
	dprintf("[NMEAW %d]	Format: %s Talker: %s\n",port_num,format,talker);
	dprintf("==> %s\n",buff);
#endif
	}

	return 1;
}

void nmeam_bufm(void *arg)	// added on 2013-01-08
{
	int sockfd;
	int port_num, len, check;
	fd_set readfds, testfds;
	char tempbuff[MAX_CMD_SIZE];
	int i;

	__u8 unit_id, func_code, byte_count;
	__u16 address, points;
	struct timeval timeout;
	modbus_res_hdr_ext *preq;
	modbus_wreq_hdr *preq_mw;
	

	port_num = (int)arg;
	pthread_detach(pthread_self());

	dprintf(" - Buffer Manager Start [Port: %d]\n",port_num);

	sockfd = INF_st.sockfd;

	FD_ZERO(&readfds);
	FD_SET(sockfd, &readfds);

	preq = (modbus_res_hdr_ext *)tempbuff;

	while(1)
	{
		testfds = readfds;

		timeout.tv_sec = 30;
		timeout.tv_usec = 0;

		if(select(sockfd+1, &testfds, (fd_set*)0, (fd_set*)0, (struct timeval*)&timeout) != 0)
		{
			if( FD_ISSET(sockfd,&testfds) ) {  // Write Command
				len = read(sockfd,tempbuff,MAX_CMD_SIZE);
				if( len >= 6 && len < 256 )
				{
					unit_id = preq->unit_id;
					func_code = preq->function_code;
					address = preq->address;
					byte_count = preq->byte_count;
					points = byte_count/2;

					if( func_code == write_mult_regs )
					{
						preq_mw = (modbus_wreq_hdr *)tempbuff;
						byte_count = preq_mw->byte_count;
						check = 1;

						if( unit_id != port_num )
						{
							dprintf("[NM CMD %d] Precheck error - id [%d]\n",port_num,unit_id);
							check = -1;
						}
		
						if(address>=2048)
						{
							dprintf("[NM CMD %d] Precheck error - address [%d]\n",port_num,address);
							check = -1;
						}

						if((address+points)>2048)
						{
							dprintf("[NM CMD %d] Precheck error - Data Space [%d]-[%d]\n",port_num,	address,address+points-1);
							check = -1;
						}

						if( byte_count > 128 )
						{
							dprintf("[MM CMD %d] Precheck error - Too large [%d]\n",port_num,byte_count);
							check = -1;
						}

						if( check > 0 )
						{
							dprintf("[MM CMD %d] MEM ADDR:%04X NO:%04X(%d)\n",port_num,address,points,byte_count);
pthread_mutex_lock(&mutex_mem);
							memcpy(&INF_mem[address],preq->data,byte_count);
pthread_mutex_unlock(&mutex_mem);
						}
					} // func_code
					if( func_code == 0xFF )
					{
						if( unit_id != port_num )
						{
							dprintf("[NM CMD %d] Precheck error - id [%d]\n",port_num,unit_id);
							continue;
						}

						if(address>=2048)
						{
							dprintf("[NM CMD %d] Precheck error - address [%d]\n",port_num,address);
							continue;
						}

						if((address+points)>2048)
						{
							dprintf("[NM CMD %d] Precheck error - Data Space [%d]-[%d]\n",port_num,	address,address+points-1);
							continue;
						}
pthread_mutex_lock(&mutex_mem);
						memcpy(&INF_mem[address],preq->data,byte_count);
pthread_mutex_unlock(&mutex_mem);
					}
				} // len
			} // FD_ISSET
		} // select

		sleep(10000);
	} // while
}

void nmeam_thread(void *arg)
{
	int slavefd,slavefds[2];
	int len, optval=1;
	fd_set readfds,	testfds;
	int fd, sockfd;
	int	nread;
	int port_num, mode, result, retries;
	int switch_no, system_no;
	struct timeval timeout;
	char write_flag;
	int tx_time,rx_time, timeout_time;
	int num_retry;

	port_num = (int)arg;
	pthread_detach(pthread_self());

	sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);	
	if(sockfd < 0)
	{
		dprintf("[NM %d] socket open error!!!\n",port_num);
		return;
	}
//	flags = fcntl(sockfd, F_GETFL, 0);
//	fcntl(sockfd, F_SETFL, flags|O_NONBLOCK);

restart:
	slavefds[0] = open_link(0);
	if( slavefds[0] < 0 ) goto restart;
	if(INF_st.num_port == 2)
	{
		slavefds[1] = open_link(1);
		if( slavefds[1] < 0 )
			INF_st.num_port = 1;
	}

	read_index = 0;
	retries = 0;
	switch_no = 0;
	write_flag = 0;
	fd_index = 0;
	slavefd = slavefds[fd_index];
//	sockfd = INF_st.sockfd;
	tx_time = INF_st.tx_time*1000;
	rx_time = INF_st.rx_time*1000;
	timeout_time = INF_st.timeout*1000;
	num_retry = INF_st.num_retry;
	mode = PERIODIC_REQ;

	dprintf("NMEA Master Start [INF: %d]\n",port_num);

	while(1){

		result = nmea_write(0,slavefd,port_num,mode);

		usleep(tx_time);
		
		if( result > 0 )
		{
			line_check(0,sockfd,port_num,ALIVE);
			retries = 0;
			update_line_info(PRIMARY,TX);
		}
		else
		{
			retries++;
			line_check(0,sockfd,port_num,BAD);

			if( retries >= num_retry )
			{
//				line_check(0,sockfd,port_num,DEAD);
				retries = 0;
			}
		}
	}

	close(slavefds[0]);
	if(INF_st.num_port == 2) close(slavefds[1]);
}

