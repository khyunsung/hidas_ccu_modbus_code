#define APP_NAME "HISCM_INF_IPMS"
#define VERSION "2.0.2.3"
#define COMPILE_TIME "2015-11-17.22:00:00"
#define COMPILE_BY "u20"
#define COMPILE_HOST "control"
#define COMPILER "arm-linux-gcc (GCC) 3.2"
#define VER_DES	"Revision 2"

/********************************************************************
//++ #2.0.2.3 2015-11-17
	; cond.c read_cmd[x].address -> read_cmd[x].mapping로 수정

//++ #2.0.2.2 2015-10-29
	; void modbusm_reopen_thread -> close(p_open[0]),close(p_open[1]) 추가

//++ #2.0.2.1 2015-10-27
	; TCP/IP Redundancy
	; modbus_master bufm scantime usleep(100000) -> usleep(10000)
//++ #2.0.2.0 2015-07-06
	; sndc, chem, ndmc, cond 추가

//++ #2.0.1.7
#define APP_NAME "HISCM_INF"
#define VERSION "2.0.1.7"
#define COMPILE_TIME "2014-01-20.09:00:00"
#define COMPILE_BY "jjang9dr"
#define COMPILE_HOST "control"
#define COMPILER "arm-linux-gcc (GCC) 3.2"
#define VER_DES	"Revision 2"
********************************************************************/
