#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#include <asm/types.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/errno.h>
#include <net/if.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <termios.h>

#include <linux/ethtool.h>
#include <linux/sockios.h>

#include "common.h"

#include "app.h"
#include "interface.h"
#include "modbus.h"
#include "utils.h"
#include "log.h"

#define	DEBUG
#ifndef	DEBUG
	#define	dprintf(format, args...) log_access(format, ## args)
#else
	#define	dprintf(format, args...) log_access(format, ## args); printf(format, ## args)
#endif	// DEBUG

#define BASE 			3000

#define DEBUG_INF
//#define		WRITE_DEBUG

extern INF_ST INF_st;
extern CMD_Q CMD_q;
extern char sys_type;
extern int interface_num;
extern int main_line;
extern int port0, port1, port_ps;
extern char redundancy_on;

extern pthread_mutex_t mutex_inf;
unsigned int inf_tx_cnt[2], inf_rx_cnt[2], inf_thr_cnt;

int fd_index, read_index;
char sendbuff[DEFAULT_BUFF_SIZE];
char recvbuff[DEFAULT_BUFF_SIZE];
char mmode;
int Inf_sts_cmd_reg = 0, Comm_Open_Err = 0;
static Inf_sts_cmd_reg_old = 1;

Inf_sts_cmd_r_bit Inf_s_cmd_b;
int sockfd_cmd;

#ifdef DG
       #define SERIAL
#else
#ifdef NMEA
	#ifndef NMEA_UDP
		#define SERIAL
	#else
		#define UDP
	#endif
#else
	#ifdef MODBUS_TCP
		#define TCP
	#else
		#ifdef MODBUS_UDP
			#define UDP
		#else
			#define SERIAL
		#endif
	#endif
#endif
#endif

#ifdef SERIAL
/////////////////////// MODBUS/NMEA/DG SERIAL //////////////////////////

static int SetSerialPort(int fd, int port)
{
//	struct termios *potio, *pntio;
	struct termios pntio;
	FILE *fp;
	int baud,nbaud;
	int databits = CS8;
	int stopbits = CSTOPB;
	int parity = 0;
	int flowctrl = 0;

	switch(INF_st.port[port].baudrate)
	{
		case   4800:  baud = B4800;   break;
		case  19200:  baud = B19200;  break;
		case  38400:  baud = B38400;  break;
		case  57600:  baud = B57600;  break;
		case 115200:  baud = B115200; break;
		default:
		case   9600:  baud = B9600;   break;
	}

	switch(INF_st.port[port].databit)
	{
		case 6: databits = CS6; break;
		case 7: databits = CS7; break;
		default:
		case 8: databits = CS8; break;
	}

	switch(INF_st.port[port].paritybit)
	{
		case 1: parity = PARENB|PARODD; break;
		case 2: parity = PARENB; break;
		default:
		case 0: parity = 0; break;
	}

	switch(INF_st.port[port].stopbit)
	{
		case 2: stopbits = CSTOPB; break;
		default:
		case 1: stopbits = 0; break;
	}

	switch(INF_st.port[port].flowctrl)
	{
		case 1: flowctrl = CRTSCTS; break;
		default:
		case 0: flowctrl = 0; break;
	}

	memset(&pntio, 0, sizeof(struct termios));
	pntio.c_cflag = baud|databits|parity|stopbits|flowctrl|CLOCAL|CREAD;
	pntio.c_lflag = 0; // set input mode (non-canonical, no echo,...)
	pntio.c_cc[VTIME] = 0;
	pntio.c_cc[VMIN] = 1;  // 최소 1문자 블로킹
	tcflush(fd, TCIFLUSH);
	tcsetattr(fd,TCSANOW, (struct termios *)&pntio);

	return 0;
}

int open_link(int port)				//serial
{
	int fd;
	char filename[256];

	sprintf(filename,"/dev/ttyS%d",(INF_st.port[port].port_num - 1));
 	fd = open(filename,O_RDWR);
	if( fd < 0) {
		dprintf("[Port %d] Error opening %s\n",port,filename);
		sleep(1);
		return -1;
	}
	dprintf("[Port %d]  Open success %s\n",port,filename);
	SetSerialPort(fd,port);

	return fd;
}

#endif

#ifdef UDP

/////////////////////// MODBUS/NMEA UDP //////////////////////////
#ifdef NMEA_UDP
struct sockaddr_in nmea_server;
#endif

static int open_socket()
{
	int sockfd;
	int result, optval=1;

	sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(sockfd < 0)
	{
		return -1;
	}

	return sockfd;
}

int open_link(int port)
{
	int sockfd;
	struct sockaddr_in servAddr;
	char servIP[32];
   	unsigned short servPort;
	int result, optval=1;

#ifdef NMEA_UDP
	struct ip_mreq mreq;
	int ip_class;
	char temp[8];
#endif

	sockfd = open_socket();	
	if(sockfd < 0 )
	{
		dprintf("[Port %d] socket open error\n",port);
		sleep(1);
		return -1;
	}

	dprintf("[Port %d] socket open!!\n",port);

	servPort = INF_st.lport[port].port;
	memset((char*)&servAddr, 0, sizeof(servAddr));
	servAddr.sin_addr.s_addr = inet_addr(INF_st.lport[port].ip);
	//servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servAddr.sin_family = AF_INET;
	servAddr.sin_port = htons(servPort);

	dprintf("[Port %d] wait for connection.. (IP:%s Port:%d)\n",port,
		INF_st.lport[port].ip,INF_st.lport[port].port);

#ifndef NMEA_UDP
	if(connect(sockfd,(struct sockaddr*)&servAddr,sizeof(servAddr)) < 0)
	{
		sleep(1);
		return -1;
	}
#else
	if(bind(sockfd,(struct sockaddr*)&servAddr,sizeof(servAddr)) < 0)
	{
		dprintf("[Port %d] socket bind error\n",port);
		sleep(1);
		return -1;
	}

	memcpy(&nmea_server,&servAddr,sizeof(servAddr));

	memcpy(temp,&INF_st.lport[port].ip,3);
	temp[3] = 0;
	ip_class = atoi(temp);

	if( ip_class >= 224 && ip_class <= 239 )
	{
		mreq.imr_multiaddr.s_addr = inet_addr(INF_st.lport[port].ip);
		mreq.imr_interface.s_addr = htonl(INADDR_ANY);
		if(setsockopt(sockfd,IPPROTO_IP,IP_ADD_MEMBERSHIP,&mreq,sizeof(mreq)) < 0)
		{
			dprintf("[Port %d] multicast mode change error\n",port);
			sleep(1);
			return -1;
		}

		dprintf("[Port %d] Multicast Mode\n",port);
	}
	else
	{
		dprintf("[Port %d] Unicast Mode\n",port);
	}
#endif

	return sockfd;
}
#endif


#ifdef TCP

/////////////////////// MODBUS/NMEA TCPIP //////////////////////////
#ifdef MODBUS_TCP

modbus_mbap_hdr mbaphdr;
__u16 trans_id;
__u16 ptc_id;

static int init_mbaphdr()
{
	trans_id = 1000;
	ptc_id = 0; // MODBUS_PROTOCOL
}
#endif

static int open_socket()
{
	int sockfd;
	int result, optval=1;

	sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(sockfd < 0)
	{
		return -1;
	}

	return sockfd;
}

static int getIP_dev(int sock, const char *dev, char *ip)
{
	struct ifreq ifc;
	int res;
	 
	strcpy(ifc.ifr_name,dev);
	res = ioctl(sock,SIOCGIFADDR,&ifc);

	if(res >= 0)
	{
		strcpy(ip,inet_ntoa(((struct sockaddr_in *)&ifc.ifr_addr)->sin_addr));
		return 1;
	}
	else
		return -1;
}

static int connect_nonb(int sock, struct sockaddr *serv_addr, int addr_len, int nsec)
{
	int flags, n, error;
	int len;
	fd_set rset, wset;
	struct timeval tval;

	flags = fcntl(sock, F_GETFL, 0);
	fcntl(sock, F_SETFL, flags|O_NONBLOCK);

	error = 0;
	   			
	if((n = connect(sock,(struct sockaddr *)serv_addr,addr_len)) < 0)
	{
		dprintf("\n Connect error return %d\t\t",n);
		if(errno != EINPROGRESS)
		{
			dprintf("\tconnect error: not in progress1!!!\n");
			return -1;
		}
	}
    return 1;
	if( n == 0 ) goto done;

	FD_ZERO(&rset);
	FD_SET(sock, &rset);
	wset = rset;
	tval.tv_sec = nsec;
	tval.tv_usec = 0;

	if((n = select(sock+1,&rset,&wset,NULL,nsec?&tval:NULL)) == 0)
	{
		dprintf("\tconnect error: timeout expired!!!\n");
		close(sock);
		errno = ETIMEDOUT;
		return -1;
	}

	if(FD_ISSET(sock,&rset)||FD_ISSET(sock,&wset))
	{
		if(getsockopt(sock,SOL_SOCKET,SO_ERROR,&error,&len) < 0)
			return -1;
	}
	else
	{
		dprintf("\tselect error: sockfd no set!!!\n");
		return -1;
	}

done:
    
	fcntl(sock, F_SETFL, flags);
	if( error )
	{
		dprintf("\tsockopt error: error(%d) occured!!!\n",error); // modified 2014/01/20
		close(sock);
		errno = error;
		sleep(5);
		return -1;
	}
    dprintf("\nConnected  return 1!");
	return 1;
}
int open_server(int port)
{
	int sockfd;	
	struct sockaddr_in servAddr;
	  	
	sockfd = open_socket();	//PF_INET, SOCK_STREAM, IPPROTO_TCP
	if(sockfd < 0 )
	{
		dprintf("[Port %d] socket open error\n",port);
		sleep(5);
		return -1;
	}

	dprintf("[Port %d] socket open!!\n",port);
	
	memset((char*)&servAddr, 0, sizeof(servAddr));
	servAddr.sin_addr.s_addr = htonl( INADDR_ANY);
	servAddr.sin_family = AF_INET;
	servAddr.sin_port = htons( port);
		

   if( -1 == bind( sockfd, (struct sockaddr*)&servAddr, sizeof( servAddr) ) )
   {
      printf( "bind() 실행 에러\n");
      exit( 1);
   }

   //while( 1)

      
	return sockfd;
}
int open_link(int port)
{
	int sockfd;
	struct sockaddr_in servAddr;
	struct sockaddr_in localAddr;
	char device[8];
	char localIP[32];
	char servIP[32];
	unsigned short localPort,servPort;
	int result, optval=1;

	sockfd = open_socket();	
	if(sockfd < 0 )
	{
		dprintf("[Port %d] socket open error\n",port);
		sleep(5);
		return -1;
	}

	dprintf("[Port %d] socket open!!\n",port);
	result = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));

	servPort = INF_st.lport[port].port;

	memset((char*)&servAddr, 0, sizeof(servAddr));

	servAddr.sin_addr.s_addr = inet_addr(INF_st.lport[port].ip);
	servAddr.sin_family = AF_INET;
	servAddr.sin_port = htons(servPort);

	dprintf("[Port %d] Request connection.. (IP:%s Port:%d)\n",port,
		INF_st.lport[port].ip,INF_st.lport[port].port);

	if(connect_nonb(sockfd,(struct sockaddr*)&servAddr,sizeof(servAddr),5) < 0)
	{
		close(sockfd);
		sleep(1);
		return -1;
	}

	dprintf("[Port %d] connected! (IP:%s Port:%d)\n",port,INF_st.lport[port].ip,INF_st.lport[port].port);
	
#ifdef MODBUS_TCP
	init_mbaphdr();
#endif

	return sockfd;
}

#endif

static int dev_read(__u16 port, __u8 *buff, int len, __u8 ps)
{
	int index, recv_byte;
	int addrlen;
	if( port <= 0 ) return -1;

#ifdef NMEA_UDP
	addrlen = sizeof(nmea_server);
	recv_byte = recvfrom(port, buff, len, 0, (struct sockaddr *)&nmea_server, &addrlen);
#else
//	recv_byte = read(port, buff, len);

	#ifndef MODBUS_TCP

	recv_byte = frame_rd(port,buff,len);

	#else

	recv_byte = read(port, buff, len);

	#endif

#endif

#ifdef DEBUG_INF
	if(INF_st.debugfd.valid)
	{
		if( ps == PRIMARY ) index = fd_index;
		else if( ps == SECONDARY ) index = (fd_index+1)%INF_st.num_port;
		else goto out;

		write(INF_st.debugfd.rxfd[index],buff,recv_byte);
	}
#endif
out:
	return recv_byte;
}

static int dev_write(__u16 port, __u8 *buff, int len, __u8 ps)
{
	int index, write_byte;
	if( port <= 0 ) return -1;

#ifdef NMEA_UDP
	write_byte = sendto(port, buff, len, 0, (struct sockaddr *)&nmea_server, sizeof(nmea_server));
#else
	write_byte = write(port, buff, len);
#endif

#ifdef DEBUG_INF
	if(INF_st.debugfd.valid)
	{
		if( ps == PRIMARY ) index = fd_index;
		else if( ps == SECONDARY ) index = (fd_index+1)%INF_st.num_port;
		else goto out;

		write(INF_st.debugfd.txfd[index],buff,write_byte);
	}
#endif
out:
	return write_byte;
}

int port_up(__u16 port, __u8 *buff, int len, int port_num)
{
	struct sockaddr_in manager;
	unsigned short mgrPort = BASE+port_num;
	char mgrIP[32]="127.0.0.1";
	int i;

	if( port <= 0 )
	{
		dprintf("[INF %d] Port(%d) Up Error(%d)!!\n",port_num,port,errno);
		return -1;
	}
	
	memset(&manager,0,sizeof(manager));
	manager.sin_family=AF_INET;
	manager.sin_addr.s_addr = inet_addr(mgrIP);
	manager.sin_port = htons(mgrPort);

	if(sendto(port,buff,len,0,(struct sockaddr *)&manager,sizeof(manager))
		== len)
	{
#ifdef DEBUG
printf("[INF %d] UP [",port_num);
for(i=0;i<len;i++) { printf("%02X|",buff[i]);}
printf("]\n");
#endif
		return 1;
	}

	dprintf("[INF %d] Port(%d) Up Error(%d)!!\n",port_num,port,errno);

	return -1;
}

int inf_read(__u16 port, __u8 *buff, int len, __u8 ps)
{
	int i,recv_byte;
	__u16 crc;

	recv_byte = dev_read(port, buff, len, ps);

#ifdef MODBUS_TCP
if( mmode == MD_MODBUS_MASTER)
{
	modbus_mbap_hdr *res_mbaphdr;
	res_mbaphdr = (modbus_mbap_hdr *)buff;

	if( mbaphdr.trans_id != res_mbaphdr->trans_id )
	{
//		dprintf("Transaction id [%d] mismatch!\n",res_mbaphdr->trans_id);
//		return -1;
	}
}	
#endif
	return recv_byte;
}

int inf_write(__u16 port, __u8 *buff, int len, __u8 ps)
{
int i;
#ifndef MODBUS_TCP

	return dev_write(port, buff, len, ps);

#else
if( mmode == MD_MODBUS_SLAVE) return dev_write(port, buff, len, ps);	//MODBUS_TCP Client인경우
else if( mmode == MD_MODBUS_MASTER)										//MODBUS_TCP Server인경우
{
	char buff_t[DEFAULT_BUFF_SIZE];

	mbaphdr.trans_id = htons(trans_id);
	trans_id++;
	if( trans_id > 60000 ) trans_id = 1000;
	mbaphdr.ptc_id = htons(ptc_id);
	mbaphdr.length = htons(len-2);

	memcpy(buff_t,&mbaphdr,6);
	memcpy(buff_t+6,buff,len-2);

	return dev_write(port, buff_t, len+4, ps);
}
#endif
}

#define RX_SET 5
int frame_rd(int out, char *tempbuff, int num)
{
	int rx_max_timeout = 100,rx_timeout = 0,rx_timeout_set = 0;
	char rd_buf[DEFAULT_BUFF_SIZE-16] = {0,};
	int rd_cnt = 0,len = 0;
	int nread = 0,nread_old = 0;

	if(INF_st.port[0].baudrate <= 9600)rx_timeout_set = RX_SET;
	rx_timeout = rx_timeout_set;
	while(rx_max_timeout-- > 0){
		usleep(5000);	// 10msec
		nread = 0;
		ioctl(out, FIONREAD,&nread);
		if((nread>=num)||((nread_old==nread)&&(--rx_timeout <= 0))){
			len = read(out,rd_buf,num);
			memcpy(&tempbuff[0], &rd_buf[0], len%num);
			break;
		}
		if(nread_old != nread){
			nread_old = nread;
			rx_timeout = rx_timeout_set;
		}
	}
	return len;
}


int cmd_handshake(int target, int port_num, int cmd)
{
	modbus_res_hdr_ext *pext;
	char writebuff[16];
	unsigned short crc;
	int ret;

	pext = (modbus_res_hdr_ext *)writebuff;
	pext->unit_id = port_num;
	pext->function_code = cmd;
	pext->address = 0x0000;
	pext->byte_count = 0x0000;
	crc = CRC16((__u8*)pext,6)&0xFFFF;
	writebuff[6] = crc>>8;
	writebuff[7] = crc&0x00FF;

	ret = port_up(target,writebuff,8,port_num);

	if( cmd == 0x06 )
	{
#ifdef DEBUG
		dprintf("[INF %d] cmd handshaking (%d)\n",port_num,ret);
#endif
	}
	else
	{
		dprintf("[INF %d] cmd buffer clear (%d)\n",port_num,ret);
	}
}

int cmd_clear(int fd)
{
	int i, ret;
	struct timeval tm;
	char temp[256];

	fd_set fds;
	FD_ZERO(&fds);
	FD_SET(fd, &fds);
	
	tm.tv_sec = 0;
	tm.tv_usec = 0;

	for( i = 0; i < 999999; i++ )
	{
		ret = select(FD_SETSIZE,&fds,0,0,&tm);
		if( ret == 0 ) break;
		recv(fd,temp,256,0);
	}

pthread_mutex_lock(&mutex_inf);
	CMD_q.write_head = 0;
	CMD_q.write_tail = 0;
pthread_mutex_unlock(&mutex_inf);

	return 1;
}

int getethlink(int no)
{
	int fd;
	int ret;
	struct ifreq ifr;
	struct ethtool_value edata;

	fd = socket(AF_INET,SOCK_DGRAM,0);

	edata.cmd = ETHTOOL_GLINK;
	edata.data = 0;
	sprintf(ifr.ifr_name,"eth%d",no);
	ifr.ifr_data = (caddr_t)&edata;

	ret = ioctl(fd,SIOCETHTOOL,&ifr);

	close(fd);

	return edata.data;
}

void update_line_info(unsigned char line, unsigned char dir)
{
	if(dir == TX)
	{
		inf_tx_cnt[line]++;
		if(inf_tx_cnt[line] >= MAX_NUM_CNT) inf_tx_cnt[line] = 0;
	}
	else
	{
		inf_rx_cnt[line]++;
		if(inf_rx_cnt[line] >= MAX_NUM_CNT) inf_rx_cnt[line] = 0;
	}
}

void line_check(int id, int out, int port_num, int mode)
{
	int i,len;
	__u8 system_no;
	modbus_res_hdr_ext *pext;
	char writebuff[16];
	char recvbuff[16];
	unsigned short crc;
	int check = 0xE0000000;
	fd_set fds;
	struct timeval timeout;
	char ok = 0;
	char str[32];

	char *alarm_cnt;

	pext = (modbus_res_hdr_ext *)writebuff;

	memset(recvbuff,0,16);

	if( INF_st.sys_alarm & check )
	{
	//	dprintf("sys_alarm: %08X\n",INF_st.sys_alarm);
	}
	else
	{
		pext->unit_id = 0;
		pext->function_code = 0x10;
		pext->address = port_num-1;
		pext->byte_count = 2;
		
		crc = CRC16((__u8*)pext,6)&0xFFFF;
		writebuff[6] = crc>>8;
		writebuff[7] = crc&0x00FF;

		port_up(out,writebuff,8,0);

		FD_ZERO(&fds);
		FD_SET(out, &fds);

		timeout.tv_sec = 1;
		timeout.tv_usec = 0;

		if(select(FD_SETSIZE,&fds,0,0,&timeout)!= 0)
		{
			if(FD_ISSET(out,&fds))
			{
				len = read(out,recvbuff,16);

				if((recvbuff[0] == 0x00)&&
				   (recvbuff[1] == 0xFF))
				{
					INF_st.sys_alarm = (recvbuff[6]<<8)|recvbuff[7];
					sprintf(str,"[Init] Sys_alarm: OK =>");
					ok = 1;
				}
			}
		}

		if( !ok ) 
		{
			sprintf(str,"[Init] Sys_alarm: Error =>");
			INF_st.sys_alarm = 0x0000;
		}

		INF_st.sys_alarm |= check;
		dprintf("%s%08X\n",str,INF_st.sys_alarm);
	}

	sys_type = ACONIS_DS;

	for( i = 0 ; i < INF_st.num_addr_map ; i++)
	{
		if( INF_st.addr_map[i].address == id )
		{
			system_no = INF_st.addr_map[i].system;

			pext->unit_id = 0;
			pext->function_code = 0x03;
			pext->address = port_num-1;
			pext->byte_count = 2;

//+++++++++++++++ Redundancy ++++++++++++++++++
			if(redundancy_on)
			{
				if(main_line) alarm_cnt = &INF_st.addr_map[i].alarm_count2;
				else alarm_cnt = &INF_st.addr_map[i].alarm_count;
#ifdef SERIAL
				system_no += main_line*8;
#else
				system_no += main_line*1;
#endif
				if( mode == ALIVE )
				{
					if( sys_type == ACONIS_DS ) INF_st.sys_alarm &= ~(1<<system_no);
					else INF_st.sys_alarm |= (1<<system_no);
					if(main_line) port1 = 1;
					else port0 = 1;
					*alarm_cnt = 0;
				}
				else if( mode == BAD )
				{
					if( ++*alarm_cnt >= INF_st.addr_map[i].alarm )
					{
						dprintf("System #%d Comm Alarm\n",system_no+1);
						if( sys_type == ACONIS_DS ) INF_st.sys_alarm |= (1<<system_no);
						else INF_st.sys_alarm &= ~(1<<system_no);
						if(main_line) port1 = 0;
						else port0 = 0;
//+++++++++++++++ Port Change ++++++++++++++++++
						if(main_line == port_ps) port_ps ^= 1;
						*alarm_cnt = 0;
					}
					else
					{
					//	dprintf("System #%d Comm Error[%d]\n",system_no+1,
					//		INF_st.addr_map[i].alarm_count);
						break;
					}
				}
				else if( mode == DEAD )
				{
					if( sys_type == ACONIS_DS ) INF_st.sys_alarm |= (1<<system_no);
					else INF_st.sys_alarm &= ~(1<<system_no);
					if(main_line) port1 = 0;
					else port0 = 0;
					*alarm_cnt = 0;
				}
#ifdef ECP_PHY
				else if( mode == PHY_CHECK )
				{
					if( sys_type == ACONIS_DS )
					{
						if( getethlink(2) ) INF_st.sys_alarm &= 0x7FFF;
						else INF_st.sys_alarm |= 0x8000;

						if( getethlink(3) ) INF_st.sys_alarm &= 0xBFFF;
						else INF_st.sys_alarm |= 0x4000;
					}
					else
					{
						if( getethlink(2) ) INF_st.sys_alarm |= 0x8000;
						else INF_st.sys_alarm &= 0x7FFF;

						if( getethlink(3) ) INF_st.sys_alarm |= 0x4000;
						else INF_st.sys_alarm &= 0xBFFF;
					}

					INF_st.sys_alarm |= check;
				}
#endif
				else break;
			}
//+++++++++++++++ Single Line +++++++++++++++++++++++++
			else
			{
				if( mode == ALIVE )
				{
					if( sys_type == ACONIS_DS ) INF_st.sys_alarm &= ~(1<<system_no);
					else INF_st.sys_alarm |= (1<<system_no);
					INF_st.addr_map[i].alarm_count = 0;
					if(main_line) port1 = 1;
					else port0 = 1;
//					port0 = 1;
				}
				else if( mode == BAD )
				{
					INF_st.addr_map[i].alarm_count++;
					if( INF_st.addr_map[i].alarm_count >= INF_st.addr_map[i].alarm )
					{
					//	dprintf("System #%d Comm Alarm\n",system_no+1);
						if( sys_type == ACONIS_DS ) INF_st.sys_alarm |= (1<<system_no);
						else INF_st.sys_alarm &= ~(1<<system_no);
						INF_st.addr_map[i].alarm_count = 0;
						if(main_line) port1 = 0;
						else port0 = 0;
//						port0 = 0;
//+++++++++++++++ Port Change ++++++++++++++++++
						if(INF_st.num_port == 2) port_ps ^= 1;
					}
					else
					{
					//	dprintf("System #%d Comm Error[%d]\n",system_no+1,
					//		INF_st.addr_map[i].alarm_count);
						break;
					}
				}
				else if( mode == DEAD )
				{
					if( sys_type == ACONIS_DS ) INF_st.sys_alarm |= (1<<system_no);
					else INF_st.sys_alarm &= ~(1<<system_no);
					INF_st.addr_map[i].alarm_count = 0;
					port0 = 0;
				}
#ifdef ECP_PHY
				else if( mode == PHY_CHECK )
				{
					if( sys_type == ACONIS_DS ) 
					{
						if( getethlink(2) ) INF_st.sys_alarm &= 0x7FFF;
						else INF_st.sys_alarm |= 0x8000;

						if( getethlink(3) ) INF_st.sys_alarm &= 0xBFFF;
						else INF_st.sys_alarm |= 0x4000;
					}
					else
					{
						if( getethlink(2) ) INF_st.sys_alarm |= 0x8000;
						else INF_st.sys_alarm &= 0x7FFF;

						if( getethlink(3) ) INF_st.sys_alarm |= 0x4000;
						else INF_st.sys_alarm &= 0xBFFF;
					}

					INF_st.sys_alarm |= check;
				}
#endif
				else break;
			}

			pext->data[0] = htons(INF_st.sys_alarm); // Compatible Code
			crc = CRC16((__u8*)pext,8)&0xFFFF;
			writebuff[8] = crc>>8;
			writebuff[9] = crc&0x00FF;

			port_up(out,writebuff,10,0);
			break;
		}
	}
}

int check_file()
{
	int log_fd;
	char *log_path_name = "/mnt/";
	char *log_name = "inf_comstatus";

	char name[256];
	char str[256];

	sprintf(name,"%s%s",log_path_name,log_name);
	log_fd = open(name,
		O_RDONLY);
	close(log_fd);

	if(log_fd < 0)return 1;
	else return 0;
}


void upload_line_info(int out, int type, int port_num)
{

	int i;	
	unsigned short lsb[2],msb[2];
	unsigned short tmp_data[4];	
	char writebuff[16];
	__u16 crc, r_crc;
	modbus_res_hdr_ext *pext;
	
	pext = (modbus_res_hdr_ext *)writebuff;
	pext->unit_id = 0;
	pext->function_code = 0x03;
	
	if(type == TX)
	{
		pext->address = TX_PACKET_CNT_BASE + ((port_num - 1) * 4);
		for(i = 0; i < 2; i++)
		{
			lsb[i] = inf_tx_cnt[i];
			msb[i] = inf_tx_cnt[i] >> 16;
			tmp_data[(i*2)] = htons(msb[i]);
			tmp_data[(i*2) + 1] = htons(lsb[i]);
		}
	}
	else
	{
		pext->address = RX_PACKET_CNT_BASE + ((port_num - 1) * 4);
		for(i = 0; i < 2; i++)
		{
			lsb[i] = inf_rx_cnt[i];
			msb[i] = inf_rx_cnt[i] >> 16;
			tmp_data[(i*2)] = htons(msb[i]);
			tmp_data[(i*2) + 1] = htons(lsb[i]);
		}	
	}
	memcpy(pext->data,&tmp_data,8);
	pext->byte_count = 8;	
	crc = CRC16((__u8*)pext,6+pext->byte_count)&0xFFFF;
	writebuff[6+pext->byte_count] = crc>>8;
	writebuff[7+pext->byte_count] = crc&0x00FF;		
	port_up(out,writebuff,8+pext->byte_count,0);
}
void cmd_reg_check(int out)
{
	char writebuff[16];
	char recvbuff[16];
	__u16 crc;
	modbus_res_hdr_ext *pext;
	fd_set fds;
	struct timeval timeout;
	int len = 0;
	char ok = 0;
	static int Inf_sts_cmd_reg_old = 0;

	pext = (modbus_res_hdr_ext *)writebuff;

	//++ Command RD
	pext->unit_id = 0;
	pext->function_code = 0x10;
	pext->address = CMD_REG_BASE;
	pext->byte_count = 2;

	crc = CRC16((__u8*)pext,6)&0xFFFF;
	writebuff[6] = crc>>8;
	writebuff[7] = crc&0x00FF;

	port_up(out,writebuff,8,0);

	memset(recvbuff,0,16);

	FD_ZERO(&fds);
	FD_SET(out, &fds);

	timeout.tv_sec = 3;
	timeout.tv_usec = 0;

	if(select(FD_SETSIZE,&fds,0,0,&timeout) != 0)
	{
		if(FD_ISSET(out,&fds))
		{
			len = read(out,recvbuff,16);

			if((recvbuff[0] == 0x00)&&
			   (recvbuff[1] == 0xFF))
			{
				Inf_sts_cmd_reg = (recvbuff[6]<<8)|recvbuff[7];
				ok = 1;
			}
		}
	}

	if(ok == 1){
		Inf_s_cmd_b.all = Inf_sts_cmd_reg;
		if(Inf_s_cmd_b.all != Inf_sts_cmd_reg_old)
		{
			dprintf("Interface Status Command Register : 0x%04X\n",Inf_s_cmd_b.all);
		}
		Inf_sts_cmd_reg_old = Inf_s_cmd_b.all;
	}
	else{
		Inf_s_cmd_b.all = 0;
		dprintf("Interface Status Command Register : Error!! \n");
	}
}


void manage_comstatus_thread(void *arg)
{
	int port_num;
	port_num = (int)arg;
	int sockfd;	

	sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(sockfd >= 0)
	{
		dprintf("[MM %d] comstatus socket open!\n",port_num);
	}
	else
//	if(sockfd < 0)
	{
		dprintf("[MM %d] comstatus socket open error!!!\n",port_num);
		close(sockfd);
		return;
	}

	while(1)
	{
/*		if(check_file()) //2015.0925 u20
		{
			inf_tx_cnt[0] = 0;
			inf_tx_cnt[1] = 0;
			inf_rx_cnt[0] = 0;
			inf_rx_cnt[1] = 0;
		}*/
		//khs upload_line_info(sockfd, TX, port_num);
		//khs upload_line_info(sockfd, RX, port_num);
		sleep(2);
	}
	close(sockfd);
}
