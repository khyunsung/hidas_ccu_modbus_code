#ifndef	__INF_MODBUS_H
#define	__INF_MODBUS_H

//#define	LGE

#define MAX_Q_SIZE	30  // 10 -> 15 (03/26/2012) -> 20 (06/27/2012)
#define MAX_CMD_SIZE 	256 

#define TX			0
#define RX			1
#define MAX_NO_FIELD  20

 enum {
	PERIODIC_REQ,
	WRITE_REQ,
	EVENT_REQ,
	WAIT_WRITE_RES,
	WAIT_READ_RES,
	WAIT_EVENT_RES,
	DIAGNOSIS,
	PERIODIC_REQ_PRI
};

enum {
 	DEFAULT_REDUNDANCY,
 	LINECHECK_REDUNDANCY,
 	LINEREAD_REDUNDANCY,
 	LINEBOTH_REDUNDANCY
};

enum {
	REDUNDANCY_RO_REQ,
	REDUNDANCY_RW_REQ
};

enum {
	PRIMARY,
	SECONDARY,
	NONE
};

enum {
	ALIVE,
	DEAD,
	BAD,
	PHY_CHECK
};

typedef struct {
	char write_flag;
	char write_head, write_tail;
	char write_buff[MAX_Q_SIZE][MAX_CMD_SIZE];
	char write_length[MAX_Q_SIZE];
}CMD_Q;

typedef struct {
	__u8 unit_id;
	__u8 function_code;
	__u16 address;
	__u16 points;
	__u16 mapping;

	__u8 priority; // 0:best prior
	__s8 aging;
	__u16 res;
} modbus_read_cmd; // 12 bytes

typedef struct {
	__u16 loop;
	__u16 number;
	__u16 mapping;

	__u8 priority; // 0:best prior
	__s8 aging;
	__u16 res;
	__u16 res2;
	__u16 res3;
	__u16 res4;	
} fds_read_cmd; // 12 bytes

typedef struct {
	__u16 page;
	__u16 number;
	__u16 mapping;

	__u8 priority; // 0:best prior
	__s8 aging;
	__u16 res;
	__u16 res2;
	__u16 res3;	
	__u16 res4;	
} dg_read_cmd; // 12 bytes

typedef struct {
	__u16 offset;
	__u16 number;
	__u16 val_length;
	__u16 mapping;

	__u8 priority; // 0:best prior
	__s8 aging;
	__u16 res;
	__u16 res2;
	__u16 res3;	
} iccp_read_cmd; // 12 bytes

typedef struct {
	__u16 address;
	__u8 points; // not used
	__u8 res;

	__u8 write_unit_id;
	__u8 write_function_code;
	__u16 write_address;
	__u16 write_data;
	__u16 res2;

	__u8 write_type; // 0:polling 1:event 2:both 16:pulse
	__u16 repository;
	__u8 res3;
} modbus_write_cmd; // 16 bytes

typedef struct {
	__u8 format[5];
	__u8 talker[5];
	__u8 option;
	__u8 res;
	
	struct {
		__u8 position;
		__u16 address;
		__u8 c;
	} field[15];
} nmea_read_cmd; // 72 bytes

typedef struct {
	__u8 format[5];
	__u8 talker[5];
	__u8 option;
	__u8 res;
	
	struct {
		__u8 position;
		__u16 address;
		__u8 c;
		__u8 s[8];
	} field[MAX_NO_FIELD];
} nmea_write_cmd; // 204 bytes

typedef struct {
	__u8 address;
	__u8 address2;

	__u8 secondary;
	__u8 secondary2;
	__u8  system;

	__u8 alarm;
	__u8 alarm_count; 
	__u8 alarm_count2;
} inf_addr_map;
#if 0
typedef struct {
	__u8 address;
	__u8 address2;

	__u8 secondary;
	__u8 secondary2;
	__u8  system;
} modbus_addr_map;
#endif

typedef struct {
	__u8  port_num;
	__u32 baudrate;
	__u8  databit;
	__u8  paritybit;
	__u8  stopbit;
	__u8  flowctrl;
} port_info;

typedef struct {
	__u8  port_num;
	__u8  ip[32];
	__u16 port;
} lan_info;

typedef struct {
	char valid;
	int txfd[2];
	int rxfd[2];
} debug_port;

typedef struct {
	int sockfd;
	debug_port debugfd;
	lan_info bypass;
	int port_type;
	union {
		port_info port[2];
		lan_info lport[2];
	};
	int num_port;
	int num_read_cmd;
	int num_write_cmd;
	int num_addr_map;
	union {
		modbus_read_cmd read_cmd[100];	// 12*100 = 1200
		nmea_read_cmd nmea_read[16];	// 72*16 = 1152
		fds_read_cmd fds_read[100];   // 12*199 = 1200
		iccp_read_cmd iccp_read[100];
	};
	union {
		modbus_write_cmd write_cmd[120]; // 16*120 = 1920
		nmea_write_cmd nmea_write[8];	// 204*8 = 1632
	};
	inf_addr_map addr_map[16];
	int num_retry;
	int redundancy;
	int tx_time,rx_time,timeout;
	int sys_alarm;
} INF_ST;

typedef union{
	struct {
		__u16 tx_valid 		:  2;
//		__u16 MasterSlave 	:  1;
		__u16 rsv 			: 14;
	} bit;
	__u16 all;
} Inf_sts_cmd_r_bit;


int open_server(int port);
int open_link(int port);
int inf_read(__u16 port, __u8 *buff, int len, __u8 ps);
int inf_write(__u16 port, __u8 *buff, int len, __u8 ps);
int frame_rd(int out, char *tempbuff, int num);
int port_up(__u16 port, __u8 *buff, int len, int port_num);
void line_check(int in, int out, int port_num, int mode);

int cmd_handshake(int target, int port_num, int cmd);
int cmd_clear(int fd);

void condm_thread(void *arg);
void condm_bufm(void *arg);
void ndmcs_thread(void *arg);
void ndmcm_thread(void *arg);
void ndmcm_bufm(void *arg);
void chemm_thread(void *arg);
void chemm_bufm(void *arg);
void sndcm_thread(void *arg);
void sndcs_thread(void *arg);
void dgm_thread(void *arg);
void iccpm_thread(void *arg);
void fdsm_thread(void *arg);
void modbusm_thread(void *arg);
void modbuss_thread(void *arg);
void nmeam_thread(void *arg);
void nmeas_thread(void *arg);
void manage_comstatus_thread(void *arg);
void modbusm_reopen_thread(void *arg);
void lanport_thread(void *arg);

void modbusm_bufm(void *arg);
void modbuss_bufm(void *arg);
void nmeam_bufm(void *arg);
void modbusm_pulse(void *arg);
extern void cmd_reg_check(int out);

extern char mmode;
extern char endian_type;

extern int sockfd_cmd;
#endif		// __INF_MODBUS_H
