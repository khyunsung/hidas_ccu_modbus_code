#ifndef	_NMEA_H
#define	_NMEA_H

#define MAX_NMEA_DEFINE  50 
#define MAX_NO_FIELD  20

enum {
	TYPE_UNDEFINE,
	TYPE_LAT,
	TYPE_LONG,
	TYPE_UTC,
	TYPE_STATUS,
	TYPE_DEFINED,
	TYPE_NUM_VAR,
	TYPE_HEX_VAR,
	TYPE_TEXT_VAR,
	TYPE_ALPHA,
	TYPE_NUM_1,
	TYPE_NUM_2,
	TYPE_TEXT_2
};

enum {
	OPT_NONE,
	OPT_BYPASS = 0x01,
	OPT_SECOND_FORMAT = 0x10,  // First Field: Talker, Second Field: Format
	OPT_REDIRECT1,
	OPT_REDIRECT2,
	OPT_REDIRECT3,
	OPT_NO_CHECKSUM = 0x20   // No Checksum Check
};

typedef struct {
	__u8 o;
	__u8 format[5];
	__u8 max;

	struct {
		__u8 type;
		__u8 c;
	} field[MAX_NO_FIELD];
} NMEA_DEF;



int decstr2num(char *s, int len);
float decstr2float(char *s, int len);
int hexstr2num(char *s, int len);
void get_nmea_utc(char *s, char *ret);
float get_nmea_latitude(char *s, char *s1);
float get_nmea_longitude(char *s, char *s1);
float get_nmea_angle(char *s, char *s1);
int get_args(char *s, char **argv);


#endif		// _NMEA_H
