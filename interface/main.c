#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <execinfo.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include <asm/types.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "common.h"
#include "icss_types.h"
#include "protocol_frame.h"
#include "modbus_down.h"

#include "app.h"
#include "interface.h"
#include "modbus.h"
#include "nmea.h"
#include "log.h"
#include "version.h"

#define RUN_CHECK
//#define CMD_INIT

#define BASE		3000

//#define MAX_TAG_SIZE	256

int interface_num;
int inf_tx_cnt[2], inf_rx_cnt[2], inf_thr_cnt;
int thr_cnt[2];

const char *app_name     = APP_NAME;
const char *app_version  = VERSION;
const char *app_time     = COMPILE_TIME;
const char *app_by       = COMPILE_BY;
const char *app_host     = COMPILE_HOST;
const char *app_compiler = COMPILER;

//#define	DEBUG
#ifndef	DEBUG
	#define	dprintf(format, args...) log_access(format, ## args)
#else
	#define	dprintf(format, args...) log_access(format, ## args); printf(format, ## args)
#endif	// DEBUG

int Debug = 0;

__u8 inf_alive;
pthread_t INF_thr, PORT_thr, DEBUG_thr, AUX_thr,INF1_thr,COMLOG_thr,PS_thr;
pthread_mutex_t mutex_inf = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex_mem = PTHREAD_MUTEX_INITIALIZER;
#ifdef RUN_CHECK
char file_name[256];
#endif

unsigned short INF_mem[MAX_INF_TAG];
CMD_Q CMD_q;
PORT_DATA PORT_data;
INF_ST INF_st;
NMEA_DEF NMEA_def[MAX_NMEA_DEFINE];
unsigned short mflag;
char mmode, sys_type, endian_type;

static void print_backtrace(void)
{
	int j, nptrs;
#define SIZE 100
	void *buffer[100];
	char **strings;

	nptrs = backtrace(buffer,SIZE);
	dprintf("[INF %d] backtrace() returned %d addresses\n",interface_num,nptrs);

	strings = backtrace_symbols(buffer,nptrs);
	if(strings == NULL) {
		perror("backtrace_symbols");
		exit(EXIT_FAILURE);
	}

	for( j = 0; j < nptrs; j++)
	{
		dprintf("\t%s\n",strings[j]);
	}

	free(strings);
}

static void backup_command(void)
{
	char fname[256];
	FILE *fp;
	int i, j;
	char head, len;

//pthread_mutex_lock(&mutex_cmd);
	dprintf("[INF %d] Dying Message: commands are remained\n",interface_num);
	dprintf("\thead: %d tail: %d\n",CMD_q.write_head,CMD_q.write_tail); 
	
	sprintf(fname,CMD_DIR"cmd%02d.tmp",interface_num);
	fp = fopen(fname,"w");
	if( fp == NULL ) return;
//	if( CMD_q.write_head == CMD_q.write_tail ) return;

	for(i=0;(CMD_q.write_head!=CMD_q.write_tail)&&(i<MAX_Q_SIZE); i++)
	{
		head = CMD_q.write_head;
		len = CMD_q.write_length[head];
		fprintf(fp,"%d ",len);
		for(j = 0; j < len ; j++)
			fprintf(fp,"%02X|",CMD_q.write_buff[head][j]);
		fprintf(fp,"\n");
		
		CMD_q.write_head++;
		CMD_q.write_head %= MAX_Q_SIZE;
	}
//pthread_mutex_unlock(&mutex_cmd);

	fclose(fp);
}

static void delete_backup(void)
{	
	char fname[256];
	FILE *fp;
	int i;

	sprintf(fname,CMD_DIR"cmd%02d.tmp",interface_num);
	fp = fopen(fname,"w");
	fclose(fp);	
}

static char *strsigno(int sig)
{
	switch(sig){
		case SIGHUP:	return "Hangup";
		case SIGINT:	return "Interrupt";
		case SIGQUIT:	return "Quit";
		case SIGILL:	return "Illegal instruction";
		case SIGTRAP:	return "Trace trap";
		case SIGABRT:	return "Abort";
		case SIGBUS:	return "BUS error";
		case SIGFPE:	return "Floating-point exception";
		case SIGKILL:	return "Kill, unblockable";
		case SIGUSR1:
			if(Debug==0){
				Debug = 1;
				printf("Debug On\n");
				dprintf("Debug On\n");
			} else{
				printf("Debug Off\n");
				dprintf("Debug Off\n");
				Debug = 0;
			}
			return "User-defined signal 1";
		case SIGSEGV:	return "Segmentation violation";
		case SIGUSR2:	return "User-defined signal 2";
		case SIGPIPE:	return "Broken pipe";
		case SIGALRM:	return "Alarm clock";
		case SIGTERM:	return "Termination";
#ifndef	PC_LINUX
		case SIGSTKFLT:	return "Stack fault";
#endif
		case SIGCHLD:	return "Child status has changed";
		case SIGCONT:	return "Continue";
		case SIGSTOP:	return "Stop, unblockable";
		case SIGTSTP:	return "Keyboard stop";
		case SIGTTIN:	return "Background read from tty";
		case SIGTTOU:	return "Background write to tty";
		case SIGURG:	return "Urgent condition on socket";
		case SIGXCPU:	return "CPU limit exceeded";
		case SIGXFSZ:	return "File size limit exceeded";
		case SIGVTALRM:	return "Virtual alarm clock";
		case SIGPROF:	return "Profiling alarm clock";
		case SIGWINCH:	return "Window size change";
		case SIGIO:		return "I/O now possible";
#ifndef	PC_LINUX
		case SIGPWR:	return "Power failure restart";
#endif
		case SIGSYS:	return "Bad system call";
		default:		return "";
	}
}

static void sig_ign(int sig)
{
	pid_t f;
	f = getpid();
	if( sig < NSIG ){
//		dprintf("[INF %d] Signal (pid-%d) : %d(%s)\n", interface_num, f, sig, strsigno(sig));

		if(sig == SIGSEGV)
		{
			backup_command();
			print_backtrace();
			exit(EXIT_FAILURE);
		}
		if(sig == SIGTERM)
		{	
#ifdef RUN_CHECK
#if 0
			unlink(file_name);
#endif
#endif
			delete_backup();
			exit(EXIT_SUCCESS);
		}
		signal(sig, sig_ign);
	}
}

static void make_daemon()
{
	// make daemon
	int i;

	pid_t f;

	f = fork();
	if ( f > 0 ){   // if parent
		exit(0);
	} else if ( f < 0 ) {   // fork error
		exit(-1);
	}

	setsid();

	chdir("/");

	for(i=1; i < NSIG; ++i) signal(i, sig_ign);
	signal(SIGINT, SIG_DFL);
}

static int initial(void)
{
	char fname[256];
	FILE *fp;
	int i, j;
	char buffer[256],*p;
	char tail, len;

	inf_tx_cnt[0] = 0;
	inf_rx_cnt[0] = 0;
	inf_tx_cnt[1] = 0;
	inf_rx_cnt[1] = 0;	
	inf_thr_cnt = 0;

	mflag = 0;
	sys_type = ACONIS_2000;

pthread_mutex_lock(&mutex_inf);
	CMD_q.write_head = 0;
	CMD_q.write_tail = 0;
pthread_mutex_unlock(&mutex_inf);
	
#ifdef CMD_INIT
	sprintf(fname,CMD_DIR"cmd%02d.tmp",interface_num);
	fp = fopen(fname,"r");
	if( fp == NULL ) return 0;

	for(i=0;(fgets(buffer,256,fp)!= NULL)&&(i<MAX_Q_SIZE);i++)
	{
		p = &buffer[0];

		char* length = strtok(p," ");
		len = atoi(length);

		for( j = 0 ; j < len ; j++ )
		{
			char *token = strtok(NULL,"|");
			if(token != NULL )
				CMD_q.write_buff[CMD_q.write_tail][j] = atoi(token);
		}
		CMD_q.write_length[CMD_q.write_tail] = len;

		CMD_q.write_tail++;
		CMD_q.write_tail %= MAX_Q_SIZE;
	}

	dprintf("[INF %d] CMD queue initialized(head: %d tail: %d)\n",
		interface_num,CMD_q.write_head,CMD_q.write_tail);

	fclose(fp);
#endif

	return 1;
}

static int _open_link(int inf_num)
{
	struct sockaddr_in myAddr;
	int result, optval=1;
	unsigned short myPort;
					
	INF_st.sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(INF_st.sockfd == -1){
		dprintf("[INF %d] socket open error!!!\n",inf_num);
		sleep(1);
		return -1;
	}

#ifdef NMEA
	result = setsockopt(INF_st.sockfd, SOL_SOCKET, SO_BROADCAST, &optval, sizeof(optval));
#endif

	myPort = BASE + BASE + inf_num;

	memset((char*)&myAddr, 0, sizeof(myAddr));
	myAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	myAddr.sin_family = AF_INET;
	myAddr.sin_port = htons(myPort);
	
	result = bind(INF_st.sockfd, (struct sockaddr*)&myAddr, sizeof(myAddr));
	if(result == -1){
		dprintf("[INF %d] bind retry...\n",inf_num);
		result = bind(INF_st.sockfd, (struct sockaddr*)&myAddr, sizeof(myAddr));
		if(result == -1){
			dprintf("[INF %d] bind error\n",inf_num);
			sleep(1);
			return -1;
		}
	}

	return 1;
}

static int open_debug(int inf_num)
{
	struct sockaddr_in myAddr;
	int sockfd[4];
	int i, result, optval=1;
	unsigned short myPort;

	for( i = 0 ; i < 4 ; i++ )
	{
		sockfd[i] = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if(sockfd[i] == -1){
			dprintf("[INF %d] debug socket (%d) open error\n",inf_num,i+1);
			sleep(1);
			return -1;
		}

		myPort = DEBUG_BASE + DEBUG_OFST*(inf_num-1) + i;

		memset(&myAddr,0,sizeof(myAddr));
		myAddr.sin_family = AF_INET;
		myAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
		myAddr.sin_port = htons(myPort);
		
		if(connect(sockfd[i],(struct sockaddr *)&myAddr,sizeof(myAddr)) < 0)
		{
			dprintf("[INF %d] debug connect error!!!\n",inf_num);
			return -1;
		}
	}

	INF_st.debugfd.txfd[0] = sockfd[0];
	INF_st.debugfd.txfd[1] = sockfd[1];
	INF_st.debugfd.rxfd[0] = sockfd[2];
	INF_st.debugfd.rxfd[1] = sockfd[3];
printf("===> %x, %x, %x, %x\n", sockfd[0], sockfd[1], sockfd[2], sockfd[3]);
	return 1;
}

static int db_change(void)
{
	return 0;
}


static int open_db(int inf_num)
{
	int i,j;
	FILE *sub_fp=NULL, *w_fp=NULL;
	char fname[256], dbname[256], fixname[256];
	int syscnt, rcnt, wcnt, total_rcnt, total_wcnt;
	int nread;

	/* PORT CONFIGURAtION READ */
	sprintf(fname,CONF_DIR"port%02d.conf",inf_num);
	sprintf(fixname,"%s.fix",fname);
	if(access(fixname,F_OK) == 0)
	{
		dprintf("[INF %d] Port Fix (%s) found\n",inf_num,fixname);
		link(fixname,fname);
		dprintf("\tCopy (%s)->(%s)\n",fixname,fname);
	}

	sprintf(fname,CONF_DIR"ptc%02d.conf",inf_num);
	if(access(fname,F_OK) != 0)
	{
		sprintf(fixname,"%s.fix",fname);
		if(access(fixname,F_OK) == 0)
		{
			dprintf("[INF %d] Protocol Fix (%s) found\n",inf_num,fixname);
			link(fixname,fname);
			dprintf("\tCopy (%s)->(%s)\n",fixname,fname);
			return 1;
		}

		sprintf(dbname,DB_DIR"PTC_%d.dwn",inf_num-1);

		sub_fp = fopen(dbname,"r");
		if(sub_fp == NULL) 
		{
			dprintf("[INF %d] IDLE due to Neither Config nor DB\n",inf_num);
			return 0;
		}
	
		if( (nread = fread(&PORT_data,1,sizeof(PORT_DATA),sub_fp)) != sizeof(PORT_DATA))
		{
			dprintf("[INF %d] IDLE due to DB data size error (%d)-(%d)\n",inf_num,nread,sizeof(PORT_DATA));
			fclose(sub_fp);
			return 0;
		}

		dprintf("[INF %d] Sub DB Open and Read (%d) bytes\n",inf_num,nread);
		dprintf("\tfile crc: %d\n",PORT_data.file.filecrc);
		dprintf("\trsc: %d\n",PORT_data.file.rsc);
		dprintf("\tsys_type: %s\n",PORT_data.file.systype?"ACONIS-DS":"ACONIS-2K");
		dprintf("\tftime: %d\n",PORT_data.file.ftime);
		dprintf("\tcnt: %d\n",PORT_data.file.cnt);
		dprintf("\tsize: %d\n",PORT_data.file.size);

		fclose(sub_fp);

		w_fp = fopen(fname,"w");
		if( w_fp == NULL)
		{
			dprintf("[INF %d] Cannot create configuration file %s!\n",inf_num,fname);
			return 0;
		}

		syscnt = PORT_data.ctrl.nodecnt;
		total_rcnt = 0;
		total_wcnt = 0;

		if(syscnt < 1 || syscnt > MAX_SYSTEM)
		{
			dprintf("[INF %d] DB Error: Number of Systems!!\n",inf_num);
			fclose(w_fp);
			return 0;
		}

		fprintf(w_fp,"# INF Protocol Configuration\n");
		fprintf(w_fp,"#\n");
		fprintf(w_fp,"# Prefix Definition\n");
		fprintf(w_fp,"#   $ @ => Control Header\n");
		fprintf(w_fp,"#   : * => Sub Header\n\n");
		fprintf(w_fp,"# INF %d\n\n",inf_num);

		for( i = 0 ; i < syscnt ; i++ )
		{
			rcnt = PORT_data.Node[i].profile.header.rcnt;
			wcnt = PORT_data.Node[i].profile.header.wcnt;
			total_rcnt += rcnt;
			total_wcnt += wcnt;
		}

		if( total_rcnt > 0 )
		{
			fprintf(w_fp,"$R%d\n",total_rcnt);
			for( i = 0 ; i < syscnt ; i++ )
			{
				rcnt = PORT_data.Node[i].profile.header.rcnt;
				for( j = 0 ; j < rcnt; j++ )
					fprintf(w_fp,":%d %d %d %d %d\n",
						PORT_data.Node[i].slave[0],
						PORT_data.Node[i].profile.data[j].read.fnccode,
						PORT_data.Node[i].profile.data[j].read.address,
						PORT_data.Node[i].profile.data[j].read.count,
						PORT_data.Node[i].profile.data[j].read.blockaddr);
				fprintf(w_fp,"\n");
			}
		}

		if( total_wcnt > 0 )
		{
			fprintf(w_fp,"$W%d\n",total_wcnt);
			for( i = 0 ; i < syscnt ; i++ )
			{
				wcnt = PORT_data.Node[i].profile.header.wcnt;
				for( j = 0 ; j < wcnt; j++ )
					fprintf(w_fp,":%d %d %d %d\n",
						PORT_data.Node[i].profile.data[j+rcnt].write.blockaddr,
						PORT_data.Node[i].slave[0],
						PORT_data.Node[i].profile.data[j+rcnt].write.fnccode,
						PORT_data.Node[i].profile.data[j+rcnt].write.address);
				fprintf(w_fp,"\n");
			}
		}

		fprintf(w_fp,"$A%d\n",syscnt);
		for( i = 0 ; i < syscnt ; i++)
		{
			fprintf(w_fp,":%d %d %d\n",i,
				PORT_data.Node[i].slave[0],
				PORT_data.Node[i].redundant?PORT_data.Node[i].slave[1]:0);
		}
		fprintf(w_fp,"\n");

		if( PORT_data.ctrl.period != 0 ) fprintf(w_fp,"@TR%d\n",PORT_data.ctrl.period*10);
		if( PORT_data.ctrl.timeout != 0 ) fprintf(w_fp,"@TO%d\n",PORT_data.ctrl.timeout*10);
		if( PORT_data.ctrl.retries != 0 ) fprintf(w_fp,"@NR%d\n",PORT_data.ctrl.retries);
#ifndef MODBUS_TCP
		if( PORT_data.uHdr.rtype == 1 ) fprintf(w_fp,"@LC ON\n");
		else if( PORT_data.uHdr.rtype == 2 ) fprintf(w_fp,"@LR ON\n");
		else if( PORT_data.uHdr.rtype == 3 ) fprintf(w_fp,"@LB ON\n");
#else
		if( PORT_data.lHdr.rtype == 1 ) fprintf(w_fp,"@LC ON\n");
		else if( PORT_data.lHdr.rtype == 2 ) fprintf(w_fp,"@LR ON\n");
		else if( PORT_data.lHdr.rtype == 3 ) fprintf(w_fp,"@LB ON\n");
#endif

		dprintf("Protocol configuration %s created\n",fname);
		
		fclose(w_fp);
	}

	return 1;
}

static int read_db(int inf_num)
{
	FILE *fp;
	char fname[256], temp[8];
	char buffer[256], *p;
	int type = 0, type2 = 0;
	int i,j, index = 0, index2;

	sprintf(fname,CONF_DIR"port%02d.conf",inf_num);
	fp = fopen(fname,"r");
	if( fp == NULL )
	{
		dprintf("[INF %d] IDLE due to %s not existed\n",inf_num,fname); 
		return 0;
	}
	else
	{
		while(fgets(buffer,256,fp)!=NULL)
		{
			p = &buffer[0];

			if( *p == '$' || *p == '@' )
			{
				p++;
				if( *p == 'U' )
				{
					p++;
					if(isdigit(*p) == 0)
					{
						dprintf("[INF %d] IDLE due to UART port number error!\n",inf_num);
						return 0;
					}
					INF_st.num_port = atoi(p);
					type = 1;
				}
				else if( *p == 'L' )
				{
					p++;
					if(isdigit(*p) == 0)
					{
						dprintf("[INF %d] IDLE due to Ethernet port number error!\n",inf_num);
						return 0;
					}
					INF_st.num_port = atoi(p);
					type = 2;
				}
				else if( *p == 'D' )
				{
					p++;
					if( *p == 'S' ) sys_type = ACONIS_DS;
				}
				else
				{
					dprintf("[INF %d] IDLE due to port configuration error!\n",inf_num);
					return 0;
				}
			}
			else if( *p == ':' || *p == '*' || *p == ';' )
			{
				p++;
				if( type == 1 )
				{
					char *port_num  = strtok(p," ");
					char *baudrate  = strtok(NULL," ");
					char *databit   = strtok(NULL," ");
					char *paritybit = strtok(NULL," ");
					char *stopbit   = strtok(NULL," ");
					char *flowctrl  = strtok(NULL," ");
				
					INF_st.port[index].port_num = atoi(port_num);
					INF_st.port[index].baudrate = atoi(baudrate);
					INF_st.port[index].databit  = atoi(databit);
					

					if( strcmp(paritybit,"ODD") == 0)
						INF_st.port[index].paritybit = 1;
					else if( strcmp(paritybit,"EVEN") == 0)
						INF_st.port[index].paritybit = 2;
					else
						INF_st.port[index].paritybit = 0;
					INF_st.port[index].stopbit = atoi(stopbit);
					if( strcmp(flowctrl,"Y") == 0 )
						INF_st.port[index].flowctrl = 1;
					else
						INF_st.port[index].flowctrl = 0;
					INF_st.port_type = 1;
				}
				else if( type == 2 )
				{
					char *port_num = strtok(p," ");
					char *ip = strtok(NULL," ");
					char *port = strtok(NULL," ");
					
					INF_st.lport[index].port_num = atoi(port_num);
					if( mmode == MD_MODBUS_MASTER)
					{
						strcpy(INF_st.lport[index].ip,ip);						
						INF_st.lport[index].port = atoi(port);							
					}
					else	//MODBUS_SERVER 인경우 자신의 IP를 사용하고 502포트를 사용하도록 바꿔줄 필요가 있음.
					{
						strcpy(INF_st.lport[index].ip,ip);						
						INF_st.lport[index].port = 4000;				
					}										
					INF_st.port_type = 2;
				}
				
				index++;
			}
		}

		fclose(fp);
		if( INF_st.num_port != index )
		{
			dprintf("[INF %d] IDLE due to port no. configuration error!\n",inf_num);
			return 0;
		}
	}

#ifdef NMEA
	index = 0;
	type = 0;
	sprintf(fname,CONF_DIR"NMEA.def");
	fp = fopen(fname,"r");
	if( fp == NULL )
	{
		dprintf("[INF %d] IDLE due to %s not existed\n",inf_num,fname);
		return 0;
	}
	else
	{
		while(fgets(buffer,256,fp)!=NULL)
		{
			p = &buffer[0];

			if( *p == '$' || *p == '@' )
			{
				p++;
				if( *p == 'D' )
				{
					p++;
					type = 1;
				}
			}
			else if( *p == ':' || *p == '*' || *p == ';' )
			{
				p++;
				if( type == 1 )
				{
					char *format = strtok(p," ");
					char *max = strtok(NULL," ");
					char *token = strtok(NULL," ");

					index2 = 0;
					type2 = 0;
					while(token != NULL)
					{
						j = 0;
						for( i = 0; i < strlen(token) ; i++)
						{
							if( type2 == 3)
							{
								NMEA_def[index].field[index2].c = token[i];
								type2 = 0;
								index2++;
								break;
#if 0
								if(token[i] == ']')
								{
									temp[j] = 0;
									NMEA_def[index].field[index2].c = token[i];
									j = 0;
									type2 = 0;
								}
								else
									temp[j++] = token[i];
#endif
							}
							else if( type2 == 2)
							{
								if(token[i] == ',' || token[i] == ']' )
								{
									temp[j] = 0;
									NMEA_def[index].field[index2].type = atoi(temp);
									j = 0;
									if( token[i] == ']' )
									{
										type2 = 0;
										index2++;
										break;
									}
									else
										type2 = 3;
								}
								else
									temp[j++] = token[i];
							}
							else if(type2 == 1)
							{
								if(token[i] == ',')
								{
									temp[j] = 0;
									index2 = atoi(temp);
									index2 -= 1;
									if( index2 < 0 || index2 > MAX_NO_FIELD-1 )
									{
										dprintf("[INF %d] IDLE due to NMEA def error\n",inf_num);
										return 0;
									}
									j = 0;
									type2 = 2;
								}
								else
									temp[j++] = token[i];
							}					
							else if(token[i] == '[')
								type2 = 1;
							else
								;
						}

						token = strtok(NULL," ");
					}

					strncpy(NMEA_def[index].format,format,5);
					NMEA_def[index].max = atoi(max);
					NMEA_def[index].o = 1;

					index++;
				}
			}
		}

		fclose(fp);
	}
#endif

	dprintf("System Version: %s\n",sys_type?"ACONIS-DS":"ACONIS-2000");
	dprintf("Port: %s%s\n",(INF_st.port_type==2)?"LAN":"UART",(INF_st.num_port==2)?" (Dual)":"");
	for(i = 0; i< INF_st.num_port ; i++)
	{
		if( INF_st.port_type == 1 )
		{
			if( INF_st.port[i].paritybit == 0 ) strcpy(temp,"None");
			if( INF_st.port[i].paritybit == 1 ) strcpy(temp,"Odd");
			if( INF_st.port[i].paritybit == 2 ) strcpy(temp,"Even");
			
			dprintf("\t%d - CH%d %d %d %s %d %s\n",i+1,
				INF_st.port[i].port_num,INF_st.port[i].baudrate,
				INF_st.port[i].databit,temp,
				INF_st.port[i].stopbit,(INF_st.port[i].flowctrl==0)?"N":"Y");
		}
		else if( INF_st.port_type == 2 )
		{
			dprintf("\t%d - EXL%d %s %d\n",i+1,
				INF_st.lport[i].port_num,INF_st.lport[i].ip,
				INF_st.lport[i].port);
		}
		else
		{
			dprintf("\t%d - Undefined\n",i+1);
		}
	}

	return 1;
}

static int close_db()
{
	return 1;
}

static int port_open(int inf_num)
{
	FILE *fp;
	char fname[256], temp[16];
	char buffer[1024], *p;
	int type, type2 = 0, cmd_type = 0, lr_type;
	int i,j, index, index2;

	index = 0;
	type = 0;
	sprintf(fname,CONF_DIR"ptc%02d.conf",inf_num);
	fp = fopen(fname,"r");
	if( fp == NULL )
	{
		dprintf("[INF %d] IDLE due to %s not existed\n",inf_num,fname);
		return 0;
	}
	else
	{
		while(fgets(buffer,1024,fp)!= NULL)
		{
			p = &buffer[0];
	
			if( *p == '$' || *p == '@' )
			{
				p++;
				if(*p == 'R')
				{
					p++;
					INF_st.num_read_cmd = atoi(p);
					type = 1;
					index = 0;
				}
				else if(*p == 'W')
				{
					p++;
					INF_st.num_write_cmd = atoi(p);
					type = 2;
					index = 0;
				}
				else if(*p == 'A')
				{
					p++;
					INF_st.num_addr_map = atoi(p);
					type = 3;
					index = 0;

#if 1
					INF_st.addr_map[0].system = 0;
					INF_st.addr_map[0].address = 0;
					INF_st.addr_map[0].secondary = 0;
					INF_st.addr_map[0].alarm = 30; // 2013-01-07 dockwise
					INF_st.addr_map[0].alarm_count = 0;
#endif
				}
				else if(*p == 'B') // bypass
				{
					p++;

					char *ip = strtok(p," ");
					char *port = strtok(NULL," ");

					if( ip != NULL )
						strncpy(INF_st.bypass.ip,ip,32);
					else
						strncpy(INF_st.bypass.ip,"127.0.0.1",32);

					if( port != NULL )
						INF_st.bypass.port = atoi(port);
					else
						INF_st.bypass.port = 5604;

					type = 4;
				}
				else if(*p == 'T')
				{
					p++;
					if(*p == 'T')
					{
						p++;
						INF_st.tx_time = atoi(p);
					}
					else if(*p == 'R')
					{
						p++;
						INF_st.rx_time = atoi(p);
					}
					else if(*p == 'O')
					{
						p++;
						INF_st.timeout = atoi(p);
					}
				}
				else if(*p == 'N')
				{
					p++;
					if(*p == 'R')
					{
						p++;
						INF_st.num_retry = atoi(p);
					}
				}
				else if(*p == 'L')
				{
					p++;
					if(*p == 'C')
					{
						lr_type = LINECHECK_REDUNDANCY;
					}
					else if(*p == 'B')
					{
						lr_type = LINEBOTH_REDUNDANCY;
					}
					else if(*p == 'R')
					{
						lr_type = LINEREAD_REDUNDANCY;
					}
					else
						lr_type = DEFAULT_REDUNDANCY;

					p++;
					char *status = strtok(p," ");

					if( status != NULL )
					{
						if(strncmp(status,"ON",2) == 0)
							INF_st.redundancy = lr_type;
						dprintf("Redundancy Type : %d\n",INF_st.redundancy);
					}
				}
			}
			else if( *p == ':' || *p == '*' || *p == ';' )
			{
				if(*p == '*' ) cmd_type = 1; // pulse-type cmd
				else cmd_type = 0;

				p++;
				if( type == 1 )
				{
#ifndef NMEA	

					char *unit_id   = strtok(p," ");					
					char *func_code = strtok(NULL," ");
					char *address   = strtok(NULL," ");
					char *points    = strtok(NULL," ");
					char *mapping   = strtok(NULL," ");
					char *priority  = strtok(NULL," ");

					INF_st.read_cmd[index].unit_id       = atoi(unit_id);
					INF_st.read_cmd[index].function_code = atoi(func_code);
					INF_st.read_cmd[index].address       = atoi(address);
					INF_st.read_cmd[index].points        = atoi(points);
					INF_st.read_cmd[index].mapping       = atoi(mapping);
					if( priority == NULL )
						INF_st.read_cmd[index].priority = 0;
					else
						INF_st.read_cmd[index].priority = atoi(priority);
					INF_st.read_cmd[index].aging = 0;
					dprintf("Read unit_id %d function code %d address %d point %d\n",
							INF_st.read_cmd[index].unit_id,
							INF_st.read_cmd[index].function_code,
							INF_st.read_cmd[index].address,
							INF_st.read_cmd[index].points
							);

#else
					char *format = strtok(p," ");
					char *talker = strtok(NULL," ");
					char *option = strtok(NULL," ");
					char *token = strtok(NULL," ");

					index2 = 0;
					type2 = 0;
					while(token != NULL)
					{
						j = 0;
						for( i = 0; i < strlen(token) ; i++)
						{
							if( type2 == 2)
							{
								if(token[i] == ']')
								{
									temp[j] = 0;
									INF_st.nmea_read[index].field[index2].address = atoi(temp);
									j = 0;
									type2 = 0;
									index2++;
								}
								else
									temp[j++] = token[i];
							}
							else if(type2 == 1)
							{
								if(token[i] == ',')
								{
									temp[j] = 0;
									INF_st.nmea_read[index].field[index2].position = atoi(temp);
									j = 0;
									type2 = 2;
								}
								else
									temp[j++] = token[i];
							}					
							else if(token[i] == '[')
								type2 = 1;
							else
								;
						}

						token = strtok(NULL," ");
					}

					strncpy(INF_st.nmea_read[index].format,format,5);
					strncpy(INF_st.nmea_read[index].talker,talker,5);
					if( strncmp(option,"B",1) == 0 ) INF_st.nmea_read[index].option = OPT_BYPASS;
					else if( strncmp(option,"K",1) == 0 ) INF_st.nmea_read[index].option = OPT_SECOND_FORMAT;
					else if( strncmp(option,"R1",2) == 0 ) INF_st.nmea_read[index].option = OPT_REDIRECT1;
					else if( strncmp(option,"R2",2) == 0 ) INF_st.nmea_read[index].option = OPT_REDIRECT2;
					else if( strncmp(option,"R3",2) == 0 ) INF_st.nmea_read[index].option = OPT_REDIRECT3;
					else INF_st.nmea_read[index].option = 0;
#endif
					index++;
				}
				if( type == 2 )
				{
#ifndef NMEA
					char *address = strtok(p," ");
					char *write_unit_id = strtok(NULL," ");
					char *write_func_code = strtok(NULL," ");
					char *write_address = strtok(NULL," ");
					char *write_data = strtok(NULL," ");
				
					INF_st.write_cmd[index].address = atoi(address);
					INF_st.write_cmd[index].write_unit_id = atoi(write_unit_id);
					INF_st.write_cmd[index].write_function_code = atoi(write_func_code);
					INF_st.write_cmd[index].write_address = atoi(write_address);
					if( write_data == NULL )
						INF_st.write_cmd[index].write_data = 0;
					else
						INF_st.write_cmd[index].write_data = atoi(write_data);
					INF_st.write_cmd[index].write_type = 0;
					if( cmd_type ) INF_st.write_cmd[index].write_type |= (1<<4);
					INF_st.write_cmd[index].repository = 0;
#else
					char *format = strtok(p," ");
					char *talker = strtok(NULL," ");
					char *token = strtok(NULL," ");

					index2 = 0;
					type2 = 0;
					while(token != NULL)
					{
						j = 0;
						for( i = 0; i < strlen(token) ; i++)
						{
							if( type2 == 3 )
							{
								if(token[i] == ']' )
								{
									temp[j] = 0;
									strcpy(INF_st.nmea_write[index].field[index2].s,temp);
									j = 0;
									type2 = 0;
									index2++;
								}
								else
									temp[j++] = token[i];
							}
							else if( type2 == 2 )
							{
								if( token[i] == ',' || token[i] == ']')
								{
									temp[j] = 0;
									INF_st.nmea_write[index].field[index2].address = atoi(temp);
									j = 0;
									if( token[i] == ']' )
									{
										type2 = 0;
										index2++;
									}
									else
										type2 = 3;
								}
								else
									temp[j++] = token[i];
							}
							else if( type2 == 1 )
							{
								if(token[i] == ',')
								{
									temp[j] = 0;
									INF_st.nmea_write[index].field[index2].position = atoi(temp);
									j = 0;
									type2 = 2;
								}
								else
									temp[j++] = token[i];
							}					
							else if(token[i] == '[')
								type2 = 1;
							else
								;
						}

						token = strtok(NULL," ");
					}

					strncpy(INF_st.nmea_write[index].format,format,5);
					strncpy(INF_st.nmea_write[index].talker,talker,5);
#endif

					index++;	
				}
				if( type == 3 )
				{
					char *system = strtok(p," ");
					char *address1 = strtok(NULL," ");
					char *address2 = strtok(NULL," ");
					char *alarm = strtok(NULL," ");
					
					INF_st.addr_map[index].system = atoi(system);
					INF_st.addr_map[index].address = atoi(address1);
					INF_st.addr_map[index].secondary = atoi(address2);
					if( alarm  == NULL )
						INF_st.addr_map[index].alarm = 30; // 2013-01-07 dockwise
					else
						INF_st.addr_map[index].alarm = atoi(alarm);
					INF_st.addr_map[index].alarm_count = 0;

					index++;	
				}
				if( type == 4 )
				{
				}
			}
		}

		fclose(fp);
	}

#ifndef NMEA
	if( INF_st.tx_time == 0 ) INF_st.tx_time = 10;
	if( INF_st.rx_time == 0 ) INF_st.rx_time = 500;
	if( INF_st.timeout == 0 ) INF_st.timeout = 500;
	if( INF_st.num_retry == 0 ) INF_st.num_retry = 5;

	dprintf("Modbus Read: %d\n",INF_st.num_read_cmd);
	for(i = 0; i< INF_st.num_read_cmd;i++)
	{
		dprintf("\t%d - %02X %02X %04X %04X %04X (%dth)\n",i+1,
			INF_st.read_cmd[i].unit_id,INF_st.read_cmd[i].function_code,
			INF_st.read_cmd[i].address,INF_st.read_cmd[i].points,INF_st.read_cmd[i].mapping,
			INF_st.read_cmd[i].priority);
	}
	dprintf("Modbus Write Map: %d\n",INF_st.num_write_cmd);
	for(i = 0; i < INF_st.num_write_cmd;i++)
	{
		sprintf(buffer,"\t%d - %04X %02X %02X %04X",i+1,
			INF_st.write_cmd[i].address,INF_st.write_cmd[i].write_unit_id,
			INF_st.write_cmd[i].write_function_code,INF_st.write_cmd[i].write_address);
		if(INF_st.write_cmd[i].write_data == 0)
			sprintf(temp,"%s\n",(INF_st.write_cmd[i].write_type>=16)?"!":"");
		else
			sprintf(temp," %04X\n",INF_st.write_cmd[i].write_data);
		strcat(buffer,temp);
		dprintf("%s",buffer);
	}
#else
	if( INF_st.tx_time == 0 ) INF_st.tx_time = 1000;
	if( INF_st.rx_time == 0 ) INF_st.rx_time = 1000;
	if( INF_st.timeout == 0 ) INF_st.timeout = 500;
	if( INF_st.num_retry == 0 ) INF_st.num_retry = 1;

	dprintf("NMEA DEF: \n");
	for(i = 0; i< MAX_NMEA_DEFINE; i++)
	{
		if(NMEA_def[i].o)
		{
			sprintf(buffer,"\t%d - %s %d",i+1,NMEA_def[i].format,NMEA_def[i].max);

			for( j = 0; j < MAX_NO_FIELD; j++ )
			{
				if( NMEA_def[i].field[j].type != TYPE_UNDEFINE )
				{
					sprintf(temp," [%d,%d",j+1,NMEA_def[i].field[j].type);
					strcat(buffer,temp);
					if( NMEA_def[i].field[j].type == TYPE_DEFINED )
					{
						sprintf(temp,",%c",NMEA_def[i].field[j].c);
						strcat(buffer,temp);
					}
					sprintf(temp,"]");
					strcat(buffer,temp);
				}
			}
		
			dprintf("%s\n",buffer);
		}
	}

	dprintf("NMEA Read: %d\n",INF_st.num_read_cmd);
	for(i = 0; i< INF_st.num_read_cmd;i++)
	{
		sprintf(buffer,"\t%d - %s %s",i+1,
			INF_st.nmea_read[i].format,INF_st.nmea_read[i].talker);

		for( j = 0; j < 15; j++ )
		{
			if( INF_st.nmea_read[i].field[j].position != 0 )
			{
				sprintf(temp," [%02X,%04X]",INF_st.nmea_read[i].field[j].position,
					INF_st.nmea_read[i].field[j].address);
				strcat(buffer,temp);
			}
		}
		
		dprintf("%s\n",buffer);
	}

	dprintf("NMEA Write: %d\n",INF_st.num_write_cmd);
	for(i = 0; i< INF_st.num_write_cmd;i++)
	{
		sprintf(buffer,"\t%d - %s %s",i+1,
			INF_st.nmea_write[i].format,INF_st.nmea_write[i].talker);

		for( j = 0; j < 15; j++ )
		{
			if( INF_st.nmea_write[i].field[j].position != 0 )
			{
				sprintf(temp," [%02X,%04X]",INF_st.nmea_write[i].field[j].position,
					INF_st.nmea_write[i].field[j].address);
				strcat(buffer,temp);
			}
		}
		
		dprintf("%s\n",buffer);
	}

#endif
	dprintf("Wait time (tx: %d ms, rx: %d ms)  Timeout: %d ms\n",
		INF_st.tx_time,INF_st.rx_time,INF_st.timeout);

	return 1;
}

static int port_close()
{
	return 1;
}

static int create_threads(int inf_num)
{
	if( mmode == MD_MODBUS_MASTER )
	{
		pthread_create(&INF_thr, NULL, (void *)&modbusm_thread,(void *)inf_num);
		pthread_create(&PORT_thr, NULL, (void *)&modbusm_bufm,(void *)inf_num);
		pthread_create(&AUX_thr, NULL, (void *)&modbusm_pulse,(void *)inf_num);
//		if(INF_st.num_port == 2){
			pthread_create(&PS_thr, NULL, (void *)&modbusm_reopen_thread,(void *)inf_num);
//		}
	}
	else if( mmode == MD_MODBUS_SLAVE )
	{
		pthread_create(&INF_thr, NULL, (void *)&modbuss_thread,(void *)inf_num);
		pthread_create(&PORT_thr, NULL, (void *)&modbuss_bufm,(void *)inf_num);
	}
	else if( mmode == MD_NMEA_READ )
	{
		pthread_create(&INF_thr, NULL, (void *)&nmeas_thread,(void *)inf_num);
	}
	else if( mmode == MD_NMEA_WRITE )
	{
		pthread_create(&INF_thr,  NULL, (void *)&nmeam_thread,(void *)inf_num);
		pthread_create(&PORT_thr, NULL, (void *)&nmeam_bufm,  (void *)inf_num);
	}
	else if( mmode == MD_NMEA_READWRITE)
	{
		pthread_create(&INF_thr,  NULL, (void *)&nmeas_thread,(void *)inf_num);	  
		pthread_create(&INF1_thr, NULL, (void *)&nmeam_thread,(void *)inf_num);
		pthread_create(&PORT_thr, NULL, (void *)&nmeam_bufm,  (void *)inf_num);		
	}
#ifdef FDS
	else if( mmode == MD_FDS_READ )
	{
		pthread_create(&INF_thr, NULL, (void *)&fdsm_thread,(void *)inf_num);
		//pthread_create(&PORT_thr, NULL, (void *)&nmeam_bufm,(void *)inf_num);
	}	
#endif	
#ifdef ICCP
	else if( mmode == MD_ICCP_READ )
	{
		pthread_create(&INF_thr, NULL, (void *)&iccpm_thread,(void *)inf_num);		
	}	
#endif
#ifdef DG
	else if( mmode == MD_DG_READ )
	{
		pthread_create(&INF_thr, NULL, (void *)&dgm_thread,(void *)inf_num);
	}	
#endif
#ifdef SNDC
	else if( mmode == MD_SNDC_READWRITE )
	{
		pthread_create(&INF_thr, NULL, (void *)&sndcm_thread,(void *)inf_num);
		pthread_create(&INF1_thr, NULL, (void *)&sndcs_thread,(void *)inf_num);
	}
#endif
#ifdef CHEM
	else if( mmode == MD_CHEM_READWRITE )
	{
		pthread_create(&INF_thr, NULL, (void *)&chemm_thread,(void *)inf_num);
		pthread_create(&PORT_thr, NULL, (void *)&chemm_bufm,  (void *)inf_num);
	}
#endif
#ifdef NDMC
	else if( mmode == MD_NDMC_READWRITE )
	{
		pthread_create(&INF_thr, NULL, (void *)&ndmcm_thread,(void *)inf_num);
		pthread_create(&INF1_thr, NULL, (void *)&ndmcs_thread,(void *)inf_num);
		pthread_create(&PORT_thr, NULL, (void *)&ndmcm_bufm,  (void *)inf_num);
	}
#endif
#ifdef COND
	else if( mmode == MD_COND_READWRITE )
	{
		pthread_create(&INF_thr, NULL, (void *)&condm_thread,(void *)inf_num);
//		pthread_create(&INF1_thr, NULL, (void *)&ndmcs_thread,(void *)inf_num);
		pthread_create(&PORT_thr, NULL, (void *)&condm_bufm,  (void *)inf_num);
	}
#endif
	else
	{
		dprintf("[INF %d] IDLE due to undefined protocol mode\n",inf_num);
		return 0;
	}

	pthread_create(&COMLOG_thr, NULL, (void *)&manage_comstatus_thread,(void *)inf_num);

	return 1;
}

static int kill_threads()
{
	return 1;
}

static int manage_threads()
{
	return 1;
}

int main(int argc, char* argv[])
{
	int i,state, inf_num, retries = 0;
	int mode, last_mode = IDLE;
	FILE *fp;

#ifndef NMEA
#ifdef  FDS
	if( argc != 2 )
	{
		printf("[%d]Usage: %s <interface_num>\n",argc,argv[0]);
		return -1;
	}
#elif ICCP
	if( argc != 2 )
	{
		printf("[%d]Usage: %s <interface_num>\n",argc,argv[0]);
		return -1;
	}
#elif DG
	if( argc != 2 )
	{
		printf("[%d]Usage: %s <interface_num>\n",argc,argv[0]);
		return -1;
	}
#elif SNDC
	if( argc != 2 )
	{
		printf("[%d]Usage: %s <interface_num>\n",argc,argv[0]);
		return -1;
	}
#elif CHEM
	if( argc != 2 )
	{
		printf("[%d]Usage: %s <interface_num>\n",argc,argv[0]);
		return -1;
	}
#elif NDMC
	if( argc != 2 )
	{
		printf("[%d]Usage: %s <interface_num>\n",argc,argv[0]);
		return -1;
	}
#elif COND
	if( argc != 2 )
	{
		printf("[%d]Usage: %s <interface_num>\n",argc,argv[0]);
		return -1;
	}
#else
	if( argc != 2 && argc != 3 && argc != 4)
	{
		printf("Usage: %s <interface_num> [<-slave>][<-little>]\n",argv[0]);
		return -1;
	}
#endif	
#else
	if( argc != 3 && argc != 4 )
	{
		printf("Usage: %s <interface_num> <nmea> [<-write>]\n",argv[0]);
		return -1;
	}
#endif
	inf_num = atoi(argv[1]);
	if( inf_num <= 0 || inf_num > MAX_INTERFACE ) return -1;
	interface_num = inf_num;

#ifndef NMEA
#ifdef FDS
	mmode = MD_FDS_READ;
#elif ICCP
	mmode = MD_ICCP_READ;
#elif DG
	mmode = MD_DG_READ;	
#elif SNDC
	mmode = MD_SNDC_READWRITE;
#elif CHEM
	mmode = MD_CHEM_READWRITE;
#elif NDMC
	mmode = MD_NDMC_READWRITE;
#elif COND
	mmode = MD_COND_READWRITE;
#else
	if( argc == 3 || argc == 4)
	{
		if(strncmp(argv[2],"-slave",6) == 0)
			mmode = MD_MODBUS_SLAVE;
		else
			mmode = MD_MODBUS_MASTER;
			
		if(strncmp(argv[2],"-little",7) == 0)
			endian_type = MD_LITTLE_ENDIAN;
		else
			endian_type = MD_BIG_ENDIAN;	
		if(argc == 4)
		{
			if(strncmp(argv[3],"-little",7) == 0)
				endian_type = MD_LITTLE_ENDIAN;
			else
				endian_type = MD_BIG_ENDIAN;	
		}				
	}
	else
	{
		mmode = MD_MODBUS_MASTER;
		endian_type = MD_LITTLE_ENDIAN; //2020.08.31 khs endian_type = MD_BIG_ENDIAN;	
	}

	if(endian_type == MD_LITTLE_ENDIAN)	printf("\n>>Endian_type is LITTLE!<<\n");
	else printf("\n>>Endian_type is BIG!<<\n");
#endif		
#else
	if( argc == 4 )
	{
		if(strncmp(argv[3],"-write",6) == 0)
			mmode = MD_NMEA_WRITE;
		else
		if(strncmp(argv[3],"-read", 5) == 0)
			mmode = MD_NMEA_READ;
		else
			mmode = MD_NMEA_READWRITE;
	}
	else
		mmode = MD_NMEA_READWRITE;

#endif

	make_daemon();			//프로그램을 background에서 실행

#ifdef RUN_CHECK
	sprintf(file_name,"/tmp/%s%s",APP_NAME,argv[1]);
	fp = fopen(file_name,"w");
	if( fp == NULL )
	{
		dprintf("[INF %d] No space!!!\n",inf_num);
		return -1;
	}
	fprintf(fp,"%d",getpid());
	fclose(fp);
#endif

	printf("\n-------------- HiSCM INF %2d  ---------------\n",inf_num);
	open_logs(1);
	dprintf("--------------- HiSCM INF %2d Start ---------------\n",inf_num);

	if( _open_link(inf_num) > 0 )
	{
		printf("Open Link [INF: %d]\n",inf_num);
		if( open_debug(inf_num) > 0 )
			INF_st.debugfd.valid = 1;
		else
			INF_st.debugfd.valid = 0;
		mode = INITIALIZE;
	}
	else
	{
		dprintf("[INF %d] EXIT\n",inf_num);
		return -1;
	}

	while(1)
	{
		switch(mode)
		{
		case INITIALIZE:
			printf("[INF %d] Initialize...\n",inf_num);
			initial();
		case OPEN_DB:
			printf("[INF %d] Open DB\n",inf_num);
			mode = (open_db(inf_num)?READ_DB:IDLE);
			break;
		case READ_DB:
			printf("[INF %d] Read Port\n",inf_num);
			mode = (read_db(inf_num)?CLOSE_DB:IDLE);
			break;
		case CLOSE_DB:
			close_db();
			mode = KILL_THREAD;
			break;
		case PORT_OPEN:
			printf("[INF %d] Read Protocol\n",inf_num);
			mode = (port_open(inf_num)?CREATE_THREAD:PORT_OPEN);
			break;
		case PORT_CLOSE:
			mode = (port_close()?PORT_OPEN:PORT_CLOSE);
			break;
		case CREATE_THREAD:
			printf("[INF %d] Ready to Start...\n",inf_num);
			mode = (create_threads(inf_num)?MANAGE_THREAD:CREATE_THREAD);
			if( mode == MANAGE_THREAD ) printf("[INF %d] Run\n",inf_num);
			break;
		case KILL_THREAD:
			mode = (kill_threads()?PORT_CLOSE:KILL_THREAD);
			break;
		case MANAGE_THREAD:
			manage_threads();
		case IDLE:
			if( db_change() )
			{
				dprintf("[INF %d] DB CHANGE!!\n",inf_num);
				mode = OPEN_DB;
			}
			else
				sleep(5);
			break;
		}
		
		if( mode < MANAGE_THREAD )
		{
			if( mode == last_mode )
				retries++;

			if( retries > 5 )
				mode = IDLE;
			usleep(200000);
		}

		last_mode = mode;
	}

	close(INF_st.sockfd);
	return -1;
}

