#ifndef	_SNDC_H
#define	_SNDC_H

#define MSG_MAX_LEN		512

#define ASCII_STX	0x02
#define ASCII_ETX	0x03

#pragma pack(1)
typedef struct{
	__u8	stx;	// STX
	__u8	spare;
	__u16	msg_id;
	__u16	msg_st;
	__u16	len;	// all message byte begining with 13(header)
	__u32	time_stamp;
}HEADER;	// 12Byte

typedef struct{
	__u16	cs;
	__u8	spare;
	__u8	etx;	// ETX
}TAIL;		// 4Byte

typedef struct{		// 12EA : 40Byte
	float ote_comp; // 4Byte
	float ote_trim;

	float sr_act;
	float hpf_act;
	float hpa_act;

	float sr_ord;
	float hpf_ord;
	float hpa_ord;

	short mode;		// 2Byte
	short sr_valid;
	short hpf_valid;
	short hpa_valid;
}SNDC_DAT;	// 40Byte

typedef struct{
	short heartbeat;
}SNDC_TXD;	// 40Byte

#pragma pack()
#endif		// _NDMC_H
