/***********************************************************************************
  Filename:     chem.c

  Description:

***********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#include <asm/types.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <termios.h>

#include "common.h"

#include "../app.h"
#include "../interface.h"
#include "../modbus.h"
#include "../utils.h"
#include "../log.h"
#include "chem.h"

//#define	DEBUG
#ifndef	DEBUG
	#define	dprintf(format, args...) log_access(format, ## args)
#else
	#define	dprintf(format, args...) log_access(format, ## args); printf(format, ## args)
#endif	// DEBUG

extern INF_ST INF_st;
extern int fd_index;
extern unsigned short INF_mem[MAX_INF_TAG];
extern pthread_mutex_t mutex_inf;
extern CMD_Q CMD_q;

extern char sendbuff[DEFAULT_BUFF_SIZE];
extern char recvbuff[DEFAULT_BUFF_SIZE];

int main_line;
/*
static int cond_write(int in, int out, int port_num) // Event
{
	int i, sendSize, ch_num;
	int transmited_bytes;
	modbus_res_hdr_ext *pext;
	int rxd = 0;
	char ch[4] = {'t','T','H','?'};

	modbus_req_hdr *wreq = (modbus_req_hdr *)recvbuff;

	for(i = 0; i < INF_st.num_write_cmd; i++)
	{
		if(INF_st.write_cmd[i].address == wreq->address)
		{
			break;
		}
		else
		{
			dprintf("\tNot Match write address: %d\n",INF_st.write_cmd[i].address);
		}
	}

	if(i >= INF_st.num_write_cmd){
		dprintf("\twrite no match! - forced query selection\n");
	}
	else
	{
		if( wreq->function_code == 0x06 || wreq->function_code == 0x10 )
		{
			ch_num = INF_st.write_cmd[i].write_address;
			sendbuff[0] = ch[ch_num];
			if( wreq->function_code == 0x06 )
			{
				rxd = wreq->points;
			}
			else if( wreq->function_code == 0x10 )
			{
				rxd = recvbuff[7]<<8;
				rxd |= recvbuff[8];
			}

			if(ch_num == 3)
			{
				sprintf(&sendbuff[1],"\r");
			}
			else sprintf(&sendbuff[1],"[%2d]\r",rxd);

			dprintf("%s\n",sendbuff);
		}
		else
		{
			dprintf("\tFunction Code Mismatch! : %d\n",wreq->function_code);
			return PERIODIC_REQ;
		}
	}

	transmited_bytes = inf_write(out,sendbuff,sendSize,PRIMARY);

	if(transmited_bytes > 0) update_line_info(main_line, TX);

	return PERIODIC_REQ;
}
*/
static int chem_write(int in, int out, int port_num) // Event
{
	int i, sendSize;
	int transmited_bytes;
	modbus_res_hdr_ext *pext;
	int rxd = 0;
	unsigned short crc;

	modbus_req_hdr *wreq = (modbus_req_hdr *)recvbuff;
//printf("\t\t\t\t %d, %d, %d, %d, %d, %d, %d\n", recvbuff[0],recvbuff[1],recvbuff[2],recvbuff[3],recvbuff[4],recvbuff[5],recvbuff[6]);
//printf("\t\t\t\t %d, %d, %d, %d, %d, %d, %d\n", recvbuff[7],recvbuff[8],recvbuff[9],recvbuff[10],recvbuff[11],recvbuff[12],recvbuff[13]);
//printf("\t\t\t\t %d, %d, %d, %d, %d, %d, %d\n", recvbuff[14],recvbuff[15],recvbuff[16],recvbuff[17],recvbuff[18],recvbuff[19],recvbuff[20]);
	//printf("\t\t\t\t << %d, %d >> %d \n", INF_st.write_cmd[0].address, wreq->address, wreq->points);

	if(INF_st.write_cmd[0].address == wreq->address)
	{ 
		
		
		sendbuff[0] = 0x0a;
		sendbuff[1] = 0x05;
		sendbuff[2] = 0xfb;
		sendbuff[3] = 0x0c;
		sendbuff[4] = 0x00;
		sendbuff[5] = 0x00;

		//sendbuff[6] = 0xff;//wreq->points;;//New State
		sendbuff[6] = recvbuff[8];//New State

		sendbuff[7] = 'P';
		sendbuff[8] = 'o';
		sendbuff[9] = 'w';
		sendbuff[10] = 'e';
		sendbuff[11] = 'r';
		sendbuff[12] = 'i';
		sendbuff[13] = 'a';

		crc = CRC16(sendbuff, 14) & 0xFFFF;
		sendbuff[14] = crc>>8;
		sendbuff[15] = crc&0x00FF;
		sendSize = 16;

		//memcpy(sendbuff, pdata, sendSize); 

		transmited_bytes = inf_write(out,sendbuff,sendSize,PRIMARY);
		if(transmited_bytes > 0) update_line_info(main_line, TX);

		return PERIODIC_REQ;
		


		/*if(1)//if( wreq->function_code == 0x06 || wreq->function_code == 0x10 )
		{
			if( wreq->function_code == 0x06 )
			{
				rxd = wreq->points;
			}
			else if( wreq->function_code == 0x10 )
			{
				rxd = recvbuff[7]<<8;
				rxd |= recvbuff[8];
			}

			if(rxd == 1)
			{
				pdata->stx 	= ASCII_STF;
				pdata->mode = IBIT_REQ_MODE;
				pdata->data = IBIT_REQ_DATA;
				pdata->cs 	= pdata->mode + pdata->data;
				pdata->etx 	= ASCII_EOF;
				sendSize 	= sizeof(CHEM_DAT);
				dprintf("\tIBIT Command OK!\n");
			}
			else
			{
				dprintf("\tIBIT rxd Data! : 0x%04X\n",rxd);
			}

		}
		else
		{
			dprintf("\tFunction Code Mismatch! : %d\n",wreq->function_code);
			return PERIODIC_REQ;
		}*/
	}
	else
	{
		i=0;
//		dprintf("\tNot Match write address: %d\n",INF_st.write_cmd[i].address);
	}

	sendbuff[0] = 0x0a;
	sendbuff[1] = 0x05;
	sendbuff[2] = 0xfc;
	sendbuff[3] = 0x04;
	sendbuff[4] = 0x00;
	sendbuff[5] = 0x00;

	crc = CRC16(sendbuff, 6) & 0xFFFF;
	sendbuff[6] = crc>>8;
	sendbuff[7] = crc&0x00FF;
	sendSize = 8;

	/*pdata->stx 	= 0x0a;//ASCII_STF;
	pdata->mode = 0x05;//IBIT_REQ_MODE;
	pdata->data = 0xfc;//IBIT_REQ_DATA;
	pdata->cs 	= 0x04;//pdata->mode + pdata->data;
	pdata->etx 	= 0x00;//ASCII_EOF;
	sendSize 	= sizeof(CHEM_DAT); */
	//memcpy(sendbuff, pdata, sendSize);


	transmited_bytes = inf_write(out,sendbuff,sendSize,PRIMARY);

	if(transmited_bytes > 0) update_line_info(main_line, TX);

	return PERIODIC_REQ;
}

static int chem_read(int in, int out,int port_num)
{
	char buf[DEFAULT_BUFF_SIZE];
	char writebuff[DEFAULT_BUFF_SIZE];
	modbus_res_hdr_ext *pext;
	C_SENSOR_DATA *pdata;
	int size, len, byte_count;
	unsigned short checksum, crc;
	__u8 *pbyte;
	__u16 sensor_status = 0;
	int i;

	size = sizeof(C_SENSOR_DATA);
	len = inf_read(in, buf, DEFAULT_BUFF_SIZE-16, PRIMARY);

	pdata = (C_SENSOR_DATA *)buf;

/*printf("[%d][%d]\n", __LINE__, len);	
for(i = 0; i < len; i++)
{
	printf("%02X ", buf[i]);
}
printf("\n");*/

	//2016.11.29-20:47,khs if(size != len)
	/*if((size + 8)!= len)
	{
		dprintf("[MM %d] CHEM Read Length Mismatch [%d]!\n",port_num,len);
		return -1;
	}*/

	if(0x05 != pdata->message_header_func_code)
	{
		dprintf("[MM %d] CHEM Wrong Header Func. Code [%02X]!\n",port_num, pdata->message_header_func_code);
		return -1;
	}

	if(0x0a != pdata->message_header)
	{
		dprintf("[MM %d] CHEM Wrong Header [%02X]!\n",port_num, pdata->message_header);
		return -1;
	}

	pext = (modbus_res_hdr_ext *)writebuff;
	pext->unit_id = port_num;
	pext->function_code = 0x03;

	if(pdata->data_func_code == 0xfc){	// ICD : GetDetection Values
		len = 149;
		pext->address = 0;//address;
		pext->byte_count = (6 + len);//byte_count;

		memcpy(pext->data, buf+6, len);
		//memcpy(pext->data,data2,byte_count);
		crc = CRC16((__u8*)pext,6+ len)&0xFFFF;
		writebuff[6+ len] = crc>>8;
		writebuff[7+ len] = crc&0x00FF;

		port_up(out,writebuff,8+ len, port_num);
	}
	if(pdata->data_func_code == 0xfb){	// ICD : Power System
		len = 2;
		pext->address = 80;//htons(75);	//address;
		pext->byte_count = len;//byte_count;

		memcpy(pext->data, buf+6, len);
dprintf("PowerSystem: %02x\n", *(buf+6));
		//memcpy(pext->data,data2,byte_count);
		crc = CRC16((__u8*)pext,6+ len)&0xFFFF;
		writebuff[6+ len] = crc>>8;
		writebuff[7+ len] = crc&0x00FF;

		port_up(out,writebuff,8+ len, port_num);
	}
	return 1;
}

static int select_nextquery(int port_num, int mode)
{
	int i;
	int next_mode = PERIODIC_REQ;
	char head,tail,length;
	char str[40], temp[8];

//printf("-->>  %d\n", __LINE__);	
//printf("-->> port_num:%d, mode:%d\n", port_num, mode);
pthread_mutex_lock(&mutex_inf);
	head = CMD_q.write_head;
	tail = CMD_q.write_tail;
pthread_mutex_unlock(&mutex_inf);
//printf("head:%d,tail:%d\n", head, tail);
	if( mode != PERIODIC_REQ_PRI && head != tail )
	{
pthread_mutex_lock(&mutex_inf);
		length = CMD_q.write_length[head];
		memcpy(recvbuff,CMD_q.write_buff[head],length);
		CMD_q.write_head++;
		CMD_q.write_head %= MAX_Q_SIZE;
pthread_mutex_unlock(&mutex_inf);
#if 0
		dprintf("[CMD dequeue inf:%d] head:%d tail:%d\n",port_num,
			CMD_q.write_head,tail);
		sprintf(str,"\t");
		for(i = 0; i < length; i++)
		{
			sprintf(temp,"%02X ",recvbuff[i]);
			strcat(str,temp);
		}
		dprintf("%s\n",str);
#endif
//		dprintf("\tIBIT Event Command Rx!\n");
		next_mode = WRITE_REQ;
	} else{
		recvbuff[2] = 0;
		recvbuff[3] = 0;
	}
	return next_mode;
}

void chemm_bufm(void *arg)
{
	int sockfd;
	int port_num, len, check, i;
	fd_set readfds,	testfds;
	char tempbuff[DEFAULT_BUFF_SIZE],writebuff[16];
	char str[40], temp[8];
	__u8 unit_id, func_code;
	__u16 address, points, byte_count;
	struct timeval timeout;
	char head,tail;
	modbus_res_hdr_ext *preq;
	modbus_res_hdr_ext *pext;

	port_num = (int)arg;
	pthread_detach(pthread_self());

	dprintf(" - Buffer Manager Start [Port: %d]\n",port_num);

	sockfd = INF_st.sockfd;

	FD_ZERO(&readfds);
	FD_SET(sockfd, &readfds);

	preq = (modbus_res_hdr_ext *)tempbuff;

	while(1)
	{
		testfds = readfds;

		timeout.tv_sec = 30;
		timeout.tv_usec = 0;
		if(select(sockfd+1, &testfds, (fd_set*)0, (fd_set*)0, (struct timeval*)&timeout) != 0)
		{
			if( FD_ISSET(sockfd,&testfds) ) {  // Write Command
				len = read(sockfd,tempbuff,MAX_CMD_SIZE);
				if( len >= 6 && len < 256 )
				{
					unit_id = preq->unit_id;
					func_code = preq->function_code;
					address = preq->address;

					if( func_code == write_single_reg || func_code == write_mult_regs )
					{
pthread_mutex_lock(&mutex_inf);
						head = CMD_q.write_head;
						tail = CMD_q.write_tail;
pthread_mutex_unlock(&mutex_inf);
						if( (tail+1)%MAX_Q_SIZE == head )
						{
						//	dprintf("[MM CMD %d] Write buff is full!!\n",port_num);
						}
						else
						{
							if( len == 6 || len == 11 )
							{
pthread_mutex_lock(&mutex_inf);
								memcpy(CMD_q.write_buff[tail],tempbuff,len);
								CMD_q.write_length[tail]=len;
								CMD_q.write_tail++;
								CMD_q.write_tail %= MAX_Q_SIZE;
pthread_mutex_unlock(&mutex_inf);
#if 1
								dprintf("[CMD queue inf:%d] head:%d tail:%d (len:%d)\n",port_num,
									head,CMD_q.write_tail,len);
								sprintf(str,"\t");
								for(i = 0; i < len; i++)
								{
									sprintf(temp,"%02X ",CMD_q.write_buff[tail][i]);
									strcat(str,temp);
								}
								dprintf("%s\n",str);
#endif

							}
							else
							{
							//	dprintf("[MM CMD %d] CMD Length error [%d]\n",port_num,len);
							}
						}
					}
					else // func_code error
					{
					//	dprintf("[MM CMD %d] Precheck error - Func code [%d]\n",port_num,func_code);
					}
				} // length error
			} //
		}
		usleep(100000);
	}
}

void chemm_thread(void *arg)
{
	int slavefd,slavefds[2];
	fd_set readfds,	testfds;
	int fd, sockfd;
	int	nread;
	int port_num, mode, result;
	int system_no;
	struct timeval timeout;
	int tx_time, rx_time, timeout_time;
	int num_port;

	port_num = (int)arg;
	pthread_detach(pthread_self());

	sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);	// pm
	if(sockfd < 0)
	{
		dprintf("[MM %d] socket open error!!!\n",port_num);
		return;
	}

restart:
	memset(sendbuff, 0, sizeof(sendbuff));

	slavefds[0] = 0;
	slavefds[1] = 0;
	num_port = INF_st.num_port;
	slavefds[0] = open_link(0);							// 3rd Party

	if( slavefds[0] < 0 ) goto restart;

	cmd_handshake(sockfd,port_num,0xAA);
	cmd_clear(sockfd);

	fd_index = 0;
	main_line = fd_index ;
	slavefd = slavefds[fd_index];
	tx_time = INF_st.tx_time*1000;
	rx_time = INF_st.rx_time*1000;
	timeout_time = INF_st.timeout*1000;

	if( INF_st.num_addr_map > 0 && INF_st.num_addr_map <= 16 )
		system_no = INF_st.num_addr_map;
	else
		system_no = 1;

	dprintf("CHEM Start [INF: %d]\n",port_num);

	FD_ZERO(&readfds);
	FD_SET(slavefd, &readfds);

	while(1){
		testfds = readfds;
		mode = select_nextquery(port_num,mode);
//		if(mode == WRITE_REQ)
		{
			mode = chem_write(sockfd,slavefd,port_num);
		}

		timeout.tv_sec = 0;
		timeout.tv_usec = timeout_time;

		usleep(rx_time);
		result = -1;

		if(select(FD_SETSIZE, &testfds, (fd_set*)0, (fd_set*)0, (struct timeval*)&timeout) != 0){
			if( FD_ISSET(slavefd,&testfds) ) {
				ioctl(slavefd, FIONREAD,&nread);
				if(nread > 0){
					result = chem_read(slavefd,sockfd,port_num);
				}
				usleep(30);
			}
		}

		if( result > 0 )
		{
			line_check(1,sockfd,port_num,ALIVE);
			update_line_info(main_line, RX);
		}
		else
		{
			line_check(1,sockfd,port_num,BAD);
		}
	}
	close(slavefd);
	close(sockfd);
}
