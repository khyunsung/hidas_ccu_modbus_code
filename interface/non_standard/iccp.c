/***********************************************************************************
  Filename:     iccp.c

  Description: this source code supports to interface with ICCP of K.C. LTD.
			   COM and ICCP are connected by ethernet and use UDP protocol.
			   COM just receive UDP stream packet from ICCP every seconds.(e.g. 1 Hz)
			   The UDP port are to be used are:
			    - COM : 7122 ?
			    - ICCP #1: 7123
		  	    - ICCP #2: 7124
			   The IP addresses to be used are:
                            - COM     : 192.168.3.1
		 	    - ICCP #1 : 192.168.3.2
			    - ICCP #2 : 192.168.3.3
			   The ICCP send the UDP packet to COM as little-endian.

***********************************************************************************/

/***********************************************************************************
* INCLUDES
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#include <asm/types.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <termios.h>

#include "common.h"

#include "../app.h"
#include "../interface.h"
#include "../modbus.h"
#include "../utils.h"
#include "../log.h"
#include "iccp.h"
/***********************************************************************************
* CONSTANTS AND DEFINES
*/
//#define	DEBUG
#ifndef	DEBUG
	#define	dprintf(format, args...) log_access(format, ## args)
#else
	#define	dprintf(format, args...) log_access(format, ## args); printf(format, ## args)
#endif	// DEBUG

//#define		WRITE_DEBUG

#define BASE 			3000
/***********************************************************************************
* GLOBAL VARIABLES
*/
extern int inf_tx_cnt, inf_rx_cnt, inf_thr_cnt;
extern __u8 inf_alive;

extern INF_ST INF_st;
extern CMD_Q CMD_q;

extern pthread_mutex_t mutex_inf;
extern pthread_mutex_t mutex_mem;

extern int fd_index, read_index;

/***********************************************************************************
* LOCAL FUNCTIONS
*/
static int iccp_cmd_read(int in, int out,int port_num)
{
	unsigned int sum_datas;
	char buff[DEFAULT_BUFF_SIZE];
	char writebuff[DEFAULT_BUFF_SIZE];
	int i, j, k, len;
	__u16 crc, r_crc;

	modbus_res_hdr_ext *pext;
	
	len = inf_read(in,buff,DEFAULT_BUFF_SIZE-16,PRIMARY);
	if(len < 5 )
	{
		dprintf("[MM %d] Slave Read Too Short [%d]!\n",port_num,len);
		return -1;
	}
	if(len >= DEFAULT_BUFF_SIZE-16)
	{
		dprintf("[MM %d] Slave Read Too Long [%d]!\n",port_num,len);
		return -1;
	}
	
//	pext->unit_id = port_num;
///	pext->function_code = 0x03;
//	pext->address = INF_st.iccp_read[0].mapping;
	
	k = 0;
	for(i = 0; i < INF_st.num_read_cmd; i++)
	{
		for(j = 0; j < INF_st.iccp_read[i].number ; i++)
		{
//			pext->data[k++] = buff[INF_st.iccp_read[i].offset++];
			if(INF_st.iccp_read[i].val_length == 2)
			{
//				pext->data[k - 1] = (pext->data[k - 1] << 8) & (INF_st.iccp_read[i].offset++);
			}
		}
	}
			
//	pext->byte_count = k * 2;
	
//	crc = CRC16((__u8*)pext,6+(pext->byte_count ))&0xFFFF;
//	writebuff[6+pext->byte_count] = crc>>8;
//	writebuff[7+pext->byte_count] = crc&0x00FF;

	port_up(out,writebuff,8+(k * 2),port_num);	
	
	return 1;
}

#define MAXQUEUE     6
#define MAXBUFFER    1024


typedef struct {
	    int           Length;
        unsigned char Buffer[MAXBUFFER];
} iccpQueueType;

int iccp1Rear;
int iccp1Front;

int iccp2Rear;
int iccp2Front;

iccpQueueType iccpQ1[MAXQUEUE],iccpQ2[MAXQUEUE];

void iccp1ReadThread(void *arg)
{
	int    iccp1len;
	int    addrlen;
	int    port;
	int    iccpSock;
	char   iccpBuffer[MAXBUFFER];	
	struct sockaddr_in server_addr;	
	struct sockaddr_in client_addr;			
	
	iccp1len = 0;
	
	if((iccpSock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
	{
		dprintf("ICCP #1 socket open error!\n");
		return;		
	}

    dprintf("ICCP #1 socket open success.\n");
	
    port = (int)arg;
  
	memset(&server_addr,0,sizeof(server_addr));
	server_addr.sin_family      = AF_INET;
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	server_addr.sin_port        = htons(port);
	
	if(bind(iccpSock,(struct sockaddr *) &server_addr,sizeof(server_addr)) < 0)
	{
	   dprintf("Bind error ICCP #1 socket!\n");
	   return;
	}

    dprintf("ICCP #1 socket bind success port [ %d ].\n",port);
   
    addrlen = sizeof(client_addr);
	while(1)
	{
	      iccp1len = recvfrom(iccpSock,(void *)iccpBuffer,MAXBUFFER,0,(struct sockaddr *) &client_addr, &addrlen);
	      if(iccp1len > 0) {
	    	 dprintf("Receive Message from ICCP#1 %X length[ %d ]\n",client_addr.sin_addr.s_addr, iccp1len);
	    	 memcpy(&iccpQ1[iccp1Front].Buffer[0],&iccpBuffer[0],iccp1len);
	    	 iccpQ1[iccp1Front].Length = iccp1len;
	    	 iccp1Front = (iccp1Front + 1) % MAXQUEUE;	    	 
	      }
	      usleep(10000);	      
	}	
	close(iccpSock);
}

void iccp2ReadThread(void *arg)
{
	int    iccp2len;
    int    addrlen;
	int    port;
	int    iccpSock;
	char   iccpBuffer[MAXBUFFER];		
	struct sockaddr_in server_addr;	
	struct sockaddr_in client_addr;		
	
	iccp2len = 0;
	
	if((iccpSock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
	{
		dprintf("ICCP #2 socket open error!\n");
		return;		
	}

    dprintf("ICCP #2 socket open success.\n");	
	
    port = (int)arg;
    
    memset(&server_addr,0,sizeof(server_addr));
	server_addr.sin_family      = AF_INET;
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	server_addr.sin_port        = htons(port);
	
	if(bind(iccpSock,(struct sockaddr *) &server_addr,sizeof(server_addr)) < 0)
	{
	   dprintf("Bind error ICCP #2 socket!\n");
	   return;
	}

    dprintf("ICCP #2 socket bind success port [ %d ].\n",port);

    addrlen = sizeof(client_addr);	
	while(1)
	{
	      iccp2len = recvfrom(iccpSock,(void *)iccpBuffer,MAXBUFFER,0,(struct sockaddr *) &client_addr,&addrlen );
	      if(iccp2len > 0) 
	      {
	    	 dprintf("Receive Message from ICCP#2 length[%d]\n",iccp2len);
	    	 memcpy(&iccpQ2[iccp2Front].Buffer[0],&iccpBuffer[0],iccp2len);
	    	 iccpQ2[iccp2Front].Length = iccp2len;	    	 
	    	 iccp2Front = (iccp2Front + 1) % MAXQUEUE;
	      }
	      usleep(10000);
	}	
	close(iccpSock);
}



void iccpm_thread(void *arg)
{
	int i;
	int udpSock;
	int port_num;
	unsigned short crc;
	unsigned char writebuff[MAXBUFFER];
    modbus_res_hdr_ext *pext;	
    
	int iccp1port;
	int iccp2port;
	iccpQueueType temp1,temp2;
  
	pthread_t iccp1Thread,iccp2Thread;
	
	dprintf("ICCP Interfase module starting.. \n");
	
	iccp1port =  INF_st.read_cmd[0].address; /* 7123 */
	iccp2port =  INF_st.read_cmd[1].address;
	
	dprintf("ICCP #1 Port [%d] \n",iccp1port);
	dprintf("ICCP #2 Port [%d] \n",iccp2port);
	
	iccp1Rear  = 0;
	iccp2Rear  = 0;
	iccp1Front = 0;
	iccp2Front = 0;
	
	pthread_create(&iccp1Thread, NULL, (void *)&iccp1ReadThread,(void *)iccp1port);		
	pthread_create(&iccp2Thread, NULL, (void *)&iccp2ReadThread,(void *)iccp2port);		

	/* Prepare for  Modbus UDP  */ 
	port_num = (int) arg;
	
	udpSock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);	
	if(udpSock < 0)
	{
		dprintf("[MM %d] socket open error!!!\n",port_num);
		return;
	}	
	cmd_handshake(udpSock,port_num,0xAA);
	cmd_clear(udpSock);	
	
	while(1)
	{
	      if(iccp1Rear != iccp1Front)
	      {
	    	    /* Here! make port up data */
	    	    /*
	    	    dprintf("--> %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X  \n",
	    	    		  iccp1Buffer[iccp1Rear][ 0],iccp1Buffer[iccp1Rear][ 1],iccp1Buffer[iccp1Rear][ 2],iccp1Buffer[iccp1Rear][ 3],
						  iccp1Buffer[iccp1Rear][ 4],iccp1Buffer[iccp1Rear][ 5],iccp1Buffer[iccp1Rear][ 6],iccp1Buffer[iccp1Rear][ 7],
	    	    		  iccp1Buffer[iccp1Rear][ 8],iccp1Buffer[iccp1Rear][ 9],iccp1Buffer[iccp1Rear][10],iccp1Buffer[iccp1Rear][11],
						  iccp1Buffer[iccp1Rear][12],iccp1Buffer[iccp1Rear][13],iccp1Buffer[iccp1Rear][14],iccp1Buffer[iccp1Rear][15]);		
	    	    */
	    	    memcpy(&temp1.Buffer[0],&iccpQ1[iccp1Rear].Buffer[0],iccpQ1[iccp1Rear].Length);
	    	    temp1.Length = iccpQ1[iccp1Rear].Length;
		        iccp1Rear = (iccp1Rear + 1) % MAXQUEUE;
		        
		        
       	        pext = (modbus_res_hdr_ext *)writebuff;

                pext->unit_id       = port_num;
                pext->function_code = 0x03;
                pext->address       = INF_st.read_cmd[0].mapping;
             
                if(INF_st.read_cmd[0].function_code)
                {
                   for(i = 0; i < INF_st.read_cmd[0].points; i++)
                   {
             	   /* Little Endian */
             	   pext->data[i] = (temp1.Buffer[i*2]   << 8) + temp1.Buffer[i*2+1];
                   }
                }
                else
                {
                   for(i = 0; i < INF_st.read_cmd[0].points; i++)
                   {     	
             	   /* Big Endian */
             	   pext->data[i] = (temp1.Buffer[i*2+1] << 8) + temp1.Buffer[i*2];	
                   }                    	 
                }
	      
                pext->byte_count              = INF_st.read_cmd[0].points * 2;

                crc                           = CRC16((__u8*)pext,6+pext->byte_count)&0xFFFF;
                writebuff[6+pext->byte_count] = crc>>8;
                writebuff[7+pext->byte_count] = crc&0x00FF;
              
                if(pext->byte_count == temp1.Length)
                {
                   port_up(udpSock,writebuff,8+pext->byte_count,port_num);	
                }
                else
                if(pext->byte_count < temp1.Length)	                    	   
                {
             	   dprintf(" Warnning : Received data %d bytes > defined data %d bytes \n",
             			    temp1.Length,pext->byte_count);
                   port_up(udpSock,writebuff,8+pext->byte_count,port_num);		                    	  
                }	  
                else
                {
                   dprintf(" Warnning : Received data %d bytes < defined data %d bytes \n",
                 	        temp1.Length,pext->byte_count);	 
                   port_up(udpSock,writebuff,8+pext->byte_count,port_num);                 
                }		        
	      }
	      
	      if(iccp2Rear != iccp2Front)
	      {
	    	    /* Here! make port up data */	    	  
	    	    memcpy(&temp2.Buffer[0],&iccpQ2[iccp2Rear].Buffer[0],iccpQ2[iccp2Rear].Length);
	    	    temp2.Length = iccpQ2[iccp2Rear].Length;	    	  
		        iccp2Rear = (iccp2Rear + 1) % MAXQUEUE;
		        
       	        pext = (modbus_res_hdr_ext *)writebuff;

                pext->unit_id       = port_num;
                pext->function_code = 0x03;
                pext->address       = INF_st.read_cmd[1].mapping;
             
                if(INF_st.read_cmd[1].function_code)
                {
                   for(i = 0; i < INF_st.read_cmd[1].points; i++)
                   {
             	       /* Little Endian */
             	       pext->data[i] = (temp2.Buffer[i*2]   << 8) + temp2.Buffer[i*2+1];
                   }  
                }
                else
                {
                    for(i = 0; i < INF_st.read_cmd[1].points; i++)
                    {
                  	   /* Big Endian */
                  	   pext->data[i] = (temp2.Buffer[i*2+1] << 8) + temp2.Buffer[i*2];	
                    }                	
                }
                
                pext->byte_count              = INF_st.read_cmd[1].points * 2;

                crc                           = CRC16((__u8*)pext,6+pext->byte_count)&0xFFFF;
                writebuff[6+pext->byte_count] = crc>>8;
                writebuff[7+pext->byte_count] = crc&0x00FF;
              
                if(pext->byte_count == temp2.Length)
                {
                   port_up(udpSock,writebuff,8+pext->byte_count,port_num);	
                }
                else
                if(pext->byte_count < temp2.Length)	                    	   
                { 
             	   dprintf(" Warnning : Received data %d bytes > defined data %d bytes \n",
             			    temp2.Length,pext->byte_count);
                   port_up(udpSock,writebuff,8+pext->byte_count,port_num);		                    	  
                }	  
                else
                {
                   dprintf(" warnning : Received data %d bytes < defined data %d bytes \n",
                 	        temp2.Length,pext->byte_count);	
                   port_up(udpSock,writebuff,8+pext->byte_count,port_num);		                    	                    
                }		            
	      } 
	      usleep(50000);
	} /* while(1) */
	close(udpSock);
}
	      
	      
