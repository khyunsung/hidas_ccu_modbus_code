/***********************************************************************************
  Filename:     ndmc.c

  Description:

***********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#include <asm/types.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <termios.h>
#include <time.h>

#include "common.h"

#include "../app.h"
#include "../interface.h"
#include "../modbus.h"
#include "../utils.h"
#include "../log.h"
#include "ndmc.h"

//#define	DEBUG
#ifndef	DEBUG
	#define	dprintf(format, args...) log_access(format, ## args)
#else
	#define	dprintf(format, args...) log_access(format, ## args); printf(format, ## args)
#endif	// DEBUG

#define MD_LITTLE_ENDIAN

extern INF_ST INF_st;
extern CMD_Q CMD_q;
extern int fd_index;
extern unsigned short INF_mem[MAX_INF_TAG];

extern pthread_mutex_t mutex_inf;
extern pthread_mutex_t mutex_mem;
extern unsigned short mflag;

int main_line, ndmc_time;
static unsigned short seq = 0;

static int ndmc_write(int in, int out, int port_num)
{
	char buf[256] = {0,};
	char writebuff[DEFAULT_BUFF_SIZE];
	modbus_res_hdr_ext *pext;
	int transmited_bytes, len, byte_count, sendSize, i;
	unsigned short checksum, crc;
	HEADER *phdr;
	IPMS_DAT *pdata;
	TAIL *ptail;
	__u8 *pbyte;
	__u16 *pword;
	int local_time;

	local_time = time(NULL);

	len = sizeof(HEADER) + sizeof(IPMS_DAT) + sizeof(TAIL);

	phdr = (HEADER *)buf;
	pdata = (IPMS_DAT *)(phdr+1);
	ptail = (TAIL *)(pdata+1);

	phdr->stx = ASCII_STX;
	phdr->spare = 0;
	phdr->msg_id = seq++;
	phdr->msg_st = 0;
	phdr->len = len;		// 32Byte
	phdr->time_stamp = local_time;

	byte_count = sizeof(IPMS_DAT);

	pext = (modbus_res_hdr_ext *)writebuff;
	pext->unit_id = port_num;
	pext->function_code = 0x10;
	pext->address = 65;//sizeof(NDMC_DAT);	// ??? �������
	pext->byte_count = byte_count;

	crc = CRC16((__u8*)pext,6)&0xFFFF;
	writebuff[6] = crc>>8;
	writebuff[7] = crc&0x00FF;
	sendSize = (byte_count+1)/2;
	port_up(INF_st.sockfd,writebuff,sendSize,port_num);
	usleep(10000);

	pbyte = (__u8 *)pdata;
	pword = (__u16 *)pdata;

pthread_mutex_lock(&mutex_mem);
//	memcpy(pbyte,&INF_mem[pext->address],sendSize*2);
	i = 0;
	//short batt_state;
	pword[i++] = htons(INF_mem[pext->address + 0]);
	//short rpm_state;	// disturbed, valid, high, too high
	pword[i++] = htons(INF_mem[pext->address + 1]);
//	pdata->batt_state = htons(INF_mem[pext->address + i++]);
	//float rpm_act;
	pword[i++] = htons(INF_mem[pext->address + 3]);
	pword[i++] = htons(INF_mem[pext->address + 2]);
//	pdata->rpm_act = htons(INF_mem[pext->address + i++])<<16 | htons(INF_mem[pext->address + i++]);
	//float rpm_sp;
	pword[i++] = htons(INF_mem[pext->address + 5]);
	pword[i++] = htons(INF_mem[pext->address + 4]);
//	pdata->rpm_sp = htons(INF_mem[pext->address + i++])<<16 | htons(INF_mem[pext->address + i++]);
	//short submarine_state;
	pword[i++] = htons(INF_mem[pext->address + 6]);
//	pdata->submarine_state = htons(INF_mem[pext->address + i++]);
	//short c_alarm;
	pword[i++] = htons(INF_mem[pext->address + 7]);
//	pdata->c_alarm = htons(INF_mem[pext->address + i++]);
pthread_mutex_unlock(&mutex_mem);

#if 0	// For Test!!
	pdata->batt_state = 80;
	pdata->rpm_act = 1.2345;
	pdata->rpm_sp = 678.9;
#endif
//	phdr->time_stamp = ndmc_time;

	ptail->cs = 0;
	ptail->spare = 0;
	ptail->etx = ASCII_ETX;
	checksum = checksum16(buf, len);
	ptail->cs = checksum;

	transmited_bytes = inf_write(out,buf,len,PRIMARY);
	if(transmited_bytes > 0) update_line_info(main_line, TX);
	return 1;
}

static int ndmc_read(int in, int out,int port_num)
{
//	char buf[DEFAULT_BUFF_SIZE]={0,};
	char *buf;
	char buff[DEFAULT_BUFF_SIZE]={0,};
	char tmp[DEFAULT_BUFF_SIZE];
	char writebuff[DEFAULT_BUFF_SIZE];
	modbus_res_hdr_ext *pext;
	HEADER *phdr;
	TAIL *ptail;
	NDMC_DAT *pdata;
	int size, len, byte_count, i;
	unsigned short checksum, crc;
	__u16 *pword;

	len = inf_read(in,buff,DEFAULT_BUFF_SIZE-16,PRIMARY);
/*	len = inf_read(in,tmp,DEFAULT_BUFF_SIZE-16,PRIMARY);
	memcpy(&buff[1],tmp,len);
	len++;
	buff[len++] = 0;*/

	//+++++ UART Error +++++
/*	if(buff[0]==0){
		if(buff[len-1]==0) len-=2;
		else len--;
		memcpy(buf,&buff[1],len);
	}
	else{
		if(buff[len-1]==0) len--;
		memcpy(buf,buff,len);
	}*/
	//+++++ UART Error +++++
	if(buff[0]==0){
		if(buff[len-1]==0) len-=2;
		else len--;
		buf = (char *) &buff[1];
	}
	else{
		if(buff[len-1]==0) len--;
		buf = buff;
	}
	//++++++++++++++++++++++

	phdr = (HEADER *)buf;
	pdata = (NDMC_DAT *)(phdr+1);
	ptail = (TAIL *)(pdata+1);
	checksum = checksum16(buf, len);

	size = sizeof(HEADER) + sizeof(NDMC_DAT) + sizeof(TAIL);
	if(size != len)
	{
		dprintf("[MM %d] NDMC Read Length Mismatch! size : %d, len : %d\n",port_num,size,len);
		return -1;
	}
	if(size != phdr->len)
	{
		dprintf("[MM %d] NDMC NDMC Length Mismatch size[0x%02X] len[0x%02X]\n",port_num,size,phdr->len);
		return -1;
	}

	if(checksum != 0)
	{
		if(len&1)checksum = ~(~htons(checksum16(buf, len-4)) + ptail->etx);
		else checksum = ~(~checksum16(buf, len-4) + htons(ptail->etx));
		dprintf("[MM %d] NDMC Checksum Mismatch! cs : 0x%04X, cal : 0x%04X\n",port_num,ptail->cs,checksum);
		return -1;
	}

	if(phdr->stx != ASCII_STX)
	{
		dprintf("[MM %d] NDMC STX Mismatch [%d]!\n",port_num,phdr->stx);
		return -1;
	}
	if(ptail->etx != ASCII_ETX)
	{
		dprintf("[MM %d] NDMC ETX Mismatch [%d]!\n",port_num,ptail->etx);
		return -1;
	}
//	ndmc_time = phdr->time_stamp;
	byte_count = sizeof(NDMC_DAT);

	pext = (modbus_res_hdr_ext *)writebuff;
	pext->unit_id = port_num;
	pext->function_code = 0x03;
	pext->address = 0;
	pext->byte_count = byte_count + byte_count%2;	//++ Make Even Number

	pword = (__u16 *)pdata;

#ifdef MD_LITTLE_ENDIAN
	//++ Make Big Endian
	i = 0;
//	unsigned short	src_attitude;
	pext->data[i++] = htons(pword[0]);
//	unsigned short	manual_status;
	pext->data[i++] = htons(pword[1]);
//	float	heading;
	pext->data[i++] = htons(pword[3]);
	pext->data[i++] = htons(pword[2]);
//	float	roll;
	pext->data[i++] = htons(pword[5]);
	pext->data[i++] = htons(pword[4]);
//	float	pitch;
	pext->data[i++] = htons(pword[7]);
	pext->data[i++] = htons(pword[6]);
//	float	heading_rate;
	pext->data[i++] = htons(pword[9]);
	pext->data[i++] = htons(pword[8]);
//	float	roll_rate;
	pext->data[i++] = htons(pword[11]);
	pext->data[i++] = htons(pword[10]);
//	float	pitch_rate;
	pext->data[i++] = htons(pword[13]);
	pext->data[i++] = htons(pword[12]);
	// position
//	unsigned short	src_position;
	pext->data[i++] = htons(pword[14]);
//	float	latitude;
	pext->data[i++] = htons(pword[16]);
	pext->data[i++] = htons(pword[15]);
//	float	longitude;
	pext->data[i++] = htons(pword[18]);
	pext->data[i++] = htons(pword[17]);
//	float	stddev_north;
	pext->data[i++] = htons(pword[20]);
	pext->data[i++] = htons(pword[19]);
//	float	stddev_east;
	pext->data[i++] = htons(pword[22]);
	pext->data[i++] = htons(pword[21]);
	// water speed
//	unsigned short	src_waterspd;
	pext->data[i++] = htons(pword[23]);
//	float	waterspd_longitudinal;
	pext->data[i++] = htons(pword[25]);
	pext->data[i++] = htons(pword[24]);
//	float	waterspd_transverse;
	pext->data[i++] = htons(pword[27]);
	pext->data[i++] = htons(pword[26]);
	// ground speed
//	unsigned short	src_groundspd;
	pext->data[i++] = htons(pword[28]);
//	float	groundspd_longitudinal;
	pext->data[i++] = htons(pword[30]);
	pext->data[i++] = htons(pword[29]);
//	float	groundspd_transverse;
	pext->data[i++] = htons(pword[32]);
	pext->data[i++] = htons(pword[31]);
//	float	groundspd_vertical;
	pext->data[i++] = htons(pword[34]);
	pext->data[i++] = htons(pword[33]);
	// diving depth
//	unsigned short	src_divingdep;
	pext->data[i++] = htons(pword[35]);
//	float	divingdep;
	pext->data[i++] = htons(pword[37]);
	pext->data[i++] = htons(pword[36]);
	// distance
//	float	distance;
	pext->data[i++] = htons(pword[39]);
	pext->data[i++] = htons(pword[38]);
	// MCU
//	unsigned short	com_mast_status;
	pext->data[i++] = htons(pword[40]);
//	float	com_mast_position;
	pext->data[i++] = htons(pword[42]);
	pext->data[i++] = htons(pword[41]);
//	float	combi_antenna_position;
	pext->data[i++] = htons(pword[44]);
	pext->data[i++] = htons(pword[43]);
//	float	hf_antenna_position;
	pext->data[i++] = htons(pword[46]);
	pext->data[i++] = htons(pword[45]);
//	unsigned short	com_mast_cmdecho;
	pext->data[i++] = htons(pword[47]);
//	unsigned short	radar_mast_status;
	pext->data[i++] = htons(pword[48]);
//	float	radar_mast_position;
	pext->data[i++] = htons(pword[50]);
	pext->data[i++] = htons(pword[49]);
//	unsigned short	radar_mast_cmdecho;
	pext->data[i++] = htons(pword[51]);
//	unsigned short	oss1_mast_status;
	pext->data[i++] = htons(pword[52]);
//	float	oss1_mast_position;
	pext->data[i++] = htons(pword[54]);
	pext->data[i++] = htons(pword[53]);
//	unsigned short	oss1_mast_cmdecho;
	pext->data[i++] = htons(pword[55]);
//	unsigned short	oss2_mast_status;
	pext->data[i++] = htons(pword[56]);
//	float	oss2_mast_position;
	pext->data[i++] = htons(pword[58]);
	pext->data[i++] = htons(pword[57]);
//	unsigned short	oss2_mast_cmdecho;
	pext->data[i++] = htons(pword[59]);
//	unsigned short	mcu_mode;
	pext->data[i++] = htons(pword[60]);
	// timestamp
//	unsigned int	timestamp;
	pext->data[i++] = htons(pword[62]);
	pext->data[i++] = htons(pword[61]);
	// timezoneoffset
//	short	timeoffset;
	pext->data[i++] = htons(pword[63]);




	//++ Make Big Endian
//	for(i = 0; i < byte_count/2 ; i++)
//		pext->data[i] = htons(pword[i]);
#else
	for(i = 0; i < byte_count/2 ; i++)
		pext->data[i] = pword[i];
#endif

	crc = CRC16((__u8*)pext,6+byte_count)&0xFFFF;
	writebuff[6+byte_count] = crc>>8;
	writebuff[7+byte_count] = crc&0x00FF;

	port_up(out,writebuff,8+byte_count,port_num);

	return 1;
}

void ndmcm_bufm(void *arg)
{
	int sockfd;
	int port_num, len, check, i;
	fd_set readfds,	testfds;
	char tempbuff[DEFAULT_BUFF_SIZE],writebuff[16];
	__u8 unit_id, func_code;
	__u16 address, points, byte_count;
	struct timeval timeout;
	modbus_res_hdr_ext *preq;
	modbus_res_hdr_ext *pext;
	char str[40], temp[8];
	char head,tail;
	__u8 delay = 0;

	port_num = (int)arg;
	pthread_detach(pthread_self());

	dprintf(" - Buffer Manager Start [Port: %d]\n",port_num);

	sockfd = INF_st.sockfd;

	FD_ZERO(&readfds);
	FD_SET(sockfd, &readfds);

	preq = (modbus_res_hdr_ext *)tempbuff;

	while(1)
	{
		testfds = readfds;

		timeout.tv_sec = 30;
		timeout.tv_usec = 0;
		if(select(sockfd+1, &testfds, (fd_set*)0, (fd_set*)0, (struct timeval*)&timeout) != 0)
		{
			if( FD_ISSET(sockfd,&testfds) ) {  // Write Command
				len = read(sockfd,tempbuff,MAX_CMD_SIZE);
				if( len >= 6 && len < 256 )
				{
					unit_id = preq->unit_id;
					func_code = preq->function_code;
					address = preq->address;

					if( func_code == 0xFF )  // for modbus read response  added on 07/05/12
					{
						byte_count = preq->byte_count;
						points = byte_count/2;
						check = 1;

						if( unit_id != port_num )
						{
						//	dprintf("[MM CMD %d] Precheck error - id [%d]\n",port_num,unit_id);
							check = -1;
						}

						if(address>=2048)
						{
						//	dprintf("[MM CMD %d] Precheck error - address [%d]\n",port_num,address);
							check = -1;
						}

						if((address+points)>2048)
						{
						//	dprintf("[MM CMD %d] Precheck error - Data Space [%d]-[%d]\n",port_num,
						//		address,address+points-1);
							check = -1;
						}

						if( byte_count > 128 )
						{
						//	dprintf("[MM CMD %d] Precheck error - Too large [%d]\n",port_num,byte_count);
							check = -1;
						}

						if( check > 0 )
						{
//							dprintf("[MM CMD %d] MEM ADDR:%04X NO:%04X(%d)\n",port_num,address,points,byte_count);
pthread_mutex_lock(&mutex_mem);
							memcpy(&INF_mem[address],preq->data,byte_count);
pthread_mutex_unlock(&mutex_mem);
//							dprintf("DATA : %d-->[%d][%d][%d][%d][%d]\n",address,preq->data[0],preq->data[1],preq->data[2],preq->data[3],preq->data[4]);
						}
					}
					else if( func_code == write_single_reg || func_code == write_mult_regs )
					{
pthread_mutex_lock(&mutex_inf);
						head = CMD_q.write_head;
						tail = CMD_q.write_tail;
pthread_mutex_unlock(&mutex_inf);
						if( (tail+1)%MAX_Q_SIZE == head )
						{
						//	dprintf("[MM CMD %d] Write buff is full!!\n",port_num);
							mflag |= (1<<14);
							delay = 0;
						}
						else
						{
							if( len == 6 || len == 11 )
							{
pthread_mutex_lock(&mutex_inf);
								memcpy(CMD_q.write_buff[tail],tempbuff,len);
								CMD_q.write_length[tail]=len;
								CMD_q.write_tail++;
								CMD_q.write_tail %= MAX_Q_SIZE;
pthread_mutex_unlock(&mutex_inf);
//#if 0
								dprintf("[CMD queue inf:%d] head:%d tail:%d (len:%d)\n",port_num,
									head,CMD_q.write_tail,len);
								sprintf(str,"\t");
								for(i = 0; i < len; i++)
								{
									sprintf(temp,"%02X ",CMD_q.write_buff[tail][i]);
									strcat(str,temp);
								}
								dprintf("%s\n",str);
//#endif

								if( delay > 10 )
								{
									mflag &= ~(1<<14);
									delay = 0;
								}
								else delay++;
							}
							else
							{
							//	dprintf("[MM CMD %d] CMD Length error [%d]\n",port_num,len);
							}

						//	cmd_handshake(sockfd,port_num,0x06);
						}
					}
					else // func_code error
					{
					//	dprintf("[MM CMD %d] Precheck error - Func code [%d]\n",port_num,func_code);
					}
				} // length error
			} //
		}
		usleep(100000);
	}
}


void ndmcm_thread(void *arg)
{
	int slavefd,slavefds[2];
//	fd_set readfds;
	int sockfd;
//	int	nread;
	int port_num, mode;
//	int system_no;
	struct timeval timeout;
	int tx_time;
//	int num_port;

	port_num = (int)arg;
	pthread_detach(pthread_self());

restart:
	slavefds[0] = 0;

//	num_port = INF_st.num_port;
	slavefds[0] = open_link(0);							// 3rd Party

	if( slavefds[0] < 0 ) goto restart;

	fd_index = 0;
	slavefd = slavefds[fd_index];
	tx_time = INF_st.tx_time*1000;

	dprintf("NDMC Write Start [INF: %d]\n",port_num);

//	FD_ZERO(&readfds);
//	FD_SET(slavefd, &readfds);

	while(1){
		usleep(tx_time);
		mode = ndmc_write(sockfd,slavefd,port_num);
	}
	close(slavefd);
}


void ndmcs_thread(void *arg)
{
	int slavefd,slavefds[2];
	fd_set readfds,	testfds;
	int fd, sockfd;
	int	nread;
	int port_num, mode, result;
	int system_no;
	struct timeval timeout;
	int tx_time, rx_time, timeout_time;
	int num_port;

	port_num = (int)arg;
	pthread_detach(pthread_self());

	sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);	// pm
	if(sockfd < 0)
	{
		dprintf("[MM %d] socket open error!!!\n",port_num);
		return;
	}

restart:
	slavefds[0] = 0;
	slavefds[1] = 0;
	num_port = INF_st.num_port;
	slavefds[0] = open_link(0);							// 3rd Party

	if( slavefds[0] < 0 ) goto restart;

	cmd_handshake(sockfd,port_num,0xAA);
	cmd_clear(sockfd);

	fd_index = 0;
	main_line = fd_index ;
	slavefd = slavefds[fd_index];
	tx_time = INF_st.tx_time*1000;
	rx_time = INF_st.rx_time*1000;
	timeout_time = INF_st.timeout*1000;

	if( INF_st.num_addr_map > 0 && INF_st.num_addr_map <= 16 )
		system_no = INF_st.num_addr_map;
	else
		system_no = 1;

	dprintf("NDMC Start [INF: %d]\n",port_num);

	FD_ZERO(&readfds);
	FD_SET(slavefd, &readfds);

	while(1){
		testfds = readfds;
//		mode = ndmc_write(sockfd,slavefd,port_num);

		timeout.tv_sec = 0;
		timeout.tv_usec = timeout_time;

		usleep(rx_time);
		result = -1;

		if(select(FD_SETSIZE, &testfds, (fd_set*)0, (fd_set*)0, (struct timeval*)&timeout) != 0){
			if( FD_ISSET(slavefd,&testfds) ) {
				ioctl(slavefd, FIONREAD,&nread);
				if(nread > 0){
					result = ndmc_read(slavefd,sockfd,port_num);
				}
				usleep(30);
			}
		}

		if( result > 0 )
		{
			line_check(1,sockfd,port_num,ALIVE);
			update_line_info(main_line, RX);
		}
		else
		{
			line_check(1,sockfd,port_num,BAD);
		}
	}
	close(slavefd);
	close(sockfd);
}
