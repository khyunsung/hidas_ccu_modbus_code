#ifndef	_NDMC_H
#define	_NDMC_H

#define MSG_MAX_LEN		512

#define ASCII_STX	0x02
#define ASCII_ETX	0x03

#pragma pack(1)
typedef struct{		// 12Byte
	__u8	stx;	// STX
	__u8	spare;
	__u16	msg_id;
	__u16	msg_st;
	__u16	len;	// all message byte begining with 13(header)
	__u32	time_stamp;
}HEADER;

typedef struct{		// 4Byte
	__u16	cs;
	__u8	spare;
	__u8	etx;	// ETX
}TAIL;

typedef struct{		// 6EA : 16Byte
	short batt_state;
	short rpm_state;	// disturbed, valid, high, too high
	float rpm_act;
	float rpm_sp;
	short submarine_state;
	short c_alarm;
}IPMS_DAT;

typedef struct{		// 41EA : 129Byte
	// attitude
	unsigned short	src_attitude;
	unsigned short	manual_status;
	float	heading;
	float	roll;
	float	pitch;
	float	heading_rate;
	float	roll_rate;
	float	pitch_rate;//44-16=28
	// position
	unsigned short	src_position;
	float	latitude;
	float	longitude;
	unsigned long stddev_north;
	unsigned long stddev_east;
	// water speed
	unsigned short	src_waterspd;
	float	waterspd_longitudinal;
	float	waterspd_transverse;//76-16=60
	// ground speed
	unsigned short	src_groundspd;
	float	groundspd_longitudinal;
	float	groundspd_transverse;
	float	groundspd_vertical;
	// diving depth
	unsigned short	src_divingdep;
	float	divingdep;
	// distance
	float	distance;	//104-16=88
	// MCU
	unsigned short	com_mast_status;
	float	com_mast_position;
	float	combi_antenna_position;
	float	hf_antenna_position;
	unsigned short	com_mast_cmdecho;
	unsigned short	radar_mast_status;
	float	radar_mast_position;
	unsigned short	radar_mast_cmdecho;
	unsigned short	oss1_mast_status;
	float	oss1_mast_position;
	unsigned short	oss1_mast_cmdecho;
	unsigned short	oss2_mast_status;
	float	oss2_mast_position;
	unsigned short	oss2_mast_cmdecho;
	unsigned short	mcu_mode;
	// timestamp
	unsigned long	timestamp;
	// timezoneoffset
	short	timeoffset;
	char	test;
}NDMC_DAT0;

typedef struct{		// 25EA : 79Byte
	// attitude
	unsigned short	src_attitude;
	unsigned short	manual_status;
	float	heading;
	float	roll;
	float	pitch;
	float	heading_rate;
	float	roll_rate;
	float	pitch_rate;//44-16=28
	// position
	unsigned short	src_position;
	float	latitude;
	float	longitude;
	unsigned long stddev_north;
	unsigned long stddev_east;
	// water speed
	unsigned short	src_waterspd;
	float	waterspd_longitudinal;
	float	waterspd_transverse;//76-16=60
	// ground speed
	unsigned short	src_groundspd;
	float	groundspd_longitudinal;
	float	groundspd_transverse;
	float	groundspd_vertical;
	// diving depth
	unsigned short	src_divingdep;
	float	divingdep;
	// timestamp
	unsigned long	timestamp;
	// timezoneoffset
	short	timeoffset;
	char	test;
}NDMC_DAT1;

typedef struct{		// 128Byte : 64Word
	// attitude
	unsigned short	src_attitude;
	unsigned short	manual_status;
	float	heading;
	float	roll;
	float	pitch;
	float	heading_rate;
	float	roll_rate;
	float	pitch_rate;
	// position
	unsigned short	src_position;
	float	latitude;
	float	longitude;
	float	stddev_north;
	float	stddev_east;
	// water speed
	unsigned short	src_waterspd;
	float	waterspd_longitudinal;
	float	waterspd_transverse;
	// ground speed
	unsigned short	src_groundspd;
	float	groundspd_longitudinal;
	float	groundspd_transverse;
	float	groundspd_vertical;
	// diving depth
	unsigned short	src_divingdep;
	float	divingdep;
	// distance
	float	distance;
	// MCU
	unsigned short	com_mast_status;
	float	com_mast_position;
	float	combi_antenna_position;
	float	hf_antenna_position;
	unsigned short	com_mast_cmdecho;
	unsigned short	radar_mast_status;
	float	radar_mast_position;
	unsigned short	radar_mast_cmdecho;
	unsigned short	oss1_mast_status;
	float	oss1_mast_position;
	unsigned short	oss1_mast_cmdecho;
	unsigned short	oss2_mast_status;
	float	oss2_mast_position;
	unsigned short	oss2_mast_cmdecho;
	unsigned short	mcu_mode;
	// timestamp
	unsigned int	timestamp;
	// timezoneoffset
	short	timeoffset;
}NDMC_DAT;

#pragma pack()
#endif		// _NDMC_H
