/***********************************************************************************
  Filename:     dg.c

  Description:  

***********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#include <asm/types.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <termios.h>

#include "common.h"

#include "../app.h"
#include "../interface.h"
#include "../modbus.h"
#include "../utils.h"
#include "../log.h"
#include "dg.h"

//#define	DEBUG
#ifndef	DEBUG
	#define	dprintf(format, args...) log_access(format, ## args)
#else
	#define	dprintf(format, args...) log_access(format, ## args); printf(format, ## args)
#endif	// DEBUG

//#define		WRITE_DEBUG

#define BASE 			3000

extern int inf_tx_cnt, inf_rx_cnt, inf_thr_cnt;
extern __u8 inf_alive;

extern INF_ST INF_st;
extern CMD_Q CMD_q;

extern pthread_mutex_t mutex_inf;
extern pthread_mutex_t mutex_mem;

extern int fd_index, read_index;

#define MAXFRAMENO  10
int	state;
int	trace;
int length;
int infblocklength;
int datacount;
unsigned short recording_counter;	
unsigned short old_recording_counter;
unsigned char  checksum;
unsigned char  block_type;
unsigned char  page_number;
unsigned char  checksum;
unsigned char  lengthHi,lengthLo;
int frontIndex;
int rearIndex;
typedef struct {
         int length;
         unsigned char buffer[DEFAULT_BUFF_SIZE];
} rxframeType;

rxframeType rxframe[MAXFRAMENO];
char writebuffer[DEFAULT_BUFF_SIZE];
char faultbuffer[DEFAULT_BUFF_SIZE]; // 1024 but only 68

typedef struct {
	    unsigned short faultNo;
		unsigned char  signalCode;
}dgSignalType;

dgSignalType dgSigTbl[68] = {
	{35,   2},{37,   2},{38,   2},{40,   2},{41,   2},{43,   2},{44,   2},{45,   2},{46,   2},
	{53,   8},{55,   8},{56,   8},{57,  14},{58,  14},{69,  14},{81,   2},{82,  14},{83,   2},
	{84,   8},{99,   2},{100,  8},{103,  2},{104,  8},{107,  2},{108,  2},{125, 14},{161,  2},
	{162,  2},{163,  2},{164,  2},{165,  2},{166,  2},{171,  2},{172,  2},{173,  2},{174,  2},
	{175,  2},{176,  2},{207, 14},{212, 14},{214, 14},{229, 14},{230, 14},{231, 14},{232, 14},
	{233, 14},{234, 14},{239, 14},{240, 14},{241, 14},{242, 14},{243, 14},{244, 14},{249,  8},
	{250,  8},{251,  8},{252,  8},{253,  8},{254,  8},{259,  8},{260,  8},{261,  8},{262,  8},
	{263,  8},{264,  8},{271, 17},{274, 17},{279, 17} 
};

typedef struct {
	    int    offset;
		unsigned char code;
} dgReturnType;
dgReturnType searchDGTable(unsigned short faultNo)
{
	int i;
	dgReturnType ret;
	for(i=0;i<68;i++)
	{
        if(dgSigTbl[i].faultNo == faultNo)
		{
			ret.offset = i;
			ret.code  = dgSigTbl[i].signalCode;
			return ret;
		}
	}
	ret.offset = 68;
	ret.code   =  1;
	return ret;
}
unsigned short page3word1;
unsigned short page3word2;
int faultRequestOn;
static int faultMessageRequest(int out)
{
	char writebuff[DEFAULT_BUFF_SIZE];
	
	writebuff[0] = 0xFA;
	writebuff[1] = 0xF5;
    writebuff[2] = 0x00;
	writebuff[3] = 0x09;
	writebuff[4] = 0x03;    
	writebuff[5] = 0x00;
	writebuff[6] = 0x81;
	writebuff[7] = 0x2A;
	writebuff[8] = 0x5A;
		
	inf_write(out,writebuff,9,PRIMARY);

	return 1;		
}
static int dg_read(int in, int out,int port_num)
{
	unsigned short crc;
	unsigned char  c;
	unsigned char  rxChecksum;
	dgReturnType   dgRet;
	unsigned short faultMessageNo;

 	char rzbuff[DEFAULT_BUFF_SIZE];
 	char writebuff[DEFAULT_BUFF_SIZE];	
	int  i,len;

	unsigned short *faultPtr;
    modbus_res_hdr_ext *pext;	
	
	len = inf_read(in,rzbuff,DEFAULT_BUFF_SIZE-16,PRIMARY);
	if(len <= 0 ) return -1;
	if(len > DEFAULT_BUFF_SIZE-16 ) return -1;
	
    memcpy(&rxframe[frontIndex].buffer,rzbuff,len);
    rxframe[frontIndex].length = len;
	frontIndex = (frontIndex+1) % MAXFRAMENO;

	while(frontIndex != rearIndex)
	{ 
	    do 
	    {
	    	   c = rxframe[rearIndex].buffer[trace];
	    	   if(state < 2)
	    	   {
		          switch(c)
		          {
		          case 0xFA: if( state == 0 )  state = 1; break;
		          case 0xF5: if( state == 1 )  state = 2; break;
		          }
		       }
		       else
		       {
                  switch(state) 
	              {
	              case 2: 
	              		 lengthHi  =  c;
	              		 checksum  =  c + 0xFA + 0xF5;
		       	         state = 3;
		          break;
		          case 3: 
		          	     lengthLo =  c;
		          	     checksum += c;
		       	         state = 4;
		          break; 	      
		          case 4:
		          	     infblocklength = lengthHi * 256 + lengthLo;
		          	     block_type = c;
		          	     checksum  += c;
		          	     length     = 5;
		          	     
				         if(0x80 & block_type) 
			                page_number = (block_type & 0x7F);
			             else
				            page_number = 0;
				         
				         datacount = 0;

                         state = 5;
                  break;
   	          	  case 5:
   	          	  	     writebuffer[datacount] = c;
   	          	  	     datacount++;
  	          	  	     checksum += c;
   	          	  	     length++;
 		          	     state = 6;
		          break;
		          case 6:
   	          	  	     writebuffer[datacount] = c;
   	          	  	     datacount++;
		          	     checksum += c;
		          	     length++;
		          	     if( (length + 2) == infblocklength) 
		          	     	  state = 7;
		          	      else
		          	          state = 5;
		          break;
		          case 7:
		          	    checksum += c;
		          	    state = 8;
		          break;	    
		          case 8:
		          	    checksum += c;
		          	    if( c != 0x5A ) 
		          	       dprintf("Error : block end syncronization %02X \n",c);
		          	    
		          	    
		          	    if( checksum != 0 ) 
		          	    {
		          	        dprintf("\nError checksum %02X",checksum);
		          	    }
		          	    else
		          	    if(page_number != 0)
		          	    {	          	    	
	          	    	   dprintf("Page %d %02X%02X %02X%02X %02X%02X %02X%02X %02X%02X\n",page_number,
		          	    			   writebuffer[ 0],writebuffer[ 1],
		          	    			   writebuffer[ 2],writebuffer[ 3],
		          	    			   writebuffer[ 4],writebuffer[ 5],
		          	    			   writebuffer[ 6],writebuffer[ 7],
		          	    			   writebuffer[ 8],writebuffer[ 9]);
		          	       /*   
	                       dprintf("\nPage %d Length %3d  ->[ %3d ]  [%5d]  [%5d] [%5d] ",
	                    	              page_number,infblocklength,datacount,
										  INF_st.read_cmd[page_number-1].mapping,
										  INF_st.read_cmd[page_number-1].address,
										  INF_st.read_cmd[page_number-1].points);
	                       */
		          	       
		          	       pext = (modbus_res_hdr_ext *)writebuff;
	
	                       pext->unit_id       = port_num;
	                       pext->function_code = 0x03;
	                       pext->address       = INF_st.read_cmd[page_number-1].mapping;
	                       
						   if(page_number == 3)
						   {
                              for(i=0;i<30;i++)
							  {
								  if( ((writebuffer[i*2] << 8)+ writebuffer[i*2+1]) == 0x0001)
								  {
									  if(i < 8)
									     page3word1 |=  (0x0100 << i );
									  else
									  if(i < 16) 
									     page3word1 |=  (0x0001  << (i % 8) );
									  else
									  if(i < 24)
                                         page3word2 |=  (0x0100 << (i % 8)  );
									  else
										 page3word2 |= ( 0x0001 << (i % 8)  );
								  }
								  else
								  {
									  if(i < 8)
									     page3word1 &=  ~(0x0100 << i );
									  else
									  if(i < 16) 
									     page3word1 &=  ~(0x0001  << (i % 8) );
									  else
									  if(i < 24)
                                         page3word2 &=  ~(0x0100 << (i % 8)  );
									  else
										 page3word2 &= ~( 0x0001 << (i % 8)  );
								  }
							  }

							  pext->byte_count = 4;
							 
							  pext->data[0] = page3word1;
							  pext->data[1] = page3word2;;

	                          crc                           = CRC16((__u8*)pext,6+pext->byte_count)&0xFFFF;
	                          writebuff[6+pext->byte_count] = crc>>8;
	                          writebuff[7+pext->byte_count] = crc&0x00FF;
                         
	                          port_up(out,writebuff,8+pext->byte_count,port_num);	


						   }
						   else
						   {
	                         if(INF_st.read_cmd[page_number-1].function_code)
	                         {
	                          for(i = INF_st.read_cmd[page_number-1].address; i < INF_st.read_cmd[page_number-1].points; i++)
	                          {
	                    	      /* Big Endian */
	                    	      pext->data[i-INF_st.read_cmd[page_number-1].address] = (writebuffer[i*2] << 8) + writebuffer[i*2+1];
	                          }
	                         }
	                         else
	                         {
	                          for(i = INF_st.read_cmd[page_number-1].address; i < INF_st.read_cmd[page_number-1].points; i++)
	                          {
	                    	   /* Little Endian */
	                    	   pext->data[i-INF_st.read_cmd[page_number-1].address] = (writebuffer[i*2+1] << 8) + writebuffer[i*2];	
	                    	  }                    	 
	                         }
						   
	                         pext->byte_count              = (INF_st.read_cmd[page_number-1].points-INF_st.read_cmd[page_number-1].address) * 2;
	
	                         crc                           = CRC16((__u8*)pext,6+pext->byte_count)&0xFFFF;
	                         writebuff[6+pext->byte_count] = crc>>8;
	                         writebuff[7+pext->byte_count] = crc&0x00FF;
                         
	                         port_up(out,writebuff,8+pext->byte_count,port_num);	
						   }
		          	    }
		          	    else /* page_number == 0 */
		          	    {
							if(block_type == 0x01) // Fault Message Dynamic Block
							{
								recording_counter = (writebuffer[1]*256+writebuffer[0]);

								if( (recording_counter - old_recording_counter ) == 1)
								{
								   faultMessageNo =  (writebuffer[3]*256+writebuffer[2]);

								   dgRet = searchDGTable(faultMessageNo & 0x7FFF);

								   if(faultMessageNo & 0x8000)
								   {
									  faultPtr = (unsigned short *) &faultbuffer[dgRet.offset*2];
									  *faultPtr = 0;
		 				              //dprintf("\nCleared Fault Message No: %d",faultMessageNo);
								   }
								   else
								   {
									  faultPtr = (unsigned short *) &faultbuffer[dgRet.offset*2]; 
									  *faultPtr = dgRet.code;
                                      //dprintf("\nSet Fault Message No: %d",faultMessageNo);
								   }

		          	               pext = (modbus_res_hdr_ext *)writebuff;	
	                               pext->unit_id       = port_num;
	                               pext->function_code = 0x03;
	                               pext->address       = INF_st.read_cmd[6].mapping;
	                       
	                               if(INF_st.read_cmd[6].function_code)
	                               {
	                                  for(i = 0; i < INF_st.read_cmd[6].points; i++)
	                                  {
	                    	              /* Big Endian */
	                    	              pext->data[i] = (faultbuffer[i*2] << 8) + faultbuffer[i*2+1];
	                                  }
	                               }
	                               else
	                               {
	                                  for(i = 0; i < INF_st.read_cmd[6].points; i++)
	                                  {
	                    	              /* Little Endian */
	                    	              pext->data[i] = (faultbuffer[i*2+1] << 8) + faultbuffer[i*2];	
	                    	          }                    	 
	                               }

	                               pext->byte_count              = INF_st.read_cmd[6].points * 2;
	
	                               crc                           = CRC16((__u8*)pext,6+pext->byte_count)&0xFFFF;
	                               writebuff[6+pext->byte_count] = crc>>8;
	                               writebuff[7+pext->byte_count] = crc&0x00FF;
                         

	                               port_up(out,writebuff,8+pext->byte_count,port_num);	

								   old_recording_counter = (recording_counter % 65536);
								}
								else
								{
								   memset(faultbuffer,0,INF_st.read_cmd[6].points * 2);
								   old_recording_counter = 0;
								   recording_counter     = 0;
								   if(faultRequestOn == 0) {
								      faultMessageRequest(in);
									  faultRequestOn = 1;
								   }
								}
							}
							else
                            if(block_type == 0x05) // Fault Message Zero Block
							{
								recording_counter = (writebuffer[1]*256+writebuffer[0]);
								if(recording_counter != old_recording_counter)
								{
								   memset(faultbuffer,0,INF_st.read_cmd[6].points * 2);
								   old_recording_counter = 0;
								   recording_counter     = 0;
								   if(faultRequestOn == 0) 
								   {
								     faultMessageRequest(in);
									 faultRequestOn = 1;
								   }
								}
							}
			                dprintf("blocktype  %d Length %3d  ->[ %3d ]  [%02X%02X] [%02X%02X] \n",
			                    	 block_type,infblocklength,datacount, 
									 writebuffer[0],writebuffer[1],
									 writebuffer[2],writebuffer[3]);
		          	    }		          	    
		          	    checksum  = 0;
		          	    state     = 0;
		          	    datacount  =0;
		          break;
		          }
		       }
		       trace++;
		 }while(trace < rxframe[rearIndex].length );
		 trace = 0;
		 rearIndex = (rearIndex+1) % MAXFRAMENO;
     }
	
    return 1;
}

static int dg_init_cmd(int out)
{
	char writebuff[DEFAULT_BUFF_SIZE];
	
	writebuff[0] = 0xFA;
	writebuff[1] = 0xF5;
    writebuff[2] = 0x00;
	writebuff[3] = 0x09;
	writebuff[4] = 0x03;    
	writebuff[5] = 0x00;
	writebuff[6] = 0x80;
	writebuff[7] = 0x2B;
	writebuff[8] = 0x5A;
		
	inf_write(out,writebuff,9,PRIMARY);
		
	return 1;		
}

void dgm_thread(void *arg)
{
    int udpSock; 
	int port_num;   
    
	int dgPort;
	int rx_time;

	int dgReadResult;
	int dgStatCheckCount;
	
	int faultRequestCount;

	port_num = (int)arg;
	
	pthread_detach(pthread_self());
		
	/* Open Modbus UDP Client socket  */
    /* DG( non Standard ) --> Modbus UDP(sockfd)  */
	udpSock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);	
	if(udpSock < 0)
	{
		dprintf("[MM %d] socket open error!!!\n",port_num);
		return;
	}	
	cmd_handshake(udpSock,port_num,0xAA);
	cmd_clear(udpSock);
	
	rearIndex  = 0;
	frontIndex = 0;
 	state      = 0;
	trace      = 0;
	length     = 0;
	checksum   = 0;
	page3word1 = 0;
	page3word2 = 0;
	faultRequestOn = 0;
	faultRequestCount = 0;
	recording_counter = 0;
	old_recording_counter = 0;
    memset(faultbuffer,0,INF_st.read_cmd[6].points * 2);
	dprintf("Diesel Generator [INF: %d]\n",port_num);

	dgPort = open_link(0);
	
	dg_init_cmd(dgPort);
	
	rx_time = INF_st.rx_time*1000;
	
	dgStatCheckCount = 0;

	while(1)
	{
		//line_check(port_num,sockfd,port_num,ALIVE);		
		dgReadResult = dg_read(dgPort,udpSock,port_num);
		usleep(rx_time);
		if(dgReadResult == -1)
		{
           dgStatCheckCount += (rx_time/1000);
		   if(dgStatCheckCount > 5000)
		   {
			  dgStatCheckCount = 0;
			  dg_init_cmd(dgPort);
		   }
		}
		else
		{
		   dgStatCheckCount = 0;
		}
		if(faultRequestOn)
		{
		   faultRequestCount += (rx_time/1000);
		   if(faultRequestCount > 3000)
		   {
			  faultRequestCount = 0;
			  faultRequestOn = 0;
		   }
		}
	}	
	close(dgPort);
	close(udpSock);		
}
