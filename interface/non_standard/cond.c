/***********************************************************************************
  Filename:     cond.c

  Description:

***********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#include <asm/types.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <termios.h>

#include "common.h"

#include "../app.h"
#include "../interface.h"
#include "../modbus.h"
#include "../utils.h"
#include "../log.h"
#include "cond.h"

//#define	DEBUG
#ifndef	DEBUG
	#define	dprintf(format, args...) log_access(format, ## args)
#else
	#define	dprintf(format, args...) log_access(format, ## args); printf(format, ## args)
#endif	// DEBUG

#define MD_LITTLE_ENDIAN

#define DEBUG_INF

extern INF_ST INF_st;
extern int fd_index;
extern unsigned short INF_mem[MAX_INF_TAG];
extern pthread_mutex_t mutex_inf;
extern CMD_Q CMD_q;

extern char sendbuff[DEFAULT_BUFF_SIZE];
extern char recvbuff[DEFAULT_BUFF_SIZE];

int main_line;

static int delay_inf_write(int out, char *buf, int len, int port_num) // Event
{
	int i;
	int ret;
	dprintf("COND Event!! %d : %s\n",len,buf);
	for(i = 0; i < len; i++){
		ret = inf_write(out,&buf[i],1,port_num);
		if(ret<0) break;
		usleep(300000);
	}
	if(ret>0) ret = len;
	return ret;
}

static int cond_write(int in, int out, int port_num) // Event
{
	int i, sendSize, ch_num;
	int transmited_bytes;
	modbus_res_hdr_ext *pext;
	int rxd = 0;
	char ch[4] = {'t','T','H','?'};

	modbus_req_hdr *wreq = (modbus_req_hdr *)recvbuff;

	for(i = 0; i < INF_st.num_write_cmd; i++)
	{
		if(INF_st.write_cmd[i].address == wreq->address)	// :'A'
		{
			break;
		}
	}

	if(i >= INF_st.num_write_cmd){
		dprintf("\twrite no match! Addr : %d - forced query selection\n",wreq->address);
		return PERIODIC_REQ;
	}
	else
	{
		if( wreq->function_code == 0x06 || wreq->function_code == 0x10 )
		{
			ch_num = INF_st.write_cmd[i].write_address;	// :A B C 'D'
			sendbuff[0] = ch[ch_num];
			if( wreq->function_code == 0x06 )
			{
				rxd = wreq->points;
			}
			else if( wreq->function_code == 0x10 )
			{
				rxd = recvbuff[7]<<8;
				rxd |= recvbuff[8];
			}

			sendSize = 4;
			if(ch_num == 3)
			{
				sprintf(&sendbuff[1],"\r");
				sendSize = 2;
			}
			else sprintf(&sendbuff[1],"%02d\r",rxd);

//			dprintf("Tx : %s\n",sendbuff);
		}
		else
		{
			dprintf("\tFunction Code Mismatch! : %d\n",wreq->function_code);
			return PERIODIC_REQ;
		}
	}

//	transmited_bytes = inf_write(out,sendbuff,sendSize,PRIMARY);
	transmited_bytes = delay_inf_write(out,sendbuff,sendSize,PRIMARY);

	if(transmited_bytes > 0) update_line_info(main_line, TX);

	return PERIODIC_REQ;
}

static int cond_read(int in, int out,int port_num)
{
//	char buff[DEFAULT_BUFF_SIZE];
	char buf[DEFAULT_BUFF_SIZE];
	char tmpbuff[DEFAULT_BUFF_SIZE];
	char writebuff[DEFAULT_BUFF_SIZE];
	modbus_res_hdr_ext *pext;
	int len = 0, address, byte_count,i, index = 0;
	unsigned short crc;
	COND_DAT pdata;
	COND_SET pset;
	COND_FLT pflt;
	__u16 *pword;
	int argc=0;
	int slen;
	char ch;
	char *argv[64];

//	len = inf_read(in,buf,DEFAULT_BUFF_SIZE-16,PRIMARY);

	while(1){
		slen = read(in, &ch, 1);
		if(slen <= 0){
			return 0;
		}
		if((ch=='$')||(ch=='#')||(ch=='?'))	len = 0;	// Start COND
		if(ch=='\r'){									// End COND
			buf[len++] = ch;
			if(len>=DEFAULT_BUFF_SIZE)	len = DEFAULT_BUFF_SIZE-1;
			break;
		} else{
			if((len<DEFAULT_BUFF_SIZE)&&(ch != 0))	buf[len++] = ch;
		}
	}

#ifdef DEBUG_INF
	if(INF_st.debugfd.valid)
	{
		index = fd_index;
		write(INF_st.debugfd.rxfd[index],buf,len);
	}
#endif

	if((buf[len-1] != '\r')&&(buf[len-1] != '\n'))
//	if(buf[len-1] != '\r')
	{
		dprintf("[TMPR %d] Temperature Sensor Protocol Error[1]!\n",port_num);
		return -1;
	}

	memcpy(tmpbuff,buf,200);
	tmpbuff[len] = 0;
	argc = get_args1(&tmpbuff[1],argv);

	pext = (modbus_res_hdr_ext *)writebuff;
	switch(tmpbuff[0])
	{
	case '$' :
		if(argc != 6)
		{
			dprintf("[TMPR %d] Temperature $ Format Error, Data Num : %d!\n",port_num,argc);
			return -1;
		}
		pdata.t1 = decstr2num(argv[index],strlen(argv[index++]));
		pdata.h1 = decstr2num(argv[index],strlen(argv[index++]));
		pdata.t2 = decstr2num(argv[index],strlen(argv[index++]));
		pdata.h2 = decstr2num(argv[index],strlen(argv[index++]));
		pdata.t3 = decstr2num(argv[index],strlen(argv[index++]));
		pdata.fan = decstr2num(argv[index],strlen(argv[index++]));
		dprintf("t1:%d, h1:%d, t2:%d, h2:%d, t3:%d, fan:%d\n",pdata.t1,pdata.h1,pdata.t2,pdata.h2,pdata.t3,pdata.fan);
		byte_count = 12;
//		address = INF_st.read_cmd[0].address;// u20 151117
		address = INF_st.read_cmd[0].mapping;
		pword = (__u16 *)(&pdata);
#ifdef MD_LITTLE_ENDIAN
		//++ Make Big Endian
		for(i = 0; i < (byte_count + 1)/2 ; i++)
			pext->data[i] = htons(pword[i]);
#endif
		break;
	case '#' :
		if(argc != 1)
		{
			dprintf("[TMPR %d] Temperature # Format Error, Data Num : %d!\n",port_num,argc);
			return -1;
		}
		pflt.FanFault = hexstr2num(argv[index],strlen(argv[index++]));
		dprintf("Fan Fault:0x%0X\n",pflt.FanFault);
		byte_count = 2;
//		address = INF_st.read_cmd[1].address;//6; // u20 151117
		address = INF_st.read_cmd[1].mapping;
#ifdef MD_LITTLE_ENDIAN
		//++ Make Big Endian
		pext->data[0] = htons(pflt.FanFault);
#endif
		break;
	case '?' :
		if(argc != 4)
		{
			dprintf("[TMPR %d] Temperature $ Format Error, Data Num : %d!\n",port_num,argc);
			return -1;
		}
		pset.FanStartTemperature = decstr2num(argv[index],strlen(argv[index++]));
		pset.FanMaxTemperature = decstr2num(argv[index],strlen(argv[index++]));
		pset.FanStartHimidity = decstr2num(argv[index],strlen(argv[index++]));
		pset.FanFaultMask = hexstr2num(argv[index],strlen(argv[index++]));
		dprintf("FanStartTemp:%d, FanMaxTemp:%d, FanStartHumi:%d, FanFaultMask:%02X\n",pset.FanStartTemperature,pset.FanMaxTemperature,pset.FanStartHimidity,pset.FanFaultMask);
		byte_count = 8;
//		address = INF_st.read_cmd[2].address;//7; // // u20 151117
		address = INF_st.read_cmd[2].mapping;
		pword = (__u16 *)(&pset);
#ifdef MD_LITTLE_ENDIAN
		//++ Make Big Endian
		for(i = 0; i < (byte_count + 1)/2 ; i++)
			pext->data[i] = htons(pword[i]);
#endif
		break;
	default : break;
	}

	pext = (modbus_res_hdr_ext *)writebuff;
	pext->unit_id = port_num;
	pext->function_code = 0x03;
	pext->address = address; // ??
	pext->byte_count = byte_count;

	crc = CRC16((__u8*)pext,6+byte_count)&0xFFFF;
	writebuff[6+byte_count] = crc>>8;
	writebuff[7+byte_count] = crc&0x00FF;

	port_up(out,writebuff,8+byte_count,port_num);

	return 1;
}

static int select_nextquery(int port_num, int mode)
{
	int i;
	int next_mode = PERIODIC_REQ;
	char head,tail,length;
	char str[40], temp[8];

pthread_mutex_lock(&mutex_inf);
	head = CMD_q.write_head;
	tail = CMD_q.write_tail;
pthread_mutex_unlock(&mutex_inf);

	if( mode != PERIODIC_REQ_PRI && head != tail )
	{
pthread_mutex_lock(&mutex_inf);
		length = CMD_q.write_length[head];
		memcpy(recvbuff,CMD_q.write_buff[head],length);
		CMD_q.write_head++;
		CMD_q.write_head %= MAX_Q_SIZE;
pthread_mutex_unlock(&mutex_inf);
#if 0
		dprintf("[CMD dequeue inf:%d] head:%d tail:%d\n",port_num,
			CMD_q.write_head,tail);
		sprintf(str,"\t");
		for(i = 0; i < length; i++)
		{
			sprintf(temp,"%02X ",recvbuff[i]);
			strcat(str,temp);
		}
		dprintf("%s\n",str);
#endif

		next_mode = WRITE_REQ;
	}
	return next_mode;
}

void condm_bufm(void *arg)
{
	int sockfd;
	int port_num, len, check, i;
	fd_set readfds,	testfds;
	char tempbuff[DEFAULT_BUFF_SIZE],writebuff[16];
	char str[40];
	__u8 unit_id, func_code;
	__u16 address, points, byte_count;
	struct timeval timeout;
	char head,tail;
	modbus_res_hdr_ext *preq;
	modbus_res_hdr_ext *pext;

	port_num = (int)arg;
	pthread_detach(pthread_self());

	dprintf(" - Buffer Manager Start [Port: %d]\n",port_num);

	sockfd = INF_st.sockfd;

	FD_ZERO(&readfds);
	FD_SET(sockfd, &readfds);

	preq = (modbus_res_hdr_ext *)tempbuff;

	while(1)
	{
		testfds = readfds;

		timeout.tv_sec = 30;
		timeout.tv_usec = 0;
		if(select(sockfd+1, &testfds, (fd_set*)0, (fd_set*)0, (struct timeval*)&timeout) != 0)
		{
			if( FD_ISSET(sockfd,&testfds) ) {  // Write Command
				len = read(sockfd,tempbuff,MAX_CMD_SIZE);
				if( len >= 6 && len < 256 )
				{
					unit_id = preq->unit_id;
					func_code = preq->function_code;
					address = preq->address;

					if( func_code == write_single_reg || func_code == write_mult_regs )
					{
pthread_mutex_lock(&mutex_inf);
						head = CMD_q.write_head;
						tail = CMD_q.write_tail;
pthread_mutex_unlock(&mutex_inf);
						if( (tail+1)%MAX_Q_SIZE == head )
						{
						//	dprintf("[MM CMD %d] Write buff is full!!\n",port_num);
						}
						else
						{
							if( len == 6 || len == 11 )
							{
pthread_mutex_lock(&mutex_inf);
								memcpy(CMD_q.write_buff[tail],tempbuff,len);
								CMD_q.write_length[tail]=len;
								CMD_q.write_tail++;
								CMD_q.write_tail %= MAX_Q_SIZE;
pthread_mutex_unlock(&mutex_inf);
#if 0
								dprintf("[CMD queue inf:%d] head:%d tail:%d (len:%d)\n",port_num,
									head,CMD_q.write_tail,len);
								sprintf(str,"\t");
								for(i = 0; i < len; i++)
								{
									sprintf(temp,"%02X ",CMD_q.write_buff[tail][i]);
									strcat(str,temp);
								}
								dprintf("%s\n",str);
#endif

							}
							else
							{
							//	dprintf("[MM CMD %d] CMD Length error [%d]\n",port_num,len);
							}
						}
					}
					else // func_code error
					{
					//	dprintf("[MM CMD %d] Precheck error - Func code [%d]\n",port_num,func_code);
					}
				} // length error
			} //
		}
		usleep(100000);
	}
}

void condm_thread(void *arg)
{
	int slavefd,slavefds[2];
	fd_set readfds,	testfds;
	int fd, sockfd;
	int	nread;
	int port_num, mode, result;
	int system_no;
	struct timeval timeout;
	int tx_time, rx_time, timeout_time;
	int num_port;

	port_num = (int)arg;
	pthread_detach(pthread_self());

	sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);	// pm
	if(sockfd < 0)
	{
		dprintf("[MM %d] socket open error!!!\n",port_num);
		return;
	}

restart:
	slavefds[0] = 0;
	slavefds[1] = 0;
	num_port = INF_st.num_port;
	slavefds[0] = open_link(0);							// 3rd Party

	if( slavefds[0] < 0 ) goto restart;

	cmd_handshake(sockfd,port_num,0xAA);
	cmd_clear(sockfd);

	fd_index = 0;
	main_line = fd_index ;
	slavefd = slavefds[fd_index];
	tx_time = INF_st.tx_time*1000;
	rx_time = INF_st.rx_time*1000;
	timeout_time = INF_st.timeout*1000;

	if( INF_st.num_addr_map > 0 && INF_st.num_addr_map <= 16 )
		system_no = INF_st.num_addr_map;
	else
		system_no = 1;

	dprintf("COND Start [INF: %d]\n",port_num);

	FD_ZERO(&readfds);
	FD_SET(slavefd, &readfds);

	while(1){
		testfds = readfds;
		mode = select_nextquery(port_num,mode);
		if(mode == WRITE_REQ)
		{
			mode = cond_write(sockfd,slavefd,port_num);
		}

		timeout.tv_sec = 0;
		timeout.tv_usec = timeout_time;

		usleep(rx_time);
		result = -1;

		if(select(FD_SETSIZE, &testfds, (fd_set*)0, (fd_set*)0, (struct timeval*)&timeout) != 0){
			if( FD_ISSET(slavefd,&testfds) ) {
				ioctl(slavefd, FIONREAD,&nread);
				if(nread > 0){
					result = cond_read(slavefd,sockfd,port_num);
				}
				usleep(30);
			}
		}

		if( result > 0 )
		{
			line_check(port_num,sockfd,port_num,ALIVE);
			update_line_info(main_line, RX);
		}
		else
		{
			line_check(port_num,sockfd,port_num,BAD);
		}
	}
	close(slavefd);
	close(sockfd);
}
