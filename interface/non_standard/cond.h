#ifndef	_TMPR_H
#define	_TMPR_H

#define MSG_MAX_LEN		512

//#define ASCII_STF		''
//#define ASCII_EOF		0xFE

#define IBIT_REQ_MODE		0x33
#define IBIT_REQ_DATA		0xCC
#define PBIT_RES_MODE		0x22
#define PBIT_RES_NORM		0x88
#define PBIT_RES_FAULT		0xFF
#define PBIT_RES_PREPARE	0x33
#define IBIT_RES_MODE		0x33
#define DETECT_MODE			0x44
#define DETECT_DATA_PURIFI	0xFF
#define DETECT_DATA_UNDETEC	0x33
#define DETECT_DATA_BLISTER	0x81
#define DETECT_DATA_NERVE	0x82
#define DETECT_DATA_BOTH	0x84

typedef struct{
	__u16	t1;
	__u16	h1;
	__u16	t2;
	__u16	h2;
	__u16	t3;
	__u16	fan;
}COND_DAT;

typedef struct{
	__u16	FanStartTemperature;
	__u16	FanMaxTemperature;
	__u16	FanStartHimidity;
	__u16	FanFaultMask;
}COND_SET;

typedef struct{
	__u16	FanFault;
}COND_FLT;
#endif		// _TMPR_H
