#pragma pack(1)
/* Modbus header */

#define MAX_SENSOR_NUM	64

typedef struct _fds_cmd_hdr {
	__u8		ascii_stx;
	__u8		ascii_cmd;
	__u8		check_sum;
	__u8		ascii_etx;
} fds_cmd_hdr;

typedef struct _fds_sensor_hdr {
	__u8		ascii_stx;
	__u16		loop_data;
	__u8		sensor_data[MAX_SENSOR_NUM];
	__u8		check_sum;
	__u8		ascii_etx;
} fds_sensor_hdr;

