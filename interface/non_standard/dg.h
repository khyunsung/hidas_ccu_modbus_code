#pragma pack(1)
/* Digel generator communication  */

#define MAX_SENSOR_NUM	128

typedef struct dg_frame_format {
	__u16		block_start_sync;
	__u16		info_block_length;
	__u8		block_type;
	__u16		data[MAX_SENSOR_NUM];	//block check, block_end_sync
} dg_frame_format;
