#ifndef	_CHEM_H
#define	_CHEM_H

#define MSG_MAX_LEN		512

#define ASCII_STF		0x4E
#define ASCII_EOF		0xFE

#define IBIT_REQ_MODE		0x33
#define IBIT_REQ_DATA		0xCC
#define PBIT_RES_MODE		0x22
#define PBIT_RES_NORM		0x88
#define PBIT_RES_FAULT		0xFF
#define PBIT_RES_PREPARE	0x33
#define IBIT_RES_MODE		0x33
#define DETECT_MODE			0x44
#define DETECT_DATA_PURIFI	0xFF
#define DETECT_DATA_UNDETEC	0x33
#define DETECT_DATA_BLISTER	0x81
#define DETECT_DATA_NERVE	0x82
#define DETECT_DATA_BOTH	0x84

typedef struct{
	__u8	stx;
	__u8	mode;
	__u8	data;
	__u8	cs;
	__u8	etx;
}CHEM_DAT;


typedef struct {
	__u8	message_header;
	__u8	message_header_func_code;
	__u8	data_func_code;
	__u8	data_length_8;
	__u8	data_length_16h;
	__u8	data_length_16l;
	unsigned short	current_loop_value[4];
	__u8	data_status;
	__u8	gas_detection_valid_flag;
	unsigned long	error_information;
	float	ims_pos[8];
	float	ims_neg[8];
	float	cell_temperature;
	float	external_temperature;
	float	humidity;
	float	flow;
	float	sccell[4];
	float	absolute_humidity;
	__u8	conc_classification;
	__u8	version;
	__u8	gas_name[32];
	__u8	message_tail[2];

} C_SENSOR_DATA; 

#endif		// _CHEM_H

