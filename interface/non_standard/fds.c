#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#include <asm/types.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <termios.h>

#include "common.h"

#include "../app.h"
#include "../interface.h"
#include "../modbus.h"
#include "../utils.h"
#include "../log.h"
#include "fire_detecting_system.h"

//#define	DEBUG
#ifndef	DEBUG
	#define	dprintf(format, args...) log_access(format, ## args)
#else
	#define	dprintf(format, args...) log_access(format, ## args); printf(format, ## args)
#endif	// DEBUG

//#define		WRITE_DEBUG

#define BASE 			3000


extern __u8 inf_alive;

extern INF_ST INF_st;
extern CMD_Q CMD_q;

extern pthread_mutex_t mutex_inf;
extern pthread_mutex_t mutex_mem;


extern unsigned short INF_mem[MAX_INF_TAG];
extern char sendbuff[DEFAULT_BUFF_SIZE];
extern char recvbuff[DEFAULT_BUFF_SIZE];
extern unsigned short mflag;


#define STX 0x02
#define ETX 0x03
#define EOT 0x04
#define ACK 0x06
#define NAK 0x15
#define ETB 0x17

static int fds_write(int fdsPort,int cmdType)
{
	unsigned char Buffer[10];
	   
	switch(cmdType)
	{
	case 0:		
	       Buffer[0] =   STX;
           Buffer[1] =   'S'; /* command S */
           Buffer[2] =  0x33; /* checksum = ((0x53 % 256) & 0x0F) + 0x30  */
           Buffer[3] =   ETX;        
    break;
	case 1:
	       Buffer[0] =   STX;		
           Buffer[1] =   ACK; /* control ACK*/
           Buffer[2] =  0x36; /* checksum = ((0x06 % 256) & 0x0F) + 0x30  */
           Buffer[3] =   ETX;        
    break;
	case 2:
	       Buffer[0] =   STX;		
           Buffer[1] =   NAK; /* condtrol NAK */
           Buffer[2] =  0x35; /* checksum = ((0x15 % 256) & 0x0F) + 0x30  */
           Buffer[3] =   ETX;        
    break;		
	case 3:
	       Buffer[0] =   STX;		
           Buffer[1] =   ACK; /* condtrol NAK */
           Buffer[2] =  0x36;
           Buffer[3] =   EOT; /* checksum = ((0x06 % 256) & 0x0F) + 0x30  */
    break;    
	}

    inf_write(fdsPort,Buffer,4,PRIMARY);
    
	return 1;
}

#define MAXQUEUE  4096
#define MAXBUFFER 1024
typedef struct
{
        int Front;
        int Rear;
        unsigned char Buffer[MAXQUEUE];
} fdsQType;

fdsQType fdsQ;

void fds_read_thread(void *arg)
{
	int in;
	int len;
	int i;
 	char rzbuff[DEFAULT_BUFF_SIZE];
 	
	in = (int) arg;

	fdsQ.Front = 0;
	fdsQ.Rear  = 0;
	
	while(1)
	{
	     len = inf_read(in,rzbuff,DEFAULT_BUFF_SIZE-16,PRIMARY);
	     
	     if((len < DEFAULT_BUFF_SIZE-16 ) && (len > 0))
	     {
	    	 for(i=0;i<len;i++) {
	    		 fdsQ.Buffer[fdsQ.Front] = rzbuff[i];
	    		 fdsQ.Front = (fdsQ.Front +1) % MAXQUEUE;
	    	 }
             //memcpy(&fdsQ.Buffer[fdsQ.Front],rzbuff,len);
	         //fdsQ.Front = (fdsQ.Front+len) % MAXQUEUE;
	     }
	}
}


int frameStart;
int fdsIndex; 
unsigned char fdsBuffer[MAXBUFFER];

int fds_decoding(int fdsPort,int udpSock,int port_num)
{
   
	unsigned short mask,value;
	unsigned short crc;
	unsigned char  c,checksum;

 	char writebuff[DEFAULT_BUFF_SIZE];	
	int  i,j,len,ret;

	ret = 0;
	
    modbus_res_hdr_ext *pext;	
	

	while(fdsQ.Front != fdsQ.Rear)
	{ 
          c = fdsQ.Buffer[fdsQ.Rear];
 		  fdsQ.Rear = (fdsQ.Rear+1) % MAXQUEUE;
	      switch(c)
		  {
		  case STX: 
			    if(!frameStart)
			    {
			       frameStart = 1;
		           fdsIndex   = 0;
			    }
			    else
			    {
			    	dprintf("STX aleady in received buffer![%d] \n",fdsIndex);
			    }
		  break;
		  case ETB:  
		  case ETX:
			   if(frameStart)
			   {			    
		          checksum = 0; 
		          for(i=0;i < (fdsIndex-1);i++)
		          {
		        	   checksum = checksum + fdsBuffer[i];
		        	   //dprintf(" %02X \n",fdsBuffer[i]);
		          } 
		          //dprintf("sum =  %02X\n",checksum);
		        
		          checksum = ((checksum % 256) & 0x0F) + 0x30;
		        
		          if(checksum == fdsBuffer[fdsIndex-1])
		          {
		        	  
		              ret = ((fdsBuffer[0] - '0') * 10) + (fdsBuffer[1] - '0');
		              if(ret != 11)
		                 fds_write(fdsPort,1); /* send ACK to FDS */
		              else
		            	 fds_write(fdsPort,3); /* send ACK and EOT  to FDS */
		        	             
		              for(i=0;i<INF_st.num_read_cmd;i++)		            	  
		              {
		                 if(INF_st.read_cmd[i].unit_id == ret)
		                 {		        	   
				            dprintf("loop no [%d unit_id %d Length %d] Mapping Address %d\n",
				            		 ret,port_num,INF_st.read_cmd[i].points,INF_st.read_cmd[i].mapping);
		                	 
		           	        pext = (modbus_res_hdr_ext *)writebuff;
		        		
		        		    pext->unit_id       = port_num;
		        		    pext->function_code = 0x03;
		        		    pext->address       = INF_st.read_cmd[i].mapping;
		        		                       
		        		    for(j = 0; j < INF_st.read_cmd[i].points; j++)
		        		    {
								value = 0;
								mask  = 0;
								switch(fdsBuffer[j+2])
								{
								case 'N': value = 0;      break;
								case 'F': value = 0x0100; break;
								case 'R': mask =  1;      break;
								case 'P': mask =  1;      break;
								case 'X': mask =  1;      break;
								case 'D': value = 0;      break;
								case 'A': value = 0x0100; break;
								}
       		                    pext->data[j] = (value | mask);
		        		    }
		        		    pext->byte_count  = INF_st.read_cmd[i].points * 2;
		        		
		        		    crc = CRC16((__u8*)pext,6+pext->byte_count)&0xFFFF;
		        		    
		        		    writebuff[6+pext->byte_count] = crc>>8;
		        		    writebuff[7+pext->byte_count] = crc&0x00FF;
		        	                     
		        		    port_up(udpSock,writebuff,8+pext->byte_count,port_num);	
		        	     }
		              }
		           }
		           else
		           {
		          	   dprintf(" Checksum Error : Received Value [%02X] != Calc Value[%02X] \n ",fdsBuffer[fdsIndex-1],checksum);
		               fds_write(fdsPort,2); /* send NAK to FDS */
		           }
		           frameStart = 0;
			    }
		   break;
		   default:
		    	 if(frameStart) 
		    	 {
                    fdsBuffer[fdsIndex] = c;
                    fdsIndex++;
		    	 }
		    	 else
		    	 {
		    		dprintf("Unknown data in received buffer![%02X] \n",c);
		    	 }
           break;
		   }

    }
	return ret;
}

void fdsm_thread(void *arg)
{
    int udpSock; 
	int port_num;   
    
	long timeOut;
	int  timeOutCount;
	int  retryCount;
	int  loop;

	int fdsPort;
	int rx_time;	
	
	pthread_t fdsReadThread;

	port_num = (int)arg;
	
	pthread_detach(pthread_self());
		
	/* Open Modbus UDP Client socket  */
    /* DG( non Standard ) --> Modbus UDP(sockfd)  */
	udpSock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);	
	if(udpSock < 0)
	{
		dprintf("[MM %d] socket open error!!!\n",port_num);
		return;
	}	
	
	cmd_handshake(udpSock,port_num,0xAA);
	cmd_clear(udpSock);

	
	frameStart = 0;
	fdsIndex   = 0;
	
	dprintf("Frie Dectection System [INF: %d]\n",port_num);

	fdsPort = open_link(0);
	
	rx_time      = INF_st.rx_time*1000;	
	timeOut      = 5000000; /* 5 seconds */ 
	timeOutCount = timeOut / rx_time;
	
	pthread_create(&fdsReadThread, NULL, (void *)&fds_read_thread,(void *)fdsPort);			
	
	while(1)
	{
		//line_check(port_num,sockfd,port_num,ALIVE);	
		dprintf(" write s command! \n");	
		fds_write(fdsPort,0);	
		
		loop       = 0;
		retryCount = 0;
		
		while(loop < 11) 
		{ 
 		  	  loop = fds_decoding(fdsPort,udpSock,port_num);		  	  
 		  	  if(loop == 0)
 		  	  {	  
 	  		     retryCount++;
 		  		 if(retryCount > timeOutCount) 
 		  		 {
 		  		    dprintf(" Timeout!  Retransmit aftre 5 seconds.\n");
 		  			break;
 		  	     }
 		  	  }
 		  	  else
 		  	  {
 		  		 retryCount = 0;
 				 line_check(0,udpSock,port_num,ALIVE);
 				 update_line_info(udpSock, RX);		  		 
 		  	  }				  	  
 			  usleep(rx_time);				  	  
		} 
	}	
	close(fdsPort);
	close(udpSock);		
}