#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#include <asm/types.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <termios.h>

#include "../../../include/common.h"

#include "../app.h"
#include "../interface.h"
#include "../modbus.h"
#include "../utils.h"
#include "../log.h"
#include "fire_detecting_system.h"

//#define	DEBUG
#ifndef	DEBUG
	#define	dprintf(format, args...) log_access(format, ## args)
#else
	#define	dprintf(format, args...) log_access(format, ## args); printf(format, ## args)
#endif	// DEBUG

//#define		WRITE_DEBUG

#define BASE 			3000

extern int inf_tx_cnt, inf_rx_cnt, inf_thr_cnt;
extern __u8 inf_alive;

extern INF_ST INF_st;
extern CMD_Q CMD_q;

extern pthread_mutex_t mutex_inf;
extern pthread_mutex_t mutex_mem;

extern int fd_index, read_index;
extern unsigned short INF_mem[MAX_INF_TAG];
extern char sendbuff[DEFAULT_BUFF_SIZE];
extern char recvbuff[DEFAULT_BUFF_SIZE];
extern unsigned short mflag;

static unsigned char ack_message;

static unsigned char cal_check_sum(unsigned int value)
{
	return ((value%256)&0x0F) + 0x30;
}
	
static int fds_cmd_write(int in, int out, int port_num, unsigned int loop_step)
{

	char writebuff[DEFAULT_BUFF_SIZE];
	fds_cmd_hdr *cmdm = (fds_cmd_hdr *)writebuff;
	
	loop_step = loop_step + 1; 			//1遺�꽣 �쒖옉�섍린 �뚮Ц��
	cmdm->ascii_stx = 0x02;
	cmdm->ascii_etx = 0x03;	

	if(loop_step == 1)
	{
		cmdm->ascii_cmd = 0x53;
	}
	else
	{
		if(ack_message = 1)	cmdm->ascii_cmd = 0x06;
		else cmdm->ascii_cmd = 0x15;
	}		
	
	cmdm->check_sum =cal_check_sum(cmdm->ascii_cmd);	
	
	inf_write(out,writebuff,4,PRIMARY);
		
	return 1;
}
static int fds_cmd_read(int in, int out,int port_num, unsigned int loop_step)
{
	unsigned int sum_datas;
	char buff[DEFAULT_BUFF_SIZE];
	char writebuff[DEFAULT_BUFF_SIZE];
	int i, len;
	__u16 crc, r_crc;
	fds_sensor_hdr *pres;
	modbus_res_hdr_ext *pext;
	
	loop_step = loop_step + 1; 			//1遺�꽣 �쒖옉�섍린 �뚮Ц��
	len = inf_read(in,buff,DEFAULT_BUFF_SIZE-16,PRIMARY);
	if(len < 5 )
	{
		dprintf("[MM %d] Slave Read Too Short [%d]!\n",port_num,len);
		return -1;
	}
	if(len >= DEFAULT_BUFF_SIZE-16)
	{
		dprintf("[MM %d] Slave Read Too Long [%d]!\n",port_num,len);
		return -1;
	}
	
	pres = (fds_sensor_hdr *)buff;
	
	if(pres->loop_data != loop_step)
	{
		dprintf("[MM %d] Loop is dismatch[%d]!\n",port_num,loop_step);
		return -1;
	}
	
	sum_datas = pres->loop_data;
	for(i = 0; i < INF_st.fds_read[loop_step].number; i++) sum_datas += pres->sensor_data[i];
	if(pres->check_sum != cal_check_sum(sum_datas))
	{
		dprintf("[MM %d] Check sum is dismatch[cal:%x, received:%x]!\n",port_num,pres->check_sum,cal_check_sum(sum_datas));
		return -1;
	}

	pext = (modbus_res_hdr_ext *)writebuff;
	
	pext->unit_id = port_num;
	pext->function_code = 0x03;
	pext->address = INF_st.fds_read[loop_step].mapping;
	for(i = 0; i < INF_st.fds_read[loop_step].number; i++)	pext->data[i] = pres->sensor_data[i];
	pext->byte_count = INF_st.fds_read[loop_step].number * 2;
	
	crc = CRC16((__u8*)pext,6+pext->byte_count)&0xFFFF;
	writebuff[6+pext->byte_count] = crc>>8;
	writebuff[7+pext->byte_count] = crc&0x00FF;

	port_up(out,writebuff,8+pext->byte_count,port_num);	
	
	return 1;
}

void fdsm_thread(void *arg)
{
	int slavefd,slavefds[2],tempfd;
	fd_set readfds,	testfds;
	int fd, sockfd, flags;
	int	nread;
	int port_num, mode, result, rresult, retries;
	unsigned int fds_loop_step;
	int read_err_cnt;
	
	struct timeval timeout;
	char write_flag;
	int tx_time, rx_time, timeout_time;
	int num_port, num_retry, num_read;
	
	port_num = (int)arg;
	pthread_detach(pthread_self());

	sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);	
	if(sockfd < 0)
	{
		dprintf("[MM %d] socket open error!!!\n",port_num);
		return;
	}
	
restart:
	read_err_cnt = 0;
	slavefds[0] = 0;
	slavefds[1] = 0;
	num_port = INF_st.num_port;

	tempfd = 0;
	slavefds[0] = open_link(0);

	
//	cmd_handshake(INF_st.sockfd,port_num,0xAA);
//	cmd_clear(INF_st.sockfd);
	cmd_handshake(sockfd,port_num,0xAA);
	cmd_clear(sockfd);

	retries = 0;
	ack_message = 0;

	fd_index = 0;

	
	slavefd = slavefds[fd_index];
//	sockfd = INF_st.sockfd;
	tx_time = INF_st.tx_time*1000;
	rx_time = INF_st.rx_time*1000;
	timeout_time = INF_st.timeout*1000;
	num_retry = INF_st.num_retry;
	num_read = INF_st.num_read_cmd;
	//if( INF_st.num_addr_map > 0 && INF_st.num_addr_map <= 16 )
		//system_no = INF_st.num_addr_map;
	//else
		//system_no = 1;
	mode = PERIODIC_REQ;


	dprintf("Modbus Master Start [INF: %d]\n",port_num);

	FD_ZERO(&readfds);
//	FD_SET(sockfd, &readfds);
	FD_SET(slavefd, &readfds);

	while(1){
		testfds = readfds;
		mode = fds_cmd_write(sockfd,slavefd,port_num,fds_loop_step);	
		result = -1;
		timeout.tv_sec = 0;
		timeout.tv_usec = timeout_time;

		if(select(FD_SETSIZE, &testfds, (fd_set*)0, (fd_set*)0, (struct timeval*)&timeout) != 0){

			if( FD_ISSET(slavefd,&testfds) ) {
				ioctl(slavefd, FIONREAD,&nread);
				if(nread > 0){
					if(fds_cmd_read(slavefd,sockfd,port_num,fds_loop_step))
					{
						line_check(port_num,sockfd,port_num,ALIVE);
						++fds_loop_step % INF_st.num_read_cmd;
						ack_message = 1;
					}
					else 
					{
						read_err_cnt++;
						if(read_err_cnt > 5)
						{
							read_err_cnt = 0;
							ack_message = 0;
							++fds_loop_step % INF_st.num_read_cmd;
							
						}
					}
				}
				usleep(tx_time);
			}
			

	#ifdef ECP_PHY
			line_check(port_num,sockfd,port_num,PHY_CHECK);
	#endif
		}
		else
		{
			retries++;
			line_check(port_num,sockfd,port_num,BAD);
			if(retries > num_retry)
			{ 
				dprintf("[MM %d] Query Conversion!\n",port_num);
				retries = 0;
				fds_loop_step = 0;
			}
			
		}
	}
	close(slavefd);
	close(sockfd);
}
