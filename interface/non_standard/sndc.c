/***********************************************************************************
  Filename:     sndc.c

  Description:

***********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#include <asm/types.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <termios.h>
#include <time.h>

#include "common.h"

#include "../app.h"
#include "../interface.h"
#include "../modbus.h"
#include "../utils.h"
#include "../log.h"
#include "sndc.h"

//#define	DEBUG
#ifndef	DEBUG
	#define	dprintf(format, args...) log_access(format, ## args)
#else
	#define	dprintf(format, args...) log_access(format, ## args); printf(format, ## args)
#endif	// DEBUG

#define MD_LITTLE_ENDIAN

extern INF_ST INF_st;
extern int fd_index;
extern unsigned short INF_mem[MAX_INF_TAG];
extern pthread_mutex_t mutex_mem;

int main_line;
static unsigned short seq = 0;

static int sndc_write(int in, int out, int port_num)
{
	char buf[DEFAULT_BUFF_SIZE];
	HEADER *phdr;
	TAIL *ptail;
	SNDC_TXD *pdata;
	int transmited_bytes, len;
	unsigned short checksum;
	__u16 *pword;
	int local_time;

	local_time = time(NULL);

	len = sizeof(HEADER) + sizeof(SNDC_TXD) + sizeof(TAIL);

	phdr = (HEADER *)buf;
	pdata = (SNDC_TXD *)(phdr+1);
	ptail = (TAIL *)(pdata+1);

	phdr->stx = ASCII_STX;
	phdr->spare = 0;
	phdr->msg_id = seq++;
	phdr->msg_st = 0;
	phdr->len = len;		// 32Byte
	phdr->time_stamp = local_time;

	pdata->heartbeat = 0xAA55;

	ptail->cs = 0;
	ptail->spare = 0;
	ptail->etx = ASCII_ETX;
	checksum = checksum16(buf, len);
	ptail->cs = checksum;

	transmited_bytes = inf_write(out,buf,len,PRIMARY);
	if(transmited_bytes > 0) update_line_info(main_line, TX);
	return 1;
}

static int sndc_read(int in, int out,int port_num)
{
	char *buf;
	char buff[DEFAULT_BUFF_SIZE]={0,};
	char writebuff[DEFAULT_BUFF_SIZE];
	modbus_res_hdr_ext *pext;
	HEADER *phdr;
	TAIL *ptail;
	SNDC_DAT *pdata;
	int size, len, byte_count, i;
	unsigned short checksum, crc;
	__u16 *pword;
	char str[500], tempstr[20];

	len = inf_read(in,buff,DEFAULT_BUFF_SIZE-16,PRIMARY);
	//+++++ UART Error +++++
	if(buff[0]==0){
		if(buff[len-1]==0) len-=2;
		else len--;
		buf = (char *) &buff[1];
	}
	else{
		if(buff[len-1]==0) len--;
		buf = buff;
	}
	//+++++++++++++++++++++++

	phdr = (HEADER *)buf;
	pdata = (SNDC_DAT *)(phdr+1);
	ptail = (TAIL *)(pdata+1);
	size = sizeof(HEADER) + sizeof(SNDC_DAT) + sizeof(TAIL);
	if(size != len)
	{
		dprintf("[MM %d] SNDC Read Length Mismatch [%d]!\n",port_num,len);
		return -1;
	}
	if(size != phdr->len)
	{
		dprintf("[MM %d] SNDC NDMC Length Mismatch size[0x%02X] len[0x%02X]\n",port_num,size,phdr->len);
		return -1;
	}
	checksum = checksum16(buf, len);
	if(checksum != 0)
	{
		if(len&1)checksum = ~(~htons(checksum16(buf, len-4)) + ptail->etx);
		else checksum = ~(~checksum16(buf, len-4) + htons(ptail->etx));
		dprintf("[MM %d] NDMC Checksum Mismatch! cs : 0x%04X, cal : 0x%04X\n",port_num,ptail->cs,checksum);
		return -1;
	}

	if(phdr->stx != ASCII_STX)
	{
		dprintf("[MM %d] SNDC STX Mismatch [%d]!\n",port_num,phdr->stx);
		return -1;
	}
	if(ptail->etx != ASCII_ETX)
	{
		dprintf("[MM %d] SNDC ETX Mismatch [%d]!\n",port_num,ptail->etx);
		return -1;
	}
#if 0
	dprintf("ote_comp : %f\n",pdata->ote_comp);
	dprintf("ote_trim : %f\n",pdata->ote_trim);
	dprintf("sr_act : %f\n",pdata->sr_act);
	dprintf("hpf_act : %f\n",pdata->hpf_act);
	dprintf("hpa_act : %f\n",pdata->hpa_act);
	dprintf("sr_ord : %f\n",pdata->sr_ord);
	dprintf("hpf_ord : %f\n",pdata->hpf_ord);
	dprintf("hpa_ord : %f\n",pdata->hpa_ord);
	dprintf("mode : %d\n",pdata->mode);
	dprintf("sr_valid : 0x%04\n",pdata->sr_valid);
	dprintf("hpf_valid : %d\n",pdata->hpf_valid);
	dprintf("hpa_valid : %d\n",pdata->hpa_valid);
#endif

	byte_count = sizeof(SNDC_DAT);

	pext = (modbus_res_hdr_ext *)writebuff;
	pext->unit_id = port_num;
	pext->function_code = 0x03;
	pext->address = 0;
	pext->byte_count = byte_count + byte_count%2;	//++ Make Even Number

	pword = (__u16 *)pdata;
#ifdef MD_LITTLE_ENDIAN
	//++ Make Big Endian
	i = 0;
//	float ote_comp; // 4Byte
	pext->data[i++] = htons(pword[1]);
	pext->data[i++] = htons(pword[0]);
//	float ote_trim;
	pext->data[i++] = htons(pword[3]);
	pext->data[i++] = htons(pword[2]);
//	float sr_act;
	pext->data[i++] = htons(pword[5]);
	pext->data[i++] = htons(pword[4]);
//	float hpf_act;
	pext->data[i++] = htons(pword[7]);
	pext->data[i++] = htons(pword[6]);
//	float hpa_act;
	pext->data[i++] = htons(pword[9]);
	pext->data[i++] = htons(pword[8]);

//	float sr_ord;
	pext->data[i++] = htons(pword[11]);
	pext->data[i++] = htons(pword[10]);
//	float hpf_ord;
	pext->data[i++] = htons(pword[13]);
	pext->data[i++] = htons(pword[12]);
//	float hpa_ord;
	pext->data[i++] = htons(pword[15]);
	pext->data[i++] = htons(pword[14]);

//	short mode;		// 2Byte
	pext->data[i++] = htons(pword[16]);
//	short sr_valid;
	pext->data[i++] = htons(pword[17]);
//	short hpf_valid;
	pext->data[i++] = htons(pword[18]);
//	short hpa_valid;
	pext->data[i++] = htons(pword[19]);

	//++ Make Big Endian
//	for(i = 0; i < byte_count/2 ; i++)
//		pext->data[i] = htons(pword[i]);
#else
	for(i = 0; i < byte_count/2 ; i++)
		pext->data[i] = pword[i];
#endif
	crc = CRC16((__u8*)pext,6+byte_count)&0xFFFF;
	writebuff[6+byte_count] = crc>>8;
	writebuff[7+byte_count] = crc&0x00FF;

	port_up(out,writebuff,8+byte_count,port_num);

	return 1;
}

void sndcs_thread(void *arg)
{
	int slavefd,slavefds[2];
	fd_set readfds,	testfds;
	int fd, sockfd;
	int	nread;
	int port_num, mode, result;
	int system_no;
	struct timeval timeout;
	int tx_time, rx_time, timeout_time;
	int num_port;

	port_num = (int)arg;
	pthread_detach(pthread_self());

	sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);	// pm
	if(sockfd < 0)
	{
		dprintf("[MM %d] socket open error!!!\n",port_num);
		return;
	}

restart:
	slavefds[0] = 0;
	slavefds[1] = 0;
	num_port = INF_st.num_port;
	slavefds[0] = open_link(0);							// 3rd Party

	if( slavefds[0] < 0 ) goto restart;

	cmd_handshake(sockfd,port_num,0xAA);
	cmd_clear(sockfd);

	fd_index = 0;
	main_line = fd_index ;
	slavefd = slavefds[fd_index];
	tx_time = INF_st.tx_time*1000;
	rx_time = INF_st.rx_time*1000;
	timeout_time = INF_st.timeout*1000;

	if( INF_st.num_addr_map > 0 && INF_st.num_addr_map <= 16 )
		system_no = INF_st.num_addr_map;
	else
		system_no = 1;

	dprintf("SNDC Read Start [INF: %d]\n",port_num);

	FD_ZERO(&readfds);
	FD_SET(slavefd, &readfds);

	while(1){
		testfds = readfds;
//		mode = sndc_write(sockfd,slavefd,port_num);

		timeout.tv_sec = 0;
		timeout.tv_usec = timeout_time;

		usleep(rx_time);
		result = -1;

		if(select(FD_SETSIZE, &testfds, (fd_set*)0, (fd_set*)0, (struct timeval*)&timeout) != 0){
			if( FD_ISSET(slavefd,&testfds) ) {
				ioctl(slavefd, FIONREAD,&nread);
				if(nread > 0){
					result = sndc_read(slavefd,sockfd,port_num);
				}
				usleep(30);
			}
		}

		if( result > 0 )
		{
			line_check(1,sockfd,port_num,ALIVE);
			update_line_info(main_line, RX);
		}
		else
		{
			line_check(1,sockfd,port_num,BAD);
		}
	}
	close(slavefd);
	close(sockfd);
}

void sndcm_thread(void *arg)
{
	int slavefd,slavefds[2];
//	fd_set readfds;
	int sockfd;
//	int	nread;
	int port_num, mode;
//	int system_no;
	struct timeval timeout;
	int tx_time;
//	int num_port;

	port_num = (int)arg;
	pthread_detach(pthread_self());

restart:
	slavefds[0] = 0;

//	num_port = INF_st.num_port;
	slavefds[0] = open_link(0);							// 3rd Party

	if( slavefds[0] < 0 ) goto restart;

	fd_index = 0;
	slavefd = slavefds[fd_index];
	tx_time = INF_st.tx_time*1000;

	dprintf("SNDC Write Start [INF: %d]\n",port_num);

//	FD_ZERO(&readfds);
//	FD_SET(slavefd, &readfds);

	while(1){
		usleep(tx_time);
		mode = sndc_write(sockfd,slavefd,port_num);
	}
	close(slavefd);
}
