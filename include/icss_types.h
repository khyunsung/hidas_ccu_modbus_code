/*
** project  : ICSS
** filename : icss_types.h
** version  : 0.1.0
** date     : 2005.11.23
** Copyright 2005.
********************
Version history
---------------
Version     : 0.1.0
date        : 2005.11.23
revised by  : bjYoum
description : Initial touch
---------------
********************
*/

#ifndef	_ICSS_TYPES_H
#define	_ICSS_TYPES_H
#include "hicsr.h"

/// Typedefs And Structures
/*
typedef signed char s8;
typedef unsigned char u8;

typedef signed short s16;
typedef unsigned short u16;

typedef signed int s32;
typedef unsigned int u32;

#ifdef	_WIN32
	typedef __int64 s64;
	typedef unsigned __int64 u64;
	#include <winsock.h>
#else
	typedef signed long long s64;
	typedef unsigned long long u64;
	#include <netinet/in.h>
#endif

typedef float f32;
typedef double f64;
*/
#define MAX_FILENAME    32

#pragma pack(4)

#if 0
typedef struct {
	u8 res1;
	u8 res2;
	u16 cnt;
} COMM_HEADER;
#endif	// not used
/*
typedef struct version_t {
	u8 major;
	u8 minor;
	u16 release;
}DB_VERSION;
*/
typedef struct dbfileInfo_t {
	u16	filecrc;
	u8	rsc;      // resource id (pcu) => HISCM unique id
	u8	systype;  // 0: ACONIS-2000, 1: ACONIS-DS
	u32	ftime;
	u32	cnt;	  // number of block <--- key
	u32	size;	  // size of real data block
	DB_VERSION ver;	  // crc
	char name[MAX_FILENAME];
}COMDBFILE, *LPCOMDBFILE;	//sizeof(COMDBFILE) = 52

#pragma pack()

#endif		// _ICSS_TYPES_H
