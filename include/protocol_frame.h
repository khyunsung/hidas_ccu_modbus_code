/*
** version  : 0.1.0
** date     : 2010.05.19
** Copyright 2010.
********************
Version history
---------------
Version     : 0.1.0
date        : 2010.05.19
revised by  : hjchoi
-------------------
********************
*/

#ifndef	_RTU_FRAME_H
#define	_RTU_FRAME_H

#include "icss_types.h"

#define PTC_DOWNNAME "PTC.dwn"   // protocol

#define MAX_UARTCNT             8
#define MAX_LANCNT              2
#define MAX_PORT		(MAX_UARTCNT + MAX_LANCNT)

#define PROFILE_DESC	      	64
#define MAX_PROFILE	     	255
#define MAX_SYSTEM              16

typedef enum{
	PTC_PROTOCOL_NO		= 0,
        PTC_MODBUS_MASTER,
	PTC_MODBUS_SLAVE,
        PTC_NMEA0183,
        PTC_LOADCOM,
        PTC_REEFERCON,
	PTC_VDR_WRITE,

	PTC_PROTOCOL_OTHERS     = 50,
        PTC_DNP_MASTER,
	PTC_DNP_SLAVE,
	PTC_IEC101_MASTER,
	PTC_IEC101_SLAVE,

	PTC_SECONDARY_PORT      = 100      // ����ȭ port
}VAL_PROTOCOL_DEF;

typedef enum {
	BAUDR_0 = 0,
	BAUDR_50,
	BAUDR_75,
	BAUDR_110,
	BAUDR_134,
	BAUDR_150,
	BAUDR_200,
	BAUDR_300,
	BAUDR_600,
	BAUDR_1200,
	BAUDR_1800,
	BAUDR_2400,
	BAUDR_4800,
	BAUDR_9600,
	BAUDR_19200,
	BAUDR_38400,
	BAUDR_57600 = 0x81,
	BAUDR_115200,
	BAUDR_230400,
	BAUDR_460800,
	BAUDR_500000,
	BAUDR_576000,
	BAUDR_921600,
	BAUDR_1000000,
	BAUDR_1152000,
	BAUDR_1500000,
	BAUDR_2000000,
	BAUDR_2500000,
	BAUDR_3000000,
	BAUDR_3500000,
	BAUDR_4000000
} VAL_BAUD_RATE;

typedef enum{
	VAL_ENABLE		=0,
	VAL_DISABLE
}VAL_ENDIS;  // enable /disable

typedef enum{
	VAL_NEVER		=0,
	VAL_SOMETIMES,
	VAL_ALWAYS
}VAL_LNKCONFIRMMODE; // line comfirm mode

// ************************************* protocol main header ***********************
#pragma pack(4)

typedef struct
{
	u8	addr;           // if addr=0, not assigned...
 	u8      ptype;          // VAL_PROTOCOL_DEF	<--- key
	u8	baud;	        // VAL_BAUD_RATE 1-142
	u8 	databits:4,     // 5,6,7,8
                stopbits:4;     // 0:1bit, 1:2bits
	u8      parity:4,       // 0:NONE, 2:EVEN 1:ODD
                flowctrl:4;     // 0:NONE, 1:RTSCTS
        u8      secport;        // 1 ~ 8
	u8	rtype;
        u8	res;
        u8      cfgfile[MAX_FILENAME];
} UARTHDR, *LPUARTHDR;  //sizeof(LANHDR) = 40

typedef struct{
        struct  in_addr ip, subnet, defgw;      // if ip.S_addr=0, not assigned...
        u8      ptype[MAX_SYSTEM];              // VAL_PROTOCOL_DEF	<--- key
	u16     portnumber[MAX_SYSTEM];		// ptype[0], portnumber[0], ip_addr[0] normally used
 	struct  in_addr ip_addr[MAX_SYSTEM]; 	// others are for later use
        u8      secport;			// 0 or 1
        u8      rtype;
	u16	res;
        u8      cfgfile[MAX_FILENAME];
} LANHDR, *LPLANHDR;    //sizeof(LANHDR) = 160

// PTC_HEADER
typedef struct
{
	COMDBFILE file;
	UARTHDR	uart[MAX_UARTCNT];
        LANHDR  lan[MAX_LANCNT];
} PTC_HEADER, *LPPTCHEADER;     // sizeof(PTC_HEADER) = 692 (52+(40*8)+(160*2))

#pragma pack()

#endif		// _RTU_FRAME_H
