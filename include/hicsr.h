#ifndef	_HICSR_H
#define	_HICSR_H

#ifndef DB_TUNER
#include "hican.h"
#endif

#define MAX_PROFILE	     	255
#define MAX_TALKER_CNT		255
#define MAX_NMEA_POINT		16
#define ERR_FRM_SIZE 2

#define MAX_IO_POINT	32
#define MAX_INF_CH		16
#define MAX_UART_CH		12
#define MAX_LAN_CH		 2

#define	MAX_IO_HISTORY	32
#define	IO_HISTORY_MASK	(MAX_IO_HISTORY-1)

#define	MAX_SLOT		130
#define	MAX_IO_SLOT		64
#define	MAX_IF_SLOT		64
#define	MAX_INF_TAG		2048

#define CSRW	1	// write
#define CSRR	2	// read
#define CSRU	3	// update

#ifndef u8
typedef unsigned char u8;
#endif

#ifndef u16
typedef unsigned short u16;
#endif

#ifndef s16
typedef short s16;
#endif

#ifndef u32
typedef unsigned int u32;
#endif

#ifdef	_WIN32
	typedef __int64 s64;
	typedef unsigned __int64 u64;
	#include <winsock.h>
#else
	typedef signed long long s64;
	typedef unsigned long long u64;
	#include <netinet/in.h>
#endif

typedef float f32;
typedef double f64;

typedef struct {
	u32 id;
	u32 high;
	u32 low;
} ERR_FRM;

typedef struct {
	u16 err_cnt;
	ERR_FRM err_frm[ERR_FRM_SIZE];
} REG_ERR_FRM;

typedef struct version_t {
	u8 aco_major;
	u8 aco_minor;
	u8 app_major;
	u8 app_minor;
}DB_VERSION;

typedef struct soeData
{
	struct timeval sTime;	// SOE occurrence time
	u8 rscid;				// resource ID
	u8 card;				// card number
	u8 point;				// point number
	u8 value;				// SOE value
	u32 tag;				// tag ID
} SOEDATA, *LPSOEDATA;


////////////////////////////////////////////////////////////////////////////////
// CAN ID Structure
typedef union{
	struct{
		u32 map:16;
		u32 dev:7;
		u32 proto:3;
		u32 dir:1;
		u32 prio:2;
		u32 rsvd:3;
	};
	u32 id;
}CANID;

enum PRIORITY{
	HICSR_SOE		= 0,
	HICSR_POINT		= 1,
	HICSR_STATUS	= 2,
	HICSR_CONFIG	= 3
};

enum PROTOCOL{
	HICSR_READ	= 0,
	HICSR_WRITE	= 1,
	HICSR_SEND	= 2,

	HICSR_RACK	= 4,
	HICSR_WACK	= 5,

	HICSR_TSYNC	= 7
};

enum DIR{
	HICSR_FROM_PCU= 0,
	HICSR_FROM_IO	= 1
};

#define	PCU_DEV_ID		0
#define	BROAD_DEV_ID	0x7F

////////////////////////////////////////////////////////////////////////////////
// IO Data Structure
typedef struct{
	time_t	time;
	u16		msec;
	u8		point;
	u8		state;
}SOE_DATA;			// sizeof(SOE_DATA) = 8

#ifdef COM
typedef struct {
	unsigned long status;
	unsigned long rx_packets;
	unsigned long tx_packets;
	unsigned long rx_bytes;
	unsigned long tx_bytes;
	unsigned long rx_dropped;
	unsigned long tx_dropped;
	unsigned long errors;
}INF_STATUS;

typedef struct{
	unsigned short	tag[MAX_INF_TAG];
}INF_DATA;

typedef struct {
	INF_DATA INF_data;
	INF_STATUS INF_status;
}INF_MEM;
#endif
typedef union{
	struct{
		u32	digital;
		union{
			u32			pulse;
			SOE_DATA	soe;
		}bin[MAX_IO_POINT];
		u32	old_digital;
		u16	pulse_out;
	};
	s16		analog[MAX_IO_POINT];
	INF_MEM INF_mem;
}IO_DATA;			// sizeof(IO_DATA) = io:260, inf:4096



////////////////////////////////////////////////////////////////////////////////
// IO Configuration Structure
typedef struct{
	u16 type;
	union{
		u16 chattime;	// DI
		u16 keeptime;	// DO
	};
}DIGITAL_CONFIG;

typedef struct{
	u16 type;
	struct {			// for RTD, TC type
		u8 ti_type;	// Specific type of RTD or TC
		u8 ti_range;
	};

	u16 offset;
	u16 slope;
	u16 sf_low;  // added 2011-08-24
	u16 sf_high; // added 2011-08-24
	u16 filter;		// filter 0,1,4,8,16
	u16 res;
}ANALOG_CONFIG;

typedef struct{
	struct{				// PI
		u32 start;
		u32 end;
	};
}PULSE_CONFIG;

typedef struct{
	union {
		struct{
			DIGITAL_CONFIG digital;
			PULSE_CONFIG pulse;
		};
		ANALOG_CONFIG  analog;
	};
}POINT_CONFIG;		// sizeof(POINT_CONFIG) = 16
typedef struct {
	u8 rcnt;   //read cnt	<--- key
	u8 wcnt;   //write cnt	<--- key
	u8 cnt;	   // rcnt+wcnt
        u8 res;
} MODBUS_PROFILE_HEADER;       //sizeof(PROFILE_HEADER) = 4

typedef struct {
	u8 talker_cnt;   //read cnt	<--- key
	u8 read_cnt_per_talker[MAX_TALKER_CNT];   //write cnt	<--- key
} NMEA_PROFILE_INFO;       //sizeof(PROFILE_HEADER) = 4

typedef struct {
	NMEA_PROFILE_INFO write;
	NMEA_PROFILE_INFO read;
} NMEA_PROFILE_HEADER;       //sizeof(PROFILE_HEADER) = 4

typedef struct{
	u8		index;
	u8		data_type;
	u16		blockaddr;
} NMEA_POINT_INFO;

typedef struct{
	u8		talker_index;
	u8		length;
	NMEA_POINT_INFO	point[MAX_NMEA_POINT]	
} NMEA_SENTENCE;        //sizeof(WRITE_PROFILE) = 8

typedef union {
        NMEA_SENTENCE read;
        NMEA_SENTENCE write;
} NMEA_PROFILE;       

typedef struct{
	u8		fnccode;
	u8		res;
	u16		address;
	u16		count;
	u16		blockaddr;      // start address of block for mapping
} MODBUS_READ_PROFILE;         //sizeof(READ_PROFILE) = 8

typedef struct{
	u8		fnccode;
	u16             address;	// dest address
        u8              event;	        // 0: Polling, 1: Event, 2: All
	u8		res;
	u16             blockaddr;	// src address
	u8              blockbpos;
} MODBUS_WRITE_PROFILE;        //sizeof(WRITE_PROFILE) = 8

typedef union {
        MODBUS_READ_PROFILE read;
        MODBUS_WRITE_PROFILE write;
} MODBUS_PROFILE;       //sizeof(MODBUS_PROFILE) = 8

typedef struct {
	MODBUS_PROFILE_HEADER	header;                 // size=4
	MODBUS_PROFILE  data[MAX_PROFILE];      // size=8*255=2040
} MODBUS_CONFIG;      //sizeof(MODBUS_PROFILEDATA) = 2044

typedef struct {
	NMEA_PROFILE_HEADER	header;                 // size=4
	NMEA_PROFILE  data[MAX_PROFILE];      // size=8*255=2040
} NMEA_CONFIG;      //sizeof(MODBUS_PROFILEDATA) = 2044

typedef struct{
	u8		parting_size;
	u16		parting_start_index;
	u8		parting_point_cnt;
	u16		blockaddr;      // start address of block for mapping
} STREAM_READ_PROFILE;         //sizeof(READ_PROFILE) = 8

typedef struct {
	u16			parting_cnt;                 // size=4
	STREAM_READ_PROFILE  data[MAX_PROFILE];      // size=8*255=2040
} STREAM_READ_CONFIG;      //sizeof(MODBUS_PROFILEDATA) = 2044

typedef struct{
	u16 ptc_type;
	union{
		MODBUS_CONFIG modbus;
		NMEA_CONFIG	  mnea;
		STREAM_READ_CONFIG stream;
		//FDS_CONFIG	  fds;
		};
}PTC_CONFIG;

typedef struct
{
	u8	baud;	        // VAL_BAUD_RATE 1-142
	u8 	databits:4,     // 5,6,7,8
                stopbits:4;     // 0:1bit, 1:2bits
	u8      parity:4,       // 0:NONE, 2:EVEN 1:ODD
                flowctrl:4;     // 0:NONE, 1:RTSCTS
    u8      secport;        // 1 ~ 8
	u16 res;
} UART_CONFIG;  //sizeof(LANHDR) = 40

typedef struct{
    struct  in_addr ip, subnet, defgw;      // if ip.S_addr=0, not assigned...
    u8      secport;			// 0 or 1
	u16     target_port;		// ptype[0], portnumber[0], ip_addr[0] normally used
	struct  in_addr target_ip_addr; 	// others are for later use		
	u16		res;
} LAN_CONFIG;    //sizeof(LANHDR) = 160


typedef struct{
	UART_CONFIG port;
	PTC_CONFIG	ptc;	
}UART_CH_CONFIG;

typedef struct{
	LAN_CONFIG port;
	PTC_CONFIG	ptc;	
}LAN_CH_CONFIG;

typedef struct{
	UART_CH_CONFIG u_ch[MAX_UART_CH];
	LAN_CH_CONFIG  l_ch[MAX_LAN_CH];
}INF_CONFIG;

typedef struct{
	union{
		POINT_CONFIG ch[MAX_IO_POINT];
		INF_CONFIG inf;
		};
}IO_CONFIG;			// sizeof(IO_CONFIG) = 512 = 0x200

typedef struct{
	short cardtype;
	short scantime;         // x * 10mSec,
	u32 res[3];
	IO_CONFIG cfg;
}CARD_CONFIG;

typedef struct{
	union{
		struct{
			unsigned short	sf : 2,
					ef : 1,
					ce : 1,
					res : 12;
		};
		unsigned short fault;
	}ch[MAX_IO_POINT];
} POINT_STATUS;


typedef struct{
	u32 stat[4];	// error '1'
	u8	cnt[128];
}LINE_STATUS;		// 132

typedef struct{
	int index;
	int res1[3];
	DB_VERSION hwver;
	int res2;
	DB_VERSION osver[2];
	DB_VERSION appver;
	int res3[3];
	float v3_3;
	float v5;
	float v12;
	int res4;
	float temp;
	int res5[3];
	float cpu_usage;
	int res6;
	float sysmem_usage;
	float chmem_usage;
	float eth_usage[2];
	float can_usage[2];
	u16 mstStatus;
	u16 cNetStatus;
	int res7[3];

	u32 iostatus[4];	// Max 128
	u32 linectrl[4];	// Max 128
	LINE_STATUS line[2];
}PCU_AVAIL;	// sizeof(PCU_AVAIL) =

////////////////////////////////////////////////////////////////////////////////
// HICSR Address Space
#define AVAIL_SPACE_BASE	(0)
#define AVAIL_SPACE_SIZE	(0xE000)
#define AVAIL_SPACE_END		(AVAIL_SPACE_BASE+AVAIL_SPACE_SIZE)

#define PRIV_SPACE_BASE		(0xE000)
#define PRIV_SPACE_SIZE		(0x1000)
#define PRIV_SPACE_END		(PRIV_SPACE_BASE+PRIV_SPACE_SIZE)

#define REG_SPACE_BASE		(0xF000)
#define REG_SPACE_SIZE		(0x1000)
#define REG_SPACE_END		(REG_SPACE_BASE+REG_SPACE_SIZE)

typedef struct{
	u16	runstat;	// ro	0
	u16	runctrl;	// rw	2
	u16	cardtype;	// ro	4
	u16	resetctrl;	// rw	6

	u32	syncsec;	// rw	8
	u32	syncmicro;	// rw	c

	u16	linestat;	// ro	10
	u16	linectrl;	// rw	12
	u16	redunstat;	// ro	14
	u16	redunctrl;	// rw	16

	u16	diagstat;	// ro	18
	u16	cfgcrc;		// ro	1a
	u16	uptime;		// rw	1c
	u16	reserved1;	//		1e

	u32	hwversion;	// ro	20
	u32	swversion;	// ro	24
	u32	runtime;	// ro	28
	u32	reserved2;	//		2c

	u32	imgpagestat;	// ro	30
	u32	imgpagectrl;	// rw	34

}HICSR_REG;

struct CSRATTR_BITS{
	u16	runstat:1;	// ro
	u16	runctrl:1;	// rw
	u16	cardtype:1;	// ro
	u16	resetctrl:1;	// rw
	
	u16	syncsec:2;	// rw
	u16	syncmicro:2;	// rw
	
	u16	linestat:1;	// ro
	u16	linectrl:1;	// rw
	u16	redunstat:1;	// ro
	u16	redunctrl:1;	// rw
	
	u16	diagstat:1;	// ro
	u16	cfgcrc:1;		// ro
	u16	uptime:1;		// rw
	u16	reserved0:1;
	
	u16	hwversion:2;	// ro
	u16	swversion:2;	// ro
	u16 runtime:2;
	u16 reserved1:2;

	u16	imgpagestat:2;	// ro
	u16	imgpagectrl:2;	// rw

	u16 reserved2:4;
};

typedef union{
	struct CSRATTR_BITS bit;
	u32 all;
}HICSR_ATTR;

#ifndef offsetof
 #define offsetof(type, member) ((size_t) &((type *)0)->member)
#endif

#define RUNSTAT		(const u32)(REG_SPACE_BASE + offsetof(HICSR_REG, runstat))	// 0xf000
#define RUNCTRL		(u32)(REG_SPACE_BASE + offsetof(HICSR_REG, runctrl))		// 0xf002
#define CARDTYPE	(u32)(REG_SPACE_BASE + offsetof(HICSR_REG, cardtype))		// 0xf004
#define RESETCTRL	(u32)(REG_SPACE_BASE + offsetof(HICSR_REG, resetctrl))		// 0xf006
#define SYNCSEC		(u32)(REG_SPACE_BASE + offsetof(HICSR_REG, syncsec))		// 0xf008
#define SYNCMICRO	(u32)(REG_SPACE_BASE + offsetof(HICSR_REG, syncmicro))		// 0xf00c
#define LINESTAT	(u32)(REG_SPACE_BASE + offsetof(HICSR_REG, linestat))		// 0xf010
#define LINECTRL	(u32)(REG_SPACE_BASE + offsetof(HICSR_REG, linectrl))		// 0xf012
#define REDUNSTAT	(u32)(REG_SPACE_BASE + offsetof(HICSR_REG, redunstat))		// 0xf014
#define REDUNCTRL	(u32)(REG_SPACE_BASE + offsetof(HICSR_REG, redunctrl))		// 0xf016
#define DIAGSTAT	(const u32)(REG_SPACE_BASE + offsetof(HICSR_REG, diagstat))		// 0xf018
#define CFGCRC		(u32)(REG_SPACE_BASE + offsetof(HICSR_REG, cfgcrc))			// 0xf01a
#define UPTIME		(u32)(REG_SPACE_BASE + offsetof(HICSR_REG, uptime))			// 0xf01c
#define HWVERSION	(u32)(REG_SPACE_BASE + offsetof(HICSR_REG, hwversion))		// 0xf020
#define SWVERSION	(u32)(REG_SPACE_BASE + offsetof(HICSR_REG, swversion))		// 0xf024
#define RUNTIME		(u32)(REG_SPACE_BASE + offsetof(HICSR_REG, runtime))		// 0xf028
#define IMGPAGESTAT	(u32)(REG_SPACE_BASE + offsetof(HICSR_REG, imgpagestat))	// 0xf030
#define IMGPAGECTRL	(u32)(REG_SPACE_BASE + offsetof(HICSR_REG, imgpagectrl))	// 0xf034

typedef struct{
	IO_DATA		*data;
	IO_CONFIG	*cfg;
}LPHICSR_AVAIL;


typedef struct{
	struct{
		u8 state;					// communication state
		u8 nextstate;				// communication state
		u8 app;
		u8 comstate;
		u16 proc;
		u8 oldstate;
		u8 res;
	};
	int msgcnt;
#ifdef _CAN_H
	struct canmsg_t msg[MAX_IO_HISTORY];	// history msg
#else
	char msg[16*MAX_IO_HISTORY];
#endif
	struct timeval rtime[MAX_IO_HISTORY];	// history msg time
	time_t ptime[MAX_IO_POINT];		// point time
}COMM_INFO;


enum{
	IOCOM_TYPE = 1,
	IOCOM_MAX_STATE
};

typedef struct{
	IO_DATA		data;		// @0000h

	IO_CONFIG	cfg;		// @1000h
	COMM_INFO	com;
}HICSR_AVAIL;			// sizeof(HICSR_AVAIL) = 1024 + com

typedef struct{
	HICSR_REG	reg;
	POINT_STATUS	alarm;

	void *priv;
	union{
		HICSR_AVAIL avail;
		PCU_AVAIL pcu;
	};
}HICSR_CARD;

typedef struct{
	void *avail;
	void *priv;
	void *reg;
}LPHICSR_CARD;

// HICSR_RUN STAT & CTRL
enum{
	HICSR_RUN_BOOT = 0,
	HICSR_RUN_INIT = 1,
	HICSR_RUN_CONFIG = 2,
	HICSR_RUN_RUN = 3,
	HICSR_RUN_DIAG = 4,
	HICSR_IMG_CTRL	= 5,
	HICSR_IMG_REQ	= 6,
	HICSR_CFG_DONE	= 7,
	HICSR_RUN_SAMEADDR = 10
};

// HICSR_CARDTYPE
enum{
	HICSR_CARDTYPE_UNKNOWN = 0,
	HICSR_CARDTYPE_DI = 0x1000,
	HICSR_CARDTYPE_DO = 0x2000,
	HICSR_CARDTYPE_AI = 0x3000,
	HICSR_CARDTYPE_AI_MA = 0x3000,
	HICSR_CARDTYPE_AI_RTD = 0x3100,
	HICSR_CARDTYPE_AI_TC = 0x3200,
	HICSR_CARDTYPE_AO = 0x4000,
	HICSR_CARDTYPE_PI = 0x5000,
	HICSR_CARDTYPE_PO = 0x6000,
	HICSR_CARDTYPE_COM = 0x7000
};

// for LGD
typedef struct{
	unsigned char opcode;
	char	prec;
	char	res[2];
	float	egul;
	float	eguh;
	unsigned int	dst;
	char	tagname[16];
	char	tagdesc[32];
}FNC_STR;		// sizeof(REAL_POINT_STR) = 64

typedef struct{
	short cardtype;
	short res;
	FNC_STR point[MAX_IO_POINT];
}REAL_CARD_STR;			// sizeof(REAL_POINT_STR) = 68
/*
//it's MPMs
// HICSR_RUN STAT & CTRL
enum{
	HICSR_RUN_BOOT = 0,
	HICSR_RUN_INIT = 1,
	HICSR_RUN_CONFIG = 2,
	HICSR_RUN_RUN = 3,
	HICSR_RUN_DIAG = 4,
	HICSR_RUN_DOWNLOAD = 5,
	HICSR_RUN_DOWNREQ = 6,
	HICSR_RUN_CFG_DONE = 7,
	HICSR_RUN_UNKNOWN
};

// HICSR_CARDTYPE
enum{
	HICSR_CARDTYPE_UNKNOWN = 0,
	HICSR_CARDTYPE_DI = 0x1000,
	HICSR_CARDTYPE_DO = 0x2000,
	HICSR_CARDTYPE_AI = 0x3000,
	HICSR_CARDTYPE_AI_MA = 0x3000,
	HICSR_CARDTYPE_AI_RTD = 0x3100,
	HICSR_CARDTYPE_AI_TC = 0x3200,
	HICSR_CARDTYPE_AO = 0x4000,
	HICSR_CARDTYPE_PI = 0x5000,
	HICSR_CARDTYPE_PO = 0x6000,
	HICSR_CARDTYPE_COM = 0x7000,

	HICSR_CARDTYPE_LGD = 0xe000,
	HICSR_CARDTYPE_PCU = 0xf000
};
*/
#define	HICSR_POINT_MASK	0xFF

// HICSR_RESET
#define	HICSR_RST_READY		0x5A5A
#define	HICSR_RST_START		0xA5A5
#define	HICSR_RST_NOW		0x5AA5


// HICSR_LINE STAT
#define	HICSR_LINESTAT_PRI		0x0001
#define	HICSR_LINESTAT_SEC		0x0100

// HICSR_REDUNDANT STAT
#define	HICSR_RED_OK		0x0000
#define	HICSR_RED_ERR		0x0001

// HICSR_REDUNDANT CTRL
#define	HICSR_RED_READY		0x0000
#define	HICSR_RED_ACTIV		0x0001

// HICSR_LINE CTRL
#define	HICSR_LINECTRL_PRI		0x0100
#define	HICSR_LINECTRL_SEC		0x0001

// HICSR_DIAG
enum{
	HICSR_DIAG_CANPWR = 0x0001,
	HICSR_DIAG_FIELDPWR = 0x0002,
	HICSR_DIAG_BUFFER = 0x0004,
	HICSR_DIAG_FLASH = 0x0008,
	HICSR_DIAG_VERSION = 0x0010,
};

// HICSR_UPTIME (mSec)
#define	HICSR_UPTIME_DEFAULT	500
#define	hicsr_uptime(N)	((N>0) ? ((N<=1000) ? (N*10) : 500) : 500)

#define PcuCsr	IoData[0]

#endif		// _HICSR_H
