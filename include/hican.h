#ifndef	_CAN_H
#define	_CAN_H

// ioctls
#define CAN_LED_MASTER	0x10001
#define CAN_LED_SLAVE	0x10002
#define CAN_SET_BAUD	0x11001

#define CAN_MSG_LENGTH	8

#define MSG_RTR   (1<<6)
#define MSG_EXT   (1<<7)

struct canmsg_t {
	unsigned int	id;
	unsigned char	len;
	unsigned char	flags;
	unsigned char	res[2];		// for sizeof(canmsg_t) == 16
	unsigned char	data[CAN_MSG_LENGTH];
};

#endif	// _CAN_H
