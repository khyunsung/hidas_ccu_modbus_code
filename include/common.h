#ifndef __COMMON_H_
#define __COMMON_H_

#define MyInfAddr				70
#define MAX_INTERFACE   1	//khs 16
#define MPM_MAX_INTERFACE	4
#define MPM_INF_PORT_0		65
#define MPM_INF_PORT_1		66
#define MPM_INF_PORT_2		67
#define MPM_INF_PORT_3		68
#define MPM_INF_PORT_4		69

#define MAX_INF_TAG		2048
#define MAX_WRITE_BUFF	30	// 10 --> 15  (03/28/2012) --> 30 (06/27/2012)

#define MAX_NUM_TX_QUERY	128

#define DEBUG_BASE		7000
#define DEBUG_OFST		8

#define DEFAULT_BUFF_SIZE 1024

#define ALARM_STATUS_BASE	0
#define CMD_REG_BASE		32
#define TX_PACKET_CNT_BASE	64
#define RX_PACKET_CNT_BASE	128
#define INF_STATUS_BASE		192

#define MAX_NUM_CNT			400000000
#define APP_DIR		"/home/app/"
#define CONF_DIR 	"/sysconfig/" //2020.08.31 khs #define CONF_DIR 	"/sysconfig/"
#define DB_DIR		"/home/app/DB/"
#define CMD_DIR		"/home/app/cmd/"
#define SCR_DIR		"/home/app/script/"
#define LOG_DIR		"/home/log/"
#define ID_DIR		"/dev/"
enum {
	ACONIS_2000 = 0,
	ACONIS_DS = 1,
	ACONIS_RIO = 2,
	MLS2 = 3,
	IPMS = 4
};

enum {
	MD_MODBUS_MASTER,
	MD_MODBUS_SLAVE,
	MD_NMEA_READ,
	MD_NMEA_WRITE,
	MD_NMEA_READWRITE,
	MD_FDS_READ,
	MD_ICCP_READ,
	MD_DG_READ,
	MD_NDMC_READWRITE,
	MD_SNDC_READWRITE,
	MD_CHEM_READWRITE,
	MD_COND_READWRITE
};

enum {
	MD_BIG_ENDIAN,
	MD_LITTLE_ENDIAN
};

enum {
	DB_TYPE,
	PROTOCOL_TYPE,
	BAUDRATE,
	DATABIT,
	PARITYBIT,
	STOPBIT,
	FLOWCTRL
};

enum {
	INITIALIZE,
	OPEN_DB,
	READ_DB,
	CLOSE_DB,
	PORT_OPEN,
	PORT_CLOSE,
	CREATE_THREAD,
	KILL_THREAD,
	MANAGE_THREAD,
	IDLE
};

#define INTERFACE_NUM_LIMIT		64
#define NUM_REDUNDANCY			2
typedef struct {
	unsigned short cmd_register[INTERFACE_NUM_LIMIT];
    unsigned short cmd_rev[INTERFACE_NUM_LIMIT];
	unsigned short comm_status[INTERFACE_NUM_LIMIT][NUM_REDUNDANCY];
	unsigned short inf_status[INTERFACE_NUM_LIMIT][NUM_REDUNDANCY];
    unsigned short status_rev[INTERFACE_NUM_LIMIT][NUM_REDUNDANCY];
	unsigned int tx_cnt[INTERFACE_NUM_LIMIT][NUM_REDUNDANCY];
	unsigned int rx_cnt[INTERFACE_NUM_LIMIT][NUM_REDUNDANCY];	
}INFS_STATUS;



#ifdef COM
#else
typedef struct {
	unsigned short tag[MAX_INF_TAG];
}INF_DATA;

typedef struct {
	unsigned long status;
	unsigned long rx_packets;
	unsigned long tx_packets;
	unsigned long rx_bytes;
	unsigned long tx_bytes;
	unsigned long rx_dropped;
	unsigned long tx_dropped;
	unsigned long errors;
}INF_STATUS;


typedef struct {
	INF_DATA INF_data;
	INF_STATUS INF_status;
}INF_MEM;
#endif

typedef struct {
	int sockfd;
	char b_flag;

	char write_flag;
	char write_head, write_tail;
	char write_buff[MAX_WRITE_BUFF][16];
	char write_length[MAX_WRITE_BUFF];

	char db_chage;
	pthread_t port_thr;
}PORT_INFO;

typedef struct {
	unsigned short block_addr;
	unsigned short byte_count;
}TX_QUERY_MEM;

typedef struct {
	unsigned short head, tail;
	TX_QUERY_MEM mem[MAX_NUM_TX_QUERY];
}TX_QUERY_INFO;


typedef struct {
	char index;
	char exe[16];
	char param1[8];
	char param2[8];
	char adapt[16];
	char adapt_param[8];

	pid_t pid,adapt_pid;
}PM_INFO;

#include <asm/types.h>
#pragma pack(1)
typedef struct _modbus_res_hdr_ext {
	__u8        unit_id;
	__u8        function_code;
	__u16       address;
	__u16       byte_count;
	__u16       data[512];
} modbus_res_hdr_ext;
#pragma pack ()

#endif
