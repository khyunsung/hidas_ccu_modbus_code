#ifndef	_MODBUS_FRAME_H
#define	_MODBUS_FRAME_H

#include "protocol_frame.h"
#include "hicsr.h"

/******************* MODBUS *********************************************/
#pragma pack(4)

typedef struct {
	u8 rcnt;   //read cnt	<--- key
	u8 wcnt;   //write cnt	<--- key
	u8 cnt;	   // rcnt+wcnt
        u8 res;
} PROFILE_HEADER;       //sizeof(PROFILE_HEADER) = 4

typedef struct{
	u8		fnccode;
	u8		res;
	u16		address;
	u16		count;
	u16		blockaddr;      // start address of block for mapping
} READ_PROFILE;         //sizeof(READ_PROFILE) = 8

typedef struct{
	u8		fnccode;
	u16             address;	// dest address
        u8              event;	        // 0: Polling, 1: Event, 2: All
	u8		res;
	u16             blockaddr;	// src address
	u8              blockbpos;
} WRITE_PROFILE;        //sizeof(WRITE_PROFILE) = 8
/*
typedef union {
        READ_PROFILE read;
        WRITE_PROFILE write;
} MODBUS_PROFILE;       //sizeof(MODBUS_PROFILE) = 8
*/
typedef struct {
	PROFILE_HEADER	header;                 // size=4
	MODBUS_PROFILE  data[MAX_PROFILE];      // size=8*255=2040
} MODBUS_PROFILEDATA, *LPMOBUSPROFILEDATA;      //sizeof(MODBUS_PROFILEDATA) = 2044

typedef struct {
	u8     	no;             // port number: 1 ~ 8
        u8      phy;            // 0: UART, 1: LAN
	u8	nodecnt;	// MAX_SYSTEM 중 실제 사용중인 Node 개수 <-- key

	u8	retries;        // retry count	 // retries, period, timeout은 0이 아니면 available
	u16	period;         // polling time (* 10msec)
	u16	timeout;        // alarm timeout (* 10msec)

        u16     res;
        u16     res2;
} PORT_CONTROL, *LPPORTCONTROL;         //sizeof(PORT_CONTROL) = 12

typedef struct {
//	PORT_CONTROL    comctrl;		// PORT_DATA로 이동
        u8      ptype;		// <--- key
	u8      protocol: 4,	// 0: TCP, 1: UDP
                alarm: 2,       // 0: NONE, 1: ALARM
		redundant: 2;   // 0: Single, 1: Dual
	u8      slave[2];       // slave address, [1]: redundant = 1이면 세팅
        union {
	        MODBUS_PROFILEDATA profile;     //size=2044
                u8 resarea[2048];
        };
} PORT_NODE, *LPPORTNODE;     //sizeof(PORT_UNODE) = 2052      // PORT_UNODE, PORT_LNODE 구분없이 통합

typedef struct {
	COMDBFILE file;                 // size=52
        PORT_CONTROL ctrl;        	// size=12	// PORT_INFO, PORT_CONTROL 통합
        union {
                UARTHDR uHdr;           // size=40
                LANHDR lHdr;            // size=160
        };
	PORT_NODE Node[MAX_SYSTEM];	// size=2052*16=32832
} PORT_DATA, *LPPORTDATA;       //sizeof(PORT_DATA) = 33056

#pragma pack()

#endif
